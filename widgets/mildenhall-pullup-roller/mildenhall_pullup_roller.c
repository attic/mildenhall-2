/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_pullup_roller.c
 *
 * Created on: Jan 17, 2013
 *
 * mildenhall_pullup_roller.c */

#include "mildenhall_pullup_roller.h"

#include <thornbury/thornbury.h>

#include "sample-variable-item.h"

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define PULLUP_ROLLER_DEBUG(...)   //g_print( __VA_ARGS__)

/* property enums */
enum _enPullupRollerProperty
{
        PROP_FIRST,
        PROP_WIDTH,
        PROP_HEIGHT,
	PROP_FIXED_ROLLER,
	PROP_ITEM_TYPE,
	PROP_FOOTER_ICON_LEFT,
	PROP_FOOTER_RATINGS,
	PROP_MODEL,
        PROP_FOOTER_MODEL,
	PROP_SHOW,
	PROP_HIDE,
        PROP_LAST
};

/* The signals emitted by the mildenhall meta info footer */
enum _enPullupRolleroSignals
{
        SIG_FIRST,
        SIG_PULLUP_ROLLER_ANIMATED,                     /* Emitted when animation cpmpletes */
        SIG_PULLUP_ROLLER_UP_ANIMATION_STARTED,            /* Emitted when up animation starts */
        SIG_PULLUP_ROLLER_DOWN_ANIMATION_STARTED,            /* Emitted when down animation starts */
	SIG_PULLUP_ROLLER_ITEM_SELECTED,		/* Emitted when pullup roller item gets selected */
        SIG_LAST
};

/* footer model columns */
enum
{
        COLUMN_LEFT_ICON,
        COLUMN_MID_TEXT,
        COLUMN_RIGHT_RATINGS,
        COLUMN_NONE
};

static guint32 pullup_roller_signals[SIG_LAST] = {0,};

G_DEFINE_TYPE (MildenhallPullupRoller, mildenhall_pullup_roller, CLUTTER_TYPE_ACTOR)

#define PULLUP_ROLLER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_PULLUP_ROLLER, MildenhallPullupRollerPrivate))

struct _MildenhallPullupRollerPrivate
{
	gfloat flWidth;                 // width of the pullup-roller
	gfloat flRollerImageHeight;
	gfloat flRollerExtraHeight;
	gfloat flSeamlessTopHeight;
	gfloat flLineWidth;
	gfloat flRollerClipArea;
	gfloat flRollerY;
	gfloat flPullupY;

	guint uinAnimationDuration;
	gint inThreshold;

	ClutterActor *pFooter;
	ClutterActor *pTopBar;
	ClutterActor *pRoller;
	ClutterActor *pRollerGrp;
	ClutterActor *pArrow;
	ClutterActor *pTopShadow;
	ClutterAnimation *pTopbarAnimation;
	ClutterColor lineColor;

	gboolean bFixedRoller;
	gboolean bTopBarPressed;
	gboolean bPullupRollerAnimate;
	gboolean bPullupRollerDown;
	gboolean bRollerShown;
	gboolean bAnimationRunning;

	ThornburyModel *pFooterModel;
	ThornburyModel *pModel;

	gchar *pSeamlessTopFilePath;
	gchar *pShadowTopFilePath;
	gchar *pArrowUpFilePath;
	gchar *pArrowDownFilePath;
};

/***************************************************************************************************************************
 * Internal Functions
 ***************************************************************************************************************************/
static void attach_animation(MildenhallPullupRoller *pPullupRoller);
static void swipe_cb(ClutterGestureAction *pAction, ClutterActor *pHeader, ClutterSwipeDirection swipeDirection, gpointer pUserData );


/********************************************************
 * Function : gesture_end
 * Description: callback on gesture end
 * Parameters:  ClutterGestureAction *, header/footer,
                userData
 * Return value: void
 ********************************************************/
static void gesture_end(ClutterGestureAction *pAction, ClutterActor *pActor, gpointer pUserData)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (MILDENHALL_PULLUP_ROLLER (pUserData));
        gfloat flPressX, flPressY;
        gfloat flReleaseX, flReleaseY;
        ClutterSwipeDirection enDirection = 0;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }

        clutter_gesture_action_get_press_coords (pAction,
                        0,
                        &flPressX, &flPressY);

        clutter_gesture_action_get_release_coords (pAction,
                        0,
                        &flReleaseX, &flReleaseY);

        PULLUP_ROLLER_DEBUG("PULLUP_ROLLER_DEBUG:press X = %f press Y = %f release X = %f release Y = %f\n", flPressX, flPressY, flReleaseX, flReleaseY);

        if ( (flReleaseX - flPressX > priv->inThreshold)
                        || (flReleaseY - flPressY > priv->inThreshold)
                        || (flPressX - flReleaseX > priv->inThreshold)
                        ||(flPressY - flReleaseY > priv->inThreshold) )
        {

                 //gdouble diff, angle;
                /* get the difference of x and y */
                //diff = (flReleaseY - flPressY)/ (flReleaseX - flPressX);
                /* calculate the angle */
                //angle = atan(diff) * 180 / 3.14159265;

                //PULLUP_ROLLER_DEBUG("PULLUP_ROLLER_DEBUG : angle = %f \n", angle);

                /* angles on swipe event

                        -90
                  +45   |     -45
                        |
                -0 _____|_____ +0
                        |
                        |
                -45     |     +45
                        +90
                */

                /* for up/down direction, angle varies between -0 to -90 and 0 to 90 */
                /* if pressed Y coord is greater than the released one, direction will be up
                 * otherwise down */
		if(flPressY >= flReleaseY)
                {
                        PULLUP_ROLLER_DEBUG("direction is up\n");
                        enDirection = CLUTTER_SWIPE_DIRECTION_UP;
                }
                else
                {
                        PULLUP_ROLLER_DEBUG("direction is down\n");
                        enDirection = CLUTTER_SWIPE_DIRECTION_DOWN;
                }

                swipe_cb(pAction, pActor, enDirection, pUserData);
        }
}

/********************************************************
 * Function : swipe_cb
 * Description: callabck on swipe action
 * Parameters: ClutterActor *, swipeDirection, pUserData
 * Return value: void
 ********************************************************/
//static void swipe_cb(ClutterSwipeAction *pAction, ClutterActor *pFooter, ClutterSwipeDirection swipeDirection, gpointer pUserData )
static void swipe_cb(ClutterGestureAction *pAction, ClutterActor *pHeader, ClutterSwipeDirection swipeDirection, gpointer pUserData )
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (MILDENHALL_PULLUP_ROLLER (pUserData));
        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid info roller object\n");
                return;
        }
	/* reset the flag to prevent release */
        priv->bTopBarPressed = FALSE;

        if(swipeDirection ==CLUTTER_SWIPE_DIRECTION_UP)
        {
                if ( ! priv->bPullupRollerDown)
                {
                        PULLUP_ROLLER_DEBUG("CLUTTER_SWIPE_DIRECTION_UP\n");
                        attach_animation(MILDENHALL_PULLUP_ROLLER(pUserData));
                }
        }
        else if(swipeDirection ==CLUTTER_SWIPE_DIRECTION_DOWN)
        {
                if (priv->bPullupRollerDown)
                {
                        PULLUP_ROLLER_DEBUG("CLUTTER_SWIPE_DIRECTION_DOWN\n");
                        attach_animation(MILDENHALL_PULLUP_ROLLER(pUserData));
                }
        }
        else
	{
                PULLUP_ROLLER_DEBUG("%d\n", swipeDirection);      /* do nothing */
	}

}


/****************************************************
 * Function : pullup_roller_item_activated_cb
 * Description: roller item activated callback
 * Parameters: rollerContainer*, uinRow, pUserData
 *
 * Return value: void
 *******************************************************/
static void pullup_roller_item_activated_cb (MildenhallRollerContainer *pCont, guint uinRow, gpointer pUserData)
{
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid pullup roller object\n");
                return;
        }
        /* emit the pullup roller item selected signal */
	g_signal_emit (G_OBJECT(pUserData), pullup_roller_signals[SIG_PULLUP_ROLLER_ITEM_SELECTED], 0, uinRow);
}



/****************************************************
 * Function : pullup_roller_timeline_completed_cb
 * Description: Callback on animation completed
 * Parameters: pAnimation, pUserData
 *
 * Return value: void
 *******************************************************/
static void pullup_roller_timeline_completed_cb(ClutterAnimation *pAnimation, gpointer pUserData)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (MILDENHALL_PULLUP_ROLLER (pUserData));
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid pullup roller object\n");
                return;
        }

        if(priv->bPullupRollerDown)
	{
		if(priv->pArrow && priv->pArrowDownFilePath)
		{
                	thornbury_ui_texture_set_from_file(priv->pArrow, priv->pArrowDownFilePath, 0, 0, FALSE, TRUE);
			/* show top shadow */
			if (!clutter_actor_is_visible (priv->pTopShadow))
				clutter_actor_show(priv->pTopShadow);

			priv->bAnimationRunning = FALSE;
		}
	}
        else
	{
		if(priv->pArrow && priv->pArrowUpFilePath)
		{
                	thornbury_ui_texture_set_from_file(priv->pArrow, priv->pArrowUpFilePath, 0, 0, FALSE, TRUE);
			/* hide top shadoew */
			if (clutter_actor_is_visible (priv->pTopShadow))
                                clutter_actor_hide(priv->pTopShadow);

			//clutter_actor_set_opacity(priv->pRoller, 0);
		}
	}

 	/* nfo roller animated  signal emission */
	g_signal_emit(MILDENHALL_PULLUP_ROLLER(pUserData), pullup_roller_signals[SIG_PULLUP_ROLLER_ANIMATED], 0, NULL);

	/* reset the pressed flag */
	priv->bTopBarPressed = FALSE;

	priv->pTopbarAnimation = NULL;
        return;
}

/****************************************************
 * Function : top_bar_pullupdown_start
 * Description: Callback on button press on footer/bottom bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean top_bar_pullupdown_start(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (MILDENHALL_PULLUP_ROLLER (pUserData));
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid pullup roller object\n");
                return FALSE;
        }

	/* update the top press flag */
	priv->bTopBarPressed = TRUE;

	if(priv->pRoller)
	{

		ThornburyModel *pModel = NULL;
		g_object_get(priv->pRoller, "model", &pModel, NULL);
		if(NULL !=pModel && (thornbury_model_get_n_rows(pModel) >= 2) )
			g_object_set(priv->pRoller, "focused-row", 1, NULL);
	}

	return TRUE;
}

/****************************************************
 * Function : top_bar_pullupdown
 * Description: Callback on button release on footer/bottom bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean top_bar_pullupdown(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (MILDENHALL_PULLUP_ROLLER (pUserData));
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pUserData) )
        {
                g_warning("invalid pullup roller object\n");
                return FALSE;
        }

	if(priv->bTopBarPressed == FALSE)
	{
		return FALSE;
	}
	/* check if roller group is created */
	if(! priv->pRollerGrp)
		return FALSE;

	attach_animation(MILDENHALL_PULLUP_ROLLER(pUserData) );

	return FALSE;
}

/****************************************************
 * Function : top_bar_event_cb
 * Description: Callback on button press/release on top/footer bar
 * Parameters: pActor,pEvent,pUserData
 *
 * Return value: gboolean (signal handled or not)
 *******************************************************/
static gboolean top_bar_event_cb(ClutterActor *pActor, ClutterEvent *pEvent, gpointer pUserData)
{
	if (pEvent->type == CLUTTER_BUTTON_PRESS || pEvent->type == CLUTTER_TOUCH_BEGIN)
		top_bar_pullupdown_start(pActor, pEvent, pUserData);
	else if (pEvent->type == CLUTTER_BUTTON_RELEASE || pEvent->type == CLUTTER_TOUCH_END)
		top_bar_pullupdown(pActor, pEvent, pUserData);
	else
	{
		return FALSE;
	}
        return FALSE;
}

/***********************************************************
 * Function : attach_animation
 * Description: attach up/down animation based on roller state
 * Parameters:  pPullupRoller*
 ************************************************************/
static void attach_animation(MildenhallPullupRoller *pPullupRoller)
{
	MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        /*Create the path for movement and attach a behavior path for the pullup actor */
        gfloat x = clutter_actor_get_x (priv->pRollerGrp);
        gfloat y = clutter_actor_get_y (priv->pRollerGrp);

        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

	/* if roller is already down, move it up */
	if ( priv->bPullupRollerDown)
	{
		y = 0.0;
		PULLUP_ROLLER_DEBUG("pull down y position = %f\n", y);
		/* update the show flag */
		priv->bRollerShown = FALSE;

	}
	/* otherwise move it down to y position */
	else
	{
		y = -priv->flPullupY;// 198.0;
		clutter_actor_set_opacity(priv->pRoller, 0xff);
		/* update the show flag */
		priv->bRollerShown = TRUE;
		/* update the show animation flag */
		priv->bAnimationRunning = TRUE;

		PULLUP_ROLLER_DEBUG("pull up y position = %f\n", y);
	}


	/* if animate flag is set, pull up/down with animation */
	if(priv->bPullupRollerAnimate == FALSE)
	{
		clutter_actor_detach_animation(priv->pRollerGrp);
		clutter_actor_set_y(priv->pRollerGrp, y);
		priv->bPullupRollerDown = ! priv->bPullupRollerDown;
		pullup_roller_timeline_completed_cb(NULL, pPullupRoller );
		priv->bPullupRollerAnimate = TRUE;
	}
	else
	{
		/* roller pull up/down with animation */
		clutter_actor_detach_animation(priv->pRollerGrp);
		priv->pTopbarAnimation =  clutter_actor_animate (priv->pRollerGrp,
				CLUTTER_EASE_OUT_SINE,
				priv->uinAnimationDuration,
				"x", x,
				"y", y,
				NULL);

		g_signal_connect_after (priv->pTopbarAnimation, "completed",
                                G_CALLBACK (pullup_roller_timeline_completed_cb),
                                pPullupRoller);

		if(priv->bPullupRollerDown)
		{
			/* animation started signal emission */
			g_signal_emit(pPullupRoller, pullup_roller_signals[SIG_PULLUP_ROLLER_DOWN_ANIMATION_STARTED], 0, NULL);
		}
		else
		{
			/* animation started signal emission */
                        g_signal_emit(pPullupRoller, pullup_roller_signals[SIG_PULLUP_ROLLER_UP_ANIMATION_STARTED], 0, NULL);

		}
		/*Change the state of the metaRollerDown flag*/
		priv->bPullupRollerDown = ! priv->bPullupRollerDown;

	}

}

/****************************************************
 * Function : create_top_bar
 * Description: create roller bottom bar
 * Parameters: pPullupRoller
 *
 * Return value: pTopBar*
 *******************************************************/
static ClutterActor *create_top_bar(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        ClutterActor *pTexture = NULL;
        ClutterActor *pLine = NULL;
        ClutterActor *pLine2 = NULL;
        /*Create the meta bottom bar group, set a shadow, seamless and all lines and the arrow*/
        ClutterActor *pTopBar = clutter_actor_new ();

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(!MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller) )
        {
                g_warning("invalid pullup roller object\n");
                return FALSE;
        }

	/* bottom shadow */
	if(priv->pShadowTopFilePath)
	{
		priv->pTopShadow =  thornbury_ui_texture_create_new(priv->pShadowTopFilePath, priv->flWidth, priv->flSeamlessTopHeight, FALSE, TRUE);
		if(priv->pTopShadow)
		{
			clutter_actor_add_child(pTopBar, priv->pTopShadow);
			clutter_actor_set_position(priv->pTopShadow, 0, 0 );
			clutter_actor_hide(priv->pTopShadow);
		}
	}

	/* create the top bar bg image */
	if(priv->pSeamlessTopFilePath)
	{
		pTexture = thornbury_ui_texture_create_new(priv->pSeamlessTopFilePath, priv->flWidth,priv->flSeamlessTopHeight , FALSE, TRUE);
		clutter_actor_set_position(pTexture, 0.0, clutter_actor_get_height(priv->pTopShadow) + 1);
		clutter_actor_add_child(pTopBar, pTexture);
	}

	/* add top line */
	pLine = clutter_actor_new();
	g_object_set(pLine, "x",  0.0, "y",  clutter_actor_get_height(priv->pTopShadow), "width",  priv->flWidth, "height", priv->flLineWidth, "background-color",  &priv->lineColor, NULL);
	clutter_actor_add_child(pTopBar, pLine);

	/* add one more top line to match the drawer color */
	pLine2 = clutter_actor_new();
	g_object_set(pLine2, "x",  0.0, "y", clutter_actor_get_height(priv->pTopShadow), "width",  priv->flWidth, "height", priv->flLineWidth, "background-color",  &priv->lineColor, NULL);
	clutter_actor_add_child(pTopBar, pLine2);

	/* mid arrow */
	if(priv->pArrowUpFilePath)
	{
		priv->pArrow = thornbury_ui_texture_create_new(priv->pArrowUpFilePath, 0, 0, FALSE, TRUE);
		if(priv->pArrow)
		{
			gint flX = (priv->flWidth - clutter_actor_get_width(priv->pArrow)) / 2;
			gint flY = (clutter_actor_get_height(pTexture) - clutter_actor_get_height(priv->pArrow)) / 2;
			flY = flY + clutter_actor_get_height(priv->pTopShadow);
			clutter_actor_set_position(priv->pArrow,
					(gfloat)flX,
					(gfloat)flY);
			clutter_actor_add_child(pTopBar, priv->pArrow);
		}
	}
        return pTopBar;
}

/********************************************************
 * Function : mildenhall_pullup_roller_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallPullupRoller *pPullupRoller = pUserData;
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        gchar *pStyleKey = NULL;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
	g_return_if_fail (MILDENHALL_IS_PULLUP_ROLLER (pUserData));

		pStyleKey = g_strdup(pKey);

        if(g_strcmp0(pStyleKey, "seamless-top-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pSeamlessTopFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        PULLUP_ROLLER_DEBUG("seamless-top-image = %s\n", priv->pSeamlessTopFilePath);
                }
        }
        else if(g_strcmp0(pStyleKey, "shadow-top-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pShadowTopFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        PULLUP_ROLLER_DEBUG("seamless-top-image = %s\n", priv->pShadowTopFilePath);
                }
        }
        if(g_strcmp0(pStyleKey, "arrow-up") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pArrowUpFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        PULLUP_ROLLER_DEBUG("arrow-up = %s\n", priv->pArrowUpFilePath);
                }
        }
        else if(g_strcmp0(pStyleKey, "arrow-down") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pArrowDownFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        PULLUP_ROLLER_DEBUG("arrow-down = %s\n", priv->pArrowDownFilePath);
                }
        }
	else if(g_strcmp0(pStyleKey, "seamless-top-height") == 0)
        {
                priv->flSeamlessTopHeight = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("seamless-top-height = %f\n", priv->flSeamlessTopHeight);
        }
	else if(g_strcmp0(pStyleKey, "roller-bg-height") == 0)
        {
                priv->flRollerImageHeight = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("roller-bg-height = %f\n", priv->flRollerImageHeight);
        }
	else if(g_strcmp0(pStyleKey, "roller-extra-height") == 0)
        {
                priv->flRollerExtraHeight = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("roller-extra-height = %f\n", priv->flRollerExtraHeight);
        }
	else if(g_strcmp0(pStyleKey, "roller-bg-width") == 0)
        {
                priv->flWidth = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("roller-bg-width = %f\n", priv->flWidth);
        }
        else if(g_strcmp0(pStyleKey, "line-color") == 0)
        {
                clutter_color_from_string(&priv->lineColor,  g_value_get_string(pValue));
        }
	else if(g_strcmp0(pStyleKey, "line-width") == 0)
        {
                priv->flLineWidth = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("line-width = %f\n", priv->flLineWidth);
        }
	else if(g_strcmp0(pStyleKey, "roller-clip-area") == 0)
        {
                priv->flRollerClipArea = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("roller-clip-area = %f\n", priv->flRollerClipArea);
        }
	else if(g_strcmp0(pStyleKey, "roller-y") == 0)
        {
                priv->flRollerY = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("roller-y = %f\n", priv->flRollerY);
        }
	else if(g_strcmp0(pStyleKey, "pullup-y") == 0)
        {
                priv->flPullupY = g_value_get_double(pValue);
                PULLUP_ROLLER_DEBUG("pullup-y = %f\n", priv->flPullupY);
        }
	else if(g_strcmp0(pStyleKey, "animation-duration") == 0)
        {
                priv->uinAnimationDuration = g_value_get_int64(pValue);
                PULLUP_ROLLER_DEBUG("animation-duration = %d\n", priv->uinAnimationDuration);

        }
	else
	{
		;
	}

	if(NULL != pStyleKey)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
}

/********************************************************
 * Function : mildenhall_pullup_roller_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_get_property (GObject *pObject,
					guint       uinPropertyID,
        				GValue     *pValue,
                               		GParamSpec *pspec)
{
        if(!MILDENHALL_IS_PULLUP_ROLLER(pObject))
        {
                g_warning("invalid roller object\n");
                return;
        }
	switch (uinPropertyID)
    	{
		case PROP_WIDTH:
			g_value_set_float (pValue,  mildenhall_pullup_roller_get_width(MILDENHALL_PULLUP_ROLLER (pObject)));
			break;
                case PROP_HEIGHT:
			g_value_set_float (pValue,  mildenhall_pullup_roller_get_height(MILDENHALL_PULLUP_ROLLER (pObject)));
                        break;
                case PROP_FOOTER_MODEL:
                        g_value_set_object (pValue, mildenhall_pullup_roller_get_footer_model(MILDENHALL_PULLUP_ROLLER (pObject)) );
                        break;
		case PROP_FOOTER_ICON_LEFT:
                        g_value_set_boolean (pValue,  mildenhall_pullup_roller_get_footer_icon_left(MILDENHALL_PULLUP_ROLLER (pObject)));
                        break;
                case PROP_FOOTER_RATINGS:
                        g_value_set_boolean (pValue,  mildenhall_pullup_roller_get_footer_ratings(MILDENHALL_PULLUP_ROLLER (pObject)));
                        break;
                case PROP_SHOW:
                        g_value_set_boolean (pValue, mildenhall_pullup_roller_get_show(MILDENHALL_PULLUP_ROLLER (pObject) ));
                        break;
                case PROP_MODEL:
                        g_value_set_object (pValue, mildenhall_pullup_roller_get_model(MILDENHALL_PULLUP_ROLLER (pObject)) );
                        break;
                case PROP_ITEM_TYPE:
                        g_value_set_gtype(pValue, mildenhall_pullup_roller_get_item_type(MILDENHALL_PULLUP_ROLLER (pObject)) );
                        break;
                 case PROP_FIXED_ROLLER:
                        g_value_set_boolean (pValue, mildenhall_pullup_roller_get_fix_roller (MILDENHALL_PULLUP_ROLLER (pObject)) );
                        break;
    		default:
      			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
    	}
}

/********************************************************
 * Function : mildenhall_pullup_roller_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_set_property (GObject *pObject,
					guint         uinPropertyID,
	                                const GValue *pValue,
        	                        GParamSpec   *pspec)
{
	if(!MILDENHALL_IS_PULLUP_ROLLER(pObject))
        {
                g_warning("invalid roller object\n");
                return;
        }
	switch (uinPropertyID)
    	{
		case PROP_WIDTH:
                        break;
                case PROP_HEIGHT:
                        break;
		case PROP_FOOTER_MODEL:
                        mildenhall_pullup_roller_set_footer_model(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_object (pValue));
                        break;
		case PROP_FOOTER_ICON_LEFT:
                        mildenhall_pullup_roller_set_footer_icon_left(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
                case PROP_FOOTER_RATINGS:
                        mildenhall_pullup_roller_set_footer_ratings(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
                case PROP_SHOW:
                        mildenhall_pullup_roller_set_show(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
		case PROP_MODEL:
                        mildenhall_pullup_roller_set_model(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_object (pValue));
                        break;
		case PROP_ITEM_TYPE:
			mildenhall_pullup_roller_set_item_type(MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_gtype(pValue));
		        break;
		 case PROP_FIXED_ROLLER:
                        mildenhall_pullup_roller_set_fix_roller (MILDENHALL_PULLUP_ROLLER (pObject), g_value_get_boolean (pValue));
                        break;
		default:
      			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
    	}
}

/********************************************************
 * Function : mildenhall_pullup_roller_dispose
 * Description: Dispose the pullup roller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_dispose (GObject *pObject)
{
	MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE(MILDENHALL_PULLUP_ROLLER(pObject));
	if(priv->pSeamlessTopFilePath)
	{
		g_free(priv->pSeamlessTopFilePath);
		priv->pSeamlessTopFilePath = NULL;
	}
	if(priv->pShadowTopFilePath)
	{
		g_free(priv->pShadowTopFilePath);
		priv->pShadowTopFilePath = NULL;
	}
	if(priv->pArrowUpFilePath)
	{
		g_free(priv->pArrowUpFilePath);
		priv->pArrowUpFilePath = NULL;
	}
	if(priv->pArrowDownFilePath)
	{
		g_free(priv->pArrowDownFilePath);
		priv->pArrowDownFilePath = NULL;
	}
	if(NULL != priv->pRoller)
        {
                clutter_actor_destroy(priv->pRoller);
                priv->pRoller = NULL;
        }
	if(NULL != priv->pTopBar)
        {
                clutter_actor_destroy(priv->pTopBar);
                priv->pTopBar = NULL;
        }
	if(NULL != priv->pFooter)
        {
                clutter_actor_destroy(priv->pFooter);
                priv->pFooter = NULL;
        }

	G_OBJECT_CLASS (mildenhall_pullup_roller_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : mildenhall_pullup_roller_finalize
 * Description: Finalize the meta pullup footer object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_pullup_roller_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_pullup_roller_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_class_init (MildenhallPullupRollerClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	//ClutterActorClass *pActorClass = CLUTTER_ACTOR_CLASS (klass);

	GParamSpec *pspec = NULL;

	g_type_class_add_private (klass, sizeof (MildenhallPullupRollerPrivate));

	pObjectClass->get_property = mildenhall_pullup_roller_get_property;
	pObjectClass->set_property = mildenhall_pullup_roller_set_property;
	pObjectClass->dispose = mildenhall_pullup_roller_dispose;
	pObjectClass->finalize = mildenhall_pullup_roller_finalize;

	/**
         *PullupRoller :width:
         *
         * Width of the pullup roller
         */
        pspec = g_param_spec_float ("width",
                        "Width",
                        "Width of thepullup roller ",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

        /**
         * PullupRoller:height:
         *
         * Height of the pullup roller
         */
        pspec = g_param_spec_float ("height",
                        "Height",
                        "Height of the pullup roller",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

	 /**
         * PullupRoller:fix-roller:
         *
         * Type of the roller to be created:
         *              TRUE = LIGHTWOOD_FIXED_ROLLER
         *              FALSE = MILDENHALL_VARIABLE_ROLLER
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("fix-roller",
                        "Fix-Roller",
                        "Roller Type to create the roller",
			TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FIXED_ROLLER, pspec);

	 /**
         * PullupRoller: roller-type:
         *
         * Type of the roller to be created:
         *              LIGHTWOOD_FIXED_ROLLER
         *              MILDENHALL_VARIABLE_ROLLER
         * Default: LIGHTWOOD_FIXED_ROLLER
         */
        pspec = g_param_spec_gtype ("item-type",
                        "ItemType",
                        "Item Type for the roller item factory",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ITEM_TYPE, pspec);

	 /**
         * PullupRoller:model:
         *
         * model for the pullup roller item
         *
         * Default:
         */
	pspec = g_param_spec_object ("model",
                        "Model",
                        "Model to use to construct the items",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

	/**
         * PullupRoller:footer-model:
         *
         * model for the pullup roller footer.
         * 1. left-icon
         * 2. left-text
         * 3. right-icon-text
         * 4. middle-text
         *
         * Default: NULL
         */
        pspec = g_param_spec_object ("footer-model",
                        "Footer-Model",
                        "footer model having pullup roller footer data",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FOOTER_MODEL, pspec);

	 /**
         * PullupRoller:footer-left-icon:
         *
         * Set this property to FALSE to set text at footer left position
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("footer-left-icon",
                        "Header-Left-Icon",
                        "Whether the icon or text on left side of footer",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FOOTER_ICON_LEFT, pspec);

         /**
         * PullupRoller:footer-ratings:
         *
         * Set this property to FALSE to set text at footer right position
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("footer-ratings",
                        "Ratings-For-Stars",
                        "Whether the footer right side ratings star required",
                        FALSE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_FOOTER_RATINGS, pspec);

	/**
         * PullupRoller:show:
         *
         * Set this property to TRUE to show pullup roller
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("show",
                        "Show-Pullup-Roller",
                        "To show the pullup roller",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW, pspec);

	/**
         * PullupRoller::pullup-roller-animated
  	 *
	 * Signal emitted when the pullup roller
         * is animated (is either pulled down or expanded upwards)
         *
         */
        pullup_roller_signals[SIG_PULLUP_ROLLER_ANIMATED] =
                g_signal_new ("pullup-roller-animated",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallPullupRollerClass,
                                	pullup_roller_animated),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);
	 /**
         * PullupRoller::pullup-roller-up-animation-started
  	 *
	 * Signal emitted when the pullup roller
         * is animation starts (expanded upwards)
         *
         */
        pullup_roller_signals[SIG_PULLUP_ROLLER_UP_ANIMATION_STARTED] =
                g_signal_new ("pullup-roller-up-animation-started",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallPullupRollerClass,
        	                        pullup_roller_up_animation_started),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);

	 /**
         * PullupRoller::pullup-roller-down-animation-started
  	 *
	 * Signal emitted when the pullup roller
         * is animation starts to move down(is either pulled down)
         *
         */
        pullup_roller_signals[SIG_PULLUP_ROLLER_DOWN_ANIMATION_STARTED] =
                g_signal_new ("pullup-roller-down-animation-started",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallPullupRollerClass,
        	                        pullup_roller_down_animation_started),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__VOID,
                        G_TYPE_NONE, 0);

	 /**
         * PullupRoller::pullup-roller-item-selected
  	 *
	 * Signal emitted when an item gets activated in pullup roller
         *
         */
        pullup_roller_signals[SIG_PULLUP_ROLLER_ITEM_SELECTED] =
                g_signal_new ("pullup-roller-item-selected",
                        G_TYPE_FROM_CLASS (pObjectClass),
                        G_SIGNAL_NO_RECURSE |
                        G_SIGNAL_RUN_LAST,
                        G_STRUCT_OFFSET (MildenhallPullupRollerClass,
	                                pullup_roller_item_selected),
                        NULL, NULL,
                        g_cclosure_marshal_VOID__UINT,
                        G_TYPE_NONE, 1,
                        G_TYPE_UINT, NULL);

}

/********************************************************
 * Function : mildenhall_pullup_roller_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_pullup_roller_init (MildenhallPullupRoller *pSelf)
{
        /* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set(PKGDATADIR"/mildenhall_pullup_roller_style.json");
        ClutterAction *pGestureAction = clutter_gesture_action_new ();
	ClutterSettings *settings = clutter_settings_get_default ();

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
	pSelf->priv = PULLUP_ROLLER_GET_PRIVATE (pSelf);

	/* initialise the priv members */
	pSelf->priv->pRollerGrp = NULL;
	pSelf->priv->pRoller = NULL;
	pSelf->priv->pArrow = NULL;
	pSelf->priv->pTopBar = NULL;
	pSelf->priv->pSeamlessTopFilePath = NULL;
	pSelf->priv->pShadowTopFilePath = NULL;
	pSelf->priv->pArrowUpFilePath = NULL;
	pSelf->priv->pArrowDownFilePath = NULL;
	pSelf->priv->pTopShadow = NULL;
	pSelf->priv->pTopbarAnimation = NULL;

	pSelf->priv->bFixedRoller = TRUE;
	pSelf->priv->bPullupRollerAnimate = TRUE;
	pSelf->priv->bPullupRollerDown = FALSE;
	pSelf->priv->bTopBarPressed = FALSE;
	pSelf->priv->bRollerShown = FALSE;
	pSelf->priv->bAnimationRunning = FALSE;

	pSelf->priv->flRollerImageHeight = 0.0;
	pSelf->priv->flSeamlessTopHeight = 0.0;
	pSelf->priv->flLineWidth = 0.0;
	pSelf->priv->flRollerClipArea = 0.0;
	pSelf->priv->flPullupY = 0.0;
	pSelf->priv->flRollerY = 0.0;
	pSelf->priv->flRollerExtraHeight = 0.0;

	pSelf->priv->uinAnimationDuration = 0;

	/* create the roller group */
	pSelf->priv->pRollerGrp = clutter_actor_new();
	clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pRollerGrp);

        /* pares the hash for styles */
        if(NULL != styleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, styleHash);
                /* iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if(NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, mildenhall_pullup_roller_parse_style, pSelf);
                        }
                }
                /* free the style hash */
                thornbury_style_free(styleHash);
        }

	/* add pullup roller top bar */
	pSelf->priv->pTopBar = create_top_bar(pSelf);
	g_object_set(pSelf->priv->pTopBar , "x", 0.0, "y", 0.0, "reactive", TRUE, NULL);
	clutter_actor_add_child(pSelf->priv->pRollerGrp, pSelf->priv->pTopBar);
	g_signal_connect(pSelf->priv->pTopBar, "button-press-event", G_CALLBACK(top_bar_event_cb), pSelf);//G_CALLBACK(top_bar_pullupdown_start), pSelf);
	g_signal_connect(pSelf->priv->pTopBar, "button-release-event", G_CALLBACK(top_bar_event_cb), pSelf);//G_CALLBACK(top_bar_pullupdown), pSelf);
	g_signal_connect(pSelf->priv->pTopBar, "touch-event", G_CALLBACK(top_bar_event_cb), pSelf);//G_CALLBACK(top_bar_pullupdown), pSelf);

# if 0
	/* add swipe action to header and connect signals signals */
	ClutterAction *pSwipeAction = clutter_swipe_action_new();
	clutter_actor_add_action(pSelf->priv->pTopBar, pSwipeAction);
	g_signal_connect(pSwipeAction, "swept", G_CALLBACK(swipe_cb), pSelf);
# endif

	/* add gesture action to get swipe event on the lightwood button*/
	/* Inhancement: May be we can implementt a generic swipe gesture */
	clutter_actor_add_action (pSelf->priv->pTopBar, pGestureAction);
	g_signal_connect (pGestureAction, "gesture-end", G_CALLBACK (gesture_end), pSelf);


	/* add meta info pullup footer */
	pSelf->priv->pFooter =  CLUTTER_ACTOR(mildenhall_meta_info_footer_new());
	if(pSelf->priv->pFooter)
	{
                pGestureAction = clutter_gesture_action_new ();
		g_object_set(pSelf->priv->pFooter, "x", 0.0, "y", clutter_actor_get_height(pSelf->priv->pTopBar) - 10,/*16.0,*/ "reactive", TRUE, NULL);

		clutter_actor_add_child(CLUTTER_ACTOR(pSelf), pSelf->priv->pFooter);
		clutter_actor_set_reactive(pSelf->priv->pFooter, TRUE);
		/* hide the top bar from the footer */
		g_object_set(pSelf->priv->pFooter, "show-top", FALSE, NULL);

		g_signal_connect(pSelf->priv->pFooter, "button-press-event", G_CALLBACK(top_bar_event_cb), pSelf);
		g_signal_connect(pSelf->priv->pFooter, "button-release-event", G_CALLBACK(top_bar_event_cb), pSelf);
		g_signal_connect(pSelf->priv->pFooter, "touch-event", G_CALLBACK(top_bar_event_cb), pSelf);

# if 0
		/* add swipe action to header and connect signals signals */
                ClutterAction *pSwipeAction = clutter_swipe_action_new();
                clutter_actor_add_action(pSelf->priv->pFooter, pSwipeAction);
                g_signal_connect(pSwipeAction, "swept", G_CALLBACK(swipe_cb), pSelf);
# endif

		/* add gesture action to get swipe event on the lightwood button*/
		/* Inhancement: May be we can implementt a generic swipe gesture */
		clutter_actor_add_action (pSelf->priv->pFooter, pGestureAction);
		g_signal_connect (pGestureAction, "gesture-end", G_CALLBACK (gesture_end), pSelf);

		/* get the width to update the priv */
		g_object_get (pSelf->priv->pFooter, "natural-width", &pSelf->priv->flWidth, NULL);
	}

        g_object_get (settings, "dnd-drag-threshold", &pSelf->priv->inThreshold, NULL);

	PULLUP_ROLLER_DEBUG(" pullup roller width = footer width = %f\n", pSelf->priv->flWidth);
}


/*******************************************************************************************************
 *
 * Exposed APIs
 *
 *******************************************************************************************************/
/**
 * mildenhall_pullup_roller_set_fix_roller :
 * @pPullupRoller           : Object reference
 * @rollerType          : Value which need to be set for roller type
 *
 * roller type as enum : FIXED_ROLLER / VARIABLE_ROLLER
 */
void mildenhall_pullup_roller_set_fix_roller(MildenhallPullupRoller *pPullupRoller, gboolean bFixedRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

	if( NULL != priv->pRoller )
        {
                clutter_actor_unparent( priv->pRoller );
                g_object_unref (priv->pRoller);
                priv->pRoller = NULL;
        }

	/* update the fix/variable roller flag */
	priv->bFixedRoller = bFixedRoller;
	/* create the roller */
	if(bFixedRoller)
		priv->pRoller = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
						"width", priv->flWidth,
						"height", priv->flRollerImageHeight + priv->flRollerExtraHeight,
						"roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER,
						"roll-over", TRUE,
						NULL);
	else
		priv->pRoller = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
						"width", priv->flWidth,
						"height", priv->flRollerImageHeight + priv->flRollerExtraHeight,
						"roller-type", MILDENHALL_VARIABLE_ROLLER_CONTAINER,
						"roll-over", TRUE,
						NULL);

	/* update the bg for pullup roller */
	if(priv->pRoller)
	{
		/* set the position  and other properties, signals */
		clutter_actor_set_clip(priv->pRoller, 0,  priv->flRollerClipArea, priv->flWidth, priv->flRollerImageHeight);
		clutter_actor_add_child(priv->pRollerGrp, priv->pRoller);
		clutter_actor_set_position(priv->pRoller, 0.0, priv->flRollerY);
		//clutter_actor_set_opacity(priv->pRoller, 0 );
		g_signal_connect (priv->pRoller, "roller-item-activated", G_CALLBACK (pullup_roller_item_activated_cb), pPullupRoller);
	}

	g_object_notify (G_OBJECT(pPullupRoller), "fix-roller");
}

/**
 * mildenhall_pullup_roller_set_item_type:
 * @pPullupRoller : pullup roller object reference
 * @gType : GType for item type
 *
 * item type as GType for roller tiem
 */
void mildenhall_pullup_roller_set_item_type(MildenhallPullupRoller *pPullupRoller, GType gType)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

        if( NULL != priv->pRoller )
        {
		g_object_set(priv->pRoller, "item-type", gType, NULL);
	}
	g_object_notify (G_OBJECT(pPullupRoller), "item-type");
}

/**
 * mildenhall_pullup_roller_set_model:
 * @pullupRoller  : Object reference
 * @model       : Value which need to be set for roller model
 *
 * Set the model used by the roller
 */
void mildenhall_pullup_roller_set_model(MildenhallPullupRoller *pPullupRoller, ThornburyModel *pModel)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

        if( NULL != priv->pRoller )
        {
                g_object_set(priv->pRoller, "model", pModel, NULL);
        }
	g_object_notify (G_OBJECT(pPullupRoller), "model");
}

/**
 * mildenhall_pullup_roller_add_attribute
 * @pPullupRoller: object reference
 * @property: Name of the attribute
 * @column: Column number
 *
 * Maps a property of the item actors to a column of the current #ThornburyModel.
 *
 */
void mildenhall_pullup_roller_add_attribute(MildenhallPullupRoller *pPullupRoller, const gchar *pProperty, gint inColumn)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

        if(NULL != priv->pRoller)
		mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRoller), pProperty, inColumn);

}

/**
 * mildenhall_pullup_roller_set_footer_icon_left:
 * @pPullupRoller : pullup roller object reference
 * @bIconLeft : TRUE if icon else FALSE(in case of text)
 *
 * whether footer left side has an icon or a text. MetaInfoHeader can
 * have more than one icon on left side which will toggle the icons
 * on click of the left icon and signal will get emitted.
 */
void mildenhall_pullup_roller_set_footer_icon_left(MildenhallPullupRoller *pPullupRoller, gboolean bIconLeft)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

	if(NULL != priv->pFooter)
	{
		g_object_set(priv->pFooter, "left-icon", bIconLeft, NULL);
	}
        g_object_notify (G_OBJECT(pPullupRoller), "footer-left-icon");
}

/**
 * mildenhall_pullup_roller_set_footer_ratings:
 * @pPullupRoller : pullup roller object reference
 * @bRatings : TRUE if icon else FALSE(in case of text)
 *
 * whether footer right end has raings stars.
 */
void mildenhall_pullup_roller_set_footer_ratings(MildenhallPullupRoller *pPullupRoller, gboolean bRatings)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

	if(NULL != priv->pFooter)
	{
		g_object_set(priv->pFooter, "ratings", bRatings, NULL);
	}

	/* TBGD: footer "ratings" property handling */
	g_object_notify (G_OBJECT(pPullupRoller), "footer-ratings");
}


/**
 * mildenhall_pullup_roller_set_show
 * @pPullupRoller : pullup roller object reference
 * @bShow : TRUE if roller needs to be shown
 *
 *  pullup roller is shown with animation if set as TRUE.
 */
void mildenhall_pullup_roller_set_show(MildenhallPullupRoller *pPullupRoller, gboolean bShow)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if (!  MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
        {
                g_warning("invalid pullup roller object\n");
                return ;
        }
	if(bShow && priv->bAnimationRunning)
	{
		return;
	}
	/* if roller has to be hidden during show animation */
	if(!bShow && priv->bAnimationRunning)
        {
                clutter_actor_detach_animation(priv->pRollerGrp);
                priv->bAnimationRunning = FALSE;
		priv->bPullupRollerDown = TRUE;
		/* as button press cb */
	        top_bar_pullupdown_start(NULL, NULL, pPullupRoller);
        	/* as button release cb */
        	top_bar_pullupdown(NULL, NULL, pPullupRoller);

        }
	/* if if roller has to be hidden if already in show state */
	else if(!bShow && ! priv->bAnimationRunning && priv->bRollerShown)
	{
		priv->bPullupRollerDown = TRUE;
		/* as button press cb */
		top_bar_pullupdown_start(NULL, NULL, pPullupRoller);
		/* as button release cb */
		top_bar_pullupdown(NULL, NULL, pPullupRoller);
	}
}

/**
 * mildenhall_pullup_roller_set_footer_model:
 * @pullupRoller          : Object reference
 * @model               : Value which need to be set for footer model
 *
 * Set the model used by the meta info footer
 */
void mildenhall_pullup_roller_set_footer_model(MildenhallPullupRoller *pPullupRoller, ThornburyModel *pModel)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        g_return_if_fail ( MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) );

        if(NULL != priv->pFooter)
		g_object_set(priv->pFooter, "model", pModel, NULL);

	g_object_notify (G_OBJECT(pPullupRoller), "footer-model");
}

/**
 * mildenhall_pullup_roller_get_fix_roller :
 * @pPullupRoller : Object reference
 * @Returns : TRUE/FALSE whether roller is of fix/variable type
 *
 * TRUE for  FIXED_ROLLER, FLASE if roller is a variable roller
 */
gboolean mildenhall_pullup_roller_get_fix_roller(MildenhallPullupRoller *pPullupRoller)
{

        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);
	 if(!MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller) )
        {
                g_warning("invalid pullup roller object\n");
                return FALSE;
        }

	return priv->bFixedRoller;
}
/**
 * mildenhall_pullup_roller_get_footer_icon_left:
 * @pPullupRoller : pullup roller object reference
 * @Returns : TRUE if footer left side has an icon, FALSE if text
 *
 *  whether footer left side has an icon or a text.
 */
gboolean mildenhall_pullup_roller_get_footer_icon_left(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        gboolean bIconLeft = FALSE;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(!MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller) )
	{
		g_warning("invalid pullup roller object\n");
		return FALSE;
	}

	/* get the footer  "left-icon" property */
	if(NULL != priv->pFooter)
		g_object_set(priv->pFooter, "left-icon", &bIconLeft, NULL);

        return bIconLeft;
}

/**
 * mildenhall_pullup_roller_get_footer_ratings
 * @pPullupRoller : pullup roller object reference
 * @Returns : TRUE if footer right side has ratings star, FALSE otherwise
 *
 *  whether footer right side has an ratings.
 */
gboolean mildenhall_pullup_roller_get_footer_ratings(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        gboolean bRatings = FALSE;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if (!  MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
	{
		g_warning("invalid pullup roller object\n");
		return FALSE;
	}

        /* get the footer "right-icon" property */
        if(NULL != priv->pFooter)
                g_object_set(priv->pFooter, "ratings", &bRatings, NULL);

        return bRatings;
}

/**
 * mildenhall_pullup_roller_get_show
 * @pPullupRoller : pullup roller object reference
 * @Returns : TRUE if pullup roller is shown
 *
 *  whether pullup roller is shown.
 */
gboolean mildenhall_pullup_roller_get_show(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if (!  MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
        {
                g_warning("invalid pullup roller object\n");
                return FALSE;
        }

	return priv->bRollerShown;
}

/**
 * mildenhall_pullup_roller_get_model:
 * @pullupRoller: Object reference
 *
 * gets the roller model containing pullup roller item data
 */
ThornburyModel *mildenhall_pullup_roller_get_model(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        ThornburyModel *pModel = NULL;
	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
	{
		g_warning("invalid pullup roller object\n");
		return NULL;
	}

	if(NULL != priv->pRoller)
		g_object_get(priv->pRoller, "model", &pModel, NULL);

	return pModel;
}

/**
 * mildenhall_pullup_roller_get_footer_model:
 * @pullupRoller           : Object reference
 *
 * gets the footer model containing pullup roller footer data
 */
ThornburyModel *mildenhall_pullup_roller_get_footer_model(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        ThornburyModel *pModel = NULL;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

	if(! MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
	{
		g_warning("invalid pullup roller object\n");
		return NULL;
	}

	if(NULL != priv->pFooter)
		g_object_get(priv->pFooter, "model", &pModel, NULL);

	return pModel;
}

/**
 * mildenhall_pullup_roller_get_item_type
 * @pPullupRoller  : Object reference
 *
 * gets the GType of roller tiem.
 */
GType mildenhall_pullup_roller_get_item_type(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        GType type = G_TYPE_NONE;

	PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
        {
                g_warning("invalid pullup roller object\n");
                return type;
        }

	if(NULL != priv->pRoller)
		g_object_get(priv->pRoller, "item-type", &type, NULL);

	return type;
}

/**
 * mildenhall_pullup_roller_get_width
 * @pPullupRoller  : Object reference
 *
 * gets the width of pullup roller.
 */
gfloat mildenhall_pullup_roller_get_width(MildenhallPullupRoller *pPullupRoller)
{
        MildenhallPullupRollerPrivate *priv = PULLUP_ROLLER_GET_PRIVATE (pPullupRoller);
        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
        {
                g_warning("invalid pullup roller object\n");
                return 0.0;
        }

	return priv->flWidth;
}

/**
 * mildenhall_pullup_roller_get_height
 * @pPullupRoller  : Object reference
 *
 * gets the height of pullup roller.
 */
gfloat mildenhall_pullup_roller_get_height(MildenhallPullupRoller *pPullupRoller)
{
        PULLUP_ROLLER_DEBUG("%s\n", __FUNCTION__);

        if(! MILDENHALL_IS_PULLUP_ROLLER (pPullupRoller ) )
        {
                g_warning("invalid pullup roller object\n");
                return 0.0;
        }

	return clutter_actor_get_height(CLUTTER_ACTOR(pPullupRoller));
}

/**
 * mildenhall_pullup_roller_new:
 * Returns: pullup roller object
 *
 * Creates a pullup roller object
 */
MildenhallPullupRoller *mildenhall_pullup_roller_new (void)
{
	return g_object_new (MILDENHALL_TYPE_PULLUP_ROLLER,  NULL);
}
