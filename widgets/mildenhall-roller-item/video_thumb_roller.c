/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* video-thumb-roller.c */

#include "video_thumb_roller.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"

typedef struct
{
	ClutterActor *icon;
	GrassmoorAVPlayer *player;
	ClutterActor *videoActor;
	ThornburyModel *model;
	gint currentRow;
	gint row;
} MildenhallVideoThumbnailRollerPrivate;

struct _MildenhallVideoThumbnailRoller
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallVideoThumbnailRoller, mildenhall_video_thumbnail_roller, CLUTTER_TYPE_ACTOR)

enum
{
	PROP_0,
	PROP_ICON_DATA,
	PROP_AV_OBJECT,
	PROP_AV_ACTOR,
	PROP_SHOW,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_MODEL,
	PROP_ROW
};

static void
mildenhall_video_thumbnail_roller_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
	MildenhallVideoThumbnailRoller *pVideoRoller = MILDENHALL_VIDEO_THUMBNAIL_ROLLER (object);
	MildenhallVideoThumbnailRollerPrivate *priv = mildenhall_video_thumbnail_roller_get_instance_private (pVideoRoller);

	switch (property_id)
	{
	case PROP_AV_OBJECT:
		g_value_set_object (value, priv->player);
		break;
	case PROP_AV_ACTOR:
                g_value_set_object (value, priv->videoActor);
                break;
	case PROP_WIDTH:
		g_value_set_float (value, clutter_actor_get_width (priv->icon));
		break;
	case PROP_HEIGHT:
		g_value_set_float (value, clutter_actor_get_height (priv->icon));
		break;
	case PROP_MODEL:
		g_value_set_object (value, priv->model);
		break;
	case PROP_ICON_DATA:
	     /* FIXME: Don't we need to get value of ICON_DATA? (T1409) */
	     /* Write-only */
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void set_video_av_actor(MildenhallVideoThumbnailRoller *pVideoRoller)
{
	GList *pChildList = NULL;
	gint nChildren,index;
	gboolean isContains = FALSE;
	gboolean isRemoved = FALSE;
	MildenhallVideoThumbnailRollerPrivate *priv = mildenhall_video_thumbnail_roller_get_instance_private (pVideoRoller);

		pChildList = clutter_actor_get_children(CLUTTER_ACTOR(pVideoRoller));
		nChildren = clutter_actor_get_n_children(CLUTTER_ACTOR(pVideoRoller));
		for(index = 0 ; index < nChildren ; index++)
		{
			gpointer data = g_list_nth_data(pChildList,index);
			if(CLUTTER_IS_ACTOR(data) && g_strcmp0(clutter_actor_get_name(CLUTTER_ACTOR(data)), "av-actor") == 0)
			{
				isContains = TRUE;
				if (priv->videoActor != CLUTTER_ACTOR (data))
				{
					clutter_actor_remove_child(CLUTTER_ACTOR(pVideoRoller),CLUTTER_ACTOR(data));
					isRemoved = TRUE;
				}
			}
		}
		clutter_actor_set_width (CLUTTER_ACTOR (priv->videoActor), clutter_actor_get_width (CLUTTER_ACTOR (pVideoRoller)));
		clutter_actor_set_height (CLUTTER_ACTOR (priv->videoActor), clutter_actor_get_height (CLUTTER_ACTOR (pVideoRoller)));
		if(isRemoved)
		{
			ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (priv->videoActor));
			if(parent)
				clutter_actor_remove_child (CLUTTER_ACTOR (parent),CLUTTER_ACTOR (priv->videoActor));
			clutter_actor_add_child (CLUTTER_ACTOR (pVideoRoller), CLUTTER_ACTOR (priv->videoActor));
			clutter_actor_hide (CLUTTER_ACTOR (priv->videoActor));
		}
		if(!(isContains))
		{
			ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (priv->videoActor));
			if(parent)
				clutter_actor_remove_child (CLUTTER_ACTOR (parent), CLUTTER_ACTOR (priv->videoActor));
			clutter_actor_add_child (CLUTTER_ACTOR (pVideoRoller), CLUTTER_ACTOR (priv->videoActor));
			clutter_actor_hide (CLUTTER_ACTOR (priv->videoActor));
		}
		clutter_actor_set_reactive (CLUTTER_ACTOR (priv->videoActor), FALSE);
}


static void
mildenhall_video_thumbnail_roller_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	MildenhallVideoThumbnailRoller *pVideoRoller = MILDENHALL_VIDEO_THUMBNAIL_ROLLER (object);
	MildenhallVideoThumbnailRollerPrivate *priv = mildenhall_video_thumbnail_roller_get_instance_private (pVideoRoller);

	switch (property_id)
	{
	case PROP_ICON_DATA:
		clutter_actor_set_content (priv->icon, CLUTTER_CONTENT (g_value_get_object (value)));
		break;
	case PROP_AV_OBJECT:
		g_object_unref (priv->player);
		priv->player = g_value_dup_object (value);
		break;
	case PROP_AV_ACTOR:
		g_object_unref (priv->videoActor);
		priv->videoActor = g_value_dup_object (value);
		if (priv->videoActor != NULL) 
		  {
		    clutter_actor_set_name (priv->videoActor, "av-actor");
		    set_video_av_actor (pVideoRoller);
		  }
                break;
	case PROP_WIDTH:
		clutter_actor_set_width (priv->icon, g_value_get_float (value));
		clutter_actor_set_width (CLUTTER_ACTOR (priv->videoActor), g_value_get_float (value));
		clutter_actor_set_width (CLUTTER_ACTOR (pVideoRoller), g_value_get_float (value));
		break;
	case PROP_HEIGHT:
		clutter_actor_set_height (priv->icon, g_value_get_float (value));
		clutter_actor_set_height (CLUTTER_ACTOR (priv->videoActor), g_value_get_float (value));
		clutter_actor_set_height (CLUTTER_ACTOR (pVideoRoller), g_value_get_float (value));
		break;
	case PROP_MODEL:
		g_object_unref (priv->model);
		priv->model = g_value_dup_object (value);
		break;
	case PROP_ROW:
		priv->currentRow = g_value_get_uint (value);
		break;
	case PROP_SHOW:
		if(g_value_get_boolean(value))
		{
			clutter_actor_show (CLUTTER_ACTOR (priv->videoActor));
		}
		else
		{
			clutter_actor_hide (CLUTTER_ACTOR (priv->videoActor));
			clutter_actor_show (CLUTTER_ACTOR (priv->icon));
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void
mildenhall_video_thumbnail_roller_dispose (GObject *object)
{
  MildenhallVideoThumbnailRoller *self = MILDENHALL_VIDEO_THUMBNAIL_ROLLER (object);
  MildenhallVideoThumbnailRollerPrivate *priv = mildenhall_video_thumbnail_roller_get_instance_private (self);

  g_clear_object (&priv->player);
  g_clear_object (&priv->videoActor);
  g_clear_object (&priv->model);

  G_OBJECT_CLASS (mildenhall_video_thumbnail_roller_parent_class)->dispose (object);
}

static void
mildenhall_video_thumbnail_roller_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_video_thumbnail_roller_parent_class)->finalize (object);
}

static void
mildenhall_video_thumbnail_roller_class_init (MildenhallVideoThumbnailRollerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->get_property = mildenhall_video_thumbnail_roller_get_property;
	object_class->set_property = mildenhall_video_thumbnail_roller_set_property;
	object_class->dispose = mildenhall_video_thumbnail_roller_dispose;
	object_class->finalize = mildenhall_video_thumbnail_roller_finalize;

	GParamSpec *pspec;
	pspec = g_param_spec_object ("icon-data",
			"icon-data",
			"texture content for the thumbnail icon",
			G_TYPE_OBJECT,
			G_PARAM_WRITABLE);
	g_object_class_install_property (object_class, PROP_ICON_DATA, pspec);


	pspec = g_param_spec_uint ("row",
			"row number",
			"row number",
			0, G_MAXUINT,
			0,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ROW, pspec);


	pspec = g_param_spec_object ("av-object",
			"AV-Object",
			"AV player object",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_AV_OBJECT, pspec);

	pspec = g_param_spec_object ("av-actor",
                        "AV-Actor",
                        "AV player actor",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_AV_ACTOR, pspec);


	pspec = g_param_spec_float ("width",
			"Width",
			"Width of the item",
			-G_MAXFLOAT,
			G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_WIDTH, pspec);

	pspec = g_param_spec_float ("height",
			"Height",
			"Height of the item",
			-G_MAXFLOAT,
			G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( object_class, PROP_HEIGHT, pspec);


	pspec = g_param_spec_object ("model",
			"Model",
			"model having button states data",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MODEL, pspec);

	pspec = g_param_spec_boolean("show",
			"av-show",
			"To show av-player object in roller",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_SHOW, pspec);

}

static void
mildenhall_video_thumbnail_roller_init (MildenhallVideoThumbnailRoller *self)
{
  MildenhallVideoThumbnailRollerPrivate *priv = mildenhall_video_thumbnail_roller_get_instance_private (self);

  clutter_actor_set_size (CLUTTER_ACTOR (self), 164, 164);
  priv->row = -1;
  priv->model = NULL;
  priv->icon = clutter_actor_new ();
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->icon);
  clutter_actor_set_size (priv->icon, 162, 162);
  clutter_actor_show (priv->icon);
  clutter_actor_show (CLUTTER_ACTOR (self));
}

MildenhallVideoThumbnailRoller *
mildenhall_video_thumbnail_roller_new (void)
{
  return g_object_new (MILDENHALL_TYPE_VIDEO_THUMBNAIL_ROLLER, NULL);
}
