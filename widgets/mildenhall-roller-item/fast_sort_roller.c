/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* sort-roller.c */

#include "fast_sort_roller.h"

#include <thornbury/thornbury.h>

#include "liblightwood-glowshader.h"

typedef struct _MildenhallSortRollerPrivate
{
  ClutterActor *icon;
  ClutterActor *extra_separator;
  ClutterActor *label;
  ClutterActor *vertical_line1;
  ClutterActor *vertical_line2;
  gchar *column_type;
  gchar *text;
  gboolean row_called;
  ClutterEffect *glow_effect_1;
} MildenhallSortRollerPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallSortRoller, mildenhall_sort_roller, CLUTTER_TYPE_GROUP)

enum
{
  PROP_0,
  PROP_ICON,
  PROP_LABEL,
  PROP_TYPE,
  PROP_FOCUSED,
  PROP_TEXT_FONT,
  PROP_ROW

};

static void
extract_column_type (MildenhallSortRoller *self)
{
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);

  if (!g_strcmp0 (priv->column_type, "alphabet"))
    {
      clutter_actor_set_position (priv->label, 25, 13);
    }
  else
    {
      if (!g_strcmp0 (priv->column_type, "numeric"))
        {
          clutter_actor_set_position (priv->label, 15, 13);
        }
    }
}

static void
sort_roller_get_property (GObject    *object,
                          guint       property_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
on_notify_language (GObject    *object,
                    GParamSpec *pspec,
                    gpointer    user_data)
{
  MildenhallSortRoller *self = MILDENHALL_SORT_ROLLER (user_data);
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);

  clutter_text_set_text (CLUTTER_TEXT (priv->label), gettext (priv->text));
}

static void
sort_roller_set_property (GObject      *object,
                          guint         property_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  MildenhallSortRoller *self = MILDENHALL_SORT_ROLLER (object);
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);

  switch (property_id)
    {
    case PROP_ICON:
      break;
    case PROP_LABEL:
      {
        gfloat x_pos;
        gfloat y_pos;

        clutter_text_set_text (CLUTTER_TEXT (priv->label),
          gettext (g_value_get_string (value)));

        g_free (priv->text);
        priv->text = g_value_dup_string (value);

        x_pos= (63 - clutter_actor_get_width (priv->label)) / 2;
        clutter_actor_set_x (priv->label, x_pos);

        y_pos= (64 - clutter_actor_get_height (priv->label)) / 2;
        clutter_actor_set_y (priv->label, y_pos + 3);
      }
      break;
    case PROP_TYPE:
      {
        g_free (priv->column_type);
        priv->column_type = g_value_dup_string (value);
        extract_column_type (self);
      }
      break;
    case PROP_FOCUSED:
      if (g_value_get_boolean (value))
        {
          if (clutter_actor_get_effect (priv->label, "glow") != NULL)
            return;

          clutter_actor_add_effect_with_name (priv->label, "glow", priv->glow_effect_1);
        }
      else
        {
          if (clutter_actor_get_effect (priv->label, "glow") == NULL)
            return;

          clutter_actor_remove_effect_by_name (priv->label, "glow");
        }
      break;
    case PROP_TEXT_FONT:
      {
          gchar *fontname = g_strdup_printf ("DejaVuSansCondensed %dpx", g_value_get_int (value));
          clutter_text_set_font_name (CLUTTER_TEXT (priv->label), fontname);
          g_free (fontname);
      }
      break;
    case PROP_ROW:
      {
        ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));

        if (FALSE == priv->row_called)
          {
            g_signal_connect ((parent),
              "notify::language",
              G_CALLBACK (on_notify_language),
              self);
          }

        priv->row_called=TRUE;
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
sort_roller_dispose (GObject *object)
{
  MildenhallSortRoller *self = MILDENHALL_SORT_ROLLER (object);
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);

  g_clear_pointer (&priv->text, g_free);
  g_clear_pointer (&priv->column_type, g_free);

  g_clear_object (&priv->glow_effect_1);

  G_OBJECT_CLASS (mildenhall_sort_roller_parent_class)->dispose (object);
}

static void
actor_paint (ClutterActor *actor)
{
  MildenhallSortRoller *self = MILDENHALL_SORT_ROLLER (actor);
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);

  if (priv->vertical_line1)
    clutter_actor_paint (priv->vertical_line1);

  if (priv->vertical_line2)
    clutter_actor_paint (priv->vertical_line2);

  CLUTTER_ACTOR_CLASS (mildenhall_sort_roller_parent_class)->paint (actor);
}



static void
mildenhall_sort_roller_class_init (MildenhallSortRollerClass *klass)
{
  GParamSpec *pspec;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  object_class->get_property = sort_roller_get_property;
  object_class->set_property = sort_roller_set_property;
  object_class->dispose = sort_roller_dispose;
  actor_class->paint = actor_paint;

  pspec = g_param_spec_boxed("icon",
			"icon",
			"icon",
			COGL_TYPE_HANDLE,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class,PROP_ICON, pspec);


  pspec = g_param_spec_string("label",
			"label",
			"Text for the label",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class,PROP_LABEL, pspec);

	pspec = g_param_spec_string("column-type",
			"column-type",
			"column-type for example:alphabet or numeric",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class,PROP_TYPE, pspec);

	 pspec = g_param_spec_boolean ("focused",
	                                  "focused",
	                                  "Whether this actor should be rendered as focused",
	                                  FALSE,
	                                  G_PARAM_WRITABLE);
	    g_object_class_install_property (object_class, PROP_FOCUSED, pspec);
	    pspec = g_param_spec_int("font-size",
	                               "font-size",
	                               "font-size of text",
	                               0,
	                               64,
	                               24.0,
	                               G_PARAM_WRITABLE);
	      g_object_class_install_property (object_class, PROP_TEXT_FONT, pspec);
	      pspec = g_param_spec_uint ("row",
	      			"row number",
	      			"row number",
	      			0, G_MAXUINT,
	      			0,
	      			G_PARAM_WRITABLE);
	      	g_object_class_install_property (object_class, PROP_ROW, pspec);


}


static ClutterActor *
DrawVerticalSeperator(ClutterColor *lineColor,ClutterColor *depthLineColor,gfloat width,gfloat height)
{
	/*Create a separator line group */
        ClutterActor *separatorLineGroup = clutter_actor_new ();

        /*The main separator line */
         ClutterActor *separatorLine = clutter_actor_new();
            clutter_actor_set_background_color(separatorLine, lineColor);
        clutter_actor_set_position (separatorLine, 0, 0);
        clutter_actor_set_size (separatorLine, width, height);
        clutter_actor_add_child(CLUTTER_ACTOR (separatorLineGroup),separatorLine);

        return separatorLineGroup;
}

static void
mildenhall_sort_roller_init (MildenhallSortRoller *self)
{
  MildenhallSortRollerPrivate *priv = mildenhall_sort_roller_get_instance_private (self);
  ClutterActor *line;
  ClutterActor *sepLine;
  ClutterColor fontColor = {0x98, 0xA9, 0xB2, 0xFF};
  ClutterColor line_horizon_color = {0xFF, 0xFF, 0xFF, 0x33};
  ClutterColor line_color = {0xFF, 0xFF, 0xFF, 0x33};
  ClutterColor depthLineColor = {0xFF, 0xFF, 0xFF, 0x33};

  priv->row_called=FALSE;
  priv->icon = thornbury_ui_texture_create_new (EXAMPLES_DATADIR "/icon_pause.png", 0, 0, FALSE, FALSE);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->icon);
  clutter_actor_set_position (priv->icon, 18, 14);
  clutter_actor_set_size (priv->icon, 0,0);
  clutter_actor_hide(priv->icon);

  priv->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (priv->label), &fontColor);
  clutter_text_set_font_name (CLUTTER_TEXT (priv->label), ROLLER_FONT (28));
  clutter_actor_set_position (priv->label, 15, 13);
  clutter_text_set_justify (CLUTTER_TEXT (priv->label), TRUE);
  clutter_text_set_line_alignment (CLUTTER_TEXT (priv->label), PANGO_ALIGN_CENTER);
  clutter_text_set_ellipsize (CLUTTER_TEXT (priv->label), PANGO_ELLIPSIZE_END);
  clutter_actor_add_child (CLUTTER_ACTOR (self), priv->label);
  clutter_actor_show (priv->label); 

  sepLine = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 63);
  clutter_actor_set_position (sepLine,0,0);
  clutter_actor_add_child (CLUTTER_ACTOR (self), sepLine);
  clutter_actor_show (sepLine);

  sepLine = DrawVerticalSeperator (&line_color,&depthLineColor,1,63);
  clutter_actor_set_position (sepLine,62,0);
  clutter_actor_add_child (CLUTTER_ACTOR (self), sepLine);
  clutter_actor_show(sepLine);

  line = clutter_actor_new();
  clutter_actor_set_background_color (line, &line_horizon_color);
  clutter_actor_set_size (line, 63, 1);
  clutter_actor_set_position (line, 0,63);
  clutter_actor_add_child (CLUTTER_ACTOR (self), line);
  clutter_actor_show (line);
  clutter_actor_set_reactive (CLUTTER_ACTOR (self), TRUE);

  priv->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
}

MildenhallSortRoller *
mildenhall_sort_roller_new (void)
{
  return g_object_new (MILDENHALL_TYPE_SORT_ROLLER, NULL);
}
