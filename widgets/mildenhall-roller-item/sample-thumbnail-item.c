/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Sample item for the flow layout of MildenhallFixedRoller
 *
 *
 */

#include <liblightwood-roller.h>
#include "liblightwood-fixedroller.h"
#include "liblightwood-expandable.h"
#include "liblightwood-expander.h"

#include "sample-thumbnail-item.h"

G_DEFINE_TYPE_WITH_CODE (SampleThumbnailItem, sample_thumbnail_item, CLUTTER_TYPE_TEXTURE,);

static gboolean
button_release_event (ClutterActor       *actor,
                      ClutterButtonEvent *event)
{
  g_debug ("actor '%s' has been activated", clutter_actor_get_name (actor));

  return FALSE;
}
#if 0

static gboolean
actor_get_paint_volume (ClutterActor       *actor,
                        ClutterPaintVolume *volume)
{
  return clutter_paint_volume_set_from_allocation (volume, actor);
}
#endif

static void
sample_thumbnail_item_class_init (SampleThumbnailItemClass *klass)
{
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);

  actor_class->button_release_event = button_release_event;
  //actor_class->get_paint_volume = actor_get_paint_volume;
}

static void
sample_thumbnail_item_init (SampleThumbnailItem *sample_thumbnail_item)
{
  clutter_actor_set_size (CLUTTER_ACTOR (sample_thumbnail_item), 200, 200);
  clutter_actor_set_reactive (CLUTTER_ACTOR (sample_thumbnail_item), TRUE);
}

ClutterActor*
sample_thumbnail_item_new ()
{
    return g_object_new (TYPE_SAMPLE_THUMBNAIL_ITEM, NULL);
}
