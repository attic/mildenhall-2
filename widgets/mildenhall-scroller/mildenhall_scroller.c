/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-speller-scroller.c
 *
 *
 * mildenhall-speller-scroller.c */

#include "mildenhall_scroller.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

#define GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SCROLLER, MildenhallScrollerPrivate))

/* List of all properties for the scroller object */
enum MildenhallScrollerProperty
{
	PROP_MILDENHALL_SCROLLER_FIRST,

	PROP_MILDENHALL_SCROLLER_TYPE,
	PROP_MILDENHALL_SCROLLER_CURRENT_ITEM,
	PROP_MILDENHALL_SCROLLER_TOTAL_ITEMS,
	PROP_MILDENHALL_SCROLLER_DISPLAYED_ITEMS,
	PROP_MILDENHALL_SCROLLER_WIDTH,
	PROP_MILDENHALL_SCROLLER_HEIGHT,
	PROP_MILDENHALL_SCROLLER_RESET_SLIDER,
	PROP_MILDENHALL_SCROLLER_LAST
};

/* List of all signals for the scroller object */
enum MildenhallScrollerSignals
{
	SIG_MILDENHALL_SCROLLER_SCROLLED_UP,
	SIG_MILDENHALL_SCROLLER_SCROLLED_DOWN,
	SIG_MILDENHALL_SCROLLER_LAST_SIGNAL
};

static guint32 mildenhall_scroller_signals[SIG_MILDENHALL_SCROLLER_LAST_SIGNAL] = {0,};

/* private structure members for the scroller object */
struct _MildenhallScrollerPrivate
{
    ClutterActor *line;
    ClutterActor *lineGroup;
    ClutterActor *fingerscroll;
    ClutterActor *viewport;
    ClutterActor *slider;
    ClutterActor *scrollbar;
    ClutterActor *background;

    gfloat height;
    gfloat width;

    gint total_items;
    gint current_item;
    gint displayed_items;
    gint type;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallScroller, mildenhall_scroller, CLUTTER_TYPE_ACTOR)

static void mildenhall_scroller_create_ui(MildenhallScroller *self , MILDENHALL_SCROLLER_TYPE scroller_type);

/********************************************************
 * Function : mildenhall_scroller_dispose
 * Description: Dispose the mildenhall scroller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_dispose (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_scroller_parent_class)->dispose (object);
}

/********************************************************
 * Function : mildenhall_scroller_finalize
 * Description: finalize the mildenhall scroller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_scroller_parent_class)->finalize (object);
}

/********************************************************
 * Function : set_scroller_displayed_items
 * Description: function to set no.of visible items for scroller
 * Parameters: The scroller object reference , num of items
 * Return value: void
 ********************************************************/
static void set_scroller_displayed_items(MildenhallScroller *self , gint value)
{
    self->priv->displayed_items = value;
}

/********************************************************
 * Function : set_scroller_width
 * Description: function to set width of scroller
 * Parameters: The scroller object reference , value
 * Return value: void
 ********************************************************/
static void set_scroller_width(MildenhallScroller *self , gfloat value)
{
    self->priv->width = value;
    clutter_actor_set_width(self->priv->background,self->priv->width);
}

/********************************************************
 * Function : set_scroller_height
 * Description: function to set height of scroller
 * Parameters: The scroller object reference , value
 * Return value: void
 ********************************************************/
static void set_scroller_height(MildenhallScroller *self , gfloat value)
{
    self->priv->height = value;
    clutter_actor_set_height(self->priv->background,self->priv->height);
}

/********************************************************
 * Function : set_scroller_current_item
 * Description: function to set current item in scroller
 * Parameters: The scroller object reference , value
 * Return value: void
 ********************************************************/
static void set_scroller_current_item(MildenhallScroller *self , gint value)
{
    gint scroll_area_height = clutter_actor_get_height (CLUTTER_ACTOR(self));
    gint slider_height = scroll_area_height;

    if(self->priv->total_items > self->priv->displayed_items)
    {
        slider_height = scroll_area_height / (1 + self->priv->total_items - self->priv->displayed_items);
    }

    clutter_actor_set_y(self->priv->slider,value * slider_height);
}

/********************************************************
 * Function : set_scroller_reset_slider
 * Description: function to reset the slider to default
 * Parameters: The scroller object reference , value
 * Return value: void
 ********************************************************/
static void set_scroller_reset_slider(MildenhallScroller *self , gboolean value)
{
    clutter_actor_set_position(self->priv->slider , 55 , 0);
}

/********************************************************
 * Function : set_scroller_total_items
 * Description: function to set max lines
 * Parameters: The scroller object reference , value
 * Return value: void
 ********************************************************/
static void set_scroller_total_items(MildenhallScroller *self , gint value)
{
    gint scroll_area_height = clutter_actor_get_height (CLUTTER_ACTOR (self));
    gint slider_height = scroll_area_height;

    MildenhallScrollerPrivate *priv = mildenhall_scroller_get_instance_private (self);
    priv->total_items = value;

    if (priv->total_items > priv->displayed_items)
    {
        slider_height = scroll_area_height / (1 + self->priv->total_items - self->priv->displayed_items);
    }
    clutter_actor_set_height (priv->slider, slider_height);
    clutter_actor_set_y (priv->slider, scroll_area_height - slider_height);
}

/********************************************************
 * Function : mildenhall_scroller_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_set_property (GObject *object, guint propertyId, const GValue  *value, GParamSpec *pspec)
{
    MildenhallScroller *self = MILDENHALL_SCROLLER (object);

    switch(propertyId)
	{
	    case PROP_MILDENHALL_SCROLLER_DISPLAYED_ITEMS:
            set_scroller_displayed_items(self, g_value_get_int(value));
            break;

        case PROP_MILDENHALL_SCROLLER_WIDTH:
            set_scroller_width(self, g_value_get_float(value));
            break;

        case PROP_MILDENHALL_SCROLLER_HEIGHT:
            set_scroller_height(self, g_value_get_float(value));
            break;

        case PROP_MILDENHALL_SCROLLER_TOTAL_ITEMS:
            set_scroller_total_items(self, g_value_get_int(value));
            break;

        case PROP_MILDENHALL_SCROLLER_CURRENT_ITEM:
            set_scroller_current_item(self, g_value_get_int(value));
            break;

        case PROP_MILDENHALL_SCROLLER_TYPE:
            self->priv->type = g_value_get_int(value);
            break;

        case PROP_MILDENHALL_SCROLLER_RESET_SLIDER:
        	set_scroller_reset_slider(self, g_value_get_boolean(value));
        	break;

        /* IN case the property is not installed for
                * this object, throw an appropriate error */
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
            break;
	}
}

/********************************************************
 * Function : mildenhall_scroller_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_get_property (GObject *object, guint propertyId, GValue  *value, GParamSpec *pspec)
{
    MildenhallScrollerPrivate *priv = MILDENHALL_SCROLLER (object)->priv;

    /* Based on the property ID take the appropriate property value) */
	switch(propertyId)
	{

		case PROP_MILDENHALL_SCROLLER_DISPLAYED_ITEMS:
			g_value_set_int (value, priv->displayed_items);
			break;

        case PROP_MILDENHALL_SCROLLER_TOTAL_ITEMS:
            g_value_set_int (value, priv->total_items);
            break;

        case PROP_MILDENHALL_SCROLLER_CURRENT_ITEM:
            g_value_set_int (value, priv->current_item);
            break;

        case PROP_MILDENHALL_SCROLLER_WIDTH:
            g_value_set_float(value, priv->width);
            break;

        case PROP_MILDENHALL_SCROLLER_HEIGHT:
            g_value_set_float(value, priv->height);
            break;

			/* IN case the property is not installed for
			 * this object, throw an appropriate error */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, propertyId, pspec);
			break;
	}
}

/********************************************************
 * Function : mildenhall_scroller_vieport_y_origin_notify
 * Description: function to notify the y viewport
 * Parameters: ClutterActor,GParamSpec,scroller ref object
 * Return value: gboolean
 ********************************************************/
static gboolean mildenhall_scroller_vieport_y_origin_notify(ClutterActor *viewport,GParamSpec *args,MildenhallScroller *self)
{
    gfloat origin_y ;
    gfloat slider_y_pos = clutter_actor_get_y (CLUTTER_ACTOR (self->priv->slider));
    gint bound = clutter_actor_get_height (self->priv->background) / 5;

    mx_viewport_get_origin (MX_VIEWPORT (viewport), NULL, &origin_y, NULL);

    if(origin_y > bound)
    {
        g_signal_emit(self, mildenhall_scroller_signals[SIG_MILDENHALL_SCROLLER_SCROLLED_UP], 0, NULL);
        mx_viewport_set_origin (MX_VIEWPORT(viewport), 0, 0, 0);


        if((slider_y_pos - clutter_actor_get_height(self->priv->slider)) > 0)
        {
            clutter_actor_set_y(CLUTTER_ACTOR(self->priv->slider),slider_y_pos - clutter_actor_get_height(self->priv->slider));
        }
        else
        {
            clutter_actor_set_y(self->priv->slider , 0);
        }
    }
    else if (origin_y < -bound)
    {
        g_signal_emit(self, mildenhall_scroller_signals[SIG_MILDENHALL_SCROLLER_SCROLLED_DOWN], 0, NULL);
        mx_viewport_set_origin (MX_VIEWPORT(viewport), 0, 0, 0);

        if((slider_y_pos + clutter_actor_get_height(self->priv->slider)) < clutter_actor_get_height(self->priv->scrollbar))
        {
            clutter_actor_set_y(self->priv->slider,slider_y_pos + clutter_actor_get_height(self->priv->slider));
        }
        else
        {
            clutter_actor_set_y(self->priv->slider , clutter_actor_get_height(self->priv->scrollbar) -
                                                                    clutter_actor_get_height(self->priv->slider));               ;
        }
    }

    return TRUE;
}

/********************************************************
 * Function : mildenhall_scroller_fingerscroll_init
 * Description: function to construct fingerscroll for scroller and slider
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_fingerscroll_init (MildenhallScroller *self)
{
    gdouble upper_bound;
    gdouble lower_bound;
    MxAdjustment *vadjust;
    gint iterator;
    ClutterColor line_color = {0xff, 0xff, 0xff, 0xff};
    gint total_lines = clutter_actor_get_height (self->priv->background) / 10;

    self->priv->fingerscroll  =  mx_kinetic_scroll_view_new ();
    self->priv->viewport      =  mx_viewport_new ();

    clutter_actor_set_size(self->priv->viewport , clutter_actor_get_width(CLUTTER_ACTOR(self)) ,clutter_actor_get_height(CLUTTER_ACTOR(self)) );
    clutter_actor_set_size(self->priv->fingerscroll , clutter_actor_get_width(CLUTTER_ACTOR(self)) ,clutter_actor_get_height(CLUTTER_ACTOR(self)) );
    mx_kinetic_scroll_view_set_use_captured(MX_KINETIC_SCROLL_VIEW(self->priv->fingerscroll) , TRUE);

    //mx_bin_set_child(MX_BIN(self->priv->fingerscroll),self->priv->viewport);
    clutter_actor_add_child(self->priv->fingerscroll,self->priv->viewport);
    clutter_actor_add_child(CLUTTER_ACTOR(self),self->priv->fingerscroll);

    g_object_notify(G_OBJECT(self->priv->viewport), "y-origin");
    g_signal_connect(self->priv->viewport,"notify::y-origin",G_CALLBACK(mildenhall_scroller_vieport_y_origin_notify),self);
    g_object_set(G_OBJECT(self->priv->viewport), "sync-adjustments", FALSE, NULL);

    mx_scrollable_get_adjustments(MX_SCROLLABLE(self->priv->viewport), NULL, &(vadjust));
    mx_adjustment_get_values(vadjust, NULL, &lower_bound, &upper_bound, NULL, NULL, NULL);

    lower_bound -= 50;
    upper_bound += 50;
    g_object_set(G_OBJECT(vadjust), "lower", lower_bound, "upper", upper_bound, "step-increment", (gdouble)10, NULL);

    self->priv->lineGroup = clutter_actor_new();

    //self->priv->line = clutter_rectangle_new_with_color (&line_color);
    self->priv->line = clutter_actor_new();
    clutter_actor_set_background_color ( self->priv->line,&line_color);
    clutter_actor_set_size (self->priv->line , 22, 1);
    clutter_actor_set_position(self->priv->line, 15 , 2 );
    clutter_actor_add_child(CLUTTER_ACTOR(self->priv->lineGroup), self->priv->line);

    for(iterator = 1 ; iterator <= total_lines; iterator++)
	{
		ClutterActor *line = clutter_clone_new (self->priv->line);
		clutter_actor_set_position(line, 15 , 2 + iterator * 15);
		clutter_actor_add_child(CLUTTER_ACTOR(self->priv->lineGroup), line);
	}
      // mx_bin_set_child(MX_BIN(self->priv->viewport),self->priv->lineGroup);
	clutter_actor_add_child(CLUTTER_ACTOR(self),self->priv->lineGroup);

}

/********************************************************
 * Function : mildenhall_scroller_create_ui
 * Description: function to construct the scroller and slider
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_create_ui(MildenhallScroller *self , MILDENHALL_SCROLLER_TYPE scroller_type)
{
    ClutterColor slider_color = {0xff, 0xff, 0xff, 0xf0};
    ClutterColor scrollbar_color = {0, 0, 0, 0xff};
    if( scroller_type == MILDENHALL_SCROLLER_2L )
    {
        self->priv->background = thornbury_ui_texture_create_new(PKGTHEMEDIR "/head_scroller_2L_background.png",0,0,FALSE,FALSE);
    }
    else
    {
        self->priv->background = thornbury_ui_texture_create_new(PKGTHEMEDIR "/head_scroller_3L_background.png",58,112,FALSE,FALSE);
    }

   // self->priv->scrollbar  = clutter_rectangle_new_with_color (&scrollbar_color);
    self->priv->scrollbar = clutter_actor_new();
    clutter_actor_set_background_color ( self->priv->scrollbar,&scrollbar_color);
    clutter_actor_set_size(self->priv->scrollbar , 2 , clutter_actor_get_height(self->priv->background)-3);

    //self->priv->slider = clutter_rectangle_new_with_color (&slider_color);
    self->priv->slider = clutter_actor_new();
    clutter_actor_set_background_color ( self->priv->slider,&slider_color);
    clutter_actor_set_size(self->priv->slider , 3 , 15);
    clutter_actor_set_position(self->priv->scrollbar , 55 , 3);
    clutter_actor_set_position(self->priv->slider , 55 ,0);
    clutter_actor_set_opacity(self->priv->slider,75);

    clutter_actor_add_child( CLUTTER_ACTOR ( self ), self->priv->background );
    clutter_actor_add_child( CLUTTER_ACTOR ( self ), self->priv->scrollbar );
    clutter_actor_add_child( CLUTTER_ACTOR ( self ), self->priv->slider );
    mildenhall_scroller_fingerscroll_init( self );
}

/********************************************************
 * Function : mildenhall_scroller_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_init (MildenhallScroller *self)
{
    self->priv = mildenhall_scroller_get_instance_private (self);
}

/********************************************************
 * Function : mildenhall_scroller_constructed
 * Description: function to construct the scroller
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_constructed (GObject *object)
{
    MildenhallScroller *self = MILDENHALL_SCROLLER(object);
    mildenhall_scroller_create_ui(self,self->priv->type);
}

/********************************************************
 * Function : mildenhall_scroller_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_scroller_class_init (MildenhallScrollerClass *klass)
{

    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GParamSpec *pspec = NULL;

    object_class->constructed = mildenhall_scroller_constructed;
    object_class->dispose = mildenhall_scroller_dispose;
    object_class->finalize = mildenhall_scroller_finalize;
    object_class->set_property = mildenhall_scroller_set_property;
    object_class->get_property = mildenhall_scroller_get_property;


    /**
     * MildenhallScroller:current-item:
     *
     * this to set current-item for scroller
     */
    pspec = g_param_spec_int ("current-item",
                            "Current item",
                            "current item in focus",
                            -1, G_MAXINT, -1,
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS );

    g_object_class_install_property (object_class, PROP_MILDENHALL_SCROLLER_CURRENT_ITEM, pspec);


    /**
     * MildenhallScroller:type:
     *
     * this to set type of scroller to set
     */
    pspec = g_param_spec_int ("type",
                            "scroller type",
                            "scroller type",
                            -1, G_MAXINT, -1,
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_property (object_class, PROP_MILDENHALL_SCROLLER_TYPE, pspec);

    /**
     * MildenhallScroller:total-item:
     *
     * this to set total-item for scroller
     */
    pspec = g_param_spec_int ("total-items",
                            "Total items",
                            "total count of items",
                            -1, G_MAXINT, -1,
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

    g_object_class_install_property (object_class, PROP_MILDENHALL_SCROLLER_TOTAL_ITEMS, pspec);

    /**
     * MildenhallScroller:displayed-item:
     *
     * this to set displayed-item for scroller
     */
    pspec = g_param_spec_int ("displayed-items",
                            "Diaplyes items",
                            "count of items diplayed out of total",
                            -1, G_MAXINT, -1,
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

	g_object_class_install_property (object_class, PROP_MILDENHALL_SCROLLER_DISPLAYED_ITEMS, pspec);

    /**
     * MildenhallScroller:width:
     *
     * this to set width for scroller
     */
    pspec = g_param_spec_float ( "width",
                                 "width",
                                 "width",
                                  0.0, G_MAXFLOAT,
                                  0.0,
                                  G_PARAM_READWRITE);
    g_object_class_install_property ( object_class,PROP_MILDENHALL_SCROLLER_WIDTH, pspec );

    /**
     * MildenhallScroller:reset-slider:
     *
     * this to set slider of scroller to default
     */
    pspec = g_param_spec_boolean ( "reset-slider",
                                 "reset-slider",
                                 "reset-slider",
                                  TRUE,
                                  G_PARAM_READWRITE);
    g_object_class_install_property ( object_class,PROP_MILDENHALL_SCROLLER_RESET_SLIDER, pspec );

    /**
     * MildenhallScroller:height:
     *
     * this to set height for scroller
     */
    pspec = g_param_spec_float ( "height",
                                 "height",
                                 "height",
                                  0.0, G_MAXFLOAT,
                                  0.0,
                                  G_PARAM_READWRITE);
    g_object_class_install_property ( object_class,PROP_MILDENHALL_SCROLLER_HEIGHT, pspec );

    /**
     * MildenhallScroller::scrolled-up:
     *
     * ::scrolled-up is emitted when scroller is moved up
     *
     */
    mildenhall_scroller_signals[SIG_MILDENHALL_SCROLLER_SCROLLED_UP] = g_signal_new ("scrolled-up",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallScrollerClass, scroller_up_cb),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);

    /**
     * MildenhallScroller::scrolled-down:
     *
     * ::scrolled-down is emitted when scroller is moved down
     *
     */
    mildenhall_scroller_signals[SIG_MILDENHALL_SCROLLER_SCROLLED_DOWN] = g_signal_new ("scrolled-down",
			G_TYPE_FROM_CLASS (object_class),
			G_SIGNAL_NO_RECURSE |
			G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallScrollerClass, scroller_down_cb),
			NULL, NULL,
			g_cclosure_marshal_VOID__VOID,
			G_TYPE_NONE, 0);
}

 /**
 * mildenhall_scroller_new:
 * Returns: scroller object
 *
 * function to create a new scroller
 *
 */
ClutterActor *mildenhall_scroller_new (MILDENHALL_SCROLLER_TYPE scroller_type)
{
    return g_object_new (MILDENHALL_TYPE_SCROLLER, "type" , scroller_type , NULL);
}
