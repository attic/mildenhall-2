/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_meta_info_footer.c
 *
 * Created on: Jan 18, 2012
 *
 * mildenhall_meta_info_footer.c */


#include "mildenhall_meta_info_footer.h"

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define META_INFO_FOOTER_DEBUG(...)   //g_print( __VA_ARGS__)

/* property enums */
enum _enFooterProperty
{
        PROP_FIRST,
        PROP_WIDTH,
        PROP_HEIGHT,
        PROP_X,
        PROP_Y,
        PROP_SHOW_TOP,
        PROP_ICON_LEFT,
        PROP_RATINGS,
        PROP_MODEL,
        PROP_LAST
};

/* footer model columns */
enum
{
        COLUMN_LEFT_ICON,
        COLUMN_MID_TEXT,
        COLUMN_RIGHT_ICON_RATINGS,
        COLUMN_NONE
};


typedef struct
{
	gfloat flImageWidth;
	gfloat flImageHeight;
	gfloat flTopImageHeight;
	gfloat flImageY;
	gfloat flMidTextOffsetX;
	gfloat flTextY;
	gfloat flSepLineY;
	gfloat flSepLineX;
	gfloat flSepLineHeight;
	gfloat flIconX;

	gboolean bIconLeft;
	gboolean bRatings;
	ThornburyModel *pModel;
	gboolean bShowTop;

	ClutterColor lineColor;
	ClutterColor depthLineColor;
	ClutterColor fontColor;
	ClutterActor *pTopBar;
	ClutterActor *pMainGroup;
	ClutterActor *pLeftButton;
	ClutterActor *pLeftText;
	ClutterActor *pMiddleText;
	ClutterActor *pLeftSep;

	gchar *pSeamlessFilePath;
	gchar *pSeamlessTopFilePath;
	gchar *pShadowTopFilePath;
	gchar *pFontName;
} MildenhallMetaInfoFooterPrivate;

struct _MildenhallMetaInfoFooter
{
  ClutterActor parent;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallMetaInfoFooter, mildenhall_meta_info_footer, CLUTTER_TYPE_ACTOR)

/***************************************************************************************************************************
 * Internal Functions
 ***************************************************************************************************************************/
static ClutterActor *meta_info_footer_create_line(gfloat x, gfloat y, gfloat width, gfloat height, ClutterColor color);
static ClutterActor *meta_info_footer_create_texture(const gchar *pImagePath, gfloat width, gfloat height, gfloat x, gfloat y);
static void meta_info_footer_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallMetaInfoFooter *pFooter);
static void meta_info_footer_update_view (MildenhallMetaInfoFooter *pFooter);

/**
 * mildenhall_meta_info_footer_get_enable_top:
 * @self: a #MildenhallMetaInfoFooter
 *
 * Return the #MildenhallMetaInfoFooter:show-top property
 *
 * Returns: the value of #MildenhallMetaInfoFooter:show-top property
 */
gboolean
mildenhall_meta_info_footer_get_show_top (MildenhallMetaInfoFooter *self)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self), FALSE);

   return priv->bShowTop;
}

/**
 * mildenhall_meta_info_footer_get_show_left_icon:
 * @self: a #MildenhallMetaInfoFooter
 *
 * Return the #MildenhallMetaInfoFooter:show-left-icon property
 *
 * Returns: the value of #MildenhallMetaInfoFooter:show-left-icon property
 */
gboolean
mildenhall_meta_info_footer_get_show_left_icon (MildenhallMetaInfoFooter *self)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);
  g_return_val_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self), FALSE);

  return priv->bIconLeft;
}

/**
 * mildenhall_metainfo_footer_get_show_ratings:
 * @self: a #MildenhallMetaInfoFooter
 *
 * Return the #MildenhallMetaInfoFooter:show-ratings property
 *
 * Returns: the value of #MildenhallMetaInfoFooter:show-ratings property
 */
gboolean
mildenhall_meta_info_footer_get_show_ratings (MildenhallMetaInfoFooter *self)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self), FALSE);

  return priv->bRatings;
}

/**
 * mildenhall_meta_info_footer_get_model:
 * @self: a #MildenhallMetaInfoFooter
 *
 * Return the #MildenhallMetaInfoFooter:model property
 *
 * Returns: (transfer none) (nullable): the value of #MildenhallMetaInfoFooter:model property
 **/
ThornburyModel *
mildenhall_meta_info_footer_get_model (MildenhallMetaInfoFooter *self)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_val_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self), NULL);

  return priv->pModel;
}

/**
 * mildenhall_meta_info_footer_set_show_top:
 * @self: a #MildenhallMetaInfoFooter
 * @enable: the new value
 *
 * Update #MildenhallMetaInfoFooter:show-top property
 */
void
mildenhall_meta_info_footer_set_show_top (MildenhallMetaInfoFooter *self, gboolean enable)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self));

  if (priv->pTopBar != NULL)
    {
      enable ?
        clutter_actor_show (priv->pTopBar) :
        clutter_actor_hide (priv->pTopBar);
    }

  priv->bShowTop = enable;
  g_object_notify (G_OBJECT (self), "show-top");
}

/**
 * mildenhall_meta_info_footer_set_show_left_icon:
 * @self: a #MildenhallMetaInfoFooter
 * @enable: the new value
 *
 * Update #MildenhallMetaInfoFooter:show-left-icon property
 */
void
mildenhall_meta_info_footer_set_show_left_icon (MildenhallMetaInfoFooter *self, gboolean enable)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self));

  /* FIXME: TBD update view */

  priv->bIconLeft = enable;
  g_object_notify (G_OBJECT (self), "show-left-icon");
}

/**
 * mildenhall_meta_info_footer_set_show_ratings:
 * @self: a #MildenhallMetaInfoFooter
 * @enable: the new value
 *
 * Update #MildenhallMetaInfoFooter:show-left-icon property
 */
void
mildenhall_meta_info_footer_set_show_ratings (MildenhallMetaInfoFooter *self, gboolean enable)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self));

  /* FIXME: TBD update view */

  priv->bRatings = enable;
  g_object_notify (G_OBJECT (self), "show-ratings");
}

/**
 * mildenhall_meta_info_footer_set_model:
 * @self: a #MildenhallMetaInfoFooter
 * @model: (transfer none): the new model
 *
 * Update #MildenhallMetaInfoFooter:model property
 *
 **/
void
mildenhall_meta_info_footer_set_model (MildenhallMetaInfoFooter *self, ThornburyModel *model)
{
  MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

  g_return_if_fail (MILDENHALL_IS_META_INFO_FOOTER (self));
  /* FIXME: ThurnburyModel is actually ClutterListModel which is deprecated. T808 */
  g_return_if_fail (THORNBURY_IS_MODEL (model) || CLUTTER_IS_LIST_MODEL (model));

  if (priv->pModel != NULL)
    {
      g_signal_handlers_disconnect_by_func (priv->pModel,
        G_CALLBACK (meta_info_footer_row_changed_cb),
        self);
    }

  g_clear_object (&priv->pModel);

  priv->pModel = g_object_ref (model);

  g_signal_connect (priv->pModel,
                    "row-changed",
                    G_CALLBACK (meta_info_footer_row_changed_cb),
                    self);

  /* FIXME: TBD update view */
  meta_info_footer_update_view (self);

  g_object_notify (G_OBJECT (self), "model");
}

/**
 * mildenhall_meta_info_footer_new:
 *
 * Returns: (transfer floating): a new #MildenhallMetaInfoFooter
 */
MildenhallMetaInfoFooter *
mildenhall_meta_info_footer_new (void)
{
  return g_object_new (MILDENHALL_TYPE_META_INFO_FOOTER, NULL);
}

/********************************************************
 * Function : meta_info_footer_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void meta_info_footer_get_property (GObject    *pObject, guint       uinPropertyID,  GValue     *pValue,                GParamSpec *pspec)
{
        MildenhallMetaInfoFooter *pFooter = MILDENHALL_META_INFO_FOOTER (pObject);

	switch (uinPropertyID)
	{
		case PROP_WIDTH:
			g_value_set_float (pValue, clutter_actor_get_height (CLUTTER_ACTOR (pFooter)));
                        break;
                case PROP_HEIGHT:
			g_value_set_float (pValue, clutter_actor_get_width (CLUTTER_ACTOR (pFooter)));
                        break;
		case PROP_X:
			g_value_set_float (pValue, clutter_actor_get_x (CLUTTER_ACTOR (pFooter)));
                        break;
                case PROP_Y:
			g_value_set_float (pValue, clutter_actor_get_y (CLUTTER_ACTOR (pFooter)));
                        break;
		case  PROP_SHOW_TOP:
                        g_value_set_boolean (pValue, mildenhall_meta_info_footer_get_show_top (pFooter));
			break;
		case PROP_ICON_LEFT:
                        g_value_set_boolean (pValue,  mildenhall_meta_info_footer_get_show_left_icon (pFooter));
                        break;
                case PROP_RATINGS:
                        g_value_set_boolean (pValue,  mildenhall_meta_info_footer_get_show_ratings (pFooter));
                        break;
		case PROP_MODEL:
			g_value_set_object (pValue,  mildenhall_meta_info_footer_get_model (pFooter));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}


/********************************************************
 * Function : meta_info_footer_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void meta_info_footer_set_property (GObject      *pObject,
                               guint         uinPropertyID,
                               const GValue *pValue,
                               GParamSpec   *pspec)
{
        MildenhallMetaInfoFooter *pFooter = MILDENHALL_META_INFO_FOOTER (pObject);

	switch (uinPropertyID)
	{
		case PROP_X:
			clutter_actor_set_x (CLUTTER_ACTOR (pFooter), g_value_get_float (pValue));
                        break;
                case PROP_Y:
			clutter_actor_set_y (CLUTTER_ACTOR (pFooter), g_value_get_float (pValue));
                        break;
		case  PROP_SHOW_TOP:
			mildenhall_meta_info_footer_set_show_top (pFooter, g_value_get_boolean (pValue));
			break;
	        case PROP_ICON_LEFT:
			mildenhall_meta_info_footer_set_show_left_icon (pFooter, g_value_get_boolean (pValue));
			break;
	        case PROP_RATINGS:
			mildenhall_meta_info_footer_set_show_ratings (pFooter, g_value_get_boolean (pValue));
			break;
 		case PROP_MODEL:
			mildenhall_meta_info_footer_set_model (pFooter, g_value_get_object (pValue));
			break;
		case PROP_WIDTH:
		case PROP_HEIGHT:
			/* Read Only */
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
	}
}

/********************************************************
 * Function : meta_info_footer_dispose
 * Description: Dispose the meta info footer object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void meta_info_footer_dispose (GObject *pObject)
{
	MildenhallMetaInfoFooter *pFooter = MILDENHALL_META_INFO_FOOTER (pObject);
	MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

	g_clear_pointer (&priv->pSeamlessFilePath, g_free);
	g_clear_pointer (&priv->pSeamlessTopFilePath, g_free);
	g_clear_pointer (&priv->pShadowTopFilePath, g_free);
	g_clear_pointer (&priv->pFontName, g_free);

	g_clear_object (&priv->pModel);

	G_OBJECT_CLASS (mildenhall_meta_info_footer_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : meta_info_footer_finalize
 * Description: Finalize the meta info heade object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void meta_info_footer_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_meta_info_footer_parent_class)->finalize (object);
}

/********************************************************
 * Function : meta_info_footer_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void
mildenhall_meta_info_footer_class_init (MildenhallMetaInfoFooterClass *klass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;

	pObjectClass->get_property = meta_info_footer_get_property;
	pObjectClass->set_property = meta_info_footer_set_property;
	pObjectClass->dispose = meta_info_footer_dispose;
	pObjectClass->finalize = meta_info_footer_finalize;

        /**
         * MildenhallMetaInfoFooter:x:
         *
         * x coord of the footer
         */
        pspec = g_param_spec_float ("x",
                        "X",
                        "x position of the footer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_X, pspec);

        /**
         * MildenhallMetaInfoFooter:y:
         *
         * y coord of the footer
         */
        pspec = g_param_spec_float ("y",
                        "Y",
                        "y position of the footer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READWRITE);
        g_object_class_install_property ( pObjectClass, PROP_Y, pspec);

        /**
         * MildenhallMetaInfoFooter :width:
         *
         * Width of the footer
         */
        pspec = g_param_spec_float ("width",
                        "Width",
                        "Width of the footer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property (pObjectClass, PROP_WIDTH, pspec);

        /**
         * MildenhallMetaInfoFooter:height:
         *
         * Height of the footer
         */
        pspec = g_param_spec_float ("height",
                        "Height",
                        "Height of the footer",
                        0.0, G_MAXFLOAT,
                        0.0,
                        G_PARAM_READABLE);
        g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pspec);

        /**
         * MildenhallMetaInfoFooter:show-top:
         *
         * Set this property to FALSE to hide footer top
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("show-top",
                        "Show-top",
                        "Whether the top of footer should be shown",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_SHOW_TOP, pspec);

        /**
         * MildenhallMetaInfoFooter:show-left-icon:
         *
         * Set this property to FALSE to set text at left position
         *
         * Default: TRUE
         */
        pspec = g_param_spec_boolean ("show-left-icon",
                        "Left-Icon",
                        "Whether the icon or text on left side of footer",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_ICON_LEFT, pspec);

        /**
         * MildenhallMetaInfoFooter:show-ratings:
         *
         * Set this property to TRUE to pass ratings values in model to show ratings stars
         * otherwise FALSE to pas images in model to set at right position
         *
         * Default: FALSE
         */
        pspec = g_param_spec_boolean ("show-ratings",
                        "Ratings",
                        "Whether the rating star or icons on right side of footer",
                        TRUE,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_RATINGS, pspec);

	/**
         * MildenhalMetaInfoFooter:model:
         *
         * model for the meta-info footer.
         * 1. left-icon
	 * 2. middle-text
	 * 3. ratings-value as GVariant
         *
         * Default: NULL
         */
        pspec = g_param_spec_object ("model",
                        "Model",
                        "model having footer data",
                        G_TYPE_OBJECT,
                        G_PARAM_READWRITE);
        g_object_class_install_property (pObjectClass, PROP_MODEL, pspec);

}

/********************************************************
 * Function : meta_info_footer_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void meta_info_footer_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallMetaInfoFooter *pFooter = pUserData;
        MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

        gchar *pStyleKey = NULL;

	g_return_if_fail (MILDENHALL_IS_META_INFO_FOOTER (pUserData));

		pStyleKey = g_strdup(pKey);

        if(g_strcmp0(pStyleKey, "seamless-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
                	priv->pSeamlessFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                	META_INFO_FOOTER_DEBUG("seamless-image = %s\n", priv->pSeamlessFilePath);
		}
        }
	else if(g_strcmp0(pStyleKey, "seamless-top-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
                	priv->pSeamlessTopFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                	META_INFO_FOOTER_DEBUG("seamless-top-image = %s\n", priv->pSeamlessTopFilePath);
		}
        }
	else if(g_strcmp0(pStyleKey, "shadow-top-image") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pShadowTopFilePath = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        META_INFO_FOOTER_DEBUG("shadow-top-image = %s\n", priv->pShadowTopFilePath);
                }
        }
	else if(g_strcmp0(pStyleKey, "mid-text-x-offset") == 0)
        {
                priv->flMidTextOffsetX = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("mid-text-x-offset = %f\n", priv->flMidTextOffsetX);
        }
	else if(g_strcmp0(pStyleKey, "icon-x") == 0)
        {
                priv->flIconX = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("icon-x = %f\n", priv->flIconX);
        }
        else if(g_strcmp0(pStyleKey, "text-y") == 0)
        {
                priv->flTextY = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("text-y = %f\n", priv->flTextY);
        }
	else if(g_strcmp0(pStyleKey, "line-color") == 0)
	{
		clutter_color_from_string(&priv->lineColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "depth-line-color")== 0)
	{
		clutter_color_from_string(&priv->depthLineColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, "image-width") == 0)
	{
		priv->flImageWidth = g_value_get_double(pValue);
		META_INFO_FOOTER_DEBUG("image-width = %f\n", priv->flImageWidth);
	}
	else if(g_strcmp0(pStyleKey, "image-height") == 0)
	{
		priv->flImageHeight = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("image-height = %f\n", priv->flImageHeight);
	}
	else if(g_strcmp0(pStyleKey, "font-color") == 0)
        {
		clutter_color_from_string(&priv->fontColor,  g_value_get_string(pValue));
        }
	else if(g_strcmp0(pStyleKey, "font-name") == 0)
        {
                g_free (priv->pFontName);
                priv->pFontName = g_value_dup_string (pValue);
                META_INFO_FOOTER_DEBUG("font-name = %s\n", priv->pFontName);
        }
	else if(g_strcmp0(pStyleKey, "top-image-height") == 0)
        {
                priv->flTopImageHeight = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("top-image-height = %f\n", priv->flTopImageHeight);
        }
	else if(g_strcmp0(pStyleKey, "separator-line-height") == 0)
        {
                priv->flSepLineHeight = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("separator-line-height = %f\n", priv->flSepLineHeight);
        }
        else if(g_strcmp0(pStyleKey, "separator-line-x") == 0)
        {
               priv->flSepLineX = g_value_get_double(pValue);
               META_INFO_FOOTER_DEBUG("separator-line-x = %f\n", priv->flSepLineX);
        }
	else if(g_strcmp0(pStyleKey, "separator-line-y") == 0)
        {
                priv->flSepLineY = g_value_get_double(pValue);
                META_INFO_FOOTER_DEBUG("separator-line-y = %f\n", priv->flSepLineY);
        }

	g_free (pStyleKey);
}

/****************************************************
 * Function : meta_info_footer_update_mid_text
 * Description: creates middle text
 * Parameters: MetaInfoFooter*, GValue*
 * Return value: None
 *******************************************************/
static void
meta_info_footer_update_mid_text (MildenhallMetaInfoFooter *pFooter, GValue *value)
{
        MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

        const gchar *pMidStr = g_value_get_string(value);

	if(NULL == priv->pMiddleText)
        {
                priv->pMiddleText = clutter_text_new();
                g_object_set(priv->pMiddleText, "color", &priv->fontColor, NULL);
		clutter_text_set_font_name (CLUTTER_TEXT (priv->pMiddleText), priv->pFontName);
		clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pMiddleText), PANGO_ELLIPSIZE_END);
		clutter_actor_set_y(priv->pMiddleText, priv->flTextY);

                clutter_actor_add_child(priv->pMainGroup, priv->pMiddleText);
	}
	/* default text if not provided by the user */
        if(NULL != pMidStr)
        {
                META_INFO_FOOTER_DEBUG("%s\n", pMidStr);
                g_object_set(priv->pMiddleText, "text", pMidStr, NULL);
        }
	if(priv->bIconLeft)
	{
		clutter_actor_set_x(priv->pMiddleText, clutter_actor_get_x(priv->pLeftSep) + priv->flMidTextOffsetX/* 12 */ ) ;
                clutter_actor_set_width(priv->pMiddleText, priv->flImageWidth - (clutter_actor_get_x(priv->pLeftSep) + (priv->flMidTextOffsetX * 2) ) );
	}
	else
	{
		META_INFO_FOOTER_DEBUG("No left icon\n");
		clutter_actor_set_x(priv->pMiddleText, (priv->flImageWidth - clutter_actor_get_width(priv->pMiddleText) ) / 2);
		if(clutter_actor_get_width(priv->pMiddleText)  > priv->flImageWidth - (priv->flMidTextOffsetX * 2) )
			clutter_actor_set_width(priv->pMiddleText, priv->flImageWidth - (priv->flMidTextOffsetX * 2) );
	}

}

/****************************************************
 * Function : meta_info_footer_update_icon_left
 * Description: creates left icon
 * Parameters: MetaInfoFooter*, GValue*
 * Return value: None
 *******************************************************/
static void
meta_info_footer_update_icon_left (MildenhallMetaInfoFooter *pFooter, GValue *value)
{
        MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

        const gchar *pLeftIcon =  g_value_get_string(value);
        if(priv->bIconLeft && pLeftIcon)
        {
                if(NULL == priv->pLeftButton)
                {
                        priv->pLeftButton = meta_info_footer_create_texture(pLeftIcon, 0, 0, priv->flIconX, priv->flTextY);
                        if(priv->pLeftButton)
                                clutter_actor_add_child(priv->pMainGroup, priv->pLeftButton);
                }
                else
                        thornbury_ui_texture_set_from_file(priv->pLeftButton, pLeftIcon, 0, 0, FALSE, TRUE);

        }
        if(NULL == priv->pLeftSep)
        {
                /* create separator line */
                priv->pLeftSep = meta_info_footer_create_line(priv->flSepLineX, priv->flSepLineY, 1, priv->flSepLineHeight, priv->lineColor);
                clutter_actor_add_child(priv->pMainGroup, priv->pLeftSep);
        }
}

/****************************************************
 * Function : meta_info_footer_update_view
 * Description: update the footer
 * Parameters: MetaInfoFooter*
 * Return value: None
 *******************************************************/
static void
meta_info_footer_update_view (MildenhallMetaInfoFooter *pFooter)
{
	MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

	if (G_IS_OBJECT(priv->pModel) && thornbury_model_get_n_rows (priv->pModel) > 0)
        {
                ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pModel, 0 );
                if(NULL != pIter)
                {
                        GValue value = { 0, };

                        /* get the first/second column value for left icon/text */
                        thornbury_model_iter_get_value(pIter, COLUMN_LEFT_ICON, &value);
                        if(priv->bIconLeft)
                        {
                                meta_info_footer_update_icon_left(pFooter, &value);
                        }
                        g_value_unset (&value);

                        /* get the column value to get middle text */
                        thornbury_model_iter_get_value(pIter, COLUMN_MID_TEXT, &value);
                        meta_info_footer_update_mid_text(pFooter, &value);
                        g_value_unset (&value);

			/* TBD: ratings star */
                }

        }

}

/****************************************************
 * Function : meta_info_footer_create_texture
 * Description: creates a texture actor
 * Parameters: gchar*, gfloat, gfloat, gfloat, gfloat
 * Return value: ClutterActor*
 *******************************************************/
static ClutterActor *meta_info_footer_create_texture(const gchar *pImagePath, gfloat width, gfloat height, gfloat x, gfloat y)
{
	ClutterActor *pTexture = thornbury_ui_texture_create_new(pImagePath, width, height, FALSE, TRUE);

	if(pTexture)
		clutter_actor_set_position(pTexture, x, y);

	return pTexture;
}

/****************************************************
 * Function : meta_info_footer_create_line
 * Description: creates a line actor
 * Parameters: gfloat, gfloat, gfloat, gfloat, ClutterColor
 * Return value: ClutterActor*
 *******************************************************/
static ClutterActor *meta_info_footer_create_line(gfloat x, gfloat y, gfloat width, gfloat height, ClutterColor color)
{
	ClutterActor *pLine = NULL;

        /*Create a new line actor of given width and height*/
        pLine = clutter_actor_new();
	if(pLine)
	{
		clutter_actor_set_background_color(pLine, &color);
        	clutter_actor_set_size(pLine, width, height);
		clutter_actor_set_position(pLine, x, y);
	}

	return pLine;
}

/****************************************************
 * Function : create_meta_bottombar
 * Description: Create the Bottom bar in the meta actor
 *              of the metainforoller object
 * Parameters: none
 * Return value: The Meta Bottom Bar actor
 *******************************************************/
static ClutterActor *
mildenhall_meta_info_create_topbar (MildenhallMetaInfoFooter *pFooter)
{
        MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (pFooter);

	ClutterActor *pTexture = NULL;
	ClutterActor *pLine = NULL;

        /*Create the meta bottom bar group, set a seamless and all lines and the arrow*/
        ClutterActor *pTopBar = clutter_actor_new();

        pTexture = meta_info_footer_create_texture(priv->pSeamlessTopFilePath, priv->flImageWidth, priv->flTopImageHeight, 0, 0);
	clutter_actor_add_child(pTopBar, pTexture);
        /* add top line */
	pLine = meta_info_footer_create_line(0, 0.0,  priv->flImageWidth, 1, priv->lineColor);
	clutter_actor_add_child(pTopBar, pLine);
	/* add one more top line to match the drawer color */
	pLine = meta_info_footer_create_line(0, 0.0,  priv->flImageWidth, 1, priv->lineColor);
	clutter_actor_add_child(pTopBar, pLine);

        return pTopBar;
}

/********************************************************
 * Function : meta_info_footer_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void
mildenhall_meta_info_footer_init (MildenhallMetaInfoFooter *self)
{
	MildenhallMetaInfoFooterPrivate *priv = mildenhall_meta_info_footer_get_instance_private (self);

	/* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set(PKGDATADIR"/mildenhall_meta_info_footer_style.json");

	ClutterActor *pSeamlessTexture;
	ClutterActor *pTopLine;

	priv->bShowTop = TRUE;
	priv->bIconLeft = TRUE;

        /* pares the hash for styles */
        if(NULL != styleHash)
        {
                GHashTableIter iter;
                gpointer key, value;

                g_hash_table_iter_init(&iter, styleHash);
                /* iter per layer */
                while (g_hash_table_iter_next (&iter, &key, &value))
                {
                        GHashTable *pHash = value;
                        if(NULL != pHash)
                        {
                                g_hash_table_foreach(pHash, meta_info_footer_parse_style, self);
                        }
                }
                /* free the style hash */
                thornbury_style_free(styleHash);
        }
	/* create bottom group */
	priv->pMainGroup = clutter_actor_new();
	clutter_actor_add_child(CLUTTER_ACTOR(self), priv->pMainGroup);

	/* create top bar */
	priv->pTopBar = mildenhall_meta_info_create_topbar(self);
	clutter_actor_add_child(CLUTTER_ACTOR(self), priv->pTopBar);

	pSeamlessTexture = meta_info_footer_create_texture (priv->pSeamlessFilePath, priv->flImageWidth, priv->flImageHeight, 0, 1);
	pTopLine = meta_info_footer_create_line (0, 0, priv->flImageWidth, 1, priv->depthLineColor);

	clutter_actor_add_child(CLUTTER_ACTOR(priv->pMainGroup), pTopLine);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->pMainGroup), pSeamlessTexture);

	clutter_actor_set_y(CLUTTER_ACTOR(priv->pMainGroup),  clutter_actor_get_height(priv->pTopBar));

	META_INFO_FOOTER_DEBUG("%f %f\n", clutter_actor_get_width(priv->pMainGroup), clutter_actor_get_height(priv->pMainGroup));

	clutter_actor_set_width (CLUTTER_ACTOR (self),
		clutter_actor_get_width (priv->pMainGroup));

	clutter_actor_set_height (CLUTTER_ACTOR (self),
		clutter_actor_get_height (priv->pMainGroup) +
		clutter_actor_get_height (priv->pTopBar));

	META_INFO_FOOTER_DEBUG("%f %f\n", clutter_actor_get_width(CLUTTER_ACTOR(self) ), clutter_actor_get_height(CLUTTER_ACTOR(self)) );
}

/********************************************************
 * Function : meta_info_footer_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MetaInfoFooter *
 * Return value: void
 ********************************************************/
static void
meta_info_footer_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallMetaInfoFooter *pFooter)
{
 	/* update the view */
}


