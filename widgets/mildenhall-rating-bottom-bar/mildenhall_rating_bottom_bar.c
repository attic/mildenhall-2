/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *	@Filename :mildenhall-rating-bottom-bar.c
 *	@Project: --
 *-----------------------------------------------------------------------------
 * 	@Created on : Mar 11, 2013
 *------------------------------------------------------------------------------
 *  @Description : This widget can be used as bottom bar with fields including
 *  				Left-Top-Field 		: Ratings-stars / Text
 *  				Left-Bottom-Field 	: Text ( Font and Color can be coutomised )
 *  				Right-field 		: Text ( Font and Color can be coutomised )
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description													Date			Name
 *	----------													----			----
 *
 *******************************************************************************/

#include "mildenhall_rating_bottom_bar.h"

/***********************************************************
 * @local function declarations
 **********************************************************/
/* Internal Function declaration */
static ClutterActor* p_rating_bottom_bar_create_text(MildenhallRatingBottomBar *pRatingBottomBar ,   gchar *pTextColor , gchar *pFont  );
static void v_rating_bottom_bar_update_left_top_text (MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText) ;
static void v_rating_bottom_bar_addratingstar(MildenhallRatingBottomBar *pRatingBottomBar,  gint inPos ,gchar *pIconPath );
static void v_rating_bottom_bar_update_ratings(MildenhallRatingBottomBar *pRatingBottomBar , gint inRating);
static void v_rating_bottom_bar_update_left_bottom_text(MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText);
static void v_rating_bottom_bar_update_right_text(MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText);
static void v_rating_bottom_bar_extract_model_info(MildenhallRatingBottomBar *pRatingBottomBar);
static void v_rating_bottom_bar_swap_view(MildenhallRatingBottomBar *pRatingBottomBar  , gboolean bSwap);

/* CALL BACK FUNCTIONS */
static void v_rating_bottom_bar_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar);
static void v_rating_bottom_bar_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar);
static void v_rating_bottom_bar_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar);




/******************************************************************************
 * @Defines
 *****************************************************************************/


/**
 * _enRatingBottomBarProperty:
 * property enums of Rating Bottom Bar
 */
typedef enum _enRatingBottomBarProperty enRatingBottomBarProperty;
enum _enRatingBottomBarProperty
{
	/*< private >*/
	PROP_RATING_BOTTOM_BAR_FIRST,
	PROP_RATING_BOTTOM_BAR_MODEL,
	PROP_RATING_BOTTOM_BAR_WIDTH,
	PROP_RATING_BOTTOM_BAR_HEIGHT,
	PROP_RATING_BOTTOM_BAR_BACKGROUND_COLOR,
	PROP_RATING_BOTTOM_BAR_RATING_OR_TEXT,
	PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_FONT,
	PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_COLOR,
	PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_FONT,
	PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_COLOR,
	PROP_RATING_BOTTOM_BAR_SWAP_VIEW,
	PROP_RATING_BOTTOM_BAR_LAST
};


/**
 * _enRatingBottomBarModelInfo
 * @RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS 					:ratings ranging from 0 to 10
 * @RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT			:Text to be displayed below ratings
 * @RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT				:Text to be displayed to right of the ratings
 * @RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT				:In case text to be displayed in place of ratings
 *
 * Model format for displaying fileds in Rating Bottom bar
 */


typedef enum _enRatingBottomBarModelInfo enRatingBottomBarModelInfo;
enum _enRatingBottomBarModelInfo
{
	RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS,
	RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT,
	RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT,
	RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT,
	RATING_BOTTOM_BAR_MODEL_COLUMN_LAST
};


#define MILDENHALL_RATING_BOTTOM_BAR_EMPTY_RATING_ICON		"empty-rating-star"
#define MILDENHALL_RATING_BOTTOM_BAR_HALF_RATING_ICON		"half-rating-star"
#define MILDENHALL_RATING_BOTTOM_BAR_FULL_RATING_ICON		"full-rating-star"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_X		"rating-star-x"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_Y		"rating-star-y"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_Y			"rating-text-y"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_X			"rating-text-x"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_FONT		"rating-text-font"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_COLOR 	"rating-text-color"
#define MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_X		"left-bottom-text-x"
#define MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_Y		"left-bottom-text-y"
#define MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH		"left-bottom-text-max-width"
#define MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_Y					"right-text-y"
#define MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_DISPLACEMENT		"right-text-displacement"
#define MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_SPACING			"rating-star-space"
#define MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_FONT		"right-text-default-font"
#define MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_COLOR		"right-text-default-color"
#define MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_DEFAULT_COLOR		"left-bottom-default-color"
#define MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_DEFAULT_FONT		"left-bottom-default-font"
#define MILDENHALL_RATING_BOTTOM_BAR_DEFAULT_BACKGROUND			 "default-background"

#define MAX_NO_OF_STARS		5






#define MILDENHALL_RATING_BOTTOM_BAR_PRINT( a ...)			 /* g_print(a); */


#define MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pointer) 		if(NULL != pointer) \
		{ \
	g_free(pointer); \
	pointer = NULL; \
		}

struct _MildenhallRatingBottomBarPrivate
{
	ThornburyModel *pModel ;
	ClutterActor *pStarGroup;
	ClutterActor *pRatingStar[MAX_NO_OF_STARS];
	ClutterActor *pLeftTopText;
	ClutterActor *pLeftBottomText;
	ClutterActor *pRightText;

	ClutterColor pLeftTopTextColor;
	ClutterColor pRightTextColor;
	ClutterColor pLeftBottomColor;
	ClutterColor BackgroundColor;
	GHashTable *pPrivHash;
	gboolean bIsRating;

	gfloat fltStarSpacing;

	gchar *pRightTextFont;
	gchar *pLeftBottomFont;
	gfloat fltHeight;
	gfloat fltWidth;
};

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallRatingBottomBar, mildenhall_rating_bottom_bar, CLUTTER_TYPE_ACTOR)




/********************************************************
 * Function :v_rating_bottom_bar_set_width
 * Description: set rating bottom bar  width internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_set_width(MildenhallRatingBottomBar *pRatingBottomBar, gfloat fltWidth)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RATING_BOTTOM_BAR(pRatingBottomBar))
	{
		g_warning("invalid rating bottom bar object\n");
		return ;
	}

	if(priv->fltWidth != fltWidth)
	{
		clutter_actor_set_width(CLUTTER_ACTOR(pRatingBottomBar), fltWidth);
		priv->fltWidth = fltWidth;
	}
}

/********************************************************
 * Function :v_rating_bottom_bar_set_height
 * Description: set rating bottom bar  height internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_set_height(MildenhallRatingBottomBar *pRatingBottomBar, gfloat fltHeight)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RATING_BOTTOM_BAR(pRatingBottomBar))
	{
		g_warning("invalid rating bottom bar object\n");
		return ;
	}

	if(priv->fltHeight != fltHeight)
	{
		clutter_actor_set_height(CLUTTER_ACTOR(pRatingBottomBar), fltHeight);
		priv->fltHeight = fltHeight;
	}
}





/**
 * v_mildenhall_rating_bottom_bar_set_model:
 * @pObject : Rating Bottom Bar pObject reference
 * @pModel : model for the Rating Bottom Bar
 *
 * Model contains the data to be displayed on the Rating Bottom Bar.
 * RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS 					:ratings ranging from 0 to 10
 * 															0 -> all empty stars
 * 															if ratings end with odd number, one half star is added
 * 															10 -> all stars are filled
 * RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT			:Text to be displayed below ratings
 * 															( Font and color can be customized via properties )
 *
 * RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT					:Text to be displayed to right of the ratings
 *															( Font and color can be customized via properties )
 *
 * RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT				:In case text to be displayed in place of ratings
 * 															(field 1 has ratings needs to be set TRUE)
 */
void v_mildenhall_rating_bottom_bar_set_model(MildenhallRatingBottomBar *pRatingBottomBar, ThornburyModel *pModel)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_RATING_BOTTOM_BAR(pRatingBottomBar))
	{
		g_warning("invalid Rating Bottom bar pObject\n");
		return ;
	}

	if(NULL != priv->pModel)
	{
		g_signal_handlers_disconnect_by_func (priv->pModel,
				G_CALLBACK (v_rating_bottom_bar_row_added_cb),
				pRatingBottomBar);
		g_signal_handlers_disconnect_by_func (priv->pModel,
				G_CALLBACK (v_rating_bottom_bar_row_changed_cb),
				pRatingBottomBar);
		g_signal_handlers_disconnect_by_func (priv->pModel,
				G_CALLBACK (v_rating_bottom_bar_row_removed_cb),
				pRatingBottomBar);
		g_object_unref (priv->pModel);

		priv->pModel = NULL;
	}

	/* update the new model with signals */
	if (pModel != NULL)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));

		priv->pModel = g_object_ref (pModel);

		g_signal_connect (priv->pModel,
				"row-added",
				G_CALLBACK (v_rating_bottom_bar_row_added_cb),
				pRatingBottomBar);

		g_signal_connect (priv->pModel,
				"row-changed",
				G_CALLBACK (v_rating_bottom_bar_row_changed_cb),
				pRatingBottomBar);

		g_signal_connect (priv->pModel,
				"row-removed",
				G_CALLBACK (v_rating_bottom_bar_row_removed_cb),
				pRatingBottomBar);

	}
	v_rating_bottom_bar_extract_model_info(pRatingBottomBar);
}




/********************************************************
 * Function : v_rating_bottom_bar_add_style_to_hash
 * Description: maintain style in priv hash
 * Parameter :  MildenhallRatingBottomBarPrivate*, *pKey, pValue
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_add_style_to_hash(MildenhallRatingBottomBarPrivate *priv, gchar *pKey, gpointer pValue)
{

	if (NULL != pKey || NULL != pValue)
	{
		/* maintain key and value pair for style name and its value */
		if (G_VALUE_HOLDS_DOUBLE(pValue))
		{
			g_hash_table_insert(priv->pPrivHash, pKey,
					g_strdup_printf("%f",g_value_get_double(pValue)));

			MILDENHALL_RATING_BOTTOM_BAR_PRINT("............ %f...........\n ", g_value_get_double(pValue));
		}
		else if (G_VALUE_HOLDS_STRING(pValue))
		{
			g_hash_table_insert(priv->pPrivHash, pKey,
					(gpointer) g_value_get_string(pValue));
			MILDENHALL_RATING_BOTTOM_BAR_PRINT("............ %s...........\n ", g_value_get_string(pValue));
		}
	}
}

/********************************************************
 * Function : v_rating_bottom_bar_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallRatingBottomBar *pRatingBottomBar = pUserData;
	MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);

	if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_EMPTY_RATING_ICON) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_HALF_RATING_ICON) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_FULL_RATING_ICON) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_X) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_Y) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_Y) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_X) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_FONT) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey , MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_COLOR ) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey , pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_X) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_Y) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_Y) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_DISPLACEMENT) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_SPACING) == 0)
	{
		priv->fltStarSpacing = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_FONT) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_DEFAULT_FONT) == 0)
	{
		v_rating_bottom_bar_add_style_to_hash(priv,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->pRightTextColor ,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_DEFAULT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->pLeftBottomColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_RATING_BOTTOM_BAR_DEFAULT_BACKGROUND) == 0)
	{
		clutter_color_from_string(&priv->BackgroundColor,   g_value_get_string(pValue) );
		clutter_actor_set_background_color (CLUTTER_ACTOR(pRatingBottomBar), &priv->BackgroundColor);
	}
}


static void
mildenhall_rating_bottom_bar_get_property (GObject    *pObject,
		guint       property_id,
		GValue     *pValue,
		GParamSpec *pSpec)
{

	switch (property_id)
	{
	case PROP_RATING_BOTTOM_BAR_MODEL:
		break;
	case PROP_RATING_BOTTOM_BAR_BACKGROUND_COLOR:
		break;
	case PROP_RATING_BOTTOM_BAR_WIDTH:
		break;
	case PROP_RATING_BOTTOM_BAR_HEIGHT:
		break;
	case PROP_RATING_BOTTOM_BAR_RATING_OR_TEXT:
		break;
	case PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_FONT:
		break;
	case PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_COLOR:
		break;
	case PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_FONT:
		break;
	case PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_COLOR:
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, property_id, pSpec);
		break;
	}
}

static void
mildenhall_rating_bottom_bar_set_property (GObject      *pObject,
		guint         property_id,
		const GValue *pValue,
		GParamSpec   *pSpec)
{
        MildenhallRatingBottomBar *pRatingBottomBar = MILDENHALL_RATING_BOTTOM_BAR (pObject);
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	if(!MILDENHALL_IS_RATING_BOTTOM_BAR(pObject))
	{
		g_warning("invalid Rating Bottom bar pObject\n");
		return;
	}

	switch (property_id)
	{
	case PROP_RATING_BOTTOM_BAR_MODEL:
	{
		v_mildenhall_rating_bottom_bar_set_model( pRatingBottomBar, g_value_get_object (pValue) );
	}
	break;
	case PROP_RATING_BOTTOM_BAR_WIDTH:
	{
		v_rating_bottom_bar_set_width( pRatingBottomBar, g_value_get_float (pValue) );
	}
	break;
	case PROP_RATING_BOTTOM_BAR_HEIGHT:
	{
		v_rating_bottom_bar_set_height( pRatingBottomBar, g_value_get_float (pValue) );
	}
	break;
	case PROP_RATING_BOTTOM_BAR_BACKGROUND_COLOR:
	{
		clutter_color_from_string(&priv->BackgroundColor,   g_value_get_string(pValue) );
		clutter_actor_set_background_color (CLUTTER_ACTOR(pRatingBottomBar), &priv->BackgroundColor);
	}
	break;
	case PROP_RATING_BOTTOM_BAR_RATING_OR_TEXT:
	{
		priv->bIsRating =  g_value_get_boolean(pValue);
		v_rating_bottom_bar_extract_model_info(pRatingBottomBar);
	}
	break;
	case PROP_RATING_BOTTOM_BAR_SWAP_VIEW :
	{
		v_rating_bottom_bar_swap_view(pRatingBottomBar , g_value_get_boolean(pValue));
	}
	break ;
	case PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_FONT:
	{
		MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(priv->pRightTextFont)
				priv->pRightTextFont = g_value_dup_string (pValue);
		if(NULL != priv->pRightText )
		{
			clutter_text_set_font_name(CLUTTER_TEXT(priv->pRightText ),priv->pRightTextFont );
		}

	}
	break;
	case PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_COLOR:
	{
		gchar *pTextColor = g_value_dup_string (pValue);
		clutter_color_from_string( &priv->pRightTextColor , pTextColor);
		if(NULL != priv->pRightText )
		{
			clutter_text_set_color(CLUTTER_TEXT(priv->pRightText ),&priv->pRightTextColor );
		}
		MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pTextColor)

	}
	break;
	case PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_FONT:
	{
		MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(priv->pLeftBottomFont)
				priv->pLeftBottomFont = g_value_dup_string (pValue);
		if(NULL != priv->pLeftBottomText )
		{
			clutter_text_set_font_name(CLUTTER_TEXT(priv->pLeftBottomText ),priv->pLeftBottomFont );
		}
	}
	break;
	case PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_COLOR:
	{
		gchar *pTextColor = g_value_dup_string (pValue);
		clutter_color_from_string( &priv->pLeftBottomColor , pTextColor);
		if(NULL != priv->pLeftBottomText )
		{
			clutter_text_set_color(CLUTTER_TEXT(priv->pLeftBottomText ),&priv->pLeftBottomColor );
		}
		MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pTextColor)
	}
	break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, property_id, pSpec);
		break;
	}
}

static void
mildenhall_rating_bottom_bar_dispose (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_rating_bottom_bar_parent_class)->dispose (pObject);
}

static void
mildenhall_rating_bottom_bar_finalize (GObject *pObject)
{
	G_OBJECT_CLASS (mildenhall_rating_bottom_bar_parent_class)->finalize (pObject);
}

static void
mildenhall_rating_bottom_bar_class_init (MildenhallRatingBottomBarClass *pKlass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
        GParamSpec *pSpec = NULL;

	pObjectClass->get_property = mildenhall_rating_bottom_bar_get_property;
	pObjectClass->set_property = mildenhall_rating_bottom_bar_set_property;
	pObjectClass->dispose = mildenhall_rating_bottom_bar_dispose;
	pObjectClass->finalize = mildenhall_rating_bottom_bar_finalize;

	/**
	 * RatingBottomBar: model:
	 *
	 * Model contains the data to be displayed on the Rating Bottom Bar.
	 * RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS 					:ratings ranging from 0 to 10
	 * 															0 -> all empty stars
	 * 															if ratings end with odd number, one half star is added
	 * 															10 -> all stars are filled
	 * 															-1 to hide the ratings
	 *
	 * RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT			:Text to be displayed below ratings
	 * 															( Font and color can be customized via properties )
	 *
	 * RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT					:Text to be displayed to right of the ratings
	 *															( Font and color can be customized via properties )
	 *
	 * RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT				:In case text to be displayed in place of ratings
	 * 															(field 1 has ratings needs to be set TRUE)
	 *
	 */

	pSpec = g_param_spec_object("model", "Model",
			"Model information of Rating Bottom Bar widget", G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_MODEL, pSpec);


	/**
	 * RatingBottomBar :width:
	 *
	 * Width of the Rating Bottom Bar
	 */
	pSpec = g_param_spec_float ("width",
			"Width",
			"Width of the Rating Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_RATING_BOTTOM_BAR_WIDTH, pSpec);


	/**
	 * RatingBottomBar :height:
	 *
	 * height of the Rating Bottom Bar
	 */
	pSpec = g_param_spec_float ("height",
			"Height",
			"Height of the Rating Bottom Bar",
			0.0, G_MAXFLOAT,
			0.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, PROP_RATING_BOTTOM_BAR_HEIGHT, pSpec);



	/**
	 *	MildenhallRatingBottomBar: background-color
	 *
	 * background-color
	 * Default: #000000 40% opacity
	 */
	pSpec = g_param_spec_string("background-color", "background-color",
			"background color for the entire widget",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_BACKGROUND_COLOR, pSpec);


	/**
	 *	MildenhallRatingBottomBar:right-text-font
	 *
	 * right-text-font
	 * Default: DejaVuSansCondensed 28px
	 */
	pSpec = g_param_spec_string("right-text-font", "right-text-font",
			"font type for the text in Right side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_FONT, pSpec);

	/**
	 *	MildenhallRatingBottomBar:right-text-color
	 *
	 * right-text-color
	 * Default: #98A9B300
	 */
	pSpec = g_param_spec_string("right-text-color", "right-text-color",
			"color for the text in Right side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_RIGHT_TEXT_COLOR, pSpec);


	/**
	 *	MildenhallRatingBottomBar: left-bottom-text-font
	 *
	 * left-bottom-text-font
	 * Default: DejaVuSansCondensed 18px
	 */
	pSpec = g_param_spec_string("left-bottom-text-font", "left-bottom-text-font",
			"font type for the text in left-bottom side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_FONT, pSpec);

	/**
	 *	MildenhallRatingBottomBar: left-bottom-text-color
	 *
	 * left-bottom-text-color
	 * Default: #98A9B300
	 */
	pSpec = g_param_spec_string("left-bottom-text-color", "left-bottom-text-color",
			"color for the text in left-bottom side",
			NULL,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_COLOR, pSpec);

	/**
	 * MildenhallRatingBottomBar: is-rating:
	 *
	 * Focused  is-rating
	 * Default: TRUE  i.e, ratings will be displayed
	 */

	pSpec = g_param_spec_boolean ("is-rating",
			"is-rating",
			"Whether the actor on left top corner is Ratings or Text",
			FALSE,
			G_PARAM_WRITABLE);
	g_object_class_install_property (pObjectClass, PROP_RATING_BOTTOM_BAR_RATING_OR_TEXT, pSpec);


	/**
	 * MildenhallRatingBottomBar: swap:
	 *
	 * swap view by 180 degree
	 * Default: FALSE  i.e, view will not be swapped
	 */

	pSpec = g_param_spec_boolean ("swap",
			"swap",
			"swap view by 180 degree",
			FALSE,
			G_PARAM_WRITABLE);
	g_object_class_install_property (pObjectClass, PROP_RATING_BOTTOM_BAR_SWAP_VIEW, pSpec);


}

static void mildenhall_rating_bottom_bar_init (MildenhallRatingBottomBar *self)
{
        int inLoop;
        /* get the hash table for style properties */
        GHashTable *pStyleHash = thornbury_style_set (PKGDATADIR"/mh_rb_style.json");
	self->priv = mildenhall_rating_bottom_bar_get_instance_private (self);
	self->priv->pStarGroup = NULL ;
	self->priv->pLeftTopText = NULL ;
	self->priv->pLeftBottomText = NULL ;
	self->priv->pRightText = NULL ;
	self->priv->pPrivHash = NULL ;
	self->priv->bIsRating = TRUE;
	self->priv->pRightTextFont = NULL;
	self->priv->pLeftBottomFont = NULL;
	self->priv->fltHeight = 0.0;
	self->priv->fltWidth = 0.0;
	for(inLoop = 0; inLoop < MAX_NO_OF_STARS ;inLoop++)
	{
		self->priv->pRatingStar[inLoop] = NULL;

	}

	/* pares the hash for styles */
	if(NULL !=  pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter,  pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				self->priv->pPrivHash = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
				g_hash_table_foreach(pHash, v_rating_bottom_bar_parse_style, self);
			}
		}
	}

	/* free the style hash */
	thornbury_style_free( pStyleHash);


}

ClutterActor* mildenhall_rating_bottom_bar_new (void)
{
	return g_object_new (MILDENHALL_TYPE_RATING_BOTTOM_BAR, NULL);
}


/********************************************************
 * Function : v_rating_bottom_bar_extract_model_info
 * Description: Model info is extracted in this function
 * Parameters: MildenhallRatingBottomBar *
 * Return value: void
 ********************************************************/


static void v_rating_bottom_bar_extract_model_info(MildenhallRatingBottomBar *pRatingBottomBar)
{
	MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	gchar *pText = NULL ;

	if (G_IS_OBJECT( priv->pModel)	&& thornbury_model_get_n_rows( priv->pModel) > 0)
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pModel, 0);
		if(NULL != pIter)
		{
			GValue value = { 0, };

			if(TRUE == priv->bIsRating)
			{
				thornbury_model_iter_get_value(pIter, RATING_BOTTOM_BAR_MODEL_COL_0_RATINGS, &value);
				v_rating_bottom_bar_update_ratings(pRatingBottomBar ,  g_value_get_int(&value));
				if(NULL != priv->pLeftTopText)
				{
					clutter_actor_hide(CLUTTER_ACTOR(priv->pLeftTopText));
				}
			}
			else
			{
				thornbury_model_iter_get_value(pIter, RATING_BOTTOM_BAR_MODEL_COL_3_LEFT_TOP_TEXT, &value);
				MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pText)
				pText = g_value_dup_string(&value);

				MILDENHALL_RATING_BOTTOM_BAR_PRINT("................%s ...*************************\n",pText);
				if(NULL != pText)
				{
					v_rating_bottom_bar_update_left_top_text(pRatingBottomBar , pText);
				}

				clutter_actor_show(CLUTTER_ACTOR(priv->pLeftTopText));
				if(NULL != priv->pStarGroup)
				{
					clutter_actor_hide(CLUTTER_ACTOR(priv->pStarGroup));
				}

			}
			g_value_unset (&value);

			thornbury_model_iter_get_value(pIter, RATING_BOTTOM_BAR_MODEL_COL_1_LEFT_BOTTOM_TEXT, &value);
			MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pText)
			pText = g_value_dup_string(&value);
			if(NULL != pText)
			{
				v_rating_bottom_bar_update_left_bottom_text(pRatingBottomBar , pText);
			}
			g_value_unset (&value);


			thornbury_model_iter_get_value(pIter, RATING_BOTTOM_BAR_MODEL_COL_2_RIGHT_TEXT, &value);
			MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pText)
			pText = g_value_dup_string(&value);
			if(NULL != pText)
			{
				v_rating_bottom_bar_update_right_text(pRatingBottomBar , pText);
			}
			g_value_unset (&value);
		}
	}
}




/********************************************************
 * Function : v_rating_bottom_bar_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallRatingBottomBar *
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar)
{
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	v_rating_bottom_bar_extract_model_info(pRatingBottomBar);
}

/********************************************************
 * Function : v_rating_bottom_bar_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallRatingBottomBar *
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar)
{
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	v_rating_bottom_bar_extract_model_info(pRatingBottomBar);
}

/********************************************************
 * Function : v_rating_bottom_bar_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallRatingBottomBar *
 * Return value: void
 ********************************************************/
static void v_rating_bottom_bar_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallRatingBottomBar *pRatingBottomBar)
{
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	v_rating_bottom_bar_extract_model_info(pRatingBottomBar);

}

/********************************************************
 * Function : p_rating_bottom_bar_create_text
 * Description: fuction to create text with default requisites
 * Parameters: MildenhallRatingBottomBar *, ClutterActor *, ClutterColor * , gchar*
 * Return value: ClutterActor*
 ********************************************************/
static ClutterActor* p_rating_bottom_bar_create_text(MildenhallRatingBottomBar *pRatingBottomBar ,  gchar *pTextColor , gchar *pFont  )
{
        ClutterColor TextColor = {0x00, 0x00, 0x00, 0x00};
        ClutterActor *pActor = clutter_text_new ();
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	clutter_text_set_single_line_mode (CLUTTER_TEXT (pActor), TRUE);
	if(NULL != pTextColor )
	{
		clutter_color_from_string(&TextColor,  pTextColor);
		clutter_text_set_color(CLUTTER_TEXT(pActor), &TextColor);
	}
	clutter_text_set_font_name(CLUTTER_TEXT(pActor),pFont);
	clutter_text_set_ellipsize(CLUTTER_TEXT(pActor),PANGO_ELLIPSIZE_END);
	clutter_text_set_use_markup(CLUTTER_TEXT(pActor) , TRUE );
	clutter_actor_add_child(CLUTTER_ACTOR(pRatingBottomBar), pActor);
	return pActor;
}

/********************************************************
 * Function : v_rating_bottom_bar_update_ratings
 * Description: fuction to create ratings stars
 * Parameters: MildenhallRatingBottomBar *, gint
 * Return value:
 ********************************************************/

static void v_rating_bottom_bar_update_ratings(MildenhallRatingBottomBar *pRatingBottomBar , gint inRating)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
        gchar *pIconPath = NULL ;
        gfloat fltVariableX , fltVariableY;
	MILDENHALL_RATING_BOTTOM_BAR_PRINT ("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	if(NULL == priv->pStarGroup)
	{
		priv->pStarGroup = clutter_actor_new();
		clutter_actor_add_child(CLUTTER_ACTOR (pRatingBottomBar),	priv->pStarGroup );
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_X), "%f", &fltVariableX);
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_Y), "%f", &fltVariableY);
		clutter_actor_set_position(priv->pStarGroup , fltVariableX  ,fltVariableY);
	}

	if(inRating != -1)
	{
		if((inRating >= 0)  && (inRating <= 10))
		{
			gint inFilled = ( inRating / 2 );
			gint inHalfFilled = (inRating % 2);
                        gint inLoop;

			pIconPath = g_hash_table_lookup(priv->pPrivHash ,MILDENHALL_RATING_BOTTOM_BAR_FULL_RATING_ICON );
			pIconPath = g_strdup_printf(PKGTHEMEDIR"/%s" , pIconPath);
			for(inLoop = 0; inLoop < inFilled; inLoop++)
			{
				v_rating_bottom_bar_addratingstar( pRatingBottomBar , inLoop, pIconPath);
			}
			MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pIconPath)
			pIconPath = g_hash_table_lookup(priv->pPrivHash ,MILDENHALL_RATING_BOTTOM_BAR_HALF_RATING_ICON );
			pIconPath = g_strdup_printf(PKGTHEMEDIR"/%s" , pIconPath);

			if ((inHalfFilled ) > 0  )
			{
				v_rating_bottom_bar_addratingstar(pRatingBottomBar , inLoop , pIconPath);
				inLoop++;
			}

			MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pIconPath)
			pIconPath = g_hash_table_lookup(priv->pPrivHash ,MILDENHALL_RATING_BOTTOM_BAR_EMPTY_RATING_ICON );
			pIconPath = g_strdup_printf(PKGTHEMEDIR"/%s" , pIconPath);
			for(; inLoop < MAX_NO_OF_STARS ; inLoop++)
			{
				v_rating_bottom_bar_addratingstar(pRatingBottomBar ,inLoop,pIconPath );
			}
			MILDENHALL_RATING_BOTTOM_BAR_FREE_MEM_IF_NOT_NULL(pIconPath)
		}
		else
		{
			g_warning("Rating %d is out of bound: Rating should be in range -1 to 10" , inRating);
		}

	}
	if(inRating == -1)
	{
		clutter_actor_hide(priv->pStarGroup);
	}
	else
	{
		clutter_actor_show(priv->pStarGroup);
	}
}

/********************************************************
 * Function : v_rating_bottom_bar_update_ratings
 * Description: fuction to add individual ratings stars
 * Parameters: MildenhallRatingBottomBar *, gint  , gchar*
 * Return value:
 ********************************************************/

static void v_rating_bottom_bar_addratingstar(MildenhallRatingBottomBar *pRatingBottomBar,  gint inPos ,gchar *pIconPath )
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	if(NULL == priv->pRatingStar[inPos] )
	{
		priv->pRatingStar[inPos] = thornbury_ui_texture_create_new(pIconPath, 0.0 ,0.0, FALSE, FALSE);
	}
	else
	{
		thornbury_ui_texture_set_from_file(priv->pRatingStar[inPos], pIconPath, 0.0 , 0.0,  FALSE, FALSE);
	}

	if (NULL != priv->pRatingStar[inPos])
	{

		if(clutter_actor_get_parent (priv->pRatingStar[inPos]) == NULL)
		{
			clutter_actor_add_child(priv->pStarGroup, priv->pRatingStar[inPos]);
		}

		clutter_actor_set_position(priv->pRatingStar[inPos],
				( inPos * priv->fltStarSpacing ),
				0.0);
	}


}

/********************************************************
 * Function : v_rating_bottom_bar_update_left_top_text
 * Description: fuction to update the text in left top corner
 * Parameters: MildenhallRatingBottomBar *,   gchar*
 * Return value:
 ********************************************************/

static void v_rating_bottom_bar_update_left_top_text (MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);
	if(NULL == priv->pLeftTopText )
	{
                gfloat fltVariableX, fltVariableY;
		gchar *pTextFont = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_FONT );
		gchar *pTextColor = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_TEXT_COLOR );
		priv->pLeftTopText = p_rating_bottom_bar_create_text (pRatingBottomBar ,pTextColor  , pTextFont);

		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_X), "%f", &fltVariableX);
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_Y), "%f", &fltVariableY);
		clutter_actor_set_position(CLUTTER_ACTOR(priv->pLeftTopText) ,fltVariableX , fltVariableY );

		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH), "%f", &fltVariableX);
		clutter_actor_set_width(CLUTTER_ACTOR(priv->pLeftTopText) , fltVariableX);
	}
	clutter_text_set_markup(CLUTTER_TEXT(priv->pLeftTopText) , pText);
}


/********************************************************
 * Function : v_rating_bottom_bar_update_right_text
 * Description: fuction to update the text in right side
 * Parameters: MildenhallRatingBottomBar *,   gchar*
 * Return value:
 ********************************************************/

static void v_rating_bottom_bar_update_right_text(MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText)
{
	MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
        ClutterColor color;
        MILDENHALL_RATING_BOTTOM_BAR_PRINT ("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	if(NULL == priv->pRightText )
	{
                gfloat fltVariableX, fltVariableY, fltDisp, fltTemp;
		if(NULL == priv->pRightTextFont)
		{
			priv->pRightTextFont = g_strdup(g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_FONT ));
		}
		//gchar *pTextColor = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RIGHT_DEFAULT_COLOR );
		MILDENHALL_RATING_BOTTOM_BAR_PRINT("................ %s...././/././/././..........\n", clutter_color_to_string(  &priv->pRightTextColor));
		priv->pRightText = p_rating_bottom_bar_create_text (pRatingBottomBar ,NULL  , priv->pRightTextFont);
		clutter_text_set_color(CLUTTER_TEXT(priv->pRightText), &priv->pRightTextColor);


		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_DISPLACEMENT), "%f", &fltDisp);
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH), "%f", &fltVariableX);
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_Y), "%f", &fltVariableY);

		fltTemp =  (priv->fltWidth -fltVariableX -(3*fltDisp) );
		clutter_actor_set_width(CLUTTER_ACTOR(priv->pRightText) , fltTemp);

		clutter_actor_set_position(CLUTTER_ACTOR(priv->pRightText) ,(priv->fltWidth -fltTemp - fltDisp), fltVariableY );
		clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pRightText) ,PANGO_ALIGN_RIGHT );
		clutter_actor_show(CLUTTER_ACTOR(priv->pRightText));
	}
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("................ pRightText =  %s.. ..........\n",pText  );
	clutter_text_set_markup(CLUTTER_TEXT(priv->pRightText) , pText);

	 clutter_text_get_color (CLUTTER_TEXT(priv->pRightText), &color);

	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: priv->pRightText \t x= %f  y = %f  width = %f,  height = %f     color = %s   font = %s  text = %s \n priv->fltHeight = %f  priv->fltWidth = %f \n",
								clutter_actor_get_x(priv->pRightText),
								 clutter_actor_get_y(priv->pRightText),
								 clutter_actor_get_width(priv->pRightText),
								 clutter_actor_get_height(priv->pRightText),
								 clutter_color_to_string(&color),
								 clutter_text_get_font_name (CLUTTER_TEXT(priv->pRightText)),
								 clutter_text_get_text(CLUTTER_TEXT(priv->pRightText)),
								 priv->fltHeight,
								 priv->fltWidth);

}


/********************************************************
 * Function : v_rating_bottom_bar_update_left_bottom_text
 * Description: fuction to update the text in left bottom side
 * Parameters: MildenhallRatingBottomBar *,   gchar*
 * Return value:
 ********************************************************/

static void v_rating_bottom_bar_update_left_bottom_text(MildenhallRatingBottomBar *pRatingBottomBar  ,gchar *pText)
{
	MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
        ClutterColor color;
        MILDENHALL_RATING_BOTTOM_BAR_PRINT ("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	if(NULL == priv->pLeftBottomText )
	{
                gfloat fltVariableX, fltVariableY;
		if(NULL == priv->pLeftBottomFont)
		{
			priv->pLeftBottomFont = g_strdup(g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_DEFAULT_FONT ));
		}
		priv->pLeftBottomText = p_rating_bottom_bar_create_text (pRatingBottomBar ,NULL  , priv->pLeftBottomFont);

		clutter_text_set_color(CLUTTER_TEXT(priv->pLeftBottomText), &priv->pLeftBottomColor);


		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_X), "%f", &fltVariableX);
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_Y), "%f", &fltVariableY);
		clutter_actor_set_position(CLUTTER_ACTOR(priv->pLeftBottomText) ,fltVariableX, fltVariableY );

		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH), "%f", &fltVariableX);
		clutter_actor_set_width(CLUTTER_ACTOR(priv->pLeftBottomText) , fltVariableX );
		clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pLeftBottomText) ,PANGO_ALIGN_LEFT );
		clutter_actor_show(CLUTTER_ACTOR(priv->pLeftBottomText));
	}
	MILDENHALL_RATING_BOTTOM_BAR_PRINT("................ pLeftBottomText =  %s.. ..........\n",pText  );
	clutter_text_set_markup(CLUTTER_TEXT(priv->pLeftBottomText) , pText);
	 clutter_text_get_color (CLUTTER_TEXT(priv->pLeftBottomText), &color);

	MILDENHALL_RATING_BOTTOM_BAR_PRINT("MILDENHALL_RATING_BOTTOM_BAR_PRINT: priv->pLeftBottomText \t x= %f  y = %f  width = %f,  height = %f     color = %s   font = %s  text = %s \n priv->fltHeight = %f  priv->fltWidth = %f \n",
								clutter_actor_get_x(priv->pLeftBottomText),
								 clutter_actor_get_y(priv->pLeftBottomText),
								 clutter_actor_get_width(priv->pLeftBottomText),
								 clutter_actor_get_height(priv->pLeftBottomText),
								 clutter_color_to_string(&color),
								 clutter_text_get_font_name (CLUTTER_TEXT(priv->pLeftBottomText)),
								 clutter_text_get_text(CLUTTER_TEXT(priv->pLeftBottomText)),
								 priv->fltHeight,
								 								 priv->fltWidth);
}

static void v_rating_bottom_bar_swap_view(MildenhallRatingBottomBar *pRatingBottomBar  , gboolean bSwap)
{
        MildenhallRatingBottomBarPrivate *priv = mildenhall_rating_bottom_bar_get_instance_private (pRatingBottomBar);
        gfloat fltPositionX, fltActorWidth, fltTotalWidth;

	MILDENHALL_RATING_BOTTOM_BAR_PRINT ("MILDENHALL_RATING_BOTTOM_BAR_PRINT: %s\n", __FUNCTION__);

	fltTotalWidth = clutter_actor_get_width(CLUTTER_ACTOR(pRatingBottomBar));

	if(bSwap == TRUE)
	{
//		if(TRUE == priv->bIsRating)
		{
			if(NULL != priv->pStarGroup)
			{
				fltPositionX = clutter_actor_get_x(priv->pStarGroup);
				fltActorWidth = clutter_actor_get_width(priv->pStarGroup);
				fltPositionX = (fltTotalWidth - fltActorWidth - fltPositionX );
				clutter_actor_set_x(priv->pStarGroup , fltPositionX);
			}
		}
//		else
		{
			if(NULL != priv->pLeftTopText)
			{
				clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pLeftTopText) ,PANGO_ALIGN_RIGHT );
				fltPositionX = clutter_actor_get_x(priv->pLeftTopText);
				fltActorWidth = clutter_actor_get_width(priv->pLeftTopText);
				fltPositionX = (fltTotalWidth - fltActorWidth - fltPositionX );
				clutter_actor_set_x(priv->pLeftTopText , fltPositionX);
			}
		}

		if(NULL != priv->pLeftBottomText)
		{
			clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pLeftBottomText) ,PANGO_ALIGN_RIGHT );
			fltPositionX = clutter_actor_get_x(priv->pLeftBottomText);
			fltActorWidth = clutter_actor_get_width(priv->pLeftBottomText);
			fltPositionX = (fltTotalWidth - fltActorWidth - fltPositionX );
			clutter_actor_set_x(priv->pLeftBottomText , fltPositionX);
		}

		if(NULL != priv->pRightText)
		{
			clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pRightText) ,PANGO_ALIGN_LEFT );
			fltPositionX = clutter_actor_get_x(priv->pRightText);
			fltActorWidth = clutter_actor_get_width(priv->pRightText);
			fltPositionX = (fltTotalWidth - fltActorWidth - fltPositionX );
			clutter_actor_set_x(priv->pRightText , fltPositionX);
		}
	}
	else
	{
		sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RATING_STARS_X), "%f", &fltPositionX);
		if(TRUE == priv->bIsRating)
		{
			if(NULL != priv->pStarGroup)
			{
				clutter_actor_set_x(priv->pStarGroup , fltPositionX);
			}
		}
		else
		{
			if(NULL != priv->pLeftTopText)
			{
				clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pLeftTopText) ,PANGO_ALIGN_LEFT );
				clutter_actor_set_x(priv->pLeftTopText , fltPositionX);
			}
		}

		if(NULL != priv->pRightText)
		{
                        gfloat fltVariableX  ,fltDisp , fltTemp;
			clutter_text_set_line_alignment (CLUTTER_TEXT (priv->pRightText), PANGO_ALIGN_RIGHT);
			sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_RIGHT_TEXT_DISPLACEMENT), "%f", &fltDisp);
			sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_MAX_WIDTH), "%f", &fltVariableX);
			fltTemp =  (priv->fltWidth -fltVariableX -(3*fltDisp) );
			clutter_actor_set_x(CLUTTER_ACTOR(priv->pRightText) ,(priv->fltWidth -fltTemp - fltDisp) );
		}
		if(NULL != priv->pLeftBottomText)
		{
			clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pLeftBottomText) ,PANGO_ALIGN_LEFT );
			sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_RATING_BOTTOM_BAR_LEFT_BOTTOM_TEXT_X), "%f", &fltPositionX);
			clutter_actor_set_x(priv->pLeftBottomText , fltPositionX);
		}
	}

}




