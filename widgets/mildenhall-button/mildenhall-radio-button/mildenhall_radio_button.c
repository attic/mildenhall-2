/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: mildenhall_radio_button
 * @title: MildenhallRadioButton
 * @short_description: MildenhallRadioButton is a widget used to toggle between
 * two states i.e,True or false.
 * @see_also: #ClutterActor, #ThornburyItemFactory
 *
 * #MildenhallRadioButton widget can be used to toggle between two states
 * i.e,True or false. Every button must have a group ID in order to differentiate.
 * Button ID is required to differentiate the Button within a group.
 * Initially all buttons will be un-set if not set to true explicitly and once any
 * button is set, at least one button will be in true state in that group.
 * "state-change" signal is emitted for state change if "reactive" property
 * is set true and button ID is passed along the signal.
 *
 * ## Freeing the widget
 *     Call g_object_unref() to free the widget.
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 *
 * GObject *object = NULL;
 * ThornburyItemFactory *item_factory = NULL;
 * MildenhallRadioButton *button = NULL;
 *
 * item_factory = thornbury_item_factory_generate_widget_with_props (
 * 		MILDENHALL_TYPE_RADIO_BUTTON
 *      	"PKGDATADIR"/mildenhall_radio_button_prop.json");
 *
 * g_object_get (item_factory, "object", &object, NULL);
 * button = MILDENHALL_RADIO_BUTTON (object);
 * clutter_actor_add_child (stage, CLUTTER_ACTOR (button));
 *
 * ]|
 *
 * Refer #ThornburyItemFactory for Thornbury Item Factory details.
 *
 * Since: 0.3.0
 */
#include "mildenhall_radio_button.h"




/***********************************************************
 * @local function declarations
 **********************************************************/

static void v_radio_button_create_new_markup_icon(MildenhallRadioButton *pRadioButton);
static void v_radio_button_update_view(MildenhallRadioButton *pRadioButton);
static void v_radio_button_resize_view(MildenhallRadioButton *pRadioButton, GParamSpec *pspec, gpointer user_data);
static void v_radio_button_judge_view(MildenhallRadioButton *pRadioButton, gboolean bState);
/* callback function */
static void v_radio_button_active_cb (MildenhallRadioButton *pRadioButton, GParamSpec *pSpec, gpointer user_data);
static void v_radio_button_press_cb(ClutterActor *pRadioButton, gpointer pUserData);



/******************************************************************************
 * @Defines
 *****************************************************************************/


/**
 * _enRadioButtonProperty:
 * property enums of radio button
 */
typedef enum _enRadioButtonProperty enRadioButtonProperty;
enum _enRadioButtonProperty
{
	/*< private >*/
	PROP_FIRST,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_CURRENT_STATE,
	PROP_GROUP_ID,
	PROP_BUTTON_ID,
	PROP_HIGHLIGHT,
	PROP_LAST
};

/* The signals emitted by the radio button */

/**
 * _enRadioButtonSignals:
 * The signals emitted by the radio button
 */
typedef enum _enRadioButtonSignals 	enRadioButtonSignals;
enum _enRadioButtonSignals
{
	SIG_FIRST,
	SIG_RB_STATE_CHANGE,                       /* Emitted when button is TOGGLED */
	SIG_LAST
};


#define	RADIO_BUTTON_TRUE_STATE_MARKUP	"markup-true"
#define	RADIO_BUTTON_FALSE_STATE_MARKUP	"markup-false"
#define RADIO_BUTTON_FONT_NAME "markup-font"
#define RADIO_BUTTON_FONT_COLOR_NORMAL "markup-font-color-normal"
#define RADIO_BUTTON_FONT_COLOR_ACTIVE "markup-font-color-active"
#define MILDENHALL_RADIO_BUTTON_PRINT( a ...)			/*g_print(a)*/

G_DEFINE_TYPE (MildenhallRadioButton, mildenhall_radio_button, LIGHTWOOD_TYPE_BUTTON)

#define MILDENHALL_RADIO_BUTTON_GET_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_RADIO_BUTTON, MildenhallRadioButtonPrivate))


#define RADIO_BUTTON_FREE_MEM_IF_NOT_NULL(pointer) 		if(NULL != pointer) \
		{ \
	g_free(pointer); \
	pointer = NULL; \
		}


/**
 * _MildenhallRadioButtonPrivate:
 * structure to hold the private variables of Radio button
 */

struct _MildenhallRadioButtonPrivate
{
	gfloat fltWidth;						/* width of the Radio button */
	gfloat fltHeight;						/* height of the Radio button */
	gboolean bCurrentState;					/* current state of button */
	gboolean bActive;						/* Whether the button should be active or not */
	gboolean bStateChangeInProgress;		/* to check multiple call to set state change property */
	gboolean bHighlight;					/* To determine active(highlight) or normal state */
	gchar *pTrueStateImage;					/* variable to hold markup string for true state from Style */
	gchar *pFalseStateImage;				/* variable to hold markup string for false state from style */
	gchar *pMarkupFont;						/* Font type for markup */
	ClutterColor MarkupFontColor;			/* Markup Color */
	ClutterColor MarkupFontColorNormal;		/* Markup Color for Normal state */
	ClutterColor MarkupFontColorActive;		/* Markup Color for Active state */
	gchar *pRbGroupID;						/* priv variable to hold the location of memory allocated for storing Group ID in hashtable */
	gpointer *pButtonID;					/* Button Id of the button */
	ClutterActor *pButtonGroup;
	ClutterActor *pTrueState;				/*for  True  state Markup creation */
	ClutterActor *pFalseState;				/*for  Flase  state Markup creation */
	gboolean bResetNeeded ;
	gboolean bChangeLock ;

};

/************************************************************
 * @Global Variables
 ***********************************************************/

/* Storage for the signals */
static guint32 mildenhall_radio_bt_signals[SIG_LAST] = {0,};




/************************************************************
 * @Functions
 ***********************************************************/

/* setter functions definition */

/**
 * mildenhall_radio_button_set_current_state:
 * @pObject: radio button object reference
 * @bState: current state of the button
 *
 * @pre-condition: Group Id must be set before setting current state.
 *
 * Sets the current state of the radio button
 */
void mildenhall_radio_button_set_current_state(MildenhallRadioButton *pRadioButton, gboolean bState)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid Radio button object\n");
		return;
	}

	/* check if state change is in progress */
	if(TRUE == priv->bStateChangeInProgress)
	{
		return;
	}

	v_radio_button_judge_view(pRadioButton,bState);
	/* emit the signal for state changed with the new state info and button ID */
	g_signal_emit(pRadioButton, mildenhall_radio_bt_signals[SIG_RB_STATE_CHANGE], 0, bState,priv->pButtonID);

	MILDENHALL_RADIO_BUTTON_PRINT(" button %s STATE CHANGED TO  = %s \n", priv->pButtonID, (priv->bCurrentState)?"true":"false");

}


/**
 * mildenhall_radio_button_set_highlight:
 * @pObject: radio button object reference
 * @bHighLightState: highlight state of the button
 *
 * This method is to highlight a particular button within a group.
 * The widget will highlight only one button in a group at a time.
 * If one button is already highlighted or two button in a group is tried
 * to be highlighted then resent request is recognized
 * and other button is turned to normal state.
 *
 */
void mildenhall_radio_button_set_highlight(MildenhallRadioButton *pRadioButton, gboolean bHighLightState)
{

	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid  radio button object\n");
		return;
	}

	/* Return if value to be set is redundant */
	if(priv->bHighlight == bHighLightState)
	{
		return;
	}

	priv->bHighlight = bHighLightState;

	priv->MarkupFontColor = (bHighLightState) ? priv->MarkupFontColorActive : priv->MarkupFontColorNormal;
	clutter_text_set_color (CLUTTER_TEXT (priv->pTrueState), &priv->MarkupFontColor);
	clutter_text_set_color (CLUTTER_TEXT (priv->pFalseState), &priv->MarkupFontColor);
	v_radio_button_update_view(pRadioButton);
	g_object_notify (G_OBJECT(pRadioButton), "highlight");
}


/**
 * mildenhall_radio_button_set_button_id:
 * @pObject: radio button object reference
 * @pButtonId: ID for button, which is sent back in call back
 *
 * Button id needs to be unique and is a generic gpointer
 *
 */


void mildenhall_radio_button_set_button_id(MildenhallRadioButton *pRadioButton,  gpointer *pButtonId)
{

	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid  radio button object\n");
		return;
	}

	priv->pButtonID = pButtonId ;

}


/**
 * mildenhall_radio_button_set_group_id:
 * @pObject: radio button object reference
 * @pValue: new value for the property
 *
 * Group ID can be App Name and must not be a NULL. Preferable to set this property prior
 * to other properties apart from width,height.
 *
 */


void mildenhall_radio_button_set_group_id(MildenhallRadioButton *pRadioButton,  const GValue *pValue)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid  radio button object\n");
		return;
	}
	if(NULL == g_value_get_string(pValue))
	{
		g_warning("Group Id for the radio button is NULL \n");
		return;
	}
	/* return if group Id to be set is same as previous */

	if(0 == g_strcmp0(g_value_get_string(pValue),priv->pRbGroupID))
	{
		return;
	}

	RADIO_BUTTON_FREE_MEM_IF_NOT_NULL(priv->pRbGroupID );
	priv->pRbGroupID = g_value_dup_string(pValue);;
	g_object_notify (G_OBJECT(pRadioButton), "group-id");
	if(TRUE == priv->bCurrentState )
	{
		v_radio_button_judge_view(pRadioButton,priv->bCurrentState);
	}
}





/******************************************************
 * getter functions
 * ****************************************************/

/**
 * mildenhall_radio_button_get_group_id:
 * @pObject: radio button object reference
 *
 * Function to get the radio button Group Id
 *
 * Returns: Group Id of the radio button
 */


gchar* mildenhall_radio_button_get_group_id(MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return NULL;
	}

	return priv->pRbGroupID ;

}



/**
 * mildenhall_radio_button_get_button_id:
 * @pObject: radio button object reference
 *
 * Function to get the radio button ID
 *
 * Returns: Button ID
 */


gpointer *mildenhall_radio_button_get_button_id(MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return NULL;
	}

	return priv->pButtonID ;

}


/**
 * mildenhall_radio_button_get_width:
 * @pObject: radio button object reference
 *
 * Function to get the radio button width
 *
 * Returns: width of the radio button
 */
gfloat mildenhall_radio_button_get_width( MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return -1;
	}

	return priv->fltWidth;

}



/**
 * mildenhall_radio_button_get_height:
 * @pObject: radio button object reference
 *
 * Function to get the radio button height
 *
 * Returns: height of the radio button
 */
gfloat mildenhall_radio_button_get_height( MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return -1;
	}

	return priv->fltHeight;

}



/**
 * mildenhall_radio_button_get_current_state:
 * @pObject: radio button object reference
 *
 * Function to get the radio button current state
 *
 * Returns: current state of the radio button
 */
gboolean mildenhall_radio_button_get_current_state( MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return FALSE;
	}

	return priv->bCurrentState;

}



/**
 * mildenhall_radio_button_get_highlight:
 * @pObject: radio button object reference
 *
 * Function to get the Active / normal state of button
 *
 * Returns: highlight state of the button
 */

gboolean mildenhall_radio_button_get_highlight(MildenhallRadioButton *pRadioButton)
{

	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid  radio button object\n");
		return FALSE;
	}
	return priv->bHighlight;
}


/******************************************************************************
 * @local function definitions
 *****************************************************************************/

/********************************************************
 * Function :v_radio_button_set_width
 * Description: set radio button  width internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_radio_button_set_width(MildenhallRadioButton *pRadioButton, gfloat fltWidth)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return ;
	}

	if(priv->fltWidth != fltWidth)
	{
		clutter_actor_set_width(CLUTTER_ACTOR(pRadioButton), fltWidth);
		priv->fltWidth = fltWidth;
	}
	g_object_notify (G_OBJECT(pRadioButton), "width");

}

/********************************************************
 * Function :v_radio_button_set_height
 * Description: set radio button  height internally
 * Parameters: The object reference, new value
 * Return value: void
 ********************************************************/
static void v_radio_button_set_height(MildenhallRadioButton *pRadioButton, gfloat fltHeight)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return ;
	}

	if(priv->fltHeight != fltHeight)
	{
		clutter_actor_set_height(CLUTTER_ACTOR(pRadioButton), fltHeight);
		priv->fltHeight = fltHeight;
	}
	g_object_notify (G_OBJECT(pRadioButton), "height");
}



/********************************************************
 * Function : mildenhall_radio_button_get_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/


static void mildenhall_radio_button_get_property (GObject *pObject,guint uinProperty_id, GValue *pValue,GParamSpec   *pSpec)
{
	MildenhallRadioButton *pRadioButton = MILDENHALL_RADIO_BUTTON (pObject);
	MILDENHALL_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pObject))
	{
		g_warning("invalid radio button object\n");
		return;
	}

	switch (uinProperty_id)
	{
	case PROP_WIDTH:
		g_value_set_float (pValue, mildenhall_radio_button_get_width(pRadioButton) );
		break;
	case PROP_HEIGHT:
		g_value_set_float (pValue, mildenhall_radio_button_get_height(pRadioButton) );
		break;
	case PROP_CURRENT_STATE:
		g_value_set_boolean (pValue, mildenhall_radio_button_get_current_state(pRadioButton) );
		break;
	case PROP_GROUP_ID:
		g_value_set_string (pValue, mildenhall_radio_button_get_group_id(pRadioButton) );
		break;
	case PROP_BUTTON_ID:
		g_value_set_pointer(pValue, mildenhall_radio_button_get_button_id(pRadioButton) );
		break;
	case PROP_HIGHLIGHT:
		g_value_set_boolean (pValue, mildenhall_radio_button_get_highlight(pRadioButton) );
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pSpec);
		break;
	}
}


/********************************************************
 * Function : mildenhall_radio_button_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/


static void mildenhall_radio_button_set_property (GObject *pObject,guint uinProperty_id, const GValue *pValue,GParamSpec   *pSpec)
{
	MildenhallRadioButton *pRadioButton = MILDENHALL_RADIO_BUTTON (pObject);
	MILDENHALL_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);

	if(!MILDENHALL_IS_RADIO_BUTTON(pObject))
	{
		g_warning("invalid radio button object\n");
		return;
	}
	switch (uinProperty_id)
	{
	case PROP_WIDTH:
		v_radio_button_set_width(pRadioButton, g_value_get_float (pValue) );
		break;
	case PROP_HEIGHT:
		v_radio_button_set_height(pRadioButton, g_value_get_float (pValue) );
		break;
	case PROP_CURRENT_STATE:
		mildenhall_radio_button_set_current_state(pRadioButton,g_value_get_boolean(pValue));
		break;
	case PROP_GROUP_ID:
		mildenhall_radio_button_set_group_id(pRadioButton,pValue);
		break;
	case PROP_BUTTON_ID:
		mildenhall_radio_button_set_button_id(pRadioButton, g_value_get_pointer (pValue) );
		break;
	case PROP_HIGHLIGHT :
		mildenhall_radio_button_set_highlight(pRadioButton,g_value_get_boolean(pValue));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pSpec);
		break;
	}
}


/********************************************************
 * Function : v_radio_button_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_radio_button_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallRadioButton *pRadioButton = pUserData;
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE(pRadioButton);

	/* store the button markup string  in private variables */

	if(g_strcmp0(pStyleKey, RADIO_BUTTON_TRUE_STATE_MARKUP) == 0)
	{
		priv->pTrueStateImage = (gchar*)g_value_get_string(pValue);
	}
	else if(g_strcmp0(pStyleKey, RADIO_BUTTON_FALSE_STATE_MARKUP) == 0)
	{
		priv->pFalseStateImage = (gchar*)g_value_get_string(pValue);
	}
	else if(g_strcmp0(pStyleKey, RADIO_BUTTON_FONT_NAME) == 0)
	{
		priv->pMarkupFont = (gchar*) g_value_get_string(pValue);
	}
	else if(g_strcmp0(pStyleKey, RADIO_BUTTON_FONT_COLOR_NORMAL) == 0)
	{
		clutter_color_from_string(&priv->MarkupFontColorNormal, g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, RADIO_BUTTON_FONT_COLOR_ACTIVE) == 0)
	{
		clutter_color_from_string(&priv->MarkupFontColorActive, g_value_get_string(pValue));
	}
	g_free(pStyleKey);
}



/********************************************************
 * Function : mildenhall_radio_button_dispose
 * Description: Dispose the mildenhall radio button object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void mildenhall_radio_button_dispose (GObject *pObject)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE(pObject);
	MildenhallRadioButtonClass *pKlass = MILDENHALL_RADIO_BUTTON_GET_CLASS(pObject);
	MILDENHALL_RADIO_BUTTON_PRINT ("%s \n",__FUNCTION__);
	if(NULL != priv)
	{
		/* Decrease the instance count */
		pKlass->inInstanceCount--;
		if(priv->pRbGroupID  != NULL)
		{
			MILDENHALL_RADIO_BUTTON_PRINT("..............Button Group Id %s \n\n",priv->pRbGroupID );

			if(NULL != pKlass->pGroupID )
			{
				MildenhallRadioButton *pResetButton = (MildenhallRadioButton *)g_hash_table_lookup(pKlass->pGroupID,priv->pRbGroupID );
				MILDENHALL_RADIO_BUTTON_PRINT(".........<><><><><><.....Button Group Id %s  %p \n\n",priv->pRbGroupID , pResetButton );
				if(pResetButton != NULL)
				{
					/* check if both refer to same instance */
					if(MILDENHALL_RADIO_BUTTON(pObject) == MILDENHALL_RADIO_BUTTON(pResetButton))
					{
						MILDENHALL_RADIO_BUTTON_PRINT(".........<><><><><><.....disposed and removed ....!!!\n\n" );
						g_hash_table_remove(pKlass->pGroupID,priv->pRbGroupID );
					}
				}
			}
			g_free(priv->pRbGroupID);
			priv->pRbGroupID = NULL;
		}

		if(pKlass->inInstanceCount == 0   &&   NULL != pKlass->pGroupID)
		{
			MILDENHALL_RADIO_BUTTON_PRINT(".........<><><><><><.....hash table destroyed....!!!\n\n" );
			g_hash_table_destroy(pKlass->pGroupID);
			pKlass->pGroupID = NULL;

		}
	}
	G_OBJECT_CLASS (mildenhall_radio_button_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : mildenhall_radio_button_finalize
 * Description: Finalize the mildenhall radio button object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void mildenhall_radio_button_finalize (GObject *pObject)
{
	MILDENHALL_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);
	G_OBJECT_CLASS (mildenhall_radio_button_parent_class)->finalize (pObject);

}


/********************************************************
 * Function : mildenhall_radio_button_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_radio_button_class_init (MildenhallRadioButtonClass *pKlass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
	GParamSpec *pSpec = NULL;
	MILDENHALL_RADIO_BUTTON_PRINT("\n\n OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO %s \n",__FUNCTION__);

	g_type_class_add_private (pKlass, sizeof (MildenhallRadioButtonPrivate));

	pObjectClass->get_property = mildenhall_radio_button_get_property;
	pObjectClass->set_property = mildenhall_radio_button_set_property;
	pObjectClass->dispose = mildenhall_radio_button_dispose;
	pObjectClass->finalize = mildenhall_radio_button_finalize;

	pKlass->pGroupID = NULL;
	pKlass->inInstanceCount = 0;
	pKlass->pLocalGroupId = NULL;
	pKlass->pRadioButonlocal = NULL;

	/* Create a signal for the button to indicate state change */
	/**
	 * MildenhallRadioButton:: radio button:
	 * @mildenhallRadioButton: The object which received the signal
	 * @oldState: name of the old state
	 * @newState: name of the new state
	 *
	 * ::state-change is emitted when button is changed from true state to flase and vice versa
	 */
	mildenhall_radio_bt_signals[SIG_RB_STATE_CHANGE] = g_signal_new ("state-change",
			G_TYPE_FROM_CLASS (pObjectClass),
			G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
			G_STRUCT_OFFSET (MildenhallRadioButtonClass, rb_state_change),
			NULL, NULL,
			g_cclosure_marshal_generic,
			G_TYPE_NONE, 2, G_TYPE_BOOLEAN , G_TYPE_POINTER);

	/**
	 * LightwoodButton:width:
	 *
	 * Width of the button
	 * Default: 64.0 px
	 */
	pSpec = g_param_spec_float ("width",
			"RBWidth",
			"Width of the radio button",
			0.0, G_MAXFLOAT,
			64.0,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_WIDTH, pSpec);

	/**
	 * LightwoodButton:height:
	 *
	 * Height of the button
	 * Default: 64.0 px
	 */
	pSpec = g_param_spec_float ("height",
			"RBHeight",
			"Height of the radio button",
			0.0, G_MAXFLOAT,
			64.0,
			G_PARAM_READWRITE);
	g_object_class_install_property ( pObjectClass, PROP_HEIGHT, pSpec);


	/**
	 * MildenhallRadioButton:current-state:
	 *
	 * gives the current state of the button as state index
	 * Default: FALSE
	 */
	pSpec = g_param_spec_boolean ("current-state",
			"CurrentState",
			"Current state of the button",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_CURRENT_STATE, pSpec);

	/**
	 * MildenhallRadioButton:current-state:
	 *
	 * gives the current state of the button as state index
	 * Default: NULL
	 */
	pSpec = g_param_spec_string ("group-id",
			"GroupId",
			"Group ID of the button",
			(const gchar*) "",
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_GROUP_ID, pSpec);


	/**
	 * MildenhallRadioButton:button-id:
	 *
	 * button-id as a identity for button
	 * Button ID is generic pointer which is returned on signal omission
	 */
	pSpec = g_param_spec_pointer("button-id",
			"ButtonId",
			"Button-ID for the radio button",
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_BUTTON_ID, pSpec);


	/**
	 * MildenhallRadioButton:highlight:
	 *
	 * Sets the button to highlight  or normal state
	 * The widget will highlight only one button in  group at a time.
	 * If one button is already highlighted or two button in a group is tried
	 * to be highlighted then resent request is recognised
	 * and other button is turned to normal state.
	 * Default: Normal state i.e, False
	 */
	pSpec = g_param_spec_boolean ("highlight",
			"HighLight",
			"Highlight state of the button ;true/false",
			FALSE,
			G_PARAM_READWRITE);
	g_object_class_install_property (pObjectClass, PROP_HIGHLIGHT, pSpec);



	/* create new Hash table to hold key as Group Id of button
	 * the value as button instance (MildenhallRadioButton*)
	 */
	pKlass->pGroupID =  g_hash_table_new_full(g_str_hash, g_str_equal,g_free , NULL);
}


/********************************************************
 * Function : mildenhall_radio_button_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_radio_button_init (MildenhallRadioButton *self)
{
	MildenhallRadioButtonClass *pKlass = MILDENHALL_RADIO_BUTTON_GET_CLASS (self);
        GHashTable *pStyleHash;
	g_autofree gchar *style_file = NULL;
	MILDENHALL_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);
	self->priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (self);


	/* initialize the priv structure */

	self->priv->fltWidth = 64.0;
	self->priv->fltHeight = 64.0;
	self->priv->bActive = TRUE;
	self->priv->bCurrentState = FALSE;
	self->priv->pButtonGroup = NULL ;
	self->priv->pRbGroupID  = NULL;
	self->priv->pButtonID = NULL;
	self->priv->pTrueState = NULL;
	self->priv->pFalseState = NULL;
	self->priv->pTrueStateImage = NULL;
	self->priv->pFalseStateImage = NULL;
	self->priv->bHighlight = FALSE;
	self->priv->bResetNeeded = FALSE;
	self->priv->bChangeLock = FALSE;


	/* initialise the button group */
	self->priv->pButtonGroup = clutter_actor_new ();
	clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pButtonGroup);

	/* get the hash table for style properties */
	style_file = _mildenhall_get_resource_path ("mildenhall_radio_button_style.json");
	pStyleHash = thornbury_style_set (style_file);

	/* pares the hash for styles */
	if (NULL != pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init (&iter, pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				g_hash_table_foreach(pHash, v_radio_button_parse_style, self);
			}
		}
	}
        /* load markup data before freeing hashtable */
        v_radio_button_create_new_markup_icon (self);
	/* free the style hash */
	thornbury_style_free (pStyleHash);


	/* create new Hash table to hold key as Group Id of button
	 * the value as button instance (MildenhallRadioButton*)
	 */
	if(NULL == pKlass->pGroupID )
	{
		pKlass->pGroupID =  g_hash_table_new_full(g_str_hash, g_str_equal,g_free , NULL);
	}



	/* increase the Instance count */
	pKlass->inInstanceCount++;
	/* connect the signals */
	g_signal_connect(self, "button-press", G_CALLBACK(v_radio_button_press_cb), NULL);

	/* connect to reactive state to update the button view  */
	g_signal_connect (self, "notify::reactive", G_CALLBACK (v_radio_button_active_cb),NULL);
	g_signal_connect (self, "notify::width", G_CALLBACK (v_radio_button_resize_view),NULL);
	g_signal_connect (self, "notify::height", G_CALLBACK (v_radio_button_resize_view),NULL);

}


/**
 * mildenhall_radio_button_new:
 *
 * Creates a Radio button object
 *
 * Returns: (transfer full): Radio button object
 */


ClutterActor *mildenhall_radio_button_new (void)
{
	MILDENHALL_RADIO_BUTTON_PRINT("%s \n",__FUNCTION__);
	return g_object_new (MILDENHALL_TYPE_RADIO_BUTTON, NULL);
}



/****************************************************************
 * Function : v_radio_button_active_cb
 * Description: callback for reactive state notification
 * Parameters: The object, GParamSpec *, gpointer
 * Returns: void
 ****************************************************************/
static void v_radio_button_active_cb (MildenhallRadioButton *pRadioButton, GParamSpec *pSpec, gpointer user_data)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	gboolean bState;
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid radio button object\n");
		return ;
	}

	g_object_get (pRadioButton, "reactive", &bState, NULL);
	if(priv->bActive != bState)
	{
		priv->bActive = bState;
		MILDENHALL_RADIO_BUTTON_PRINT("\n \n reactive STATE = %s\n", (priv->bActive)?"true":"false");
	}
}




/********************************************************
 * Function : v_radio_button_press_cb
 * Description: callabck on radio button press
 * Parameters: MildenhallRadioButton*, pUserData
 * Return value: void
 ********************************************************/
static void v_radio_button_press_cb(ClutterActor *pRadioButton, gpointer pUserData)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	gboolean bstate;
	MILDENHALL_RADIO_BUTTON_PRINT ("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_RADIO_BUTTON(pRadioButton))
	{
		g_warning("invalid button\n");
		return;
	}
	/* check if the button property reactive is set true */
	if(TRUE == priv->bActive)
	{
		bstate = !priv->bCurrentState;
		g_object_set(pRadioButton, "current-state", bstate, NULL);

	}
}


/********************************************************
 * Function : v_radio_button_create_new_markup_icon
 * Description: create a group to hold icon
 * Parameters: The object
 * Return value: void
 ********************************************************/
static void v_radio_button_create_new_markup_icon(MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);

	priv->MarkupFontColor = (priv->bHighlight) ? priv->MarkupFontColorActive : priv->MarkupFontColorNormal;
	if(NULL != priv->pTrueStateImage)
	{
		priv->pTrueState = clutter_text_new ();
		clutter_text_set_color (CLUTTER_TEXT (priv->pTrueState), &priv->MarkupFontColor);
		clutter_text_set_font_name (CLUTTER_TEXT (priv->pTrueState), priv->pMarkupFont );
		clutter_text_set_use_markup (CLUTTER_TEXT (priv->pTrueState), TRUE);
		/* Use the Right arrow markup from style property */
		clutter_text_set_markup (CLUTTER_TEXT (priv->pTrueState), priv->pTrueStateImage );
		clutter_actor_set_position(priv->pTrueState,(priv->fltWidth - clutter_actor_get_width (priv->pTrueState)) / 2,
				(priv->fltHeight - clutter_actor_get_height (priv->pTrueState)) / 2 );
		clutter_actor_add_child (priv->pButtonGroup,priv->pTrueState);
	}

	if(NULL != priv->pFalseStateImage   )
	{
		priv->pFalseState = clutter_text_new ();
		clutter_text_set_color (CLUTTER_TEXT (priv->pFalseState), &priv->MarkupFontColor);
		clutter_text_set_font_name (CLUTTER_TEXT (priv->pFalseState), priv->pMarkupFont);
		clutter_text_set_use_markup (CLUTTER_TEXT (priv->pFalseState), TRUE);
		/* Use the Left arrow markup from style property */
		clutter_text_set_markup (CLUTTER_TEXT (priv->pFalseState), priv->pFalseStateImage);
		clutter_actor_set_position(priv->pFalseState,(priv->fltWidth - clutter_actor_get_width (priv->pTrueState)) / 2,
				(priv->fltHeight - clutter_actor_get_height (priv->pTrueState)) / 2 );
		clutter_actor_add_child (priv->pButtonGroup,priv->pFalseState);
	}

}

/********************************************************
 * Function : v_radio_button_update_view
 * Description: update the button view
 * Parameters: MildenhallRadioButton *
 * Return value: void
 ********************************************************/
static void v_radio_button_update_view(MildenhallRadioButton *pRadioButton)
{
	MildenhallRadioButton *pRadiobuton = MILDENHALL_RADIO_BUTTON (pRadioButton);
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pRadioButton);
	MildenhallRadioButtonClass *pKlass = MILDENHALL_RADIO_BUTTON_GET_CLASS (pRadioButton);
        gchar *pFunGroupId;
	MILDENHALL_RADIO_BUTTON_PRINT("MILDENHALL_RADIO_BUTTON_PRINT: %s\n", __FUNCTION__);


	pFunGroupId = g_strdup (priv->pRbGroupID);
	/* to Prevent recursive call to set current state boolean is used */
	priv->bStateChangeInProgress = TRUE;
	//pLocalGroupId = priv->pRbGroupID ;
	if (NULL != pFunGroupId)
	{
		/* check if Button instance is found for the Group ID */
		MildenhallRadioButton *pResetButton = g_hash_table_lookup (pKlass->pGroupID, pFunGroupId);

		if (NULL != pResetButton)
		{
			/* check if both refer to same instance */
			if (pRadiobuton != MILDENHALL_RADIO_BUTTON (pResetButton))
			{
				if (TRUE == priv->bResetNeeded)
				{
					priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE (pResetButton);
					/* Place a lock to indicate state change request from button Instance */
					priv->bChangeLock = TRUE;
					/* Reset the other button Instance from present button instance */
					g_object_set (pResetButton, "current-state", FALSE, NULL);
					priv->bChangeLock = FALSE;
					priv->bResetNeeded = FALSE;
				}
				/* set the other button instance to Normal state */
				g_object_set (pResetButton, "highlight", FALSE, NULL);
			}
		}
	}
	priv->bStateChangeInProgress = FALSE ;
	g_free (pFunGroupId);
	pFunGroupId = NULL ;

	if(NULL != priv->pTrueState &&  NULL != priv->pFalseState )
	{
		/* update icon depending on state of button */
		if(TRUE == priv->bCurrentState )
		{
			clutter_actor_hide(priv->pFalseState);
			clutter_actor_show(priv->pTrueState);

		}
		else
		{
			clutter_actor_hide(priv->pTrueState );
			clutter_actor_show(priv->pFalseState);
		}
	}

}


/********************************************************
 * Function : v_radio_button_resize_view
 * Description: update the button dimensions in case dimension of button changes.
 * Parameters: MildenhallRadioButton * , GparamSpec* , gpointer
 * Return value: void
 ********************************************************/
static void v_radio_button_resize_view(MildenhallRadioButton *pRadioButton, GParamSpec *pspec, gpointer user_data)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE(pRadioButton);
        gfloat fltHeight, fltWidth;
	if(NULL == priv->pTrueState || NULL == priv->pFalseState )
	{
		return;
	}

	/* if markup size is more then the button width adjust the button width
	 * to markup width  to reatin minimum possible dimension based on the Markup font */
	/* True State*/
	fltHeight = clutter_actor_get_height(priv->pTrueState);
	if(fltHeight > clutter_actor_get_height(priv->pButtonGroup))
	{
		g_object_set(priv->pButtonGroup, "height", fltHeight, NULL);
	}

	fltWidth = clutter_actor_get_width(priv->pTrueState);
	if(fltWidth > priv->fltWidth)
	{
		g_object_set(priv->pButtonGroup, "width", fltWidth, NULL);
	}

	clutter_actor_set_position(priv->pTrueState,(priv->fltWidth - fltWidth) / 2,
			(priv->fltHeight - fltHeight) / 2 );


	/* if markup size is more then the button width adjust the button width
	 * to markup width  to reatin minimum possible dimension based on the Markup font */
	/* False State*/
	fltHeight = clutter_actor_get_height(priv->pFalseState);
	if(fltHeight > clutter_actor_get_height(priv->pButtonGroup))
	{
		g_object_set(priv->pButtonGroup, "height", fltHeight, NULL);
	}

	fltWidth = clutter_actor_get_width(priv->pFalseState);
	if(fltWidth > priv->fltWidth)
	{
		g_object_set(priv->pButtonGroup, "width", fltWidth, NULL);
	}

	clutter_actor_set_position(priv->pFalseState,(priv->fltWidth - fltWidth) / 2,
			(priv->fltHeight - fltHeight) / 2 );

}



static void v_radio_button_update_hash_table(MildenhallRadioButton *pRadioButton  )
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE(pRadioButton);
	MildenhallRadioButtonClass *pKlass = MILDENHALL_RADIO_BUTTON_GET_CLASS(pRadioButton);
	/* update the Hash table only if the state of the button is changed to True */
		if(TRUE == priv->bCurrentState  )
		{

			/* store the button instance in static  global variable to insert into hash table */
			pKlass->pRadioButonlocal = pRadioButton;
			pKlass->pLocalGroupId = priv->pRbGroupID;

			if(!g_hash_table_size(pKlass->pGroupID))
			{
				/* First entry to Hash Table */
				g_hash_table_insert(pKlass->pGroupID,g_strdup(pKlass->pLocalGroupId),pKlass->pRadioButonlocal);
			}
			else
			{
				/* insert the currently touched button to Hash table */
				if(NULL != pKlass->pLocalGroupId )
				{
					g_hash_table_insert(pKlass->pGroupID,g_strdup(pKlass->pLocalGroupId),pKlass->pRadioButonlocal);
				}
			}
		}
}

/********************************************************
 * Function : v_radio_button_judge_view
 * Description: judge  the button state based on other group buttons
 * Parameters: MildenhallRadioButton * , GparamSpec* , gpointer
 * Return value: void
 ********************************************************/

static void v_radio_button_judge_view(MildenhallRadioButton *pRadioButton, gboolean bState)
{
	MildenhallRadioButtonPrivate *priv = MILDENHALL_RADIO_BUTTON_GET_PRIVATE(pRadioButton);
	MildenhallRadioButtonClass *pKlass = MILDENHALL_RADIO_BUTTON_GET_CLASS(pRadioButton);

	gchar *pFunGroupId = g_strdup(priv->pRbGroupID) ;

	if(NULL != priv->pRbGroupID  )
	{
		if(0 != g_hash_table_size(pKlass->pGroupID) )
		{
			/*  Entry present in Hash Table indcating some button is already set to True state */
			/* The Look for button previously set to true belongs to same group */
			MildenhallRadioButton *pResetButton = g_hash_table_lookup(pKlass->pGroupID,pFunGroupId);
			if(NULL != pResetButton )
			{
				if(pRadioButton == MILDENHALL_RADIO_BUTTON(pResetButton))
				{
					/* Last button set to true and requested button are same instance of same group*/
					priv->bResetNeeded = FALSE;
					if(TRUE == priv->bChangeLock )
					{
						/* Check whether the request is from another button instance */
						priv->bCurrentState = bState;
					}
					else
					{
						/* request is direct from app / user and don't change the state to false
						 * as no other button in group is in True state
						 */
						if(TRUE == bState )
						{
							priv->bCurrentState = TRUE;
						}
					}
				}
				else
				{
					/* Last button set to true and requested button are different instance of same group*/
					/* Reset of another button instance has to be made */
					if(TRUE == bState )
					{
						priv->bResetNeeded = TRUE;
						priv->bCurrentState = TRUE;
					}
				}
			}
			else
			{
				/* No entry of any button instance pertaining to the button Group */
				/* if state request is True set to true state */
				if(TRUE == bState )
				{
					priv->bCurrentState = TRUE;
				}
			}

		}
		else
		{
			/* No entry in Hash table and set the button to true if state requested is true */
			if(TRUE == bState )
			{
				priv->bCurrentState = TRUE;
			}

		}
	}
	/* Update the view based on the judged state of the button */
	v_radio_button_update_view(pRadioButton);

	/* update the Hash table only if the state of the button is changed to True */
	v_radio_button_update_hash_table(pRadioButton  );

	RADIO_BUTTON_FREE_MEM_IF_NOT_NULL( pFunGroupId );
}

/*** END OF FILE *************************************************************/
