/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_key_pad.c */

#include "mildenhall_speller_key_pad.h"

#include <thornbury/thornbury.h>

#define MILDENHALL_SPELLER_KEYPAD_PRINT(...)  //g_print(__VA_ARGS__)

G_DEFINE_TYPE (MildenhallSpellerKeyPad, mildenhall_speller_key_pad, CLUTTER_TYPE_ACTOR)

#define SPELLER_KEY_PAD_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_KEY_PAD, MildenhallSpellerKeyPadPrivate))

static guint32 mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_LAST_SIGNAL] = {0,};
static GList *p_convert_json_to_hash(gchar *pFile);


/********************************************************
 * Function : free_data_function
 * Description:function to free data
 * Parameters: gchar*
 * Return value: void
 ********************************************************/
static void free_data_function(gchar* data)
{
	if(NULL != data)
	{
		g_free(data);
		data = NULL;
	}
}

/********************************************************
 * Function : b_leave_event_cb
 * Description:callback function , invoke at event leave
 * Parameters: ClutterActor,ClutterButtonEvent,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean b_leave_event_cb(ClutterActor *pActor, ClutterButtonEvent *pEvent, gpointer pUserData)
{
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_KEY_PAD (pUserData) , FALSE);

	g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData),mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_LEAVE],
			0,(gchar*)clutter_actor_get_name(pActor));

	return FALSE;
}

/********************************************************
 * Function : v_update_default_text
 * Description:function to update the text
 * Parameters:gpointer
 * Return value: void
 ********************************************************/
static void v_update_default_text(gpointer pUserData)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;
        int inCnt = 0;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	if(!MILDENHALL_IS_SPELLER_KEY_PAD(pUserData))
	{
		g_warning("invalid speller object\n");
		return;
	}
	for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->buttonList); inCnt++)
	{
		ClutterActor *pDefaultButton = g_list_nth_data(priv->buttonList,inCnt);
		ThornburyModel *model = NULL;
                MildenhallButtonInfo *pDefaultData = g_list_nth_data (priv->pButtonDataList, inCnt);
                GValue value = {0};
		g_object_get(pDefaultButton,"model",&model,NULL);
		g_value_init(&value, G_TYPE_STRING);
		g_value_set_static_string (&value,g_strdup(pDefaultData->defaultText));
		thornbury_model_insert_value(model, 0, 0, &value);
		g_value_unset (&value);
	}
}

/********************************************************
 * Function : v_update_shift_text
 * Description:function to update the shift text
 * Parameters:gpointer
 * Return value: void
 ********************************************************/
static void v_update_shift_text(gpointer pUserData)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;
        int inCnt = 0;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	if(!MILDENHALL_IS_SPELLER_KEY_PAD(pUserData))
	{
		g_warning("invalid speller object\n");
		return;
	}
	for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->buttonList); inCnt++)
	{
		ClutterActor *pDefaultButton = g_list_nth_data(priv->buttonList,inCnt);
		ThornburyModel *model = NULL;
                MildenhallButtonInfo *pDefaultData = NULL;
                GValue value = {0};
		g_object_get(pDefaultButton,"model",&model,NULL);
		pDefaultData = g_list_nth_data (priv->pButtonDataList,inCnt);
		g_value_init(&value, G_TYPE_STRING);
		g_value_set_static_string (&value,g_strdup(pDefaultData->shiftText));
		thornbury_model_insert_value(model, 0, 0, &value);
		g_value_unset (&value);
	}
}

/********************************************************
 * Function : v_update_symbol_text
 * Description:function to update the symbol text
 * Parameters:gpointer
 * Return value: void
 ********************************************************/
static void v_update_symbol_text(gpointer pUserData)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;
        int inCnt = 0;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	if(!MILDENHALL_IS_SPELLER_KEY_PAD(pUserData))
	{
		g_warning("invalid speller object\n");
		return;
	}
	for (inCnt = 0; (unsigned int) inCnt < g_list_length (priv->buttonList); inCnt++)
	{
		ClutterActor *pDefaultButton = g_list_nth_data(priv->buttonList,inCnt);
		ThornburyModel *model = NULL;
                MildenhallButtonInfo *pDefaultData = NULL;
                GValue value = {0};
		g_object_get(pDefaultButton,"model",&model,NULL);
		pDefaultData = g_list_nth_data (priv->pButtonDataList, inCnt);
		g_value_init(&value, G_TYPE_STRING);
		g_value_set_static_string (&value,g_strdup(pDefaultData->symbolText));
		thornbury_model_insert_value(model, 0, 0, &value);
		g_value_unset (&value);
	}
}

/********************************************************
 * Function : b_release_cb
 * Description:callback function , invoke at key release
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean b_release_cb(ClutterActor *pActor, gpointer pUserData)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;
        ThornburyModel *infoModel = NULL;
        gchar *valueStr = NULL;
        ThornburyModelIter *pIter = NULL;

	MILDENHALL_SPELLER_KEYPAD_PRINT(" MILDENHALL_SPELLER_KEYPAD_PRINT  : %s %d %s \n",__FUNCTION__ ,__LINE__,clutter_actor_get_name(pActor));
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_KEY_PAD (pUserData) , FALSE);

	if(priv->isLongpress)
	{
		if(priv->longpress_source)
		{
			g_source_remove(priv->longpress_source);
			priv->longpress_source = 0;
		}
		priv->isLongpress = FALSE;
	}


	g_object_get(pActor,"model",&infoModel,NULL);

	pIter = thornbury_model_get_iter_at_row (infoModel, 0);
	if(NULL != pIter)
	{
		GValue value = { 0, };
                gchar* pData = NULL;
		thornbury_model_iter_get_value(pIter, 0, &value);
		valueStr = (gchar*)g_value_get_string(&value);

		pData = g_strrstr_len (priv->layout_file, -1, "phone_style.json");
		if(pData != NULL)
		{
			valueStr = (gchar*)g_strdup(clutter_actor_get_name(pActor));
		}
		if(!g_strcmp0(valueStr,"GO!") || (!g_strcmp0(valueStr,"RETURN")))
		{
			valueStr = g_strdup("go");
		}
		if(!g_strcmp0(clutter_actor_get_name(pActor),"SHIFT"))
		{
			valueStr = g_strdup("SHIFT");
		}
	}

	g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData), mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_RELEASED], 0, valueStr);

	if(FALSE == priv->isShift && !g_strcmp0((gchar*)clutter_actor_get_name(pActor),"SHIFT") && priv->isLongShift == FALSE)
	{
		priv->isShift = TRUE;
		priv->isLongShift = FALSE;
		clutter_actor_show(priv->pressedImage);
	}
	else if(TRUE == priv->isShift && g_strcmp0((gchar*)clutter_actor_get_name(pActor),"SYMBOLS"))
	{
		clutter_actor_hide(priv->pressedImage);
		v_update_default_text(pUserData);
		priv->isShift = FALSE;
	}
	else if(TRUE == priv->isLongShift && !g_strcmp0((gchar*)clutter_actor_get_name(pActor),"SHIFT"))
	{
		v_update_shift_text(pUserData);
		priv->isSymbol = FALSE;
	}

	if(!g_strcmp0("go",clutter_actor_get_name(pActor)) || !g_strcmp0("RETURN",clutter_actor_get_name(pActor)))
	{
		g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData), mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_GO_PRESS], 0, NULL);
	}
	return FALSE;
}

static gboolean b_longpress_key_cb(gpointer pUserData)
{
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;

	if(priv->isLongpress)
	{
		if(!g_strcmp0(clutter_actor_get_name(priv->pButtonPressedActor),"SHIFT"))
			{
				v_update_shift_text(pUserData);
				priv->isLongShift = TRUE; // long-press enabled
				priv->isSymbol = FALSE;
				priv->isShift = FALSE;
				clutter_actor_show(priv->pressedImage);
			}
			g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData), mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_CLICK], 0,(gchar*)clutter_actor_get_name(priv->pButtonPressedActor));
	}

	return FALSE;
}


/********************************************************
 * Function : b_press_cb
 * Description:callback function , invoke at key press
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
static gboolean b_press_cb(ClutterActor *pActor, gpointer pUserData)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n MILDENHALL_SPELLER_KEYPAD_PRINT : %s %d %s \n",__FUNCTION__ ,__LINE__,clutter_actor_get_name(pActor));
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_KEY_PAD (pUserData) , FALSE);

	priv->isLongpress = TRUE;
	priv->pButtonPressedActor = pActor;

	if(!g_strcmp0("SYMBOLS",clutter_actor_get_name(pActor)))
	{
		if(FALSE == priv->isSymbol)
		{
			v_update_symbol_text(pUserData);
			priv->isSymbol = TRUE;
			priv->isShift = FALSE;
		}
		else
		{
			v_update_default_text(pUserData);
			priv->isSymbol = FALSE;
		}
		clutter_actor_hide(priv->pressedImage);
	}

	if(!g_strcmp0("SHIFT",clutter_actor_get_name(pActor)))
	{
		if(priv->isLongShift == TRUE)
		{
			priv->isShift = !priv->isShift;
			priv->isLongShift = FALSE;
		}
		if(FALSE == priv->isShift)
		{
			v_update_shift_text(pUserData);
			clutter_actor_show(priv->pressedImage);
		}
		else
		{
			v_update_default_text(pUserData);
			clutter_actor_hide(priv->pressedImage);
		}
		priv->isSymbol = FALSE;
	}
	g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData), mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_PRESSED], 0, NULL);
	priv->longpress_source = g_timeout_add(500,b_longpress_key_cb,pUserData);

	return FALSE;
}

/********************************************************
 * Function : b_long_press_cb
 * Description:callback function , invoke at long press
 * Parameters: ClutterActor,gpointer
 * Return value: gboolean
 ********************************************************/
/*
static gboolean b_long_press_cb(ClutterActor *pActor, gpointer pUserData)
{
	MILDENHALL_SPELLER_KEYPAD_PRINT(" MILDENHALL_SPELLER_KEYPAD_PRINT : \n %s %d %s \n",__FUNCTION__ ,__LINE__,clutter_actor_get_name(pActor));
	g_return_val_if_fail(MILDENHALL_IS_SPELLER_KEY_PAD (pUserData) , FALSE);
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pUserData)->priv;

	if(!g_strcmp0(clutter_actor_get_name(pActor),"SHIFT"))
	{
		v_update_shift_text(pUserData);
		priv->isLongShift = TRUE; // long-press enabled
		priv->isSymbol = FALSE;
		priv->isShift = FALSE;
		clutter_actor_show(priv->pressedImage);
	}
	g_signal_emit(MILDENHALL_SPELLER_KEY_PAD(pUserData), mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_CLICK], 0,(gchar*)clutter_actor_get_name(pActor));
	return TRUE;
}
*/

/********************************************************
 * Function : v_init_struct_member_variables
 * Description:function init the members
 * Parameters: MildenhallSpellerKeyPad
 * Return value: void
 ********************************************************/
static void v_init_struct_member_variables(MildenhallSpellerKeyPad *pSpellerKeyPad)
{
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD(pSpellerKeyPad)->priv;
	priv->layout_file = NULL;
	priv->layout_state = NULL;
	priv->buttonList = NULL;
	priv->isSymbol = FALSE;
	priv->isShift = FALSE;
	priv->isLongShift = FALSE;
	priv->isLongpress = FALSE;
	priv->pressedImage = NULL;
	priv->normalImage = NULL;
}

/********************************************************
 * Function : p_button_info_db
 * Description:function to update properties to set
 * Parameters: MildenhallSpellerKeyPad,GHashTable
 * Return value: MildenhallButtonInfo
 ********************************************************/
static MildenhallButtonInfo *p_button_info_db(MildenhallSpellerKeyPad *pSpellerKeyPad, GHashTable *pHash)
{
	MildenhallButtonInfo *ButtonInfo = g_new0(MildenhallButtonInfo,1);
	GHashTableIter gIter;
	gpointer pKeyInfo, pValueInfo;
	g_hash_table_iter_init(&gIter, pHash);
	while (g_hash_table_iter_next (&gIter, &pKeyInfo, &pValueInfo))
	{
		if(!g_strcmp0(pKeyInfo,"x"))
		{
			ButtonInfo->x = g_value_get_double(pValueInfo);
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->x = %lg ",(gchar*)pKeyInfo,ButtonInfo->x);
		}
		else  if(!g_strcmp0(pKeyInfo,"y"))
		{
			ButtonInfo->y = g_value_get_double(pValueInfo);
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->y = %lg ",(gchar*)pKeyInfo,ButtonInfo->y);
		}
		else  if(!g_strcmp0(pKeyInfo,"width"))
		{
			ButtonInfo->width = g_value_get_double(pValueInfo);
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->width = %lg ",(gchar*)pKeyInfo,ButtonInfo->width);
		}
		else  if(!g_strcmp0(pKeyInfo,"height"))
		{
			ButtonInfo->height = g_value_get_double(pValueInfo);
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->height = %lg ",(gchar*)pKeyInfo,ButtonInfo->height);
		}
		else if(!g_strcmp0(pKeyInfo,"name"))
		{
			ButtonInfo->name = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->name = %s ",(gchar*)pKeyInfo,ButtonInfo->name);
		}
		else  if(!g_strcmp0(pKeyInfo,"type"))
		{
			ButtonInfo->type = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->type = %s ",(gchar*)pKeyInfo,ButtonInfo->type);
		}
		else  if(!g_strcmp0(pKeyInfo,"default-text"))
		{
			ButtonInfo->defaultText = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->defaultText = %s ",(gchar*)pKeyInfo,ButtonInfo->defaultText);
		}
		else  if(!g_strcmp0(pKeyInfo,"shift-text"))
		{
			ButtonInfo->shiftText = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->shiftText = %s ",(gchar*)pKeyInfo,ButtonInfo->shiftText);
		}
		else  if(!g_strcmp0(pKeyInfo,"symbol-text"))
		{
			ButtonInfo->symbolText = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->symbolText = %s ",(gchar*)pKeyInfo,ButtonInfo->symbolText);
		}
		else  if(!g_strcmp0(pKeyInfo,"normal-image"))
		{
			ButtonInfo->normalImage = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->normalImage = %s ",(gchar*)pKeyInfo,ButtonInfo->normalImage);
		}
		else  if(!g_strcmp0(pKeyInfo,"pressed-image"))
		{
			ButtonInfo->pressedImage = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->pressedImage = %s ",(gchar*)pKeyInfo,ButtonInfo->pressedImage);
		}
		else  if(!g_strcmp0(pKeyInfo,"long-press-enabled"))
		{
			ButtonInfo->longPress =g_value_get_boolean(pValueInfo);
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->longPress = %d ",(gchar*)pKeyInfo,ButtonInfo->longPress);
		}
		else  if(!g_strcmp0(pKeyInfo,"font-type"))
		{
			ButtonInfo->fontType = (gchar *)g_strdup(g_value_get_string(pValueInfo));
			//MILDENHALL_SPELLER_KEYPAD_PRINT(" \n pKeyInfo = %s ButtonInfo->fontType = %s ",(gchar*)pKeyInfo,ButtonInfo->fontType);
		}
		else
		{
			g_warning(" key not present \n");
		}
	}
	return ButtonInfo;
}

/********************************************************
 * Function : create_speller_keypad_model
 * Description:function to create speller keypad model
 * Parameters: None
 * Return value: ThornburyModel
 ********************************************************/
ThornburyModel *
create_speller_keypad_model (void)
{
	ThornburyModel *pModel = NULL;
	pModel = (ThornburyModel *)thornbury_list_model_new (1, G_TYPE_STRING, NULL,-1);
	return pModel;
}

/********************************************************
 * Function : v_free_structure_data
 * Description:function to free structure
 * Parameters: MildenhallButtonInfo
 * Return value: void
 ********************************************************/
static void v_free_structure_data( MildenhallButtonInfo *pButtonData )
{
	if(NULL != pButtonData)
	{
		free_data_function(pButtonData->name);
		free_data_function(pButtonData->type);
		free_data_function(pButtonData->defaultText);
		free_data_function(pButtonData->shiftText);
		free_data_function(pButtonData->symbolText);
		free_data_function(pButtonData->normalImage);
		free_data_function(pButtonData->pressedImage);

		g_free(pButtonData);
		pButtonData = NULL;
	}
}

/********************************************************
 * Function : p_speller_construct_button
 * Description:function to construct buttons
 * Parameters: MildenhallSpellerKeyPad,MildenhallButtonInfo
 * Return value: ClutterActor
 ********************************************************/
static ClutterActor* p_speller_construct_button(MildenhallSpellerKeyPad *self,MildenhallButtonInfo *pButtonData)
{
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD(self)->priv;
	ClutterActor *pDefaultButton = g_object_new(MILDENHALL_TYPE_BUTTON_SPELLER,
			"type", pButtonData->type,
			"name", pButtonData->name,
			"width",pButtonData->width,
			"height",pButtonData->height,
			"x",pButtonData->x,
			"y",pButtonData->y,
			"normal-image", pButtonData->normalImage,
			"pressed-image", pButtonData->pressedImage,
			"long-press-enabled",pButtonData->longPress,
			"reactive",TRUE,
			"font-type", pButtonData->fontType,
			"model", priv->pModel,
			NULL);
	return pDefaultButton;
}

/********************************************************
 * Function : v_speller_update_speller_layout
 * Description:function to update the speller layout
 * Parameters: MildenhallSpellerKeyPad,MildenhallButtonInfo
 * Return value: void
 ********************************************************/
static void v_speller_update_speller_layout(MildenhallSpellerKeyPad *self,MildenhallButtonInfo *pButtonData)
{
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD(self)->priv;
	if(!g_strcmp0(priv->layout_state,"upper"))
	{
		thornbury_model_append (priv->pModel,0,g_strdup(pButtonData->shiftText),-1);
		priv->isLongShift = TRUE; // long-press enabled
		priv->isSymbol = FALSE;
		priv->isShift = FALSE;
	}
	else if(!g_strcmp0(priv->layout_state,"symbol"))
	{
		thornbury_model_append (priv->pModel,0,g_strdup(pButtonData->symbolText),-1);
		priv->isSymbol = TRUE;
	}
	else
	{
		thornbury_model_append (priv->pModel,0,g_strdup(pButtonData->defaultText),-1);
	}
}

/********************************************************
 * Function : parse_and_construct_buttons
 * Description:function to parse and construct buttons
 * Parameters: MildenhallSpellerKeyPad,GHashTable
 * Return value: void
 ********************************************************/
static void parse_and_construct_buttons(MildenhallSpellerKeyPad *self,GHashTable *pHashValue)
{
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (self)->priv;
	MILDENHALL_SPELLER_KEYPAD_PRINT("%s %d\n", __FUNCTION__ , __LINE__);
	if( NULL != pHashValue )
	{
		GHashTableIter pIter;
		GHashTable *pHash = NULL;
		gpointer pKeyData, pValueData=NULL;
		g_hash_table_iter_init(&pIter, pHashValue);

		while (g_hash_table_iter_next (&pIter, &pKeyData, &pValueData))
		{
			pHash = pValueData;
			if( NULL != pHash )
			{
				MildenhallButtonInfo *pButtonData = p_button_info_db( self,pHash);
                                ClutterActor *pDefaultButton = NULL;
				priv->pModel = create_speller_keypad_model();

				v_speller_update_speller_layout(self,pButtonData);

				pDefaultButton =  p_speller_construct_button (self, pButtonData);

				if(!g_strcmp0(pButtonData->name,"SHIFT"))
				{
					gchar* pImage = g_strdup_printf(PKGTHEMEDIR"/%s", g_strdup(pButtonData->normalImage));
                                        gchar* pNormalImage = NULL;
					priv->normalImage = thornbury_ui_texture_create_new(pImage,0,0, FALSE, FALSE);
					clutter_actor_add_child(pDefaultButton, priv->normalImage);

					pNormalImage = g_strdup_printf (PKGTHEMEDIR"/%s", g_strdup (pButtonData->pressedImage));
					priv->pressedImage = thornbury_ui_texture_create_new(pNormalImage,0,0, FALSE, FALSE);
					clutter_actor_add_child(pDefaultButton, priv->pressedImage);
					clutter_actor_hide(priv->pressedImage);
				}

				priv->buttonList = g_list_append(priv->buttonList ,pDefaultButton );
				priv->pButtonDataList = g_list_append(priv->pButtonDataList ,pButtonData );

				clutter_actor_add_child( CLUTTER_ACTOR(priv->pButtonGroup), pDefaultButton);
				g_signal_connect( pDefaultButton, "button-press",     G_CALLBACK(b_press_cb),self);
				g_signal_connect( pDefaultButton, "button-release",   G_CALLBACK(b_release_cb),self);
				//g_signal_connect( pDefaultButton, "button-long-press",G_CALLBACK(b_long_press_cb),self);
				g_signal_connect( pDefaultButton, "leave-event",      G_CALLBACK(b_leave_event_cb),self);
			}
		}
	}
}

/********************************************************
 * Function : v_create_speller_key_pad_ui
 * Description:function to create speller key pad
 * Parameters: MildenhallSpellerKeyPad
 * Return value: void
 ********************************************************/
static void v_create_speller_key_pad_ui(MildenhallSpellerKeyPad *self)
{
        ClutterColor backGroundColor = {0x00, 0x00, 0x00, 0xff};
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (self)->priv;
        GList *pHashList = p_convert_json_to_hash (priv->layout_file);
        ClutterActor *blankRect = clutter_actor_new ();
        guint inCount = 0;

	MILDENHALL_SPELLER_KEYPAD_PRINT( "%s\n", __FUNCTION__ );

	MILDENHALL_SPELLER_KEYPAD_PRINT("\n pHashList = %d \n",g_list_length(pHashList));

	priv->pButtonGroup = clutter_actor_new();
	clutter_actor_add_child (CLUTTER_ACTOR(self), priv->pButtonGroup);
	clutter_actor_set_position( priv->pButtonGroup , 0,0);

	clutter_actor_set_background_color (blankRect,&backGroundColor);
	clutter_actor_add_child(priv->pButtonGroup, blankRect);

	for( inCount=0; inCount< g_list_length(pHashList); inCount++ )
	{
		gchar *pFilepath = (gchar *)g_list_nth_data(pHashList ,inCount);
                GHashTable *pHashValue = NULL;
		pFilepath = g_strconcat(PKGDATADIR,"/",pFilepath,NULL);

		MILDENHALL_SPELLER_KEYPAD_PRINT( " pFilepath = %s\n", pFilepath );
		pHashValue = thornbury_style_set (pFilepath);

		parse_and_construct_buttons(self,pHashValue);
		thornbury_style_free(pHashValue);
	}
	clutter_actor_set_size(blankRect,clutter_actor_get_width(priv->pButtonGroup)-70 ,clutter_actor_get_height(priv->pButtonGroup));
	clutter_actor_set_size(priv->pButtonGroup,clutter_actor_get_width(priv->pButtonGroup)-70,clutter_actor_get_height(priv->pButtonGroup));
	clutter_actor_set_position( blankRect,0,0);
}

/********************************************************
 * Function : p_convert_json_to_hash
 * Description:function to convert json to hash
 * Parameters: gchar
 * Return value: GList
 ********************************************************/
static GList *p_convert_json_to_hash(gchar *pFile)
{
	GHashTable *pStyleHash = NULL;
	GHashTable *pHashTable = NULL;
	GList *pGlist = NULL;
        MILDENHALL_SPELLER_KEYPAD_PRINT ("\n %s %s \n", __FUNCTION__, pFile);
	if(NULL == pFile)
	{
		return NULL;
	}
	pStyleHash = thornbury_style_set(pFile);
	if( NULL != pStyleHash )
	{
		GHashTableIter iter;
		gpointer pKey, pValue;
		g_hash_table_iter_init(&iter, pStyleHash);
		while (g_hash_table_iter_next (&iter, &pKey, &pValue))
		{
			pHashTable= pValue;
			if( NULL != pHashTable )
			{
				GHashTableIter gIter;
				gpointer pKeyInfo, pValueInfo;
				g_hash_table_iter_init(&gIter, pHashTable);
				while (g_hash_table_iter_next (&gIter, &pKeyInfo, &pValueInfo))
				{
					pGlist = g_list_append( pGlist,g_strdup((gchar *)g_value_get_string(pValueInfo)));
				}
			}
		}
	}
	thornbury_style_free(pStyleHash);
	return pGlist;
}

/********************************************************
 * Function : mildenhall_speller_key_pad_init
 * Description: Class initialisation function for the pObject type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_init (MildenhallSpellerKeyPad *self)
{
        MildenhallSpellerKeyPadPrivate *priv;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);

	self->priv = priv =SPELLER_KEY_PAD_PRIVATE (self);
	v_init_struct_member_variables(self);
}

/********************************************************
 * Function : mildenhall_speller_key_pad_get_property
 * Description: get a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
        MildenhallSpellerKeyPad *pSpellerKeyPad = MILDENHALL_SPELLER_KEY_PAD (object);
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pSpellerKeyPad)->priv;
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);

	if(!MILDENHALL_IS_SPELLER_KEY_PAD(object))
	{
		g_warning("invalid speller object\n");
		return;
	}

	switch (property_id)
	{
	case MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_FILE:
		g_value_set_string (value, priv->layout_file);
		break;

	case MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_STATE:
		g_value_set_string (value, priv->layout_state);
		break;

	case MILDENHALL_KEYPAD_PROP_ENUM_LONGPRESS_STATUS:
		g_value_set_boolean (value, priv->isLongShift);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : mildenhall_speller_key_pad_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
        MildenhallSpellerKeyPad *pSpellerKeyPad = MILDENHALL_SPELLER_KEY_PAD (object);
        MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD (pSpellerKeyPad)->priv;
	if(!MILDENHALL_IS_SPELLER_KEY_PAD(object))
	{
		g_warning("invalid speller object\n");
		return;
	}
	switch (property_id)
	{
	case MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_FILE :
	{
		priv->layout_file = g_strdup(g_value_get_string(value));
		v_create_speller_key_pad_ui(pSpellerKeyPad);
		break;
	}
	case MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_STATE :
	{
		gchar *layout_state= g_strdup(g_value_get_string(value));

		if(g_strcmp0(priv->layout_state,layout_state) == 0)
			return;

		priv->layout_state = layout_state;

		if(priv->layout_file != NULL)
		{
			if(!g_strcmp0("symbol",priv->layout_state))
			{
				if(FALSE == priv->isSymbol)
				{
					v_update_symbol_text(pSpellerKeyPad);
					priv->isSymbol = TRUE;
					priv->isShift = FALSE;
				}
				else
				{
					v_update_default_text(pSpellerKeyPad);
					priv->isSymbol = FALSE;
				}
				clutter_actor_hide(priv->pressedImage);
			}

			if(!g_strcmp0("upper",priv->layout_state))
			{
				priv->isLongShift =TRUE;
			}
			if(!g_strcmp0("lower",priv->layout_state))
			{
				priv->isLongShift =FALSE;
				priv->isShift = TRUE;
			}

			if( (!g_strcmp0("upper",priv->layout_state)) || (!g_strcmp0("lower",priv->layout_state)))
			{
				if(priv->isLongShift == TRUE)
				{
					priv->isShift = !priv->isShift;
					priv->isLongShift = FALSE;
				}
				if(FALSE == priv->isShift)
				{
					v_update_shift_text(pSpellerKeyPad);
					clutter_actor_show(priv->pressedImage);
				}
				else
				{
					v_update_default_text(pSpellerKeyPad);
					clutter_actor_hide(priv->pressedImage);
				}
				priv->isSymbol = FALSE;
			}
		}
		break;
	}
	case MILDENHALL_KEYPAD_PROP_ENUM_LONGPRESS_STATUS :
	{
		priv->isLongShift = g_value_get_boolean(value);
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

/********************************************************
 * Function : mildenhall_speller_key_pad_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_dispose (GObject *object)
{
	MildenhallSpellerKeyPad *pSpellerKeyPad = MILDENHALL_SPELLER_KEY_PAD(object);
	MildenhallSpellerKeyPadPrivate *priv = MILDENHALL_SPELLER_KEY_PAD(pSpellerKeyPad)->priv;
	int i = 0;

        MILDENHALL_SPELLER_KEYPAD_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);
        G_OBJECT_CLASS (mildenhall_speller_key_pad_parent_class)->dispose(object);

	if(NULL !=priv->buttonList)
	{
		g_list_free(priv->buttonList);
		priv->buttonList = NULL;
	}
	for (i = 0; (unsigned int) i < g_list_length (priv->pButtonDataList); i++)
	{
		MildenhallButtonInfo *pButtonData  = (MildenhallButtonInfo *)g_list_nth_data(priv->pButtonDataList,i);
		v_free_structure_data(pButtonData);
	}
	if(NULL !=priv->pButtonDataList)
	{
		g_list_free(priv->pButtonDataList);
		priv->pButtonDataList = NULL;
	}
}

/********************************************************
 * Function : mildenhall_speller_key_pad_finalize
 * Description: finalize the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_finalize (GObject *object)
{
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	G_OBJECT_CLASS (mildenhall_speller_key_pad_parent_class)->finalize (object);
}

/********************************************************
 * Function : mildenhall_speller_key_pad_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_key_pad_class_init (MildenhallSpellerKeyPadClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
        GParamSpec *pspec = NULL;
        MILDENHALL_SPELLER_KEYPAD_PRINT (" %s\n", __FUNCTION__);
	g_type_class_add_private (object_class, sizeof (MildenhallSpellerKeyPadPrivate));

	object_class->get_property = mildenhall_speller_key_pad_get_property;
	object_class->set_property = mildenhall_speller_key_pad_set_property;
	object_class->dispose = mildenhall_speller_key_pad_dispose;
	object_class->finalize = mildenhall_speller_key_pad_finalize;

	pspec = g_param_spec_string("layout", "layout", "Current Layout of the Speller", NULL ,
			(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property ( object_class, MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_FILE, pspec );

	pspec = g_param_spec_string("layout-state", "layout-state", "Current layout-state of the Speller", NULL ,
			(G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS ));
	g_object_class_install_property ( object_class, MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_STATE, pspec );

	pspec = g_param_spec_boolean ( "long-press-status", "long-press-status", "long-press-status",FALSE, G_PARAM_READWRITE);
	g_object_class_install_property ( object_class, MILDENHALL_KEYPAD_PROP_ENUM_LONGPRESS_STATUS, pspec );

	mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_GO_PRESS] = g_signal_new("go-press",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,0, NULL, NULL,
			g_cclosure_marshal_VOID__VOID, G_TYPE_NONE,0);

	mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_PRESSED] = g_signal_new("key-pressed",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);

	mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_RELEASED] = g_signal_new("key-released",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);

	mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_CLICK] = g_signal_new("key-clicked",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);

	mildenhall_speller_keypad_signals[SIG_MILDENHALL_SPELLER_KEYPAD_KEY_LEAVE] = g_signal_new("key-leave",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_LAST, 0, NULL, NULL,
			g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);


}

/**
 * mildenhall_speller_key_pad_new:
 * Returns: speller key pad object
 *
 * Creates a speller key pad  object
 *
 */
ClutterActor *mildenhall_speller_key_pad_new (void)
{
	MILDENHALL_SPELLER_KEYPAD_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
	return g_object_new (MILDENHALL_TYPE_SPELLER_KEY_PAD, NULL);
}
