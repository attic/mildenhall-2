/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_four_toggle_entry.c */

#include "mildenhall_speller.h"
#include "mildenhall_speller_four_toggle_entry.h"
#include "mildenhall_text_box_entry.h"

G_DEFINE_TYPE (MildenhallSpellerFourToggleEntry, mildenhall_speller_four_toggle_entry, CLUTTER_TYPE_ACTOR)

#define SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SPELLER_FOUR_TOGGLE_ENTRY, MildenhallSpellerFourToggleEntryPrivate))


#define MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(...) //g_print(__VA_ARGS__)
#define MILDENHALL_BLANK_RECT_WIDTH 648
#define MILDENHALL_BLANK_RECT_HEIGHT 64

enum
{
    SIG_FOUR_ENTRY_FIRST_SIGNAL,
    SIG_FOUR_GO_ACTION,
    SIG_FOUR_ENTRY_LAST_SIGNAL
};

guint32 four_entry_action_signals[SIG_FOUR_ENTRY_LAST_SIGNAL] = {0,};
static void v_model_data_for_four_entry( MildenhallSpellerFourToggleEntry *pSelf );

/* private structure mem of fourtoggle entry speller */
struct _MildenhallSpellerFourToggleEntryPrivate
{
    ThornburyModel *model;
    ThornburyModel *pTextBoxModel;

    ThornburyModel *pOneModel;
    ThornburyModel *pTwoModel;
    ThornburyModel *pThreeModel;
    ThornburyModel *pFourModel;

    ClutterActor *blankRect;
    GVariant *argList;
    ClutterActor *textBoxEntry;

    ClutterActor *FourButton;
    ClutterActor *ThreeButton;
    ClutterActor *TwoButton;
    ClutterActor *OneButton;

    ClutterActor *leftLine;
    ClutterActor *rightLine;
    ClutterActor *middleleftLine;
    ClutterActor *middlerightLine;

    gboolean clearText;
    gboolean entry_editable;

    gchar *toggleOneState;
    gchar *toggleTwoState;
    gchar *toggleThreeState;
    gchar *toggleFourState;

    gchar *toggleOneId;
    gchar *toggleTwoId;
    gchar *toggleThreeId;
    gchar *toggleFourId;

    gchar *entryId;

    gfloat oneWidth;
    gfloat twoWidth;
    gfloat threeWidth;
    gfloat fourWidth;

    ClutterActor *SpellerGroup;
    gint inColumnId;
};

/* toggle button model columns */
enum
{
	COLUMN_TEXT,                // set text to toggle button
	COLUMN_ACTIVE_ICON_PATH,    // set active image to toggle button
	COLUMN_INACTIVE_ICON_PATH,  // set inactive image to toggle button
	COLUMN_STATE_ID,            // set stateId to toggle button
	COLUMN_LAST
};

/* four toggle button properties */
enum _MildenhallDefaultSpellerProperties
{
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_FIRST,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENABLE_HISTORY,
    MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_LAST
};

/****************************************************
 * Function : create_four_entry_text_box_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
create_four_entry_text_box_model (void)
{
	ThornburyModel *pModel = NULL;
	pModel = (ThornburyModel *)thornbury_list_model_new (1 ,G_TYPE_STRING, NULL ,-1);
	return pModel;
}

/****************************************************
 * Function : create_four_entry_toggle_model
 * Description: create model
 * Parameters:
 * Return value: ThornburyModel
 *******************************************************/
ThornburyModel *
create_four_entry_toggle_model (void)
{
    ThornburyModel *model = NULL;
    model = (ThornburyModel *)thornbury_list_model_new (COLUMN_LAST,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_POINTER, NULL,
                                  -1);
    return model;
}

/********************************************************
 * Function : v_four_entry_speller_row_added_cb
 * Description: callback on model row added
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerFourToggleEntry *
 * Return value: void
 ********************************************************/
static void v_four_entry_speller_row_added_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerFourToggleEntry *pSelf)
{
        MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
	MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);

    if( 1 == thornbury_model_get_n_rows(pModel) )
    {
        v_model_data_for_four_entry(pSelf);
        clutter_actor_show(priv->leftLine);
        clutter_actor_show(priv->rightLine);
        clutter_actor_show(priv->middleleftLine);
        clutter_actor_show(priv->middlerightLine);
        clutter_actor_show(priv->textBoxEntry);
    }
    else
    {
        g_warning("\n speller cannot have more than one row \n ");
    }
}



/********************************************************
 * Function : v_four_entry_speller_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerFourToggleEntry *
 * Return value: void
 ********************************************************/
static void v_four_entry_speller_row_changed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter,MildenhallSpellerFourToggleEntry *pSelf)
{
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
	GValue value = {0, };
	MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    priv->model = pModel;

	//gint inColumnId;
    //g_object_get(pModel,"column-changed",&inColumnId,NULL);

    if(1 == thornbury_model_get_n_rows(pModel) && priv->inColumnId != -1)
    {
        thornbury_model_iter_get_value(pIter, priv->inColumnId, &value);
        thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
		g_value_unset (&value);
    }
    else
    {
        g_warning(" \n no row to change \n");
    }
}



/********************************************************
 * Function : v_four_entry_speller_remove_model_data
 * Description: function to remove the model data
 * Parameters: ThornburyModel *
 * Return value: void
 ********************************************************/
void v_four_entry_speller_remove_model_data(ThornburyModel *pModel)
{
        gint inCnt = 0;
        gint inTotalRows;
	MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
	inTotalRows = thornbury_model_get_n_rows (pModel);

	if(NULL != pModel)
	{
		for(inCnt=0;inCnt<inTotalRows;inCnt++)
		{
			thornbury_model_remove(pModel,0);
		}
		thornbury_model_remove(pModel,0);
	}
}



/********************************************************
 * Function : v_four_entry_speller_row_removed_cb
 * Description: callback on model row remove
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSpellerFourToggleEntry *
 * Return value: void
 ********************************************************/
static void v_four_entry_speller_row_removed_cb (ThornburyModel *pModel,ThornburyModelIter *pIter, MildenhallSpellerFourToggleEntry *pSelf)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT ("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);

    v_four_entry_speller_remove_model_data(priv->pOneModel);
    v_four_entry_speller_remove_model_data(priv->pTwoModel);
    v_four_entry_speller_remove_model_data(priv->pThreeModel);
    v_four_entry_speller_remove_model_data(priv->pFourModel);

    if(NULL != priv->pTextBoxModel)
	{
		thornbury_model_remove(priv->pTextBoxModel,0);
	}

	clutter_actor_hide(priv->leftLine);
	clutter_actor_hide(priv->rightLine);
	clutter_actor_hide(priv->middleleftLine);
	clutter_actor_hide(priv->middlerightLine);
	clutter_actor_hide(priv->textBoxEntry);
}

/****************************************************
 * Function : update_one_button
 * Description: function to update the button one model
 * Parameters:MildenhallSpellerFourToggleEntry,GVariant,gchar
 * Return value: void
 *******************************************************/
static void update_one_button(MildenhallSpellerFourToggleEntry *pSelf,GVariant *gvOneIcon,gchar *pType)
{
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE( pSelf );
    if(gvOneIcon)
    {
        GVariantIter inCount;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        int inCnt =0;
        g_variant_iter_init (&inCount, gvOneIcon);

        while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
        {
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
            if(!g_strcmp0("text-only",pType))
            {
                g_object_set (priv->OneButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
                              NULL);
                thornbury_model_append (priv->pOneModel, COLUMN_TEXT, g_strdup(pValue),
                    COLUMN_ACTIVE_ICON_PATH, NULL,
                    COLUMN_INACTIVE_ICON_PATH, NULL,
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("image-only",pType))
            {
                g_object_set (priv->OneButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
                              NULL);
                thornbury_model_append (priv->pOneModel, COLUMN_TEXT, NULL,
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("text-image",pType))
            {
                g_object_set (priv->OneButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
                              NULL);
                thornbury_model_append (priv->pOneModel, COLUMN_TEXT, g_strdup(pKey),
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            if(0 == inCnt)
            {
                priv->toggleOneState =g_strdup(pKey);
                inCnt++;
            }
        }
       g_object_set(priv->OneButton, "state-model", priv->pOneModel, NULL);
    }
    clutter_actor_set_position( priv->OneButton, 0,0);
    clutter_actor_set_position( priv->leftLine,priv->oneWidth+1,0);
}

/****************************************************
 * Function : update_two_button
 * Description: function to update the button two model
 * Parameters:MildenhallSpellerFourToggleEntry,GVariant,gchar
 * Return value: void
 *******************************************************/
static void update_two_button(MildenhallSpellerFourToggleEntry *pSelf,GVariant *gvTwoIcon,gchar *pType2)
{
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE( pSelf );
    if(gvTwoIcon)
    {
        GVariantIter inCount;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        int inCnt =0;
        g_variant_iter_init (&inCount, gvTwoIcon);

        while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
        {
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
            if(!g_strcmp0("text-only",pType2))
            {
                 g_object_set (priv->TwoButton,
                               "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
                               NULL);
                thornbury_model_append (priv->pTwoModel, COLUMN_TEXT, g_strdup(pValue),
                    COLUMN_ACTIVE_ICON_PATH, NULL,
                    COLUMN_INACTIVE_ICON_PATH, NULL,
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("image-only",pType2))
            {
                g_object_set (priv->TwoButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
                              NULL);
                thornbury_model_append (priv->pTwoModel, COLUMN_TEXT, NULL,
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("text-image",pType2))
            {
                g_object_set (priv->TwoButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
                              NULL);
                thornbury_model_append (priv->pTwoModel, COLUMN_TEXT, g_strdup(pKey),
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            if(0 == inCnt)
            {
                priv->toggleTwoState =g_strdup(pKey);
                inCnt++;
            }
        }
       g_object_set(priv->TwoButton, "state-model", priv->pTwoModel, NULL);
    }
    clutter_actor_set_position( priv->TwoButton,priv->oneWidth+1,0);
    clutter_actor_set_position( priv->middleleftLine,priv->oneWidth+priv->twoWidth+2,0);
}

/****************************************************
 * Function : update_three_button
 * Description: function to update the button three model
 * Parameters:MildenhallSpellerFourToggleEntry,GVariant,gchar
 * Return value: void
 *******************************************************/
static void update_three_button(MildenhallSpellerFourToggleEntry *pSelf,GVariant *gvThreeIcon,gchar *pType3)
{
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE( pSelf );
    if(gvThreeIcon)
    {
        GVariantIter inCount;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        int inCnt =0;
        g_variant_iter_init (&inCount, gvThreeIcon);

        while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
        {
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
            if(!g_strcmp0("text-only",pType3))
            {
                 g_object_set (priv->ThreeButton,
                               "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
                               NULL);
                thornbury_model_append (priv->pThreeModel, COLUMN_TEXT, g_strdup(pValue),
                    COLUMN_ACTIVE_ICON_PATH, NULL,
                    COLUMN_INACTIVE_ICON_PATH, NULL,
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("image-only",pType3))
            {
                g_object_set (priv->ThreeButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
                              NULL);
                thornbury_model_append (priv->pThreeModel, COLUMN_TEXT, NULL,
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("text-image",pType3))
            {
                g_object_set (priv->ThreeButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
                              NULL);
                thornbury_model_append (priv->pThreeModel, COLUMN_TEXT, g_strdup(pKey),
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            if(0 == inCnt)
            {
                priv->toggleThreeState =g_strdup(pKey);
                inCnt++;
            }
        }
       g_object_set(priv->ThreeButton, "state-model", priv->pThreeModel, NULL);
    }
    clutter_actor_set_position( priv->ThreeButton,priv->oneWidth+priv->twoWidth+2,0);
    clutter_actor_set_position( priv->middlerightLine,priv->oneWidth+priv->twoWidth+priv->threeWidth+3,0);
}

/****************************************************
 * Function : update_four_button
 * Description: function to update the button four model
 * Parameters:MildenhallSpellerFourToggleEntry,GVariant,gchar
 * Return value: void
 *******************************************************/
static void update_four_button(MildenhallSpellerFourToggleEntry *pSelf,GVariant *gvFourIcon,gchar *pType4)
{
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE( pSelf );
    if(gvFourIcon)
    {
        GVariantIter inCount;
        gchar *pValue = NULL;
        gchar *pKey = NULL;
        int inCnt =0;
        g_variant_iter_init (&inCount, gvFourIcon);

        while( g_variant_iter_next ( &inCount, "{ss}", &pKey, &pValue) )
        {
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("pKey = %s pValue= %s \n",pKey,pValue);
            if(!g_strcmp0("text-only",pType4))
            {
                 g_object_set (priv->FourButton,
                               "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_TEXT_ONLY,
                               NULL);
                thornbury_model_append (priv->pFourModel, COLUMN_TEXT, g_strdup(pValue),
                    COLUMN_ACTIVE_ICON_PATH, NULL,
                    COLUMN_INACTIVE_ICON_PATH, NULL,
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("image-only",pType4))
            {
                g_object_set (priv->FourButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_ONLY,
                              NULL);
                thornbury_model_append (priv->pFourModel, COLUMN_TEXT, NULL,
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            else if(!g_strcmp0("text-image",pType4))
            {
                g_object_set (priv->FourButton,
                              "display-type", MILDENHALL_TOGGLE_BUTTON_DISPLAY_IMAGE_TEXT,
                              NULL);
                thornbury_model_append (priv->pFourModel, COLUMN_TEXT, g_strdup(pKey),
                    COLUMN_ACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_INACTIVE_ICON_PATH, g_strdup(pValue),
                    COLUMN_STATE_ID, g_strdup(pKey),-1);
            }
            if(0 == inCnt)
            {
                priv->toggleFourState =g_strdup(pKey);
                inCnt++;
            }
        }
       g_object_set(priv->FourButton, "state-model", priv->pFourModel, NULL);
    }
    clutter_actor_set_position( priv->FourButton, MILDENHALL_BLANK_RECT_WIDTH-priv->fourWidth ,0);
    clutter_actor_set_position( priv->rightLine,MILDENHALL_BLANK_RECT_WIDTH-priv->fourWidth-1,0);
    clutter_actor_set_x( priv->textBoxEntry,priv->oneWidth+priv->twoWidth+priv->threeWidth+4 );
    g_object_set(priv->textBoxEntry,"width", MILDENHALL_BLANK_RECT_WIDTH -(priv->oneWidth+priv->twoWidth+priv->threeWidth+priv->fourWidth+4),NULL);
}

/****************************************************
 * Function : v_model_data_for_four_entry
 * Description: update the speller entry with model info
 * Parameters: MildenhallSpellerFourToggleEntry*
 * Return value: void
 *******************************************************/
static void v_model_data_for_four_entry( MildenhallSpellerFourToggleEntry *pSelf )
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE( pSelf );
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT ("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);

    if (G_IS_OBJECT(priv->model) && thornbury_model_get_n_rows ( priv->model) > 0 )
    {
        ThornburyModelIter *pIter = thornbury_model_get_iter_at_row( priv->model, 0 );
        if(NULL != pIter)
        {
            GValue value = {0, };
            gchar *pType = NULL;
            gpointer gPtr3;
            gpointer gPtr;
            GVariant *gvOneIcon;
            gchar *pType2 = NULL;
            gpointer gPtr1;
            GVariant *gvTwoIcon;
            gchar *pType3;
            gpointer gPtr2;
            GVariant *gvThreeIcon;
            gchar *pType4;
            GVariant *gvFourIcon;

            thornbury_model_iter_get_value(pIter, 0, &value);
            pType = g_value_dup_string (&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" pType = %s \n",pType);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 1, &value);
            priv->oneWidth = g_value_get_float(&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" fltWidth = %f \n",priv->oneWidth);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 2, &value);
            priv->toggleOneId = g_value_dup_string(&value);
            g_value_unset (&value);

            g_object_set(priv->OneButton ,"width",priv->oneWidth,NULL);

            thornbury_model_iter_get_value(pIter, 3, &value);
            gPtr = g_value_get_pointer (&value);
            gvOneIcon = (GVariant*) gPtr;

            update_one_button(pSelf,gvOneIcon,pType);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 4, &value);
            pType2 = g_value_dup_string (&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" pType2 = %s \n",pType2);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 5, &value);
            priv->twoWidth = g_value_get_float(&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" twoWidth = %f \n",priv->twoWidth);
            g_value_unset (&value);

            g_object_set(priv->TwoButton ,"width",priv->twoWidth,NULL);

            thornbury_model_iter_get_value(pIter, 6, &value);
            priv->toggleTwoId = g_value_dup_string(&value);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 7, &value);
            gPtr1 = g_value_get_pointer (&value);
            gvTwoIcon = (GVariant*) gPtr1;

            update_two_button(pSelf,gvTwoIcon,pType2);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 8, &value);
            pType3 = g_value_dup_string (&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" pType3 = %s \n",pType3);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 9, &value);
            priv->threeWidth = g_value_get_float(&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" threeWidth = %f \n",priv->threeWidth);
            g_value_unset (&value);

            g_object_set(priv->ThreeButton ,"width",priv->threeWidth,NULL);

            thornbury_model_iter_get_value(pIter, 10, &value);
            priv->toggleThreeId = g_value_dup_string(&value);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 11, &value);
            gPtr2 = g_value_get_pointer (&value);
            gvThreeIcon = (GVariant*) gPtr2;

            update_three_button(pSelf,gvThreeIcon,pType3);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 12, &value);
            pType4 = g_value_dup_string (&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" pType4 = %s \n",pType4);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 13, &value);
            priv->fourWidth = g_value_get_float(&value);
            MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" fourWidth = %f \n",priv->fourWidth);
            g_value_unset (&value);

            g_object_set(priv->FourButton ,"width",priv->fourWidth,NULL);

            thornbury_model_iter_get_value(pIter, 14, &value);
            priv->toggleFourId = g_value_dup_string(&value);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 15, &value);
            gPtr3 = g_value_get_pointer (&value);
            gvFourIcon = (GVariant*) gPtr3;

            update_four_button(pSelf,gvFourIcon,pType4);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 16, &value);
            thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
            g_object_set(priv->textBoxEntry,"model",priv->pTextBoxModel,NULL);
            g_value_unset (&value);

            thornbury_model_iter_get_value(pIter, 17, &value);
            priv->entryId = g_value_dup_string(&value);
            g_value_unset (&value);
        }
    }
}


/****************************************************
 * Function : v_four_entry_speller_update_view
 * Description: update the speller entry
 * Parameters: MildenhallSpellerFourToggleEntry*
 * Return value: void
 *******************************************************/
static void v_four_entry_speller_update_view( MildenhallSpellerFourToggleEntry *pSelf )
{
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    v_model_data_for_four_entry(pSelf);
}

/****************************************************
 * Function : v_key_pressed_cb
 * Description: callback function, invoke at key pressed
 * Parameters: GObject*,gint,gpointer
 * Return value: void
 *******************************************************/
static void v_key_pressed_cb(GObject *pObject,gint event,gpointer pUserData)
{
	MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pUserData);
	MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);

	switch (event)
	{
		  case CLUTTER_KEY_Escape:
		  case CLUTTER_KEY_Shift_L:
		  case CLUTTER_KEY_Shift_R:
		  case CLUTTER_KEY_Left:
		  case CLUTTER_KEY_KP_Left:
		  case CLUTTER_KEY_Right:
		  case CLUTTER_KEY_KP_Right:
		  case CLUTTER_KEY_End:
		  case CLUTTER_KEY_KP_End:
		  case CLUTTER_KEY_Begin:
		  case CLUTTER_KEY_Home:
		  case CLUTTER_KEY_KP_Home:
		  case CLUTTER_KEY_Up:
		  case CLUTTER_KEY_Down:
		  case CLUTTER_KEY_Delete:
		  case CLUTTER_KEY_KP_Delete:
			break;

		  case CLUTTER_KEY_Return:
		  {
				GVariantBuilder *pVariant = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
				gchar *pText = NULL;
				g_object_get(priv->textBoxEntry, "text", &pText, NULL);
				g_variant_builder_add (pVariant, "{ss}",priv->entryId, g_strdup(pText));

				g_variant_builder_add (pVariant, "{ss}",priv->toggleOneId, priv->toggleOneState);
				g_variant_builder_add (pVariant, "{ss}",priv->toggleTwoId, priv->toggleTwoState);
				g_variant_builder_add (pVariant, "{ss}",priv->toggleThreeId,priv->toggleThreeState);
				g_variant_builder_add (pVariant, "{ss}",priv->toggleFourId, priv->toggleFourState);

				priv->argList = g_variant_builder_end (pVariant);
				g_variant_builder_unref(pVariant);

				g_signal_emit(pSelf, four_entry_action_signals[SIG_FOUR_GO_ACTION], 0,NULL);
				break;
		  }
		  case CLUTTER_KEY_BackSpace:
		  {
				break;
		  }
		  default:
		  {
			  break;
		  }
	}
}

/**
 * v_four_entry_speller_set_text:
 * @MildenhallSpellerFourToggleEntry : object reference
 * @pText : text to set on textEntryBox
 *
 * set the text on TextEntryBox
 *
 */
void v_four_entry_speller_set_text(MildenhallSpellerFourToggleEntry *pSelf, GVariant *pArgList)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    GVariantIter iter;
    gchar *pText = NULL;
    gchar *key = NULL;
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_SPELLER_FOUR_TOGGLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

    if(NULL == pArgList)
	    return;

    g_variant_iter_init (&iter, pArgList);
    while (g_variant_iter_next (&iter, "{ss}", &key, &pText))
    {
        if(!g_strcmp0(pText,"SPACE"))
        {
            GValue value = {0};
            g_value_init(&value, G_TYPE_STRING);
            g_value_set_static_string (&value," ");
            thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
            g_value_unset (&value);
        }
        else if(!g_strcmp0(pText,"RETURN") || !g_strcmp0(pText,"go") )
        {
            GVariantBuilder *pVariant = g_variant_builder_new (G_VARIANT_TYPE ("a{ss}"));
            gchar *pEnteredText = NULL;
            g_object_get(priv->textBoxEntry, "text", &pEnteredText, NULL);
            g_variant_builder_add (pVariant, "{ss}",priv->entryId, g_strdup(pEnteredText));

			g_variant_builder_add (pVariant, "{ss}",priv->toggleOneId, priv->toggleOneState);
			g_variant_builder_add (pVariant, "{ss}",priv->toggleTwoId, priv->toggleTwoState);
			g_variant_builder_add (pVariant, "{ss}",priv->toggleThreeId,priv->toggleThreeState);
			g_variant_builder_add (pVariant, "{ss}",priv->toggleFourId, priv->toggleFourState);

            priv->argList = g_variant_builder_end (pVariant);
            g_variant_builder_unref(pVariant);
        }
        else if(!g_strcmp0(pText,"dot"))
        {
            GValue value = {0};
            g_value_init(&value, G_TYPE_STRING);
            g_value_set_static_string (&value,".");
            thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
            g_value_unset (&value);
        }
        else if(!g_strcmp0(pText,"CANCEL"))
        {
	    lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (priv->textBoxEntry));
        }
        else if(!g_strcmp0(pText,"SYMBOLS") || !g_strcmp0(pText,"SHIFT") || !g_strcmp0(pText,"AUTOCOMPLETE") || !g_strcmp0(pText,"SHIFT"))
        {
            //TBD
        }
        else if(!g_strcmp0(pText,"BKSP") || !g_strcmp0(pText,"←") )
        {
        	GValue value = {0};
			g_value_init(&value, G_TYPE_STRING);
			g_value_set_static_string (&value,g_strdup(pText));
			thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
			g_value_unset (&value);
        }
        else
        {
            GValue value = {0};
            g_value_init(&value, G_TYPE_STRING);
            g_value_set_static_string (&value,g_strdup(pText));
            thornbury_model_insert_value(priv->pTextBoxModel, 0, 0, &value);
            g_value_unset (&value);
        }
        if(pText!=NULL)
        {
            g_free (pText);
            pText = NULL;
        }
        if(key!=NULL)
        {
            g_free (key);
            key = NULL;
        }
    }
}

/**
 * v_four_entry_speller_set_clear_text:
 * @MildenhallSpellerFourToggleEntry : object reference
 * @clearText : set to true/false to clear the text
 *
 * set to clear the text on entry
 *
 */
void v_four_entry_speller_set_clear_text(MildenhallSpellerFourToggleEntry *pSelf, gboolean clearText)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT :%s clearText = %d \n", __FUNCTION__,clearText);
    if(!MILDENHALL_IS_SPELLER_FOUR_TOGGLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

	priv->clearText = clearText;
    if( TRUE == priv->clearText)
	{
	    lightwood_text_box_clear_text (LIGHTWOOD_TEXT_BOX (priv->textBoxEntry));
	    priv->clearText = FALSE;
    }
}

/**
 * v_four_entry_speller_set_entry_editable:
 * @MildenhallSpellerFourToggleEntry : object reference
 * @clearText : set to true/false for entry to edit
 *
 * set to enable/disable to edit the entry
 *
 */
void v_four_entry_speller_set_entry_editable(MildenhallSpellerFourToggleEntry *pSelf, gboolean editable)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT :%s editable = %d \n", __FUNCTION__,editable);
    if(!MILDENHALL_IS_SPELLER_FOUR_TOGGLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->entry_editable = editable;
    g_object_set( priv->textBoxEntry ,"text-editable", priv->entry_editable, NULL );

    if(TRUE == priv->entry_editable)
    {
        g_object_set(priv->textBoxEntry, "focus-cursor",TRUE, NULL);
    }
    else
    {
        g_object_set(priv->textBoxEntry, "focus-cursor",FALSE, NULL);
    }
}

void v_four_entry_speller_column_changed_cb(gint col,gpointer pSelf)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_SPELLER_FOUR_TOGGLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}
	priv->inColumnId = col;
}

/**
 * v_four_entry_speller_set_model:
 * @MetaInfoHeader: meta info header object reference
 * @pModel: model for the speller
 *
 * Model format:
 *	"left-icon-istext"  - gboolean
 *	"left-icon-width"   - gfloat
 *	"left-icon-info"    - GVariant*
 *	"right-icon-istext" - gboolean
 *	"right-icon-width"  - gfloat
 *	"right-icon-info"   - GVariant*
 * 	"default-text"     - gchar*
 **/

void v_four_entry_speller_set_model(MildenhallSpellerFourToggleEntry *pSelf, ThornburyModel *pModel)
{
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    if(!MILDENHALL_IS_SPELLER_FOUR_TOGGLE_ENTRY(pSelf))
	{
		g_warning("invalid instance\n");
		return;
	}

    if(NULL != priv->model)
    {
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_four_entry_speller_row_added_cb),   pSelf);
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_four_entry_speller_row_changed_cb), pSelf);
        g_signal_handlers_disconnect_by_func (priv->model, G_CALLBACK (v_four_entry_speller_row_removed_cb), pSelf);
        //g_object_unref (priv->model);
	thornbury_list_model_destroy(priv->model);
        priv->model = NULL;
    }
    /* update the new model with signals */
	if ( NULL != pModel)
    {
        g_return_if_fail (G_IS_OBJECT (pModel));
        priv->model = g_object_ref(pModel);
        g_signal_connect (priv->model, "row-added",   G_CALLBACK (v_four_entry_speller_row_added_cb),   pSelf );
        g_signal_connect (priv->model, "row-changed", G_CALLBACK (v_four_entry_speller_row_changed_cb), pSelf );
        g_signal_connect (priv->model, "row-removed", G_CALLBACK (v_four_entry_speller_row_removed_cb), pSelf );
		thornbury_model_register_column_changed_cb(priv->model,v_four_entry_speller_column_changed_cb,pSelf);
    }
    v_four_entry_speller_update_view(pSelf);
	g_object_notify (G_OBJECT(pSelf), "model");
}

/********************************************************
 * Function : v_mildenhall_speller_four_toggle_entry_get_property
 * Description: Get a property value
 * Parameters: The object reference, property Id,
 *              return location for where the property
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_four_toggle_entry_get_property (GObject *pObject, guint uinPropertyId,GValue *pValue, GParamSpec *pPspec)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pObject);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT ("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    switch (uinPropertyId)
    {
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT:
        {
            g_value_set_variant (pValue, pSelf->priv->argList);
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT:
        {
            g_value_set_boolean (pValue, pSelf->priv->clearText);
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE:
        {
            g_value_set_boolean (pValue, pSelf->priv->entry_editable);
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL:
        {
            g_value_set_object (pValue, pSelf->priv->model);
            break;
        }
        default:
        {
        	G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
            break;
        }
    }
}

/********************************************************
 * Function : v_mildenhall_speller_four_toggle_entry_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_four_toggle_entry_set_property (GObject *pObject, guint uinPropertyId,
                                                      const GValue *pValue, GParamSpec *pPspec)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pObject);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT ("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    switch (uinPropertyId)
    {
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT:
        {
            v_four_entry_speller_set_text (pSelf, g_value_get_variant( pValue ));
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT:
        {
            v_four_entry_speller_set_clear_text (pSelf, g_value_get_boolean( pValue ));
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE:
        {
            v_four_entry_speller_set_entry_editable (pSelf, g_value_get_boolean( pValue ));
            break;
        }
        case MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL:
        {
            v_four_entry_speller_set_model (pSelf, g_value_get_object ( pValue ));
            break;
        }
        default:
        {
        	G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyId, pPspec);
        	break;
        }
    }
}

/********************************************************
 * Function : v_mildenhall_speller_four_toggle_entry_dispose
 * Description: Dispose the spller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_four_toggle_entry_dispose (GObject *pObject)
{
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    G_OBJECT_CLASS (mildenhall_speller_four_toggle_entry_parent_class)->dispose (pObject);
}

/********************************************************
 * Function : v_mildenhall_speller_four_toggle_entry_finalize
 * Description: Finalize the speller object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_four_toggle_entry_finalize (GObject *pObject)
{
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT("MILDENHALL_FOUR_ENTRY_SPELLER_PRINT: %s\n", __FUNCTION__);
    G_OBJECT_CLASS (mildenhall_speller_four_toggle_entry_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : v_one_toggle_button_cb
 * Description: callback function invoke at toggle button
 * Parameters: The object reference,gpointer,gpointer
 * Return value: void
 ********************************************************/
static void v_one_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pUserData);
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);
    priv->toggleOneState = g_strdup(newState);
}

/********************************************************
 * Function : v_two_toggle_button_cb
 * Description: callback function invoke at toggle button
 * Parameters: The object reference,gpointer,gpointer
 * Return value: void
 ********************************************************/
static void v_two_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pUserData);
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);
    priv->toggleTwoState = g_strdup(newState);
}

/********************************************************
 * Function : v_three_toggle_button_cb
 * Description: callback function invoke at toggle button
 * Parameters: The object reference,gpointer,gpointer
 * Return value: void
 ********************************************************/
static void v_three_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pUserData);
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);
    priv->toggleThreeState = g_strdup(newState);
}

/********************************************************
 * Function : v_four_toggle_button_cb
 * Description: callback function invoke at toggle button
 * Parameters: The object reference,gpointer,gpointer
 * Return value: void
 ********************************************************/
static void v_four_toggle_button_cb(GObject *pObject, gpointer newState,gpointer pUserData)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pUserData);
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);
    priv->toggleFourState = g_strdup(newState);
}

/********************************************************
 * Function : v_mildenhall_speller_four_toggle_entry_constructed
 * Description: the constructor function is called by g_object_new()
                to complete the object initialization after all the
                construction properties are set.
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void v_mildenhall_speller_four_toggle_entry_constructed (GObject *pObject)
{
    MildenhallSpellerFourToggleEntry *pSelf = MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY(pObject);
    MildenhallSpellerFourToggleEntryPrivate *priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE(pSelf);

    ClutterColor lineColor = {0xFF,0xFF,0xFF,0x33};
    ClutterColor backGroundColor = {0x00,0x00,0x00,0xCC};
    ThornburyItemFactory *itemFactory = NULL;
    MildenhallTextBoxEntry *pTextBox = NULL;
    MildenhallToggleButton *OneToggle = NULL;
    MildenhallToggleButton *TwoToggle = NULL;
    MildenhallToggleButton *ThreeToggle = NULL;
    GObject *pTextObject = NULL;
    ThornburyItemFactory *pOneitemFactory = NULL;
    GObject *pOneObject = NULL;
    ThornburyItemFactory *pTwoitemFactory = NULL;
    GObject *pTwoObject = NULL;
    ThornburyItemFactory *pThreeitemFactory = NULL;
    GObject *pThreeObject = NULL;
    ThornburyItemFactory *pFouritemFactory = NULL;
    GObject *pFourObject = NULL;
    MildenhallToggleButton *FourToggle = NULL;
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT (" \n %s %d \n", __FUNCTION__, __LINE__);

    //create a group for speller
    priv->SpellerGroup = clutter_actor_new();
    clutter_actor_set_position( priv->SpellerGroup ,0 ,0);
    clutter_actor_add_child(CLUTTER_ACTOR(pSelf), priv->SpellerGroup);

    // create a blank rectangle to add entry items
    priv->blankRect = clutter_actor_new();
    clutter_actor_set_background_color ( priv->blankRect,&backGroundColor);
    clutter_actor_set_size(priv->blankRect,MILDENHALL_BLANK_RECT_WIDTH,MILDENHALL_BLANK_RECT_HEIGHT);
    clutter_actor_add_child(priv->SpellerGroup, priv->blankRect);
    clutter_actor_set_position( priv->blankRect,0,0);
    clutter_actor_set_reactive( priv->blankRect,TRUE);

    itemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TEXT_BOX_ENTRY, PKGDATADIR"/mildenhall_speller_text_box_prop.json");
    g_object_get(itemFactory, "object", &pTextObject, NULL);
    pTextBox = MILDENHALL_TEXT_BOX_ENTRY (pTextObject);
    priv->textBoxEntry = CLUTTER_ACTOR(pTextBox);
    clutter_actor_add_child(priv->SpellerGroup, priv->textBoxEntry);
    g_object_set(priv->textBoxEntry, "focus-cursor",TRUE, NULL);
    g_signal_connect(priv->textBoxEntry,"keypress-event",  G_CALLBACK(v_key_pressed_cb),pObject);

    pOneitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_fv_one_toggle_prop.json");
    g_object_get(pOneitemFactory, "object", &pOneObject, NULL);
    OneToggle = MILDENHALL_TOGGLE_BUTTON (pOneObject);
    priv->OneButton = CLUTTER_ACTOR(OneToggle);
    clutter_actor_add_child(priv->SpellerGroup,priv->OneButton);

    pTwoitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_fv_two_toggle_prop.json");
    g_object_get(pTwoitemFactory, "object", &pTwoObject, NULL);
    TwoToggle = MILDENHALL_TOGGLE_BUTTON (pTwoObject);
    priv->TwoButton = CLUTTER_ACTOR(TwoToggle);
    clutter_actor_add_child(priv->SpellerGroup,priv->TwoButton);

    pThreeitemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_fv_three_toggle_prop.json");
    g_object_get(pThreeitemFactory, "object", &pThreeObject, NULL);
    ThreeToggle = MILDENHALL_TOGGLE_BUTTON (pThreeObject);
    priv->ThreeButton = CLUTTER_ACTOR(ThreeToggle);
    clutter_actor_add_child(priv->SpellerGroup,priv->ThreeButton);

    pFouritemFactory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_TOGGLE_BUTTON, PKGDATADIR"/mildenhall_fv_four_toggle_prop.json");
    g_object_get(pFouritemFactory, "object", &pFourObject, NULL);
    FourToggle = MILDENHALL_TOGGLE_BUTTON (pFourObject);
    priv->FourButton = CLUTTER_ACTOR(FourToggle);
    clutter_actor_add_child(priv->SpellerGroup,priv->FourButton);

    g_signal_connect(priv->OneButton,	"button-toggled",  G_CALLBACK(v_one_toggle_button_cb), 	 pObject);
    g_signal_connect(priv->TwoButton,	"button-toggled",  G_CALLBACK(v_two_toggle_button_cb),	 pObject);
    g_signal_connect(priv->ThreeButton, "button-toggled",  G_CALLBACK(v_three_toggle_button_cb), pObject);
    g_signal_connect(priv->FourButton,	"button-toggled",  G_CALLBACK(v_four_toggle_button_cb),  pObject);

    /*separator on four entry speller */
    priv->leftLine = clutter_actor_new();
    clutter_actor_set_background_color ( priv->leftLine,&lineColor);
    clutter_actor_add_child(priv->SpellerGroup, priv->leftLine);
    clutter_actor_set_size(priv->leftLine,1,63);

    priv->rightLine = clutter_actor_new();
    clutter_actor_set_background_color ( priv->rightLine,&lineColor);
    clutter_actor_add_child(priv->SpellerGroup, priv->rightLine);
    clutter_actor_set_size(priv->rightLine,1,63);

    priv->middleleftLine = clutter_actor_new();
    clutter_actor_set_background_color ( priv->middleleftLine,&lineColor);
    clutter_actor_add_child(priv->SpellerGroup, priv->middleleftLine);
    clutter_actor_set_size(priv->middleleftLine,1,63);

    priv->middlerightLine = clutter_actor_new();
    clutter_actor_set_background_color ( priv->middlerightLine,&lineColor);
    clutter_actor_add_child(priv->SpellerGroup, priv->middlerightLine);
    clutter_actor_set_size(priv->middlerightLine,1,63);

    priv->pTextBoxModel = create_four_entry_text_box_model();
    priv->pOneModel     = create_four_entry_toggle_model();
    priv->pTwoModel     = create_four_entry_toggle_model();
    priv->pThreeModel   = create_four_entry_toggle_model();
    priv->pFourModel    = create_four_entry_toggle_model();
}

/********************************************************
 * Function : mildenhall_speller_four_toggle_entry_class_init
 * Description: Class initialisation function for the pObject type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_four_toggle_entry_class_init (MildenhallSpellerFourToggleEntryClass *pKlass)
{
    GObjectClass *pObject_class = G_OBJECT_CLASS (pKlass);
    GParamSpec *pPspec = NULL;
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
    g_type_class_add_private (pKlass, sizeof (MildenhallSpellerFourToggleEntryPrivate));

    pObject_class->get_property = v_mildenhall_speller_four_toggle_entry_get_property;
    pObject_class->set_property = v_mildenhall_speller_four_toggle_entry_set_property;
    pObject_class->dispose      = v_mildenhall_speller_four_toggle_entry_dispose;
    pObject_class->finalize     = v_mildenhall_speller_four_toggle_entry_finalize;
    pObject_class->constructed  = v_mildenhall_speller_four_toggle_entry_constructed;

    /**
     * MildenhallSpellerFourToggleEntry:text:
     *
     * this to set to text
     */
    pPspec = g_param_spec_variant("text",
                                 "text",
                                 "text",
                                 G_VARIANT_TYPE("a{ss}"),NULL,
                                 (G_PARAM_READWRITE));
    g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_TEXT, pPspec );

    /**
     * MildenhallSpellerFourToggleEntry:clear-text:
     *
     * this to clear to text
     */
    pPspec = g_param_spec_boolean ( "clear-text",
                                    "clear-text",
                                    "clear-text",
                                    TRUE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_CLEAR_TEXT, pPspec );

    /**
     * MildenhallSpellerFourToggleEntry:entry-editable:
     *
     * this to enable/disable the entry editable
     */
    pPspec = g_param_spec_boolean ( "entry-editable",
                                    "entry-editable",
                                    "entry-editable",
                                    TRUE,
                                    G_PARAM_READWRITE);
    g_object_class_install_property ( pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_ENTRY_EDITABLE, pPspec );

    /**
     * MildenhallSpellerFourToggleEntry:model:
     *
     * this to set model to entry
     */
    pPspec = g_param_spec_object ("model",
                                  "model",
                                  "model",
                                G_TYPE_OBJECT,
                                G_PARAM_READWRITE);
    g_object_class_install_property (pObject_class, MILDENHALL_DEFAULT_SPELLER_PROP_ENUM_MODEL, pPspec);

    /**
     * MildenhallSpellerDefaultEntry::go-action:
     *
     * ::go-action is emitted when go button is pressed
     *
     */
    four_entry_action_signals[SIG_FOUR_GO_ACTION] = g_signal_new("go-action",
                                    G_TYPE_FROM_CLASS (pObject_class),
                                    G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,0,
                                    NULL, NULL,
                                    g_cclosure_marshal_VOID__VOID,
                                    G_TYPE_NONE, 0);
}

/********************************************************
 * Function : mildenhall_speller_four_toggle_entry_init
 * Description: Class initialization function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_speller_four_toggle_entry_init (MildenhallSpellerFourToggleEntry *pSelf)
{
    pSelf->priv = SPELLER_FOUR_TOGGLE_ENTRY_PRIVATE (pSelf);
    pSelf->priv->blankRect    = NULL;
    pSelf->priv->textBoxEntry = NULL;
    pSelf->priv->SpellerGroup = NULL;

    pSelf->priv->toggleOneState = NULL;
    pSelf->priv->toggleTwoState = NULL;
    pSelf->priv->toggleThreeState = NULL;
    pSelf->priv->toggleFourState = NULL;
    pSelf->priv->inColumnId = -1;

}

/**
 * mildenhall_speller_four_toggle_entry_new:
 * Returns: speller entry object
 *
 * Creates a speller entry  object
 *
 */
ClutterActor *mildenhall_speller_four_toggle_entry_new (void)
{
    MILDENHALL_FOUR_ENTRY_SPELLER_PRINT(" \n %s %d \n",__FUNCTION__ ,__LINE__);
    return g_object_new (MILDENHALL_TYPE_SPELLER_FOUR_TOGGLE_ENTRY, NULL);
}

