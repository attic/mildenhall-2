/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_key_pad.h */

#ifndef __MILDENHALL_SPELLER_KEY_PAD_H__
#define __MILDENHALL_SPELLER_KEY_PAD_H__

#include <clutter/clutter.h>
#include <glib-object.h>
#include <thornbury/thornbury.h>

#include "mildenhall_button_speller.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_KEY_PAD mildenhall_speller_key_pad_get_type()

#define MILDENHALL_SPELLER_KEY_PAD(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_KEY_PAD, MildenhallSpellerKeyPad))

#define MILDENHALL_SPELLER_KEY_PAD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_KEY_PAD, MildenhallSpellerKeyPadClass))

#define MILDENHALL_IS_SPELLER_KEY_PAD(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_KEY_PAD))

#define MILDENHALL_IS_SPELLER_KEY_PAD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_KEY_PAD))

#define MILDENHALL_SPELLER_KEY_PAD_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_KEY_PAD, MildenhallSpellerKeyPadClass))


typedef struct _MildenhallSpellerKeyPad MildenhallSpellerKeyPad;
typedef struct _MildenhallSpellerKeyPadClass MildenhallSpellerKeyPadClass;
typedef struct _MildenhallSpellerKeyPadPrivate MildenhallSpellerKeyPadPrivate;
typedef struct _MildenhallButtonInfo MildenhallButtonInfo;
typedef enum _MildenhallSpellerKeypadSignals MildenhallSpellerKeypadSignals;
typedef enum _MildenhallKeypadProperties MildenhallKeypadProperties;

struct _MildenhallSpellerKeyPad
{
  ClutterActor parent;
  MildenhallSpellerKeyPadPrivate *priv;
};

struct _MildenhallSpellerKeyPadClass
{
  ClutterActorClass parent_class;
};

struct _MildenhallButtonInfo
{
    gchar *name ;
    gchar *type ;
    gchar *defaultText ;
    gchar *shiftText ;
    gchar *symbolText ;
    gchar *normalImage ;
    gchar *pressedImage ;
    gchar *fontType ;
    gfloat width;
    gfloat height;
    gfloat x;
    gfloat y;
    gboolean longPress;
};

struct _MildenhallSpellerKeyPadPrivate
{
    ClutterActor *pButtonGroup;
    ClutterActor *pButtonPressedActor;
    gchar *layout_file;
    gchar *layout_state;
    ClutterActor *pressedImage;
    ClutterActor *normalImage;
    GList *buttonList;
    guint key_repeat_source;
    guint longpress_source;
    gboolean isSymbol;
    gboolean isShift;
    gboolean isLongShift;
    gboolean isLongpress;
    ThornburyModel *pModel ;
    GList *pButtonDataList;
};


enum _MildenhallSpellerKeypadSignals
{
    SIG_MILDENHALL_SPELLER_KEYPAD_FIRST_SIGNAL,
	SIG_MILDENHALL_SPELLER_KEYPAD_KEY_PRESSED,
	SIG_MILDENHALL_SPELLER_KEYPAD_KEY_RELEASED,
	SIG_MILDENHALL_SPELLER_KEYPAD_KEY_CLICK,
	SIG_MILDENHALL_SPELLER_KEYPAD_GO_PRESS,
	SIG_MILDENHALL_SPELLER_KEYPAD_KEY_LEAVE,
	SIG_MILDENHALL_SPELLER_KEYPAD_LAST_SIGNAL
};

enum _MildenhallKeypadProperties
{
    MILDENHALL_KEYPAD_PROP_ENUM_FIRST,
    MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_FILE,
    MILDENHALL_KEYPAD_PROP_ENUM_LAYOUT_STATE,
    MILDENHALL_KEYPAD_PROP_ENUM_LONGPRESS_STATUS,
    MILDENHALL_KEYPAD_PROP_ENUM_LAST
};


ClutterActor *mildenhall_speller_key_pad_new (void);
GType mildenhall_speller_key_pad_get_type (void) G_GNUC_CONST;
ThornburyModel *create_speller_keypad_model (void);
G_END_DECLS
#endif /* __MILDENHALL_SPELLER_KEY_PAD_H__ */
