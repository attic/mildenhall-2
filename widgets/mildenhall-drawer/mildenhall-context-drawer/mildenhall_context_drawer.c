/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-context_drawer.c
 * Created on: Aug 28, 2012
 *
 * mildenhall_context_drawer */

#include "mildenhall_context_drawer.h"

G_DEFINE_TYPE (MildenhallContextDrawer, mildenhall_context_drawer, MILDENHALL_DRAWER_TYPE_BASE)

#define MILDENHALL_CONTEXT_DRAWER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_CONTEXT_DRAWER, MildenhallContextDrawerPrivate))

#define CONTEXT_DRAWER_POSITION         660.0,394.0

struct _MildenhallContextDrawerPrivate
{
	gboolean bDummy;
};

/**
 * mildenhall_context_drawer_new:
 * Returns: Mildenhall Context Drawer object 
 *
 * Creates a context drawer object
 */
ClutterActor *mildenhall_context_drawer_new (void)
{
  return g_object_new (MILDENHALL_TYPE_CONTEXT_DRAWER, NULL);
}


/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/

/********************************************************
 * Function : mildenhall_context_drawer_get_property
 * Description: Get a property value 
 * Parameters: The object reference, property Id, 
 *              return location for where the property 
 *              value is to be returned and
 *              the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_get_property (GObject *pObject, guint uinPropertyID, GValue *pValue, GParamSpec *pspec)
{
        if(!MILDENHALL_IS_CONTEXT_DRAWER(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        switch (uinPropertyID)
        {
		default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
        }
}

/********************************************************
 * Function : mildenhall_context_drawer_set_property
 * Description: set a property value 
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_set_property (GObject *pObject, guint uinPropertyID, const GValue *pValue, GParamSpec *pspec)
{
        if(! MILDENHALL_IS_CONTEXT_DRAWER(pObject))
        {
                g_warning("invalid drawer object\n");
                return;
        }

        switch (uinPropertyID)
        {
		 default:
                        G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinPropertyID, pspec);
        }
}
/********************************************************
 * Function : mildenhall_button_drawer_dispose
 * Description: Dispose the mildenhall context drawer  object
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_dispose (GObject *pObject)
{
        G_OBJECT_CLASS (mildenhall_context_drawer_parent_class)->dispose (pObject);

        /* TBD: free all memory */
        //MildenhallContextDrawerPrivate *priv = MILDENHALL_CONTEXT_DRAWER_GET_PRIVATE(pObject);

}

/********************************************************
 * Function : mildenhall_context_drawer_finalize
 * Description: Finalize the mildenhall context drawer object 
 * Parameters: The object reference
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_finalize (GObject *pObject)
{
        G_OBJECT_CLASS (mildenhall_context_drawer_parent_class)->finalize (pObject);
}

/********************************************************
 * Function : mildenhall_context_drawer_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_class_init (MildenhallContextDrawerClass *klass)
{
        GObjectClass *pObjectClass = G_OBJECT_CLASS (klass);

        g_type_class_add_private (klass, sizeof (MildenhallContextDrawerPrivate));

        pObjectClass->get_property = mildenhall_context_drawer_get_property;
        pObjectClass->set_property = mildenhall_context_drawer_set_property;
        pObjectClass->dispose = mildenhall_context_drawer_dispose;
        pObjectClass->finalize = mildenhall_context_drawer_finalize;
}

/********************************************************
 * Function : mildenhall_context_drawer_init
 * Description: Instance initialisation function for the object type.
 *              Called automatically on every call to g_object_new
 * Parameters: The object's reference
 * Return value: void
 ********************************************************/
static void mildenhall_context_drawer_init (MildenhallContextDrawer *self)
{
        self->priv = MILDENHALL_CONTEXT_DRAWER_GET_PRIVATE (self);
	g_object_set(self, "type", MILDENHALL_CONTEXT_DRAWER, NULL);
        clutter_actor_set_position(CLUTTER_ACTOR(self), CONTEXT_DRAWER_POSITION);

}
