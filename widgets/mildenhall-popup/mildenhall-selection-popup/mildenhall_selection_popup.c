/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/******************************************************************************
 *	@Filename : mildenhall_selection_popup.c
 *	@Project: --
 *-----------------------------------------------------------------------------
 * 	@Created on : Jan 24, 2013
 *------------------------------------------------------------------------------
 *  @Description : This widget can be used to display the options to user.
 *  				One option among them can be selected and the info is communicated
 *  				to the application.
 *
 *
 * Description of FIXES:
 * -----------------------------------------------------------------------------------
 *	Description													Date			Name
 *	----------													----			----
 *	1 .	 *Text Header Display issue solved						5/2/2013
 *		 *Multiple Signal Connection avoided
 *		 *Improper Updation in Previous Roller Item Fields solved
 *
 *	2 .	*replace of depreciated Cogltexture with ui_texture		7/3/2013
 *
 *	3. 	* Dynamic update of Rows handled(add/replace/remove)	26/3/2013
 *	4.	* Complexity score reduced								28/05/2013
 *	5.	* Support to update Http linked Icons in seperate
 *			Thread without blocking HMI.						21/03/2014
 *******************************************************************************/

#include "mildenhall_selection_popup.h"

#include <thornbury/thornbury.h>

/***********************************************************
 * @local function declarations
 **********************************************************/
/* Internal Function declaration */
static void v_selection_popup_change_model_string_field (ThornburyModelIter *pIter , guint uinColunm , gchar *pText );
static void v_selection_popup_change_model_uint_field (ThornburyModelIter *pIter , guint uinColunm , guint bValue );
static void v_selection_popup_change_model_pointer_field (ThornburyModelIter *pIter , guint uinColunm , gpointer pPointer);
static guint u_selection_popup_find_match_in_model (ThornburyModel *pModel, const gchar* pStringToSearch, guint uinColumnNo);
static void v_mildenhall_selection_popup_update_up_down_arrows(MildenhallSelectionPopup *pSelectionPopup);
static gchar *p_selection_popup_extract_string_from_model(ThornburyModel *pModel , guint uinRow ,guint uinColumn);
static void v_selection_popup_update_roller_model(MildenhallSelectionPopup *pSelf , gpointer Row_hash , gint inCurrentRow  );

static void v_selection_popup_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static void v_mildenhall_selection_popup_create_default_view (MildenhallSelectionPopup *pSelf);
static void v_selection_popup_extract_model_info(MildenhallSelectionPopup *pSelf);
static void v_selection_popup_hide_with_animation(MildenhallSelectionPopup *pSelectionPopup, gfloat fltX, gfloat fltY);
static void v_selection_popup_show_with_animation(MildenhallSelectionPopup *pSelectionPopup, gfloat fltX, gfloat fltY);
static gboolean b_selection_popup_roller_adjustment (MildenhallSelectionPopup *pSelectionPopup, MxAdjustment *pVadjustment, gboolean bDirectionUp);
static void v_mildenhall_selection_popup_reposition_actors( MildenhallSelectionPopup *pSelectionPopup , gfloat pTextHeightBefore);

/* CALL BACK FUNCTIONS */



static void v_selection_popup_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup);
static void v_selection_popup_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup);
static void v_selection_popup_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup);
static void v_selection_popup_animation_completed_cb(ClutterAnimation *anim, MildenhallSelectionPopup *pSelectionPopup);
static void v_selection_popup_up_press_cb(ClutterActor *pDownButton,  gpointer pUserData);
static void v_selection_popup_down_press_cb(ClutterActor *pDownButton,  gpointer pUserData);
static void v_selection_popup_back_press_cb(ClutterActor *pDownButton,  gpointer pUserData);
static void v_selection_popup_swipe_cb(ClutterDragAction   *pAction, ClutterActor *pActor, gfloat  fltEventX, gfloat  fltEventY,
		ClutterModifierType  modifiers, gpointer  *pSelectionPopup);
static void v_selection_popup_scroll_completed_cb(MildenhallRollerContainer *pRollerCont , gpointer pUserData);
static void v_selection_popup_roller_item_activated_cb (MildenhallRollerContainer *pRollerCont,guint uinRow, gpointer pUserData);
static void v_selection_popup_go_press_cb(ClutterActor *pGoButton,  gpointer pUserData);


static void v_selection_popup_fetch_http_icons_clb(ClutterContent *pImage, GError *pError, gpointer pSelectionPopup);


/******************************************************************************
 * @Defines
 *****************************************************************************/



/**
 * _enSelectionPopupProperty:
 * property enums of Selection popup
 */
typedef enum _enSelectionPopupProperty enSelectionPopupProperty;
enum _enSelectionPopupProperty
{
	/*< private >*/
	PROP_FIRST,
	PROP_MODEL,
	PROP_REMOVE,
	PROP_LAST
};


/**
 * _enSelectionPopupModelInfo
 * @TEXT_ENTRY_MSGSTYLE 		:Key for displaying info Text about selection : Row 1 -> key = Static Row 2->Key = Dynamic
 * @TEXT_ENTRY_MSGTEXT			:Text to be displayed to give info Text about selection
 * @IMAGEICON_KEY				:Key for Icon Row 1 -> key = AppIcon Row 2 -> MsgIcon
 * @SELECTION_POPUP_ROWS_INFO	:Three / Four field Array of Key-Value pair for Roller Item Type
 *
 *
 * Model format for displaying selection Popup
 */


typedef enum _enSelectionPopupModelInfo enSelectionPopupModelInfo;
enum _enSelectionPopupModelInfo {
	TEXT_ENTRY_MSGSTYLE,
	TEXT_ENTRY_MSGTEXT,
	IMAGEICON_KEY,
	IMAGEPATH_VALUE,
	SELECTION_POPUP_ROWS_INFO,
	SELECTION_POPUP_COLUMN_LAST

};


/**
 * _enRollerItemModel:
 * Model info for SElection Popup Roller Item
 */
typedef enum _enRollerItemModel enRollerItemModel;
enum _enRollerItemModel
{
	/*< private >*/
	COL_LEFT_ICON_FIELD,
	COL_MIDDLE_TEXT_FIELD,
	COL_RIGHT_FIELD,
	COL_SELECTION_ROLLER_ITEM_TYPE,
	COL_SELECTION_ROLLER_ITEM_NAME,
	COL_ROLLER_ITEM_MODEL_LAST
};


/**
 * _enSpellerButonModel:
 * Model info for speller button
 */
typedef enum _enSpellerButonModel enSpellerButonModel;
enum _enSpellerButonModel
{
	/*< private >*/
	COL_TEXT_OR_TEXTURE,
	COL_SPELLER_BUTTON_LAST
};

typedef struct _SelectionPopupRollerFields SelectionPopupRollerFields ;
struct _SelectionPopupRollerFields
{
	gchar *pRowId  ;
	gchar *pMidFieldText ;
	gchar *pRightFieldText ;
	gchar* pModelIconToBeSent ;
};



G_DEFINE_TYPE (MildenhallSelectionPopup, mildenhall_selection_popup, LIGHTWOOD_TYPE_POPUP_BASE)

#define MILDENHALL_SELECTION_POPUP_GET_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_SELECTION_POPUP, MildenhallSelectionPopupPrivate))

#define MILDENHALL_SELECTION_POPUP_PRINT( a ...)			/* g_print(a); */


/* Style json file related constants */
#define MILDENHALL_SELECTION_POPUP_RECT_COLOR 							"background-color"
#define MILDENHALL_SELECTION_POPUP_RECT_WIDTH							"background-width"
#define MILDENHALL_SELECTION_POPUP_RECT_HEIGHT							"background-height"
#define MILDENHALL_SELECTION_POPUP_RECT_OPACITY						"background-opacity"

#define MILDENHALL_SELECTION_POPUP_ROLLER_X							"roller-x"
#define MILDENHALL_SELECTION_POPUP_ROLLER_Y							"roller-y"

#define MILDENHALL_SELECTION_POPUP_HEAD_ICON 							"popup-head-icon"
#define MILDENHALL_SELECTION_POPUP_HEAD_X								"head-arrow-x"
#define MILDENHALL_SELECTION_POPUP_HEAD_Y								"head-arrow-y"
#define MILDENHALL_SELECTION_POPUP_HEAD_HEIGHT							"head-arrow-height"

#define MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_HEIGHT					"text-bottom-height"
#define MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_X						"text-bottom-x"
#define MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_Y						"text-bottom-y"
#define MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_ICON 					"text-bottom-icon"

#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH					"text-entry-width"
#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT					"text-entry-height"
#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_X						"text-entry-x"
#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_Y						"text-entry-y"
#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_FONT						"text-entry-font"
#define MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_COLOR					"text-entry-color"

#define MILDENHALL_SELECTION_POPUP_UP_X								"up-button-x"
#define MILDENHALL_SELECTION_POPUP_UP_Y								"up-button-y"
#define MILDENHALL_SELECTION_POPUP_UP_WIDTH							"up-button-width"
#define MILDENHALL_SELECTION_POPUP_UP_HEIGHT							"up-button-height"

#define MILDENHALL_SELECTION_POPUP_DOWN_X								"down-button-x"
#define MILDENHALL_SELECTION_POPUP_DOWN_Y								"down-button-y"
#define MILDENHALL_SELECTION_POPUP_DOWN_WIDTH							"down-button-width"
#define MILDENHALL_SELECTION_POPUP_DOWN_HEIGHT							"down-button-height"

#define MILDENHALL_SELECTION_POPUP_BACK_X								"back-button-x"
#define MILDENHALL_SELECTION_POPUP_BACK_Y								"back-button-y"
#define MILDENHALL_SELECTION_POPUP_BACK_WIDTH							"back-button-width"
#define MILDENHALL_SELECTION_POPUP_BACK_HEIGHT							"back-button-height"
#define MILDENHALL_SELECTION_POPUP_BACK_TEXT_FONT						"back-text-font"

#define MILDENHALL_SELECTION_POPUP_BUTTON_PRESSED	 					"button-pressed"
#define MILDENHALL_SELECTION_POPUP_BUTTON_NORMAL						"button-normal"

#define MILDENHALL_SELECTION_POPUP_SEAMLESS_WIDTH						"seamless-width"
#define MILDENHALL_SELECTION_POPUP_SEAMLESS_ICON						"seamless-icon"
#define MILDENHALL_SELECTION_POPUP_SEAMLESS_TEXTURE_DISPLACEMENT 		"seamless-displacement"
#define MILDENHALL_SELECTION_POPUP_SEAMLESS_OPACITY 					"seamless-opacity"
#define MILDENHALL_SELECTION_POPUP_SEAMLESS_COLOR 						"seamless-color"

#define MILDENHALL_SELECTION_POPUP_UP_FILLED							"up-filled-icon"
#define MILDENHALL_SELECTION_POPUP_UP_EMPTY							"up-empty-icon"
#define MILDENHALL_SELECTION_POPUP_DOWN_FILLED							"down-filled-icon"
#define MILDENHALL_SELECTION_POPUP_DOWN_EMPTY							"down-empty-icon"

#define MILDENHALL_SELECTION_POPUP_GO_X								"go-x"
#define MILDENHALL_SELECTION_POPUP_GO_Y								"go-y"
#define MILDENHALL_SELECTION_POPUP_GO_WIDTH							"go-width"
#define MILDENHALL_SELECTION_POPUP_GO_HEIGHT							"go-height"
#define MILDENHALL_SELECTION_POPUP_GO_FONT								"go-font"
#define MILDENHALL_SELECTION_POPUP_GO_PRESSED							"go-pressed-icon"
#define MILDENHALL_SELECTION_POPUP_GO_NORMAL							"go-normal-icon"

/* Internal constants */

#define SPELLER_BUTTON_TEXTURE 									"texture"
#define SPELLER_BUTTON_TEXT										"text"
#define MILDENHALL_SELECTION_POPUP_ON_SHOW_X            				76.0
#define MILDENHALL_SELECTION_POPUP_ON_SHOW_Y            				114.0
#define MILDENHALL_SELECTION_POPUP_ON_HIDE_X            				76.0
#define MILDENHALL_SELECTION_POPUP_ON_HIDE_Y            				458.0

#define MILDENHALL_SELECTION_POPUP_SHOWTIME          					1000
#define MILDENHALL_SELECTION_POPUP_AUTOSCROLL_FREQUENCY   				 250

#define ROLLER_ITEM_FIELD_ONE 									"RowId"
#define ROLLER_ITEM_FIELD_TWO_IMAGE								"Image"
#define ROLLER_ITEM_FIELD_TWO_TEXT								"Text"
#define ROLLER_ITEM_FIELD_THREE									"Text"
#define ROLLER_ITEM_FIELD_FOUR_TEXT								"Text"
#define ROLLER_ITEM_FIELD_FOUR_RADIO							"Radio"
#define ROLLER_ITEM_FIELD_FOUR_INFO_CONFIRM						"InfoConfirm"
#define MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID						"@SELECTION$POPUP$DUMMY@"

#define LEFT_ICON_MAX_W	58.0
#define LEFT_ICON_MAX_H 58.0


#define MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pointer) 		if(NULL != pointer) \
		{ \
	g_free(pointer); \
	pointer = NULL; \
		}


#define MILDENHALL_SELECTION_POPUP_RETURN_IF_NULL(pointer ,string)  if(NULL != pointer) \
		{ \
	MILDENHALL_SELECTION_POPUP_PRINT("%s is NULL \n" , string); \
	return; \
		}


/**
 * _MildenhallSelectionPopupPrivate:
 * structure to hold the private variables of Selection Popup
 */

struct _MildenhallSelectionPopupPrivate
{
	gfloat fltHeight;										/* height of the popup */
	gfloat fltWidth;										/* width of the Popup */
	guint uinActivatedRow;									/* No of the activated row */
	gfloat fltPrevDownValue;								/* previous value of position roller item for Internal calculation */
	gint inNoOfRows;										/* Total No of Rows created */
	gfloat fltSeemlessDisp;									/* Displacement of the Line on either side */
	enMildenhallSelectionPopupItemType enSelectionPopupType;		/* Enum for Type of Selection Popup Roller item Type */

	ThornburyModel *pRollerItemModel;							/* Selection Popup Roller item Type  Model */
	ThornburyModel *pPopupModel;								/* Selection Popup Model */
	ThornburyModel *pModelUpButton ;							/* Up button Model */
	ThornburyModel *pModelDownButton ;							/* Down Button Model */

	GHashTable	*pImageHash;								/* Hash table for storing  Image data for style */
	GHashTable *pPrivHash;									/* Hash table for storing style related data */
	GHashTable	*pRollerId;									/* Hash table for storing  Roller Item Id*/
	ClutterActor *pPopup;
	ClutterActor *pPopupRect;
	ClutterActor *pButtonUp;
	ClutterActor *pButtonDown;
	ClutterActor *pButtonBack;
	ClutterActor *pPopupHead;
	ClutterActor *pPopupText;
	ClutterActor *pPopupTextEntry;
	ClutterActor *pPopupTextBottom;
	ClutterActor *pRollerContainer;
	ClutterActor *pAppIcon;
	ClutterActor *pMsgIcon;
	ClutterActor *pSeemlessLeft;
	ClutterActor *pSeemlessRight;
	ClutterActor *pDragArea ;
	ClutterActor *pButtonGo;
	ClutterActor *pBottomGroup;

	ClutterAction *pDragAction;								/* Clutter Actor to implement Drag Action */

	ClutterColor TextEntryColor;
	ClutterColor RectBackgroundColor;
	ClutterColor SeamlessColor;

	gboolean bAnimationStart;
	gboolean bHideStart;
	gboolean animate_show;
	gboolean bGoConnected;
	gboolean bNeedToAppend;
	gboolean bPositionExtreme ;
	gboolean bRemove ;

	GQueue *pHttpIconQ;
	gchar *pHttpIconId;
	gboolean bPopupShown ;
	gboolean bWaitingResponse;

};


/* getter functions */

/**
 * mildenhall_selection_popup_get_width:
 * @pObject : popup object reference
 * @Returns : width of the popup widget
 *
 * Function to get the popup widget width
 */
gfloat mildenhall_selection_popup_get_width( MildenhallSelectionPopup *pSelectionPopup)
{
	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return -1;
	}
	return clutter_actor_get_width(CLUTTER_ACTOR(pSelectionPopup));

}



/**
 * mildenhall_selection_popup_get_height:
 * @pObject : popup object reference
 * @Returns : height of the popup widget
 *
 * Function to get the popup widget height
 */
gfloat mildenhall_selection_popup_get_height( MildenhallSelectionPopup *pSelectionPopup)
{
	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return -1;
	}
	return clutter_actor_get_height(CLUTTER_ACTOR(pSelectionPopup));

}




/**
 * v_mildenhall_selection_popup_set_model:
 * @pObject : selection popup object reference
 * @pModel : model for the  selection popup
 *
 * Model contains the data to be displayed on the selection popup.
 * Selection popup model contains:
 * 	TEXT_ENTRY_MSGSTYLE,		: Key for displaying info Text about selection : Row 1 -> key = Static Row 2->Key = Dynamic
 * 	TEXT_ENTRY_MSGTEXT,			: Text to be displayed to give info Text about selection
 * 	IMAGEICON_KEY,				: Key for Icon Row 1 -> key = AppIcon Row 2 -> MsgIcon
 * 	IMAGEPATH_VALUE,			: Path of the Icon for corresponding row's Icon key
 * 	SELECTION_POPUP_ROWS_INFO,	: Three / Four field Array of Key-Value pair
 * 									Field1: Row Id
 * 									Field2:	Left Image/Text
 * 									Field3: Mid Text
 * 									Field4: Text/Radio
 */
void v_mildenhall_selection_popup_set_model(MildenhallSelectionPopup *pSelectionPopup, ThornburyModel *pModel)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return ;
	}

	if(NULL != priv->pPopupModel)
	{
		g_signal_handlers_disconnect_by_func (priv->pPopupModel,
				G_CALLBACK (v_selection_popup_row_added_cb),
				pSelectionPopup);
		g_signal_handlers_disconnect_by_func (priv->pPopupModel,
				G_CALLBACK (v_selection_popup_row_changed_cb),
				pSelectionPopup);
		g_signal_handlers_disconnect_by_func (priv->pPopupModel,
				G_CALLBACK (v_selection_popup_row_removed_cb),
				pSelectionPopup);
		g_object_unref (priv->pPopupModel);

		priv->pPopupModel = NULL;
	}

	/* update the new model with signals */
	if (pModel != NULL)
	{
		g_return_if_fail (G_IS_OBJECT (pModel));

		priv->pPopupModel = g_object_ref (pModel);

		g_signal_connect (priv->pPopupModel,
				"row-added",
				G_CALLBACK (v_selection_popup_row_added_cb),
				pSelectionPopup);

		g_signal_connect (priv->pPopupModel,
				"row-changed",
				G_CALLBACK (v_selection_popup_row_changed_cb),
				pSelectionPopup);

		g_signal_connect (priv->pPopupModel,
				"row-removed",
				G_CALLBACK (v_selection_popup_row_removed_cb),
				pSelectionPopup);

	}
	v_selection_popup_extract_model_info(pSelectionPopup);
	g_object_notify (G_OBJECT(pSelectionPopup), "popup-model");
}


/*****************************************************************************************************
 * Internal functions
 *****************************************************************************************************/



/********************************************************
 * Function : v_mildenhall_selection_popup_get_property
 * Description: get a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_get_property (GObject    *pObject, guint  uinProperty_id, GValue     *pValue,  GParamSpec *pSpec)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pObject);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SELECTION_POPUP(pObject))
	{
		g_warning("invalid popup object\n");
		return;
	}

	switch (uinProperty_id)
	{
	case PROP_MODEL:
		g_value_set_object ( pValue, priv->pPopupModel);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pSpec);
		break;
	}

}

/********************************************************
 * Function : v_mildenhall_selection_popup_set_property
 * Description: set a property value
 * Parameters: The object reference, property Id, new value
 *             for the property and the param spec of the object
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_set_property (GObject      *pObject, guint  uinProperty_id, const GValue *pValue,  GParamSpec   *pSpec)
{
        MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pObject);
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pObject);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	if(!MILDENHALL_IS_SELECTION_POPUP(pObject))
	{
		g_warning("invalid popup object\n");
		return;
	}
	switch (uinProperty_id)
	{
	case PROP_MODEL:
		v_mildenhall_selection_popup_set_model( pSelectionPopup, g_value_get_object (pValue) );
		break;
	case PROP_REMOVE:
	{
		priv->bRemove = g_value_get_boolean(pValue);
	}
	break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (pObject, uinProperty_id, pSpec);
		break;
	}
}


/********************************************************
 * Function : v_selection_popup_add_style_to_hash
 * Description: maintain style hash for background images
 * Parameter :  *pButtonDrawer, *pKey, pValue
 * Return value: void
 ********************************************************/
static void v_selection_popup_add_style_to_hash(MildenhallSelectionPopup *pSelectionPopup, gchar *pKey, gpointer pValue)
{

	if (NULL != pKey || NULL != pValue)
	{
		MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
		/* maintain key and value pair for style name and its value */
		if (G_VALUE_HOLDS_DOUBLE(pValue))
		{
			g_hash_table_insert(priv->pPrivHash, pKey,
					g_strdup_printf("%f",g_value_get_double(pValue)));
		}
		else if (G_VALUE_HOLDS_STRING(pValue))
		{
			g_hash_table_insert (priv->pPrivHash,
			                     pKey, g_value_dup_string (pValue));
		}
		else if (G_VALUE_HOLDS_INT64(pValue))
		{
			g_hash_table_insert (priv->pPrivHash, pKey,
					g_strdup_printf ("%ld", g_value_get_int64 (pValue)));
		}

	}
}


/********************************************************
 * Function : v_selection_popup_add_image_to_hash
 * Description: maintain style hash for background images
 * Parameter :  *pButtonDrawer, *pKey, pValue
 * Return value: void
 ********************************************************/
static void v_selection_popup_add_image_to_hash(MildenhallSelectionPopup *pSelectionPopup, gchar *pKey, gpointer pValue , gboolean bAppend)
{
	if(NULL != pKey || NULL != pValue)
	{
		MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
		const gchar *pFilePath = NULL;
		pFilePath = g_value_get_string(pValue);
		/* maintain key and value pair for style name and releated image path */

		if(bAppend == FALSE)
			g_hash_table_insert(priv->pImageHash, pKey, g_strdup_printf("%s", pFilePath));
		else
			g_hash_table_insert(priv->pImageHash, pKey, g_strdup_printf(PKGTHEMEDIR"/%s" ,pFilePath ));
	}

}



/********************************************************
 * Function : v_selection_popup_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_selection_popup_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallSelectionPopup *pSelectionPopup = pUserData;
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

	if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_RECT_COLOR) == 0)
	{
		clutter_color_from_string(&priv->RectBackgroundColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_RECT_WIDTH) == 0)
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_RECT_HEIGHT) == 0)
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_RECT_OPACITY) == 0)
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_ROLLER_X) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_ROLLER_Y) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_HEAD_ICON) == 0)
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey, pValue , FALSE);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_HEAD_X) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_HEAD_Y) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_HEAD_HEIGHT ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_HEIGHT ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_X ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_Y ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_ICON ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey, pValue , FALSE);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_X) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_Y ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_FONT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_COLOR) == 0 )
	{
		clutter_color_from_string(&priv->TextEntryColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_SEAMLESS_COLOR) == 0 )
	{
		clutter_color_from_string(&priv->SeamlessColor,  g_value_get_string(pValue));
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_UP_X ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_UP_Y ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_UP_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_UP_HEIGHT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_DOWN_X ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_DOWN_Y ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_DOWN_HEIGHT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_DOWN_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_BACK_X) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_BACK_Y) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_BACK_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_BACK_HEIGHT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_BUTTON_PRESSED ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey, pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_BUTTON_NORMAL) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey, pValue , TRUE);
	}
#if 0
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_APPICON_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_APPICON_HEIGHT ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_MSGICON_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_MSGICON_HEIGHT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
#endif
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_UP_FILLED ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_UP_EMPTY ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_DOWN_FILLED ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_DOWN_EMPTY ) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_GO_X ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_GO_Y) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_GO_WIDTH ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_GO_HEIGHT ) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey,MILDENHALL_SELECTION_POPUP_GO_FONT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_BACK_TEXT_FONT) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_SEAMLESS_WIDTH) == 0 )
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_SEAMLESS_TEXTURE_DISPLACEMENT) == 0 )
	{
		priv->fltSeemlessDisp = g_value_get_double(pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_SEAMLESS_ICON) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue,FALSE);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_SEAMLESS_OPACITY) == 0)
	{
		v_selection_popup_add_style_to_hash(pSelectionPopup,pStyleKey,pValue);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_GO_PRESSED) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}
	else if(g_strcmp0(pStyleKey, MILDENHALL_SELECTION_POPUP_GO_NORMAL) == 0 )
	{
		v_selection_popup_add_image_to_hash(pSelectionPopup,pStyleKey,pValue , TRUE);
	}


}


/********************************************************
 * Function : v_mildenhall_selection_popup_dispose
 * Description: Dispose the MILDENHALL selection Popup
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_dispose (GObject *pObject)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pObject);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_selection_popup_parent_class)->dispose (pObject);
	if(NULL != priv)
	{

		if(NULL != priv->pRollerId)
		{
			g_hash_table_destroy(priv->pRollerId);
			priv->pRollerId = NULL;
		}

		if(NULL != priv->pImageHash)
		{
			g_hash_table_destroy (priv->pImageHash);
			priv->pImageHash = NULL;
		}
		if(NULL != priv->pPrivHash)
		{
			g_hash_table_destroy(priv->pPrivHash);
			priv->pPrivHash = NULL;
		}

	}
}



/********************************************************
 * Function : v_mildenhall_selection_popup_finalize
 * Description: Finalise the MILDENHALL selection Popup
 * Parameters: The object reference
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_finalize (GObject *object)
{
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	G_OBJECT_CLASS (mildenhall_selection_popup_parent_class)->finalize (object);
}



/********************************************************
 * Function : mildenhall_selection_popup_class_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/

static void mildenhall_selection_popup_class_init (MildenhallSelectionPopupClass *pKlass)
{
	GObjectClass *pObjectClass = G_OBJECT_CLASS (pKlass);
	GParamSpec *pSpec = NULL;
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	g_type_class_add_private (pKlass, sizeof (MildenhallSelectionPopupPrivate));

	pObjectClass->get_property = v_mildenhall_selection_popup_get_property;
	pObjectClass->set_property = v_mildenhall_selection_popup_set_property;
	pObjectClass->dispose = v_mildenhall_selection_popup_dispose;
	pObjectClass->finalize = v_mildenhall_selection_popup_finalize;


	/**
	 * SelectionPopup: model:
	 *
	 * Selection popup model contains:
	 * 	TEXT_ENTRY_MSGSTYLE,		: Key for displaying info Text about selection : Row 1 -> key = Static Row 2->Key = Dynamic
	 * 	TEXT_ENTRY_MSGTEXT,			: Text to be displayed to give info Text about selection
	 * 	IMAGEICON_KEY,				: Key for Icon Row 1 -> key = AppIcon Row 2 -> MsgIcon
	 * 	IMAGEPATH_VALUE,			: Path of the Icon for corresponding row's Icon key
	 * 	SELECTION_POPUP_ROWS_INFO,	: Three / Four field Array of Key-Value pair
	 * 									Field1: Row Id
	 * 									Field2:	Left Image/Text
	 * 									Field3: Mid Text
	 * 									Field4: Text/Radio
	 *
	 */

	pSpec = g_param_spec_object("popup-model", "Model",
			"Model information of popup widget", G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property(pObjectClass, PROP_MODEL, pSpec);

	/**
	 * SelectionPopup: remove:
	 *
	 * remove property: boolean to decide whether to replace or remove the Row when conflicting row Id is found
	 * Default: FALSE
	 */

	pSpec = g_param_spec_boolean ("remove",
			"replace",
			"boolean to decide whether to replace or remove the Row when conflicting row Id is found",
			FALSE,
			G_PARAM_WRITABLE);
	g_object_class_install_property (pObjectClass, PROP_REMOVE, pSpec);



}

/********************************************************
 * Function : mildenhall_selection_popup_init
 * Description: Class initialisation function for the object type.
 *              Called automatically on the first call to g_object_new
 * Parameters: The object's class reference
 * Return value: void
 ********************************************************/



static void mildenhall_selection_popup_init (MildenhallSelectionPopup *self)
{
	GHashTable *style_table = NULL;

	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	self->priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (self);

	/* initialize the priv structure */
	self->priv->fltWidth = 0.0;
	self->priv->fltHeight = 0.0;
	self->priv->pRollerItemModel = NULL;
	self->priv->pPopupRect = NULL;
	self->priv->pButtonUp = NULL;
	self->priv->pButtonBack = NULL;
	self->priv->pButtonDown =NULL;
	self->priv->pPopupHead = NULL;
	self->priv->pImageHash = NULL;
	self->priv->enSelectionPopupType = TYPE_ICON_LABEL_LABEL;
	self->priv->pPopupTextEntry = NULL;
	self->priv->pPopupTextBottom = NULL;
	/* initialise the button group */
	self->priv->pPopup = clutter_actor_new ();
	self->priv->pRollerContainer = NULL;
	self->priv->pPrivHash = NULL;
	self->priv->pPopupModel = NULL;
	self->priv->pAppIcon = NULL;
	self->priv->pMsgIcon = NULL;
	self->priv->bAnimationStart = FALSE;
	self->priv->bHideStart = FALSE;
	self->priv->pDragArea = NULL;
	self->priv->pDragAction = NULL;
	self->priv->pRollerId = NULL;
	self->priv->pButtonGo = NULL;
	self->priv->bGoConnected = FALSE;
	self->priv->fltPrevDownValue = 0.0;
	self->priv->inNoOfRows = 0;
	self->priv->pModelUpButton = NULL;
	self->priv->pModelDownButton = NULL;
	self->priv->pSeemlessLeft = NULL;
	self->priv->pSeemlessRight = NULL;
	self->priv->pPopupText = NULL;
	self->priv->fltSeemlessDisp = 64.0;
	self->priv->pBottomGroup = NULL;
	self->priv->bNeedToAppend  = FALSE;
	self->priv->bPositionExtreme = FALSE;
	self->priv->bRemove = FALSE;
	self->priv->pHttpIconQ = g_queue_new();
	self->priv->pHttpIconId = NULL ;
	self->priv->bPopupShown = FALSE ;
	self->priv->bWaitingResponse = FALSE ;

	clutter_actor_add_child (CLUTTER_ACTOR(self), self->priv->pPopup);

	self->priv->pPrivHash = g_hash_table_new_full (g_str_hash,g_str_equal, g_free, g_free);
	self->priv->pImageHash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);

	/* get the hash table for style properties */
	style_table = thornbury_style_set (PKGDATADIR "/mildenhall_selection_popup_style.json");

	/* Todo : image for bordering downward arrow */

	/* parse style hash table */
	if (style_table != NULL)
	  {
	    GHashTableIter iter;
	    gpointer key, value;

	    g_hash_table_iter_init (&iter, style_table);
	    while (g_hash_table_iter_next (&iter, &key, &value))
	      {
	        GHashTable *table = value;

	        if (table == NULL)
	          continue;

	        g_hash_table_foreach (table, v_selection_popup_parse_style, self);
	      }
	  }

	/* free the style hash */
	thornbury_style_free (style_table);

	self->priv->pRollerId =  g_hash_table_new_full(g_str_hash,g_str_equal, g_free, g_free);

	/* create static default view of Selection Popup */
	v_mildenhall_selection_popup_create_default_view(self);

	/* set the position to minimum by default */
	clutter_actor_set_position(CLUTTER_ACTOR(self), MILDENHALL_SELECTION_POPUP_ON_HIDE_X,(MILDENHALL_SELECTION_POPUP_ON_HIDE_Y + 2));
	/* Hide the popup until requested to show */
	clutter_actor_hide(CLUTTER_ACTOR(self));

	/* connect to transitions-completed signal for connecting actors to  other events on completion of animation */
	g_signal_connect(CLUTTER_ACTOR(self), "transitions-completed",G_CALLBACK(v_selection_popup_animation_completed_cb), self);
	g_signal_connect (G_OBJECT (self->priv->pRollerContainer), "roller-item-activated",
			G_CALLBACK (v_selection_popup_roller_item_activated_cb), self);


	g_signal_connect(self->priv->pButtonDown,"button-press", G_CALLBACK(v_selection_popup_down_press_cb), self);
	g_signal_connect(self->priv->pButtonUp,"button-press", G_CALLBACK(v_selection_popup_up_press_cb), self);
	g_signal_connect(self->priv->pButtonBack,"button-press", G_CALLBACK(v_selection_popup_back_press_cb), self);
	g_signal_connect (self->priv->pDragAction, "drag-end", G_CALLBACK (v_selection_popup_swipe_cb), self);

	g_signal_connect (G_OBJECT (self->priv->pRollerContainer), "roller-locking-finished",
			G_CALLBACK (v_selection_popup_scroll_completed_cb), self);

}


/**
 * mildenhall_selection_popup_new :
 * @parameters : void
 * @Returns: MILDENHALL Selection popup
 *
 * Since: 1.0
 * Creates a MILDENHALL Selection popup
 */


ClutterActor * mildenhall_selection_popup_new (void)
{
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	return g_object_new (MILDENHALL_TYPE_SELECTION_POPUP, NULL);
}



/********************************************************
 * Function : v_selection_popup_swipe_cb
 * Description: callabck on press of UP button
 * Parameters: ClutterActor*, pUserData
 * Return value: void
 ********************************************************/


static void v_selection_popup_swipe_cb(ClutterDragAction   *pAction, ClutterActor *pActor, gfloat  fltEventX, gfloat  fltEventY,
		ClutterModifierType  modifiers, gpointer  *pSelectionPopup)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	MILDENHALL_SELECTION_POPUP_PRINT("\n---- SWIPE -----\n \n");
	if (priv->bHideStart == FALSE)
	{
		/* Hide the popup */
		v_selection_popup_hide_with_animation((MildenhallSelectionPopup *) pSelectionPopup, MILDENHALL_SELECTION_POPUP_ON_HIDE_X, MILDENHALL_SELECTION_POPUP_ON_HIDE_Y);
	}

}

/********************************************************
 * Function : v_selection_popup_up_press_cb
 * Description: callabck on press of UP button
 * Parameters: ClutterActor*, pUserData
 * Return value: void
 ********************************************************/


static void v_selection_popup_up_press_cb(ClutterActor *pDownButton,  gpointer pUserData)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP(pUserData);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

	/* get 	MxAdjustment for modifing roller position */
	MxAdjustment *pVadjust;
        gboolean bStatus;
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	g_object_get (priv->pRollerContainer, "vadjust", &pVadjust, NULL);
	g_return_if_fail (pVadjust != NULL);

	bStatus = b_selection_popup_roller_adjustment (pSelectionPopup, pVadjust, TRUE);

	if(bStatus == FALSE)
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n----NO UP -----\n \n");
	}


}




/********************************************************
 * Function : v_selection_popup_go_press_cb
 * Description: callabck on press of GO! button
 * Parameters: ClutterActor*, pUserData
 * Return value: void
 ********************************************************/


static void v_selection_popup_go_press_cb(ClutterActor *pDownButton,  gpointer pUserData)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pUserData);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(NULL != priv->pRollerId )
	{
		gchar *pSelectedItemId = NULL ;

		if(TYPE_ICON_LABEL_RADIO == priv->enSelectionPopupType)
		{
			pSelectedItemId = p_selection_popup_extract_string_from_model(priv->pRollerItemModel ,priv->uinActivatedRow , COL_SELECTION_ROLLER_ITEM_NAME );
		}
		else
		{
			pSelectedItemId = p_selection_popup_extract_string_from_model(priv->pRollerItemModel ,0 , COL_SELECTION_ROLLER_ITEM_NAME );
		}

		if(0 != g_strcmp0(MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID,pSelectedItemId))
		{
			g_signal_emit_by_name(pSelectionPopup, "popup-action", pSelectedItemId,GINT_TO_POINTER(priv->uinActivatedRow ));
		}
	}

	if (priv->bHideStart == FALSE)
	{
		/* hide the Popup */
		v_selection_popup_hide_with_animation(pSelectionPopup, MILDENHALL_SELECTION_POPUP_ON_HIDE_X, MILDENHALL_SELECTION_POPUP_ON_HIDE_Y);
	}
}

/********************************************************
 * Function : b_selection_popup_roller_adjustment
 * Description: callabck on press of DOWN button
 * Parameters: ClutterActor*, pUserData
 * Return value: gboolean
 ********************************************************/
static gboolean b_selection_popup_roller_adjustment (MildenhallSelectionPopup *pSelectionPopup, MxAdjustment *pVadjustment, gboolean bDirectionUp)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	gfloat fltPosition;
	gfloat fltModifiedPosition;

        /* get the current position */
        gdouble fldStart = mx_adjustment_get_value (pVadjustment);

        gint inNoOfRowAbove = 0;
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	/* Get item height  */
	fltPosition = mx_adjustment_get_step_increment (pVadjustment) ;

	MILDENHALL_SELECTION_POPUP_PRINT("\n\n........fresh values fldStart = %f fltPosition = %f   priv->fltPrevDownValue %f......\n\n",
			fldStart,fltPosition,priv->fltPrevDownValue);

	if (bDirectionUp)
	{
		fltModifiedPosition = fldStart - fltPosition;
	}
	else
	{

		fltModifiedPosition = fldStart + fltPosition;
	}

	inNoOfRowAbove =  ((gint)fldStart) / 66;
	if((bDirectionUp == TRUE && inNoOfRowAbove == 0 ) || (bDirectionUp == FALSE && (inNoOfRowAbove + 4) >= priv->inNoOfRows))
	{
		return FALSE;
	}
	mx_adjustment_interpolate (pVadjustment, fltModifiedPosition, MILDENHALL_SELECTION_POPUP_AUTOSCROLL_FREQUENCY,
			CLUTTER_EASE_IN_OUT_QUAD);

	return TRUE;
}


/********************************************************
 * Function : v_selection_popup_down_press_cb
 * Description: callabck on press of DOWN button
 * Parameters: ClutterActor*, pUserData
 * Return value: void
 ********************************************************/


static void v_selection_popup_down_press_cb(ClutterActor *pDownButton,  gpointer pUserData)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pUserData);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);

	MxAdjustment *pVadjust;
        gboolean bStatus;

        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	g_object_get (priv->pRollerContainer, "vadjust", &pVadjust, NULL);
        bStatus = b_selection_popup_roller_adjustment (pSelectionPopup, pVadjust, FALSE);

	if(bStatus == FALSE)
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n----NO DOWN -----\n \n");
	}


}


/********************************************************
 * Function : v_selection_popup_back_press_cb
 * Description: callabck on press of DOWN button
 * Parameters: ClutterActor*, pUserData
 * Return value: void
 ********************************************************/


static void v_selection_popup_back_press_cb(ClutterActor *pDownButton,  gpointer pUserData)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pUserData);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	MILDENHALL_SELECTION_POPUP_PRINT("\n---- back -----\n \n");

	if (priv->bHideStart == FALSE)
	{
		v_selection_popup_hide_with_animation(pSelectionPopup, MILDENHALL_SELECTION_POPUP_ON_HIDE_X, MILDENHALL_SELECTION_POPUP_ON_HIDE_Y);
	}
}



/********************************************************
 * Function : v_mildenhall_selection_popup_create_default_view
 * Description: create the default view of popup
 * Parameters: MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_create_default_view (MildenhallSelectionPopup *pSelf)
{
	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pSelf);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);

	gfloat fltWidth = 0;
	gfloat fltHeight = 0;
	gfloat fltX = 0;
	gfloat fltY = 0;
	gchar* pPopupIconPressed = NULL;
	gchar* pStrPopupIcon = NULL;
	gchar *pTextFont = NULL ;
	gint 	inOpacity;
        gchar* pStrSpellerIcon = NULL;
	g_autofree gchar *json_filename = _mildenhall_get_resource_path ("mh_rc_selection_prop.json");

        /* Back Button Creation */
        ThornburyModel *pModelBackButton = NULL;

	ThornburyItemFactory *pRollerItemFactory = thornbury_item_factory_generate_widget_with_props (
				MILDENHALL_TYPE_ROLLER_CONTAINER,
				json_filename);
        GObject *pRollerItemObject = NULL;

        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	/* Background Rectangle creation for text entry  */

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_HEAD_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_HEAD_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_RECT_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_RECT_HEIGHT), "%f", &fltHeight);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_RECT_OPACITY), "%d", &inOpacity);



	priv->pPopupRect = clutter_actor_new();
	clutter_actor_set_background_color (priv->pPopupRect, &priv->RectBackgroundColor);
	clutter_actor_set_opacity (priv->pPopupRect, inOpacity);
	clutter_actor_set_width(priv->pPopupRect, fltWidth);

	clutter_actor_set_position(priv->pPopupRect,fltX ,fltY);

	/* add rect to the popup group */
	clutter_actor_add_child (CLUTTER_ACTOR(pSelf) , priv->pPopupRect);

	/* Text Box  Group Creation */

	priv->pPopupTextEntry = clutter_actor_new();
	clutter_actor_add_child (CLUTTER_ACTOR(pSelf), priv->pPopupTextEntry);
	clutter_actor_set_position (priv->pPopupTextEntry,fltX, fltY );

	/* Bottom Group creation for holding Roller and Buttons */
	priv->pBottomGroup = clutter_actor_new();
	clutter_actor_add_child (CLUTTER_ACTOR(pSelf), priv->pBottomGroup);
	clutter_actor_set_position (priv->pBottomGroup,fltX, 80.0 );


	/* Popup head with arrow icon  creation */
#if 1
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_HEAD_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_HEAD_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_HEAD_HEIGHT), "%f", &fltHeight);
#endif

	pStrPopupIcon = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_HEAD_ICON );


	pStrPopupIcon = g_strdup_printf(PKGTHEMEDIR"/%s", pStrPopupIcon);
	priv->pPopupHead = thornbury_ui_texture_create_new(pStrPopupIcon,fltWidth , fltHeight, FALSE, FALSE);

	clutter_actor_add_child (priv->pPopupTextEntry, priv->pPopupHead);
	clutter_actor_set_width(priv->pPopupTextEntry,fltWidth);
	g_object_set(priv->pPopupHead, "x", fltX, "y", fltY , NULL);


	/* Left seamless texture creation */
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_SEAMLESS_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT), "%f", &fltHeight);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_SEAMLESS_OPACITY), "%d", &inOpacity);


	priv->pSeemlessLeft = clutter_actor_new();
	clutter_actor_add_child (priv->pPopupTextEntry, priv->pSeemlessLeft);
	clutter_actor_set_background_color (priv->pSeemlessLeft, &priv->SeamlessColor);
	clutter_actor_set_opacity (priv->pSeemlessLeft, inOpacity);
	clutter_actor_set_height(priv->pSeemlessLeft, fltHeight - 4.0 );
	clutter_actor_set_width(priv->pSeemlessLeft, fltWidth);
	clutter_actor_set_position(priv->pSeemlessLeft, priv->fltSeemlessDisp,( clutter_actor_get_height(priv->pPopupHead)  + 2.0 ));

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH), "%f", &fltY);
	fltY = fltY - priv->fltSeemlessDisp;


	/* Right seamless texture creation */
	priv->pSeemlessRight = clutter_actor_new();
	clutter_actor_add_child (priv->pPopupTextEntry, priv->pSeemlessRight);
	clutter_actor_set_background_color (priv->pSeemlessRight, &priv->SeamlessColor);
	clutter_actor_set_opacity (priv->pSeemlessRight, inOpacity);
	clutter_actor_set_height(priv->pSeemlessRight, fltHeight - 4.0 );
	clutter_actor_set_width(priv->pSeemlessRight, fltWidth);
	clutter_actor_set_position(priv->pSeemlessRight, fltY ,( clutter_actor_get_height(priv->pPopupHead) + 2.0 ) );


	/* creation of  inactive text display area */


	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pStrPopupIcon);

	pStrPopupIcon = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_FONT );
	priv->pPopupText = clutter_text_new ();
	/* add to the button group */
	clutter_text_set_line_wrap(	CLUTTER_TEXT (priv->pPopupText), TRUE);
	clutter_text_set_line_wrap_mode(CLUTTER_TEXT(priv->pPopupText), PANGO_WRAP_WORD_CHAR);
	clutter_text_set_line_alignment(CLUTTER_TEXT(priv->pPopupText), PANGO_ALIGN_CENTER);

	clutter_text_set_ellipsize(CLUTTER_TEXT (priv->pPopupText), PANGO_ELLIPSIZE_END);
	clutter_text_set_font_name (CLUTTER_TEXT (priv->pPopupText),pStrPopupIcon);

	clutter_actor_add_child (priv->pPopupTextEntry, priv->pPopupText);
	clutter_actor_set_position (priv->pPopupText,64.0, 10.0 );

	clutter_text_set_color(CLUTTER_TEXT (priv->pPopupText),&priv->TextEntryColor);
	clutter_actor_show(priv->pPopupText);

	/*  creation of icon_bar  Between text and roller  */


	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_HEIGHT), "%f", &fltHeight);


	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pStrPopupIcon);
	pStrPopupIcon = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_TEXT_BOTTOM_ICON );

	pStrPopupIcon = g_strdup_printf(PKGTHEMEDIR"/%s", pStrPopupIcon);
	priv->pPopupTextBottom = thornbury_ui_texture_create_new(pStrPopupIcon,fltWidth , fltHeight, FALSE, FALSE);
	clutter_actor_add_child (priv->pPopupTextEntry, priv->pPopupTextBottom);
	g_object_set(priv->pPopupTextBottom, "x", fltX, "y", fltY , NULL);

	/****************************************************Button creation **********************************************************/

	pStrSpellerIcon =  g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_UP_EMPTY );


	//ThornburyModel *pModelUpButton = NULL;
	priv->pModelUpButton = (ThornburyModel *)thornbury_list_model_new (COL_SPELLER_BUTTON_LAST, G_TYPE_STRING, NULL,-1);
	thornbury_model_append (priv->pModelUpButton,COL_TEXT_OR_TEXTURE,pStrSpellerIcon,-1);

	/* UP button creation */

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_UP_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_UP_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_UP_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_UP_HEIGHT), "%f", &fltHeight);



	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pStrPopupIcon);
	pStrPopupIcon = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_BUTTON_NORMAL );
	pPopupIconPressed = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_BUTTON_PRESSED );

	priv->pButtonUp =  g_object_new(MILDENHALL_TYPE_BUTTON_SPELLER,
			"type",SPELLER_BUTTON_TEXTURE,"name","UP",
			"width",fltWidth, "height",fltHeight,
			"normal-image",pStrPopupIcon,
			"pressed-image",pPopupIconPressed,
			"reactive",FALSE,"model",priv->pModelUpButton,
			NULL);

	clutter_actor_add_child (priv->pBottomGroup, priv->pButtonUp);
	g_object_set(priv->pButtonUp, "x", fltX, "y", fltY , NULL);


	//TODO : change to enums
	pModelBackButton = (ThornburyModel *)thornbury_list_model_new (COL_SPELLER_BUTTON_LAST, G_TYPE_STRING, NULL,-1);

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_BACK_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_BACK_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_BACK_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_BACK_HEIGHT), "%f", &fltHeight);

	pTextFont = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_BACK_TEXT_FONT );

	priv->pButtonBack =  g_object_new(MILDENHALL_TYPE_BUTTON_SPELLER,
			"type",SPELLER_BUTTON_TEXT,"name","BACK",
			"width",fltWidth, "height",fltHeight,
			"normal-image",pStrPopupIcon,
			"pressed-image",pPopupIconPressed,
			"reactive",FALSE, "model",pModelBackButton,"font-type",pTextFont,
			NULL);

	thornbury_model_append (pModelBackButton,COL_TEXT_OR_TEXTURE,"BACK",-1);
	if(NULL != priv->pButtonBack )
	{
		clutter_actor_add_child (priv->pBottomGroup, priv->pButtonBack);
		g_object_set(priv->pButtonBack, "x", fltX, "y", fltY , NULL);
	}

	/* DOWN button cration */


	pStrSpellerIcon =  g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_DOWN_EMPTY );

	priv->pModelDownButton = (ThornburyModel *)thornbury_list_model_new (COL_SPELLER_BUTTON_LAST, G_TYPE_STRING, NULL,-1);
	thornbury_model_append (priv->pModelDownButton,COL_TEXT_OR_TEXTURE,pStrSpellerIcon,-1);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_DOWN_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_DOWN_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_DOWN_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_DOWN_HEIGHT), "%f", &fltHeight);

	priv->pButtonDown =  g_object_new(MILDENHALL_TYPE_BUTTON_SPELLER,
			"type",SPELLER_BUTTON_TEXTURE,"name","DOWN",
			"width",fltWidth, "height",fltHeight,
			"normal-image",pStrPopupIcon,
			"pressed-image",pPopupIconPressed,"model",priv->pModelDownButton,
			"reactive",FALSE,
			NULL);

	clutter_actor_add_child (priv->pBottomGroup, priv->pButtonDown);
	g_object_set(priv->pButtonDown, "x", fltX, "y", fltY , NULL);





	/* Roller Item Creation */

	priv->pRollerItemModel = (ThornburyModel *)thornbury_list_model_new (COL_ROLLER_ITEM_MODEL_LAST,
			G_TYPE_POINTER, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_STRING, NULL,
			G_TYPE_UINT,   NULL,
			G_TYPE_STRING, NULL,
			-1);


	g_object_get(pRollerItemFactory, "object", &pRollerItemObject, NULL);
	priv->pRollerContainer = CLUTTER_ACTOR(pRollerItemObject);
	g_object_set(pRollerItemObject,"item-type", MILDENHALL_TYPE_SELECTION_POPUP_ITEM,
			"model", priv->pRollerItemModel,
			"background",FALSE,
			"reactive",FALSE,
			NULL);

	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "left-icon", COL_LEFT_ICON_FIELD);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "main-label", COL_MIDDLE_TEXT_FIELD);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "right-string", COL_RIGHT_FIELD);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "item-type", COL_SELECTION_ROLLER_ITEM_TYPE);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "name", COL_SELECTION_ROLLER_ITEM_NAME);

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_ROLLER_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_ROLLER_Y), "%f", &fltY);
	clutter_actor_set_position (priv->pRollerContainer,fltX,fltY);
	clutter_actor_add_child (priv->pBottomGroup,priv->pRollerContainer);



	/* creation of the drag area */
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT), "%f", &fltHeight);

	priv->pDragArea = clutter_actor_new();
	clutter_actor_set_height(priv->pDragArea, fltHeight);
	clutter_actor_set_width(priv->pDragArea, fltWidth);

	clutter_actor_set_position(priv->pDragArea,0.0 ,0.0);
	/* add rect to the popup group */

	clutter_actor_add_child (priv->pPopupTextEntry , priv->pDragArea);

	/* creation of drag action */
	priv->pDragAction = clutter_drag_action_new ();
	clutter_actor_add_action (priv->pDragArea, priv->pDragAction);
	clutter_drag_action_set_drag_axis (CLUTTER_DRAG_ACTION (priv->pDragAction), CLUTTER_DRAG_Y_AXIS);
	clutter_drag_action_set_drag_threshold (CLUTTER_DRAG_ACTION (priv->pDragAction), 10, 10);



#if 1
	fltHeight = clutter_actor_get_height(priv->pBottomGroup) + clutter_actor_get_height(priv->pPopupTextEntry ) ;
	clutter_actor_set_height(priv->pPopupRect, fltHeight);
#endif

	clutter_actor_set_height(CLUTTER_ACTOR(pSelectionPopup),fltHeight);
	fltHeight = clutter_actor_get_width(priv->pPopupTextEntry );
	clutter_actor_set_width(CLUTTER_ACTOR(pSelectionPopup),fltHeight);


	clutter_actor_set_reactive (priv->pDragArea, FALSE);
}


/********************************************************
 * Function : v_selection_popup_row_added_cb
 * Description: callback on model row add
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/


static void v_selection_popup_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	GValue Row_hash = { 0, };
        MILDENHALL_SELECTION_POPUP_PRINT ("%s %d \n", __FUNCTION__, __LINE__);
	thornbury_model_iter_get_value(pIter, SELECTION_POPUP_ROWS_INFO, &Row_hash);
	v_selection_popup_update_roller_model(pSelectionPopup ,&Row_hash , thornbury_model_iter_get_row(pIter));
	v_mildenhall_selection_popup_update_up_down_arrows(pSelectionPopup);
	if (priv->bPopupShown == TRUE &&  priv->bWaitingResponse == FALSE)
		v_selection_popup_fetch_http_icons_clb(NULL , NULL , pSelectionPopup  );

}

/********************************************************
 * Function : v_selection_popup_row_changed_cb
 * Description: callback on model row change
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/
static void v_selection_popup_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
        GValue Row_hash = {0, };
        MILDENHALL_SELECTION_POPUP_PRINT ("%s %d \n", __FUNCTION__, __LINE__);
	MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	thornbury_model_iter_get_value(pIter, SELECTION_POPUP_ROWS_INFO, &Row_hash);
	v_selection_popup_update_roller_model(pSelectionPopup ,&Row_hash , thornbury_model_iter_get_row(pIter));
	v_mildenhall_selection_popup_update_up_down_arrows(pSelectionPopup);
	if (priv->bPopupShown == TRUE &&  priv->bWaitingResponse == FALSE)
		v_selection_popup_fetch_http_icons_clb(NULL , NULL , pSelectionPopup  );
}

/********************************************************
 * Function : v_selection_popup_row_removed_cb
 * Description: callback on model row removed
 * Parameters: ThornburyModel *, ThornburyModelIter *, MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/
static void v_selection_popup_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallSelectionPopup *pSelectionPopup)
{
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	//	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

#if 0
	priv->inNoOfRows--;
	guint inRemovedRow = thornbury_model_iter_get_row (pIter);

	if(priv->pRollerItemModel != NULL)
	{
		thornbury_model_remove(priv->pRollerItemModel,inRemovedRow);
	}
#endif
}

/********************************************************
 * Function : v_selection_popup_change_model_string_field
 * Description: Function to set string value to particular row of Iter
 * Parameters: ThornburyModelIter *iter , guint col , value
 * Return value: void
 ********************************************************/
static void v_selection_popup_change_model_string_field (ThornburyModelIter *pIter , guint uinColunm , gchar *pText )
{
	GValue pIcon = G_VALUE_INIT;
	g_value_init (&pIcon, G_TYPE_STRING);
	g_value_set_static_string (&pIcon, pText);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&pIcon );
	g_value_unset (&pIcon);
}

/********************************************************
 * Function : v_selection_popup_change_model_pointer_field
 * Description: Function to set pointer value to particular row of Iter
 * Parameters: ThornburyModelIter *iter , guint col , value
 * Return value: void
 ********************************************************/
static void v_selection_popup_change_model_pointer_field (ThornburyModelIter *pIter , guint uinColunm , gpointer pPointer)
{
	GValue pIcon = G_VALUE_INIT;
	g_value_init (&pIcon, G_TYPE_POINTER);
	g_value_set_pointer (&pIcon, pPointer);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&pIcon );
	g_value_unset (&pIcon);
}



/********************************************************
 * Function : v_selection_popup_change_model_uint_field
 * Description: Function to set guint value to particular row of Iter
 * Parameters: ThornburyModelIter *iter , guint col , value
 * Return value: void
 ********************************************************/

static void v_selection_popup_change_model_uint_field (ThornburyModelIter *pIter , guint uinColunm , guint bValue )
{
	GValue pIcon = G_VALUE_INIT;
	g_value_init (&pIcon, G_TYPE_UINT);
	g_value_set_uint (&pIcon, bValue);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&pIcon );
	g_value_unset (&pIcon);
	//g_object_unref(pIter);
}

/********************************************************
 * Function : u_selection_popup_find_match_in_model
 * Description: Function to find the row with id
 * Parameters: ThornburyModel *model pointer , const gchar* pStringToSearch , guint col No of Model
 * Return value: void
 ********************************************************/

static guint u_selection_popup_find_match_in_model (ThornburyModel *pModel, const gchar* pStringToSearch, guint uinColumnNo)
{
	guint uinRowNo = -1 ;
	if(G_IS_OBJECT(pModel))
	{
		guint uinTotalRows = thornbury_model_get_n_rows(pModel);
		guint uinIter ;
		for(uinIter = 0 ; uinIter < uinTotalRows ; uinIter++)
		{
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pModel, uinIter);
			if(NULL != pIter)
			{
				GValue value = { 0, };
                                gchar *pStringInModel = NULL;
				thornbury_model_iter_get_value(pIter,uinColumnNo, &value);
				pStringInModel = (gchar *) g_value_get_string (&value);
				if(!g_strcmp0(pStringInModel , pStringToSearch))
				{
					/* Match Found */
					uinRowNo = uinIter;
					break;
				}
				g_value_unset (&value);
			}
			g_object_unref(pIter);
		}
	}
	return uinRowNo;
}


static void v_mildenhall_selection_popup_insert_dummy_rows(MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

	/* If No of Rows is less than 4 Then insert extra Rows to Fill the Gap */
	int inAddExtraRow = 4 - priv->inNoOfRows ;
	int inIterCount;

	for(inIterCount = 0; inIterCount < inAddExtraRow ;inIterCount++ )
	{
		MildenhallSelPopupItemIconDetail *pMildenhallSelPopupItemIconDetail = g_new0(MildenhallSelPopupItemIconDetail ,1);
		pMildenhallSelPopupItemIconDetail->pIconContent = NULL ;
		pMildenhallSelPopupItemIconDetail->pRowId = g_strdup(MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID);
		pMildenhallSelPopupItemIconDetail->pLocalFilePath  = NULL;
		pMildenhallSelPopupItemIconDetail->pHttpFilePath = NULL ;
		pMildenhallSelPopupItemIconDetail->bHttpIconUpdated = FALSE ;

		/* filling  model to display Blank Roller item */
		thornbury_model_append (priv->pRollerItemModel,COL_LEFT_ICON_FIELD, pMildenhallSelPopupItemIconDetail,
				COL_MIDDLE_TEXT_FIELD, "",
				COL_RIGHT_FIELD,"",
				COL_SELECTION_ROLLER_ITEM_TYPE, (enMildenhallSelectionPopupItemType)1,
				COL_SELECTION_ROLLER_ITEM_NAME ,MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID ,
				-1);
		priv->inNoOfRows++;
	}
}





static void v_mildenhall_selection_popup_update_roller_model(MildenhallSelectionPopup *pSelectionPopup , guint uinRow , SelectionPopupRollerFields *pRollerField  )
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	ThornburyModelIter *pIter = thornbury_model_get_iter_at_row (priv->pRollerItemModel, uinRow);
        MildenhallSelPopupItemIconDetail *pMildenhallSelPopupItemIconDetail = g_new0 (MildenhallSelPopupItemIconDetail, 1);
        MILDENHALL_SELECTION_POPUP_PRINT ("%s %d \n", __FUNCTION__, __LINE__);

	/* Todo:
	 * handle row changed
	 *
	 */
	pMildenhallSelPopupItemIconDetail->pIconContent = NULL ;
	pMildenhallSelPopupItemIconDetail->pRowId = g_strdup(pRollerField->pRowId);
	pMildenhallSelPopupItemIconDetail->bHttpIconUpdated = FALSE ;
	if(g_str_has_prefix(pRollerField->pModelIconToBeSent , "http://" ) ||
			g_str_has_prefix( pRollerField->pModelIconToBeSent , "https://" ) )
	{
		pMildenhallSelPopupItemIconDetail->pHttpFilePath = g_strdup(pRollerField->pModelIconToBeSent);
		pMildenhallSelPopupItemIconDetail->pLocalFilePath = g_strdup(PKGTHEMEDIR"/icon_request_inactive.png") ;
		g_queue_push_tail(priv->pHttpIconQ ,pMildenhallSelPopupItemIconDetail );
	}
	else
	{
		pMildenhallSelPopupItemIconDetail->pLocalFilePath  = g_strdup(pRollerField->pModelIconToBeSent);
		pMildenhallSelPopupItemIconDetail->pHttpFilePath = NULL ;

	}
	v_selection_popup_change_model_pointer_field(pIter ,COL_LEFT_ICON_FIELD  , pMildenhallSelPopupItemIconDetail);
	v_selection_popup_change_model_string_field(pIter ,COL_MIDDLE_TEXT_FIELD , pRollerField->pMidFieldText);
	v_selection_popup_change_model_string_field(pIter ,COL_RIGHT_FIELD , pRollerField->pRightFieldText );
	v_selection_popup_change_model_uint_field(pIter ,COL_SELECTION_ROLLER_ITEM_TYPE ,priv->enSelectionPopupType);
	v_selection_popup_change_model_string_field(pIter ,COL_SELECTION_ROLLER_ITEM_NAME , pRollerField->pRowId);
	g_object_unref(pIter);

}



static SelectionPopupRollerFields *p_mildenhall_selection_popup_read_roller_model_fields(MildenhallSelectionPopup *pSelectionPopup , gpointer Row_hash )
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

	gchar *pKey = NULL;
	gchar *pValue = NULL;
	gchar *pFieldKeyLocal = NULL;
	GVariantIter iter;
	guint uinRow=0;
	gint inIterCount = 0;
	SelectionPopupRollerFields *pRollerField = g_new0( SelectionPopupRollerFields , 1);
	MILDENHALL_SELECTION_POPUP_PRINT ("inside Rows info  LOOP \n");
	uinRow = 0;
	g_variant_iter_init(&iter, (GVariant * )g_value_get_pointer(Row_hash));

	while (g_variant_iter_next(&iter, "{ss}", &pKey, &pValue))
	{
		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pFieldKeyLocal);
		pFieldKeyLocal  =   g_strdup_printf("%s", pKey );
		if(NULL != pFieldKeyLocal)
		{
			MILDENHALL_SELECTION_POPUP_PRINT ("key not null key = %s| value = %s| \n",pFieldKeyLocal,pValue);

			if(g_strcmp0( ROLLER_ITEM_FIELD_ONE ,pFieldKeyLocal ) == 0)
			{
				MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pRowId);
				pRollerField->pRowId = g_strdup ( pValue );
				if(TRUE == priv->bRemove)
					break;
			}
			else if(g_strcmp0(ROLLER_ITEM_FIELD_TWO_IMAGE ,pFieldKeyLocal) == 0)
			{
				//Image
				MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pModelIconToBeSent);
				pRollerField->pModelIconToBeSent =  g_strdup(  pValue ) ;
			}
			else if(inIterCount == 2 && (g_strcmp0(ROLLER_ITEM_FIELD_THREE,pFieldKeyLocal ) == 0))
			{
				//Text
				MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pMidFieldText);
				pRollerField->pMidFieldText = g_strdup(  pValue );
			}
			else if (inIterCount == 3 && (g_strcmp0(ROLLER_ITEM_FIELD_FOUR_TEXT ,pFieldKeyLocal ) == 0))
			{
				//Radio/Text
				MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pRightFieldText);
				pRollerField->pRightFieldText =  g_strdup( pValue );
				priv->enSelectionPopupType = TYPE_ICON_LABEL_LABEL;
			}
			else if(inIterCount == 3 )
			{
				if(0 == g_strcmp0(ROLLER_ITEM_FIELD_FOUR_RADIO ,pFieldKeyLocal ))
				{
					priv->enSelectionPopupType = TYPE_ICON_LABEL_RADIO;
				}
				else if	(0 == g_strcmp0(ROLLER_ITEM_FIELD_FOUR_INFO_CONFIRM ,pFieldKeyLocal))
				{
					priv->enSelectionPopupType = TYPE_ICON_LABEL_CONFIRM;
				}
			}

			uinRow++;
			inIterCount++;
		}
		else
		{
			MILDENHALL_SELECTION_POPUP_PRINT ("key is null \n");
		}
	}

	return pRollerField ;
}

static void v_mildenhall_selection_popup_free_roller_model_fields(SelectionPopupRollerFields *pRollerField )
{
	MILDENHALL_SELECTION_POPUP_RETURN_IF_NULL(pRollerField , "pRollerField");
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pRowId);
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pRightFieldText);
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pMidFieldText);
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pRollerField->pModelIconToBeSent);
	g_free(pRollerField);
	pRollerField = NULL ;
}

static void v_mildenhall_selection_popup_update_row_dynamically(MildenhallSelectionPopup *pSelectionPopup , SelectionPopupRollerFields *pRollerField )
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
        guint uinDummyRowNo = u_selection_popup_find_match_in_model (priv->pRollerItemModel, MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID, COL_SELECTION_ROLLER_ITEM_NAME);

	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s  %d  \n", __FUNCTION__ , __LINE__ );

	if ((signed int) uinDummyRowNo != -1)
	{
		MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s  %d \n", __FUNCTION__ , __LINE__);
		g_hash_table_insert(priv->pRollerId ,
				g_strdup_printf("%s", pRollerField->pRowId),
				g_strdup_printf("%d", uinDummyRowNo ));
		v_mildenhall_selection_popup_update_roller_model(pSelectionPopup ,uinDummyRowNo , pRollerField);
	}
	else
	{
                MildenhallSelPopupItemIconDetail *pMildenhallSelPopupItemIconDetail = NULL;
		MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s  %d \n", __FUNCTION__ , __LINE__);
		g_hash_table_insert(priv->pRollerId ,
				g_strdup_printf("%s", pRollerField->pRowId),
				g_strdup_printf("%d", priv->inNoOfRows ));

		pMildenhallSelPopupItemIconDetail = g_new0 (MildenhallSelPopupItemIconDetail, 1);
		pMildenhallSelPopupItemIconDetail->pIconContent = NULL ;
		pMildenhallSelPopupItemIconDetail->pRowId = g_strdup(pRollerField->pRowId);
		pMildenhallSelPopupItemIconDetail->bHttpIconUpdated = FALSE ;

		if(g_str_has_prefix(pRollerField->pModelIconToBeSent , "http://" ) ||
				g_str_has_prefix( pRollerField->pModelIconToBeSent , "https://" ) )
		{
			pMildenhallSelPopupItemIconDetail->pHttpFilePath = g_strdup(pRollerField->pModelIconToBeSent);
			pMildenhallSelPopupItemIconDetail->pLocalFilePath = g_strdup(PKGTHEMEDIR"/icon_request_inactive.png") ;
			g_queue_push_tail(priv->pHttpIconQ ,pMildenhallSelPopupItemIconDetail );
		}
		else
		{
			pMildenhallSelPopupItemIconDetail->pLocalFilePath  = g_strdup(pRollerField->pModelIconToBeSent);
			pMildenhallSelPopupItemIconDetail->pHttpFilePath = NULL ;
		}

		thornbury_model_append (priv->pRollerItemModel, COL_LEFT_ICON_FIELD, pMildenhallSelPopupItemIconDetail,
				COL_MIDDLE_TEXT_FIELD, pRollerField->pMidFieldText,
				COL_RIGHT_FIELD,pRollerField->pRightFieldText,
				COL_SELECTION_ROLLER_ITEM_TYPE,priv->enSelectionPopupType,
				COL_SELECTION_ROLLER_ITEM_NAME , pRollerField->pRowId,
				-1);
		priv->inNoOfRows++;
	}
}


static void v_mildenhall_selection_popup_remove_or_replace_row(MildenhallSelectionPopup *pSelectionPopup , SelectionPopupRollerFields *pRollerField )
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	guint uinRowNo = u_selection_popup_find_match_in_model (priv->pRollerItemModel, (gchar *) pRollerField->pRowId, COL_SELECTION_ROLLER_ITEM_NAME);
	if ((signed int) uinRowNo != -1)
	{
		if(FALSE == priv->bRemove)
		{
			MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s  %d %d \n", __FUNCTION__ , __LINE__ , uinRowNo);
			v_mildenhall_selection_popup_update_roller_model(pSelectionPopup ,uinRowNo , pRollerField);
		}
		else
		{
			thornbury_model_remove(priv->pRollerItemModel ,uinRowNo );
			g_hash_table_remove(priv->pRollerId ,pRollerField->pRowId);
			MILDENHALL_SELECTION_POPUP_PRINT("Replaced Row = %d  \n\n",uinRowNo);
			priv->inNoOfRows--;
			if(priv->inNoOfRows < 4)
			{
                                MxAdjustment *pVadjust = NULL;
				/* If No of Rows is less than 4 Then insert extra Rows to Fill the Gap */
				v_mildenhall_selection_popup_insert_dummy_rows(pSelectionPopup);
				/* get 	MxAdjustment for explicitly alligning the Roller Items to (0.0 , 0.0) */
				g_object_get(priv->pRollerContainer ,"vadjust",&pVadjust,NULL);
				mx_adjustment_set_value(pVadjust,0.0);

			}
		}
	}
}



/********************************************************
 * Function : v_selection_popup_update_roller_model
 * Description: Model info is extracted in this function
 * Parameters: MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/

static void v_selection_popup_update_roller_model(MildenhallSelectionPopup *pSelf , gpointer Row_hash , gint inCurrentRow )
{

	MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pSelf);
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
        MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(NULL != g_value_get_pointer(Row_hash))
	{
		SelectionPopupRollerFields *pRollerField = p_mildenhall_selection_popup_read_roller_model_fields(pSelectionPopup ,  Row_hash );
		if(NULL  != priv->pRollerId && NULL != pRollerField->pRowId )
		{
			gchar *pHashEntry = NULL;
			pHashEntry =  g_hash_table_lookup(priv->pRollerId ,pRollerField->pRowId );
			MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s  %d   pHashEntry = %s  \n", __FUNCTION__ , __LINE__  , pHashEntry);

			if(TRUE == priv->bRemove && NULL == pHashEntry )
			{
				g_warning("Roller Item not removed as no Matching Row with Row-Id '%s' found\n\n " ,pRollerField->pRowId );
				return ;
			}
			if( NULL == pHashEntry )
			{
				/* create the Roller Item type by providing model details */
				v_mildenhall_selection_popup_update_row_dynamically(pSelectionPopup ,pRollerField );
				MILDENHALL_SELECTION_POPUP_PRINT("after append \n\n");
			}
			else
			{
				v_mildenhall_selection_popup_remove_or_replace_row(pSelectionPopup ,pRollerField );
			}
		}
		else if(NULL == pRollerField->pRowId )
		{
			g_warning("MILDENHALL_SELECTION_POPUP: Row = %d ,Skipped as unique Row Id is not specified",inCurrentRow);
		}
		v_mildenhall_selection_popup_free_roller_model_fields(pRollerField);
	}
}



static void v_selection_popup_remove_model_completly(MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	if (G_IS_OBJECT( priv->pRollerItemModel)	&& thornbury_model_get_n_rows( priv->pRollerItemModel) > 0)
	{
		gint inTotalRows = thornbury_model_get_n_rows(priv->pRollerItemModel);
		gint inCurrentRow;
		for(inCurrentRow = inTotalRows ; inCurrentRow != 0;inCurrentRow --)
		{
			thornbury_model_remove(priv->pRollerItemModel,inCurrentRow);
			priv->inNoOfRows --;
		}
		thornbury_model_remove(priv->pRollerItemModel,0);
	}
}

static void v_selection_popup_update_app_msg_icons(MildenhallSelectionPopup *pSelectionPopup , GValue *gpIcon , GValue *gpImagePath)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	gchar *pAppMsgIconPath = NULL;
	gchar *pAppMsgIconKey = NULL;

	pAppMsgIconKey = g_value_dup_string(gpIcon);
	pAppMsgIconPath = g_value_dup_string(gpImagePath);
	if(NULL == pAppMsgIconPath)
	{
		pAppMsgIconPath = g_strdup(PKGTHEMEDIR"/icon_request_inactive.png");
	}

	if (!g_strcmp0(pAppMsgIconKey, "AppIcon"))
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n AppIcon loop \n");
		if (NULL != priv->pAppIcon)
		{
			clutter_actor_destroy(priv->pAppIcon);
			priv->pAppIcon = NULL;
		}
		priv->pAppIcon = thornbury_ui_texture_create_new(pAppMsgIconPath, 0, 0, FALSE, FALSE);
	}
	if (!g_strcmp0(pAppMsgIconKey, "MsgIcon"))
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n MsgIcon loop \n");
		if (NULL != priv->pMsgIcon)
		{
			clutter_actor_destroy(priv->pMsgIcon);
			priv->pMsgIcon = NULL;
		}
		priv->pMsgIcon = thornbury_ui_texture_create_new(pAppMsgIconPath, 0, 0, FALSE, FALSE);
	}
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pAppMsgIconKey);
	MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pAppMsgIconPath);
}


static void v_mildenhall_selection_popup_update_popup_text_header(MildenhallSelectionPopup *pSelectionPopup ,GValue *gpMsgStyle ,  GValue *gpMsgtext )
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	gchar *pAppMsgIconPath = NULL;
	gfloat pTextHeightBefore =0.0;
	gfloat fltTextX;

	if( !g_strcmp0(g_value_get_string(gpMsgStyle), "static"))
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n static loop \n");
		pAppMsgIconPath = g_value_dup_string(gpMsgtext);
		pTextHeightBefore = clutter_actor_get_height(priv->pPopupText) ;

		if(NULL != pAppMsgIconPath)
		{
			clutter_text_set_text (CLUTTER_TEXT (priv->pPopupText),gettext(pAppMsgIconPath));
		}
		else
		{
			clutter_text_set_text (CLUTTER_TEXT (priv->pPopupText),"SELECT ANY ONE" );
		}

		fltTextX = clutter_actor_get_width(priv->pPopupTextEntry) - priv->fltSeemlessDisp - priv->fltSeemlessDisp - 10.0 ;
		if( clutter_actor_get_width(priv->pPopupText) > fltTextX )
		{
			clutter_actor_set_width(priv->pPopupText,fltTextX );
		}
		if( clutter_actor_get_width(priv->pPopupText) > fltTextX )
		{
			clutter_actor_set_width(priv->pPopupText,fltTextX );
		}

		if(pTextHeightBefore < (clutter_actor_get_height(priv->pPopupText)) )
		{
			priv->bNeedToAppend = TRUE;
		}
	}

	/* check for  the text to be displayed in second row */
	if( !g_strcmp0(g_value_get_string(gpMsgStyle), "dynamic"))
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\n dynamic loop \n");
		pAppMsgIconPath = g_value_dup_string(gpMsgtext);

		if(NULL != pAppMsgIconPath)
		{
			if(TRUE != priv->bNeedToAppend)
			{
				MILDENHALL_SELECTION_POPUP_PRINT("\n dynamic loop new line inserted \n");
				clutter_text_insert_text (CLUTTER_TEXT (priv->pPopupText),"\n  ",-1);
			}
			clutter_text_insert_text (CLUTTER_TEXT (priv->pPopupText),pAppMsgIconPath,-1);
			priv->bNeedToAppend = TRUE;
		}
	}
}



static void v_selection_popup_from_model_update_view(MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	int inCurrentRow = 0;
	for ( ; (unsigned int) inCurrentRow < thornbury_model_get_n_rows (priv->pPopupModel) ; inCurrentRow++)
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(priv->pPopupModel, inCurrentRow);
		MILDENHALL_SELECTION_POPUP_PRINT("\n inCurrentField3Row %d\n", inCurrentRow);
		if (NULL != pIter)
		{
			GValue Icon = {0, };
                        GValue ImagePath = {0, };
                        GValue Msgstyle = {0, };
                        GValue Msgtext = {0, };
                        GValue Row_hash = {0, };
			thornbury_model_iter_get_value (pIter, IMAGEICON_KEY, &Icon);
			thornbury_model_iter_get_value (pIter, IMAGEPATH_VALUE, &ImagePath);
			thornbury_model_iter_get_value (pIter, TEXT_ENTRY_MSGSTYLE, &Msgstyle);
			thornbury_model_iter_get_value (pIter, TEXT_ENTRY_MSGTEXT, &Msgtext);
			thornbury_model_iter_get_value (pIter, SELECTION_POPUP_ROWS_INFO, &Row_hash);

			/* check for the text to be displayed in first row */
			v_mildenhall_selection_popup_update_popup_text_header(pSelectionPopup , &Msgstyle , &Msgtext );
			/* set the left right icon */
			v_selection_popup_update_app_msg_icons(pSelectionPopup , &Icon , &ImagePath);
			v_selection_popup_update_roller_model(pSelectionPopup , &Row_hash , inCurrentRow );
			/* Unset all Gvalues */
			g_value_unset(&Icon);
			g_value_unset(&Msgstyle);
			g_value_unset(&Msgtext);
			g_value_unset(&ImagePath);
			g_value_unset(&Row_hash);
		}
	}
}




static void v_mildenhall_selection_popup_create_connect_go_button(MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	gfloat fltX = 0.0;
	gfloat fltY = 0.0;
	gfloat fltWidth = 0.0;
	gfloat fltHeight = 0.0;

	gchar *pStrPopupIcon = NULL;
	gchar *pPopupIconPressed = NULL;
	gchar *pGoFont = NULL;
	ThornburyModel *pModelGoButton = NULL;
	pModelGoButton = (ThornburyModel *)thornbury_list_model_new (COL_SPELLER_BUTTON_LAST, G_TYPE_STRING, NULL,-1);
	/* GO! Button Creation  creation */
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_X), "%f", &fltX);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_Y), "%f", &fltY);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_WIDTH), "%f", &fltWidth);
	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_HEIGHT), "%f", &fltHeight);

	pStrPopupIcon = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_GO_NORMAL );
	pGoFont = g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_FONT );

	pPopupIconPressed = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_GO_PRESSED );

	priv->pButtonGo =  g_object_new(MILDENHALL_TYPE_BUTTON_SPELLER,
			"type",SPELLER_BUTTON_TEXT,"name","GO",
			"width",fltWidth, "height",fltHeight,
			"normal-image",pStrPopupIcon,
			"pressed-image",pPopupIconPressed,
			"model",pModelGoButton,"font-type",pGoFont,NULL);


	thornbury_model_append (pModelGoButton,COL_TEXT_OR_TEXTURE,"GO!",-1);

	clutter_actor_add_child (CLUTTER_ACTOR(pSelectionPopup), priv->pButtonGo);
	g_object_set(priv->pButtonGo, "x", fltX, "y", fltY , NULL);

	if(TRUE == priv->animate_show && priv->bAnimationStart == FALSE && FALSE == priv->bGoConnected )
	{
		/* set Go Button To reactive and Connect signal if not connected and when not in animation */
		priv->bGoConnected = TRUE;
		g_object_set(priv->pButtonGo,"reactive",TRUE, NULL);
		g_signal_connect(priv->pButtonGo,"button-press",
				G_CALLBACK(v_selection_popup_go_press_cb), pSelectionPopup);
	}
}

static void v_mildenhall_selection_popup_create_default_icons_if_path_is_invalid(MildenhallSelectionPopup *pSelectionPopup)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);
	gchar *pAppMsgIconPath = NULL;

	if(NULL == priv->pAppIcon )
	{
		g_warning("MILDENHALL_SELECTION_POPUP : provide complete path for App Icon  : Default icon is loaded\n");

		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pAppMsgIconPath);
		pAppMsgIconPath = g_strdup(PKGTHEMEDIR"/icon_request_inactive.png");
		priv->pAppIcon = thornbury_ui_texture_create_new(pAppMsgIconPath, 0, 0, FALSE, FALSE);

	}

	if(NULL == priv->pMsgIcon )
	{
		g_warning("MILDENHALL_SELECTION_POPUP : provide complete path for Message Icon Field : Default icon is loaded\n");
		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pAppMsgIconPath);
		pAppMsgIconPath = g_strdup(PKGTHEMEDIR"/icon_request_inactive.png");
		priv->pMsgIcon = thornbury_ui_texture_create_new(pAppMsgIconPath, 0, 0, FALSE, FALSE);

	}
	if(clutter_actor_get_parent (priv->pAppIcon) == NULL)
		clutter_actor_add_child(priv->pPopupTextEntry, priv->pAppIcon);

	if(clutter_actor_get_parent (priv->pMsgIcon) == NULL)
		clutter_actor_add_child(priv->pPopupTextEntry, priv->pMsgIcon);
}


static void v_selection_popup_free_icon_data(gpointer pQData , gpointer pUserData)
{
	MildenhallSelPopupItemIconDetail *pIconDetail = (MildenhallSelPopupItemIconDetail*)pQData;
	if(NULL != pIconDetail)
	{
		if(NULL != pIconDetail->pIconContent)
		{
			g_object_unref( pIconDetail->pIconContent);
			 pIconDetail->pIconContent = NULL ;
		}
		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pIconDetail->pRowId)
		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pIconDetail->pLocalFilePath)
		MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(pIconDetail->pHttpFilePath)
		g_free(pIconDetail);
		pIconDetail = NULL ;
	}
}




/********************************************************
 * Function : v_selection_popup_extract_model_info
 * Description: Model info is extracted in this function
 * Parameters: MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/


static void v_selection_popup_extract_model_info(MildenhallSelectionPopup *pSelf)
{
        MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pSelf);
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);

        gfloat pTextHeightBefore = 0.0 ;
        MxAdjustment *pVadjust = NULL;

	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if (G_IS_OBJECT( priv->pPopupModel)	&& thornbury_model_get_n_rows( priv->pPopupModel) > 0)
	{
		/* remove the Previous roller Items and append the Fresh ones */

		MILDENHALL_SELECTION_POPUP_PRINT("\n\n/* remove the Previous roller Items and append the Fresh ones */ \n")
				if(NULL != priv->pRollerId )
					g_hash_table_remove_all(priv->pRollerId );

#if 1
		v_selection_popup_remove_model_completly(pSelectionPopup);
		/* Todo:
		 * clear the Image queue.
		 * cancel pending callbacks
		 *
		 */
		g_queue_foreach(priv->pHttpIconQ  ,  v_selection_popup_free_icon_data , NULL );
		g_queue_clear(priv->pHttpIconQ );

#endif

		priv->enSelectionPopupType = TYPE_ICON_LABEL_LABEL;

		/* clear text on new request */
		if(NULL != priv->pPopupText )
		{
			gint inTotalNoChars = 0;
			inTotalNoChars = strlen(clutter_text_get_text(CLUTTER_TEXT (priv->pPopupText)));
			MILDENHALL_SELECTION_POPUP_PRINT("\n\n before delete %s \n\n", clutter_text_get_text(CLUTTER_TEXT (priv->pPopupText)));
			clutter_text_delete_text(CLUTTER_TEXT (priv->pPopupText), 0,inTotalNoChars);
			MILDENHALL_SELECTION_POPUP_PRINT("\n\n after delete=%s| \n\n", clutter_text_get_text(CLUTTER_TEXT (priv->pPopupText)));
			pTextHeightBefore = clutter_actor_get_height(priv->pPopupText) ;
			/* Reposition the actors based on the changed dimensions in previous request */
			v_mildenhall_selection_popup_reposition_actors(pSelectionPopup,pTextHeightBefore);

		}
		v_selection_popup_from_model_update_view(pSelectionPopup);
	}

	if(! (priv->inNoOfRows > 4 ))
	{
		/* If No of Rows is less than 4 Then insert extra Rows to Fill the Gap */
		v_mildenhall_selection_popup_insert_dummy_rows(pSelectionPopup);
	}

	v_mildenhall_selection_popup_update_up_down_arrows(pSelectionPopup);

	if(priv->enSelectionPopupType == TYPE_ICON_LABEL_RADIO
			|| priv->enSelectionPopupType == TYPE_ICON_LABEL_CONFIRM)
	{
		/* Create The Go Button If Field4 is of type Radio */
		if(NULL == priv->pButtonGo)
		{
			v_mildenhall_selection_popup_create_connect_go_button(pSelectionPopup);
		}
		else
		{
			/* show the Go button */
			clutter_actor_show(CLUTTER_ACTOR(priv->pButtonGo));
			g_signal_handlers_disconnect_by_func(priv->pButtonGo,v_selection_popup_go_press_cb, NULL);
		}
	}
	else
	{
		if(NULL != priv->pButtonGo)
		{
			clutter_actor_hide(CLUTTER_ACTOR(priv->pButtonGo));
		}
	}
	v_mildenhall_selection_popup_create_default_icons_if_path_is_invalid(pSelectionPopup);

	//	MILDENHALL_SELECTION_POPUP_PRINT("\n\n cursor postion = %d",clutter_text_get_cursor_position(CLUTTER_TEXT (priv->pPopupText)) );

	/* Reposition the actors based on the changed dimensions */
	v_mildenhall_selection_popup_reposition_actors(pSelectionPopup,pTextHeightBefore);

	/* get 	MxAdjustment for explicitly alligning the Roller Items to (0.0 , 0.0) */
	g_object_get(priv->pRollerContainer ,"vadjust",&pVadjust,NULL);

	mx_adjustment_set_value(pVadjust,0.0);

}




/********************************************************
 * Function : v_mildenhall_selection_popup_reposition_actors
 * Description: For Repositioning the actors based on the changed dimensions
 * Parameters: MildenhallSelectionPopup * , gfloat
 * Return value: void
 ********************************************************/

static void v_mildenhall_selection_popup_reposition_actors( MildenhallSelectionPopup *pSelectionPopup , gfloat pTextHeightBefore)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);

        gfloat fltTextX = 0.0, fltTextY = 0.0;
        gfloat fltTextHeight = 0.0;
	MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	/* width available for the Text entry */

	fltTextX = clutter_actor_get_width(priv->pPopupTextEntry) - priv->fltSeemlessDisp - priv->fltSeemlessDisp - 10.0 ;

	/* if Text doesnt fit inside the width, restrcit width */

	if( clutter_actor_get_width(priv->pPopupText) > fltTextX )
	{
		clutter_actor_set_width(priv->pPopupText,fltTextX );
	}
	/* Max of two lines can be displayed */
	MILDENHALL_SELECTION_POPUP_PRINT("Max of two lines can be displayed = %f\n\n",pTextHeightBefore);

	pTextHeightBefore += pTextHeightBefore;
	if( pTextHeightBefore < clutter_actor_get_height(priv->pPopupText ) )
	{
		/* if Height exceeds the limit,restrict height */
		clutter_actor_set_height(priv->pPopupText,pTextHeightBefore);
		MILDENHALL_SELECTION_POPUP_PRINT("if Height exceeds the limit,restrict height = %f \n\n",clutter_actor_get_height(priv->pPopupText));
	}

	sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT), "%f", &fltTextHeight);



	if(priv->bNeedToAppend == TRUE)
	{
		/* No of Lines is more than two and hence needs reposition of other actors */

		priv->bNeedToAppend = FALSE;
		/* y postion of the selection popup needs to be shifted */
		priv->bPositionExtreme = TRUE;

		/* Increase the height of the Text Entry Group by the amount of the Text Height */
		fltTextY = (clutter_actor_get_height(priv->pPopupTextEntry) + fltTextHeight );
		clutter_actor_set_height(priv->pPopupTextEntry,fltTextY);
		clutter_actor_set_position(priv->pPopupTextEntry,0.0,0.0 );
		clutter_actor_set_position(priv->pPopupTextBottom,0.0,(fltTextY - clutter_actor_get_height(priv->pPopupTextBottom) ));
		clutter_actor_set_height(priv->pSeemlessLeft,( fltTextHeight*2 - 4 ) );
		clutter_actor_set_height(priv->pSeemlessRight,( fltTextHeight*2 - 4));

		g_object_get(MILDENHALL_ROLLER_CONTAINER(priv->pRollerContainer),"height",&fltTextY,NULL );
		clutter_actor_set_height(priv->pBottomGroup ,fltTextY );
		clutter_actor_set_position(priv->pBottomGroup,0.0, clutter_actor_get_height(priv->pPopupTextEntry));


		if(NULL !=priv->pButtonGo )
		{
			/* if Go button is needed the shift the y position down */
			g_object_get(priv->pButtonGo,"y",&fltTextY,NULL );
			g_object_set(priv->pButtonGo,"y",fltTextY  + fltTextHeight,NULL );

		}
	}
	else
	{

		/* Rearrange the actors to their default position for next show */

		priv->bPositionExtreme = FALSE;
		fltTextY = (clutter_actor_get_height(priv->pPopupHead) + fltTextHeight  + clutter_actor_get_height(priv->pPopupTextBottom) );
		clutter_actor_set_height(priv->pPopupTextEntry,fltTextY);
		clutter_actor_set_position(priv->pPopupTextEntry,0.0,0.0 );
		clutter_actor_set_position(priv->pPopupTextBottom,0.0,(fltTextY - clutter_actor_get_height(priv->pPopupTextBottom) ));
		clutter_actor_set_height(priv->pSeemlessLeft,( fltTextHeight - 4 ) );
		clutter_actor_set_height(priv->pSeemlessRight,( fltTextHeight - 4));



		g_object_get (MILDENHALL_ROLLER_CONTAINER (priv->pRollerContainer), "height", &fltTextY, NULL);
		clutter_actor_set_height(priv->pBottomGroup ,fltTextY );
		clutter_actor_set_position(priv->pBottomGroup,0.0, clutter_actor_get_height(priv->pPopupTextEntry));

		if(NULL !=priv->pButtonGo )
		{
			/* if Go is needed then postion to its default postion */
			sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_GO_Y), "%f", &fltTextY);
			g_object_set(priv->pButtonGo,"y",fltTextY ,NULL );
		}

	}

	/* Re-adjust the Background */
	fltTextX = clutter_actor_get_height(priv->pBottomGroup)  + clutter_actor_get_height(priv->pPopupTextEntry);
	clutter_actor_set_height(priv->pPopupRect,fltTextX );



	/* set the postion of the Text to mid of Text Entry Box */
	fltTextX =((clutter_actor_get_width(priv->pPopupTextEntry) -
			clutter_actor_get_width(priv->pPopupText))/2) ;

	fltTextY = ((clutter_actor_get_height(priv->pPopupTextEntry) - clutter_actor_get_height(priv->pPopupHead)
			- clutter_actor_get_height (priv->pPopupTextBottom ) -
			clutter_actor_get_height(priv->pPopupText)) / 2) +
					clutter_actor_get_height(priv->pPopupHead);

	clutter_actor_set_position(priv->pPopupText,fltTextX,fltTextY);

	if(NULL != priv->pAppIcon)
	{
		fltTextY = (clutter_actor_get_height(CLUTTER_ACTOR(priv->pPopupTextEntry)) -
				clutter_actor_get_height(CLUTTER_ACTOR(priv->pAppIcon))) /2;
		fltTextX = ( priv->fltSeemlessDisp - clutter_actor_get_width(CLUTTER_ACTOR(priv->pAppIcon))) /2;
		clutter_actor_set_position(priv->pAppIcon, fltTextX,fltTextY);
	}


	if(NULL != priv->pMsgIcon)
	{
		gfloat fltTextEntryWidth;
		fltTextY = (clutter_actor_get_height(CLUTTER_ACTOR(priv->pPopupTextEntry)) -
				clutter_actor_get_height(CLUTTER_ACTOR(priv->pMsgIcon))) /2;

		sscanf((gchar*) g_hash_table_lookup( priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_WIDTH), "%f", &fltTextEntryWidth);
		fltTextX =  fltTextEntryWidth - (( priv->fltSeemlessDisp + clutter_actor_get_width(CLUTTER_ACTOR(priv->pMsgIcon)))/2);
		clutter_actor_set_position(priv->pMsgIcon, fltTextX,fltTextY);

	}

	clutter_actor_set_position(priv->pDragArea,0.0 ,0.0);

}



/**
 * v_mildenhall_selection_popup_show:
 * @pObject : MildenhallSelectionPopup * object reference
 *
 * Function to display the popup with animation
 */

void v_mildenhall_selection_popup_show( MildenhallSelectionPopup *pSelectionPopup)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return ;
	}

	clutter_actor_set_position(CLUTTER_ACTOR(pSelectionPopup), MILDENHALL_SELECTION_POPUP_ON_HIDE_X,(MILDENHALL_SELECTION_POPUP_ON_HIDE_Y + 2));

	clutter_actor_show(CLUTTER_ACTOR(pSelectionPopup));
	if (priv->bAnimationStart == FALSE)
	{
                gfloat fltHeight = 0.0;
		MILDENHALL_SELECTION_POPUP_PRINT("started animation: %s\n", __FUNCTION__);
		fltHeight = clutter_actor_get_height(priv->pBottomGroup) + clutter_actor_get_height(priv->pPopupTextEntry ) ;

		clutter_actor_set_height(CLUTTER_ACTOR(pSelectionPopup),fltHeight);

		if(priv->bPositionExtreme == TRUE)
		{
			gfloat fltTextHeight;
			sscanf((gchar*) g_hash_table_lookup(priv->pPrivHash,MILDENHALL_SELECTION_POPUP_TEXT_ENTRY_HEIGHT), "%f", &fltTextHeight);
			fltHeight = MILDENHALL_SELECTION_POPUP_ON_SHOW_Y  - fltTextHeight;
		}
		else
		{
			fltHeight = MILDENHALL_SELECTION_POPUP_ON_SHOW_Y;
		}
		v_selection_popup_show_with_animation(pSelectionPopup, MILDENHALL_SELECTION_POPUP_ON_SHOW_X, fltHeight);
	}
}

/**
 * v_mildenhall_selection_popup_hide:
 * @pObject : MildenhallSelectionPopup * object reference
 *
 * Function to hide the popup with animation
 */

void v_mildenhall_selection_popup_hide( MildenhallSelectionPopup *pSelectionPopup)
{

        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return ;
	}

	if (priv->bHideStart == FALSE)
		v_selection_popup_hide_with_animation(pSelectionPopup, MILDENHALL_SELECTION_POPUP_ON_HIDE_X, MILDENHALL_SELECTION_POPUP_ON_HIDE_Y);

}




static void v_mildenhall_selection_popup_update_up_down_arrows(MildenhallSelectionPopup *pSelectionPopup)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);

        MxAdjustment *pVadjust = NULL;
        gfloat position = 0.0;
        gdouble start = 0.0;
        gchar* pStrSpellerIcon = NULL;
        gint inNoOfRowAbove = 0;
        ThornburyModelIter *pIter = NULL;
        GValue IconPath = G_VALUE_INIT;

	MILDENHALL_SELECTION_POPUP_PRINT ("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	g_object_get(priv->pRollerContainer ,"vadjust",&pVadjust,NULL);

	/* Get item height  */
	position = mx_adjustment_get_step_increment(pVadjust) ;

	/* get the current position */
	start = mx_adjustment_get_value (pVadjust);

	inNoOfRowAbove =  ((gint) start) / (gint) position;

	if(start < 0)
	{
		inNoOfRowAbove = 0 ;
	}


	MILDENHALL_SELECTION_POPUP_PRINT("\nMILDENHALL_SELECTION_POPUP_PRINT: statr %lf  %d position =%f \n",start  , __LINE__ , position);

	pIter = thornbury_model_get_iter_at_row (priv->pModelUpButton, 0);
	g_value_init (&IconPath, G_TYPE_STRING);
	g_assert (G_VALUE_HOLDS_STRING (&IconPath));
	MILDENHALL_SELECTION_POPUP_PRINT("\nMILDENHALL_SELECTION_POPUP_PRINT: %s  %d inNoOfRowAbove =%d \n", __FUNCTION__ , __LINE__ , inNoOfRowAbove);

	if(inNoOfRowAbove == 0 || inNoOfRowAbove < 0)
	{
		MILDENHALL_SELECTION_POPUP_PRINT("\nMILDENHALL_SELECTION_POPUP_PRINT: %s  %d inNoOfRowAbove =%d \n", __FUNCTION__ , __LINE__ , inNoOfRowAbove);
		pStrSpellerIcon = g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_UP_EMPTY );
	}
	else
	{
		pStrSpellerIcon =  g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_UP_FILLED );
	}


	g_value_set_static_string (&IconPath, pStrSpellerIcon);
	thornbury_model_iter_set_value(pIter  ,0,&IconPath );

	pIter = thornbury_model_get_iter_at_row(priv->pModelDownButton , 0);
	if( ((inNoOfRowAbove + 4) >= priv->inNoOfRows) )
	{
		pStrSpellerIcon =  g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_DOWN_EMPTY );
	}
	else
	{
		pStrSpellerIcon =  g_hash_table_lookup(priv->pImageHash,MILDENHALL_SELECTION_POPUP_DOWN_FILLED );
	}

	g_value_set_static_string (&IconPath, pStrSpellerIcon);
	thornbury_model_iter_set_value(pIter  ,0,&IconPath );
	g_value_unset (&IconPath);
}



/********************************************************
 * Function : v_selection_popup_scroll_completed_cb
 * Description: cb on scroll complete for updating UP / DOWN button icons
 * Parameters: RollerContainer * , gpointer
 * Return value: void
 ********************************************************/


static void v_selection_popup_scroll_completed_cb(MildenhallRollerContainer *pRollerCont , gpointer pUserData)
{
        MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pUserData);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_SELECTION_POPUP(pUserData))
	{
		g_warning("invalid popup object\n");
		return ;
	}
	/* MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup); */


	v_mildenhall_selection_popup_update_up_down_arrows(pSelectionPopup);

}



/*********************************************************************************************
 * Function: p_selection_popup_extract_string_from_model
 * Description: function to extract string from model at given row and column
 * Parameters:
 * Return:   gchar * string if Found or Null if Not
 ********************************************************************************************/

static gchar *p_selection_popup_extract_string_from_model(ThornburyModel *pModel , guint uinRow ,guint uinColumn)
{
	gchar *pString = NULL ;
	if(G_IS_OBJECT(pModel))
	{
		ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pModel , uinRow);
		if(NULL != pIter )
		{
			GValue value = { 0, };
			thornbury_model_iter_get_value(pIter,uinColumn, &value);
			pString = g_value_dup_string(&value);
			g_value_unset (&value);
			//return pString ;
			g_object_unref(pIter);
		}
	}
	return pString ;
}



/********************************************************
 * Function : v_selection_popup_roller_item_activated_cb
 * Description: cb on roller-item-activated siganl
 * Parameters: RollerContainer * , guint ,gpointer
 * Return value: void
 ********************************************************/
static void v_selection_popup_roller_item_activated_cb (MildenhallRollerContainer *pRollerCont,guint uinRow, gpointer pUserData)
{
        MildenhallSelectionPopup *pSelectionPopup = MILDENHALL_SELECTION_POPUP (pUserData);
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_SELECTION_POPUP(pUserData))
	{
		g_warning("invalid popup object\n");
		return ;
	}

	priv->uinActivatedRow = uinRow;

	if(TYPE_ICON_LABEL_RADIO != priv->enSelectionPopupType &&
			TYPE_ICON_LABEL_CONFIRM != priv->enSelectionPopupType)
	{

		gchar *pSelectedItemId = NULL ;
		pSelectedItemId = p_selection_popup_extract_string_from_model(priv->pRollerItemModel ,uinRow , COL_SELECTION_ROLLER_ITEM_NAME );

		/*MILDENHALL_SELECTION_POPUP_PRINT("pSelectedItemId = %s Row No = %d \n\n",pSelectedItemId,uinRow);*/
		if(0 != g_strcmp0(MILDENHALL_SELECTION_POPUP_DUMMY_ITEMS_ID,pSelectedItemId))
		{
			g_signal_emit_by_name(pSelectionPopup, "popup-action", pSelectedItemId,GINT_TO_POINTER(uinRow));

			if (priv->bHideStart == FALSE)
			{
				v_selection_popup_hide_with_animation((MildenhallSelectionPopup *) pSelectionPopup,
						MILDENHALL_SELECTION_POPUP_ON_HIDE_X, MILDENHALL_SELECTION_POPUP_ON_HIDE_Y);
			}
		}
	}
}


/********************************************************
 * Function : v_selection_popup_show_with_animation
 * Description: function to do animation on popup show
 * Parameters: MildenhallSelectionPopup *, gfloat, gfloat
 * Return value: void
 ********************************************************/


static void v_selection_popup_show_with_animation(MildenhallSelectionPopup *pSelectionPopup, gfloat fltX, gfloat fltY)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	priv->animate_show = TRUE;
	priv->bAnimationStart = TRUE;
	clutter_actor_save_easing_state(CLUTTER_ACTOR(pSelectionPopup));
	clutter_actor_set_easing_mode(CLUTTER_ACTOR(pSelectionPopup), CLUTTER_EASE_OUT_BOUNCE);
	clutter_actor_set_easing_duration(CLUTTER_ACTOR(pSelectionPopup), MILDENHALL_SELECTION_POPUP_SHOWTIME);
	clutter_actor_set_x(CLUTTER_ACTOR(pSelectionPopup), fltX);
	clutter_actor_set_y(CLUTTER_ACTOR(pSelectionPopup), fltY);
	clutter_actor_restore_easing_state(CLUTTER_ACTOR(pSelectionPopup));
	MILDENHALL_SELECTION_POPUP_PRINT("exiting: %s\n", __FUNCTION__);
}


/********************************************************
 * Function : v_selection_popup_hide_with_animation
 * Description: function to do animation on popup hide
 * Parameters: MildenhallSelectionPopup *, gfloat, gfloat
 * Return value: void
 ********************************************************/


static void v_selection_popup_hide_with_animation(MildenhallSelectionPopup *pSelectionPopup, gfloat fltX, gfloat fltY)
{
	MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE(pSelectionPopup);

	priv->animate_show = FALSE;
	priv->bHideStart = TRUE;
	clutter_actor_save_easing_state(CLUTTER_ACTOR(pSelectionPopup));
	clutter_actor_set_easing_mode(CLUTTER_ACTOR(pSelectionPopup), CLUTTER_EASE_OUT_BOUNCE);
	clutter_actor_set_easing_duration(CLUTTER_ACTOR(pSelectionPopup), MILDENHALL_SELECTION_POPUP_SHOWTIME);
	clutter_actor_set_x(CLUTTER_ACTOR(pSelectionPopup), fltX);
	clutter_actor_set_y(CLUTTER_ACTOR(pSelectionPopup), fltY);
	clutter_actor_restore_easing_state(CLUTTER_ACTOR(pSelectionPopup));

}

/********************************************************
 * Function : v_selection_popup_animation_completed_cb
 * Description: function to do signal connect and other tasks
 * 				on completion of animation
 * Parameters: ClutterAnimation *, MildenhallSelectionPopup *
 * Return value: void
 ********************************************************/

static void v_selection_popup_animation_completed_cb(ClutterAnimation *anim, MildenhallSelectionPopup *pSelectionPopup)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);

	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return ;
	}

	if (priv->animate_show)
	{
		priv->bAnimationStart = FALSE;
		g_object_set(priv->pButtonDown,"reactive",TRUE, NULL);
		g_object_set(priv->pButtonUp,"reactive",TRUE, NULL);
		g_object_set(priv->pButtonBack,"reactive",TRUE, NULL);
		clutter_actor_set_reactive (priv->pDragArea, TRUE);
		g_object_set(priv->pRollerContainer ,"reactive",TRUE, NULL);


		if((TYPE_ICON_LABEL_RADIO == priv->enSelectionPopupType  ||
				TYPE_ICON_LABEL_CONFIRM == priv->enSelectionPopupType)
				&& NULL != priv->pButtonGo   )
		{
			g_object_set(priv->pButtonGo,"reactive",TRUE, NULL);
			if(FALSE == priv->bGoConnected)
			{
				priv->bGoConnected = TRUE;
				g_signal_connect(priv->pButtonGo,"button-press", G_CALLBACK(v_selection_popup_go_press_cb), pSelectionPopup);
			}
		}
		priv->bPopupShown = TRUE ;
		priv->bWaitingResponse = FALSE ;
		g_signal_emit_by_name(pSelectionPopup, "popup-shown");
		v_selection_popup_fetch_http_icons_clb(NULL , NULL , pSelectionPopup  );

	}
	else
	{
		priv->bHideStart = FALSE;

		g_object_set(priv->pButtonDown,"reactive",FALSE, NULL);
		g_object_set(priv->pButtonUp,"reactive",FALSE, NULL);
		g_object_set(priv->pButtonBack,"reactive",FALSE, NULL);
		clutter_actor_set_reactive (priv->pDragArea, FALSE);
		g_object_set(priv->pRollerContainer ,"reactive",FALSE, NULL);

		if(TRUE == priv->bGoConnected)
		{
			g_object_set(priv->pButtonGo,"reactive",FALSE, NULL);
		}

		/* set the popup to hide  */
		clutter_actor_hide(CLUTTER_ACTOR(pSelectionPopup));
		MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: popup-hidden emitted \n");
		priv->bPopupShown = FALSE;
		priv->bWaitingResponse = FALSE ;
		g_signal_emit_by_name(pSelectionPopup, "popup-hidden");
	}
}


static void v_selection_popup_find_and_update_http_icon(ClutterContent *pImage,
		ThornburyModel *pModel , gchar* pStringToSearch , guint uinColumnNo)
{
        ThornburyModelIter *pIter = NULL;
	MILDENHALL_SELECTION_POPUP_PRINT("MILDENHALL_SELECTION_POPUP_PRINT: %s\n", __FUNCTION__);
	if(G_IS_OBJECT(pModel))
	{
		guint uinTotalRows = thornbury_model_get_n_rows(pModel);
		guint uinIter ;
		for(uinIter = 0 ; uinIter < uinTotalRows ; uinIter++)
		{
			pIter = thornbury_model_get_iter_at_row(pModel, uinIter);
			if(NULL != pIter)
			{
				GValue value = { 0, };
                                MildenhallSelPopupItemIconDetail *pIconDetail = NULL;
				thornbury_model_iter_get_value(pIter,uinColumnNo, &value);
				pIconDetail= g_value_get_pointer (&value);
				if(NULL != pIconDetail && 0 == g_strcmp0(pIconDetail->pRowId, pStringToSearch))
				{
					MILDENHALL_SELECTION_POPUP_PRINT(" %s %d \n",__FUNCTION__ , __LINE__);
					/* Match Found */
					pIconDetail->pIconContent = pImage ;
					/* Update the icon in the model */
					thornbury_model_iter_set(pIter ,uinColumnNo , pIconDetail , -1 );
					g_value_unset (&value);
					g_object_unref(pIter);
					break;
				}
				g_value_unset (&value);
			}
			g_object_unref(pIter);
		}
	}
}


static void v_selection_popup_fetch_http_icons_clb(ClutterContent *pImage, GError *pError, gpointer pSelectionPopup)
{
        MildenhallSelectionPopupPrivate *priv = MILDENHALL_SELECTION_POPUP_GET_PRIVATE (pSelectionPopup);
	if(!MILDENHALL_IS_SELECTION_POPUP(pSelectionPopup))
	{
		g_warning("invalid popup object\n");
		return ;
	}
	MILDENHALL_SELECTION_POPUP_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
	priv->bWaitingResponse = FALSE ;
	if(NULL != pImage)
	{
		v_selection_popup_find_and_update_http_icon(pImage ,priv->pRollerItemModel,
				priv->pHttpIconId , COL_LEFT_ICON_FIELD );
	}
	while(FALSE == g_queue_is_empty(priv->pHttpIconQ) && TRUE == priv->bPopupShown )
	{
                MildenhallSelPopupItemIconDetail *pIconDetail = (MildenhallSelPopupItemIconDetail*) g_queue_pop_head (priv->pHttpIconQ);
		MILDENHALL_SELECTION_POPUP_PRINT("%s %d \n",__FUNCTION__ , __LINE__);
		if(NULL != pIconDetail && NULL != pIconDetail->pHttpFilePath)
		{
			MILDENHALL_SELECTION_POPUP_FREE_MEM_IF_NOT_NULL(priv->pHttpIconId);
			priv->pHttpIconId = g_strdup(pIconDetail->pRowId) ;
			thornbury_web_texture_create_content_async(pIconDetail->pHttpFilePath,LEFT_ICON_MAX_W , LEFT_ICON_MAX_H ,
					pSelectionPopup ,v_selection_popup_fetch_http_icons_clb);
			priv->bWaitingResponse = TRUE ;
			break;
		}
	}
}



/*** END OF FILE *************************************************************/
