/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_bottom_bar_type_one.c */

#include "mildenhall-internal.h"
#include "mildenhall_bottom_bar_type_one.h"

#include <thornbury/thornbury.h>

typedef struct
{
  gfloat bottomBarWidth;
  gfloat bottomBarHeight;

  gboolean bottomBarColumnOne;
  gboolean bottomBarColumnTwo;
  gboolean bottomBarColumnThree;
  gboolean bottomBarColumnFour;

  gboolean columnOneState;
  gboolean columnTwoState;
  gboolean columnThreeState;

  ThornburyModel *pColumnModel;

  GHashTable *pStyleHash;
  GHashTable *pLocalHash;

  ClutterActor *columnOneGroup;
  ClutterActor *columnOneTexture;

  ClutterActor *columnTwoGroup;
  ClutterActor *columnTwoTexture;

  ClutterActor *columnThreeGroup;
  ClutterActor *columnThreeTexture;

  ClutterActor *columnFourGroup;
  ClutterActor *columnFourText;

  ClutterActor *betweenLine;
  ClutterActor *betweenLineTwo;

  gchar *columnOneTextureName;
  gchar *columnTwoTextureName;
  gchar *columnThreeTextureName;

  gchar *columnOneActiveImagePath;
  gchar *columnOneInactiveImagePath;
  gchar *columnTwoActiveImagePath;
  gchar *columnTwoInactiveImagePath;
  gchar *columnThreeActiveImagePath;
  gchar *columnThreeInactiveImagePath;
  gchar *columnFourData;
} MildenhallBottomBarPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallBottomBar, mildenhall_bottom_bar, CLUTTER_TYPE_ACTOR)

/* Set the environment variable in terminal to enable traces: export BOTTOM_BAR_DEBUG=bottom-bar */
enum _enBottomBarDebugFlag
{
	BOTTOM_BAR_DEBUG = 1 << 0,
};

static guint  bottom_bar_debug_flags = 0;

static const GDebugKey bottom_bar_debug_keys[] =
{
		{ "bottom-bar",   BOTTOM_BAR_DEBUG }
};

#define BOTTOM_BAR_HAS_DEBUG               ((bottom_bar_debug_flags ) & 1)
#define BOTTOM_BAR_PRINT( a ...) \
		if (G_LIKELY (BOTTOM_BAR_HAS_DEBUG )) \
		{                               \
			g_print(a);           \
		}

#define COLUMN_ONE_X "column-one-x"
#define COLUMN_ONE_Y "column-one-y"
#define COLUMN_ONE_WIDTH "column-one-width"
#define COLUMN_ONE_HEIGHT "column-one-height"

#define COLUMN_TWO_X "column-two-x"
#define COLUMN_TWO_Y "column-two-y"
#define COLUMN_TWO_WIDTH "column-two-width"
#define COLUMN_TWO_HEIGHT "column-two-height"

#define COLUMN_THREE_X "column-three-x"
#define COLUMN_THREE_Y "column-three-y"
#define COLUMN_THREE_WIDTH "column-three-width"
#define COLUMN_THREE_HEIGHT "column-three-height"

#define COLUMN_FOUR_X "column-four-x"
#define COLUMN_FOUR_Y "column-four-y"
#define COLUMN_FOUR_WIDTH "column-four-width"
#define COLUMN_FOUR_HEIGHT "column-four-height"
#define COLUMN_FOUR_FONT "column-four-font"

#define BETWEEN_LINE_ONE_X "between-line-one-x"
#define BETWEEN_LINE_ONE_Y "between-line-one-y"
#define BETWEEN_LINE_ONE_WIDTH "between-line-one-width"
#define BETWEEN_LINE_ONE_HEIGHT "between-line-one-height"

#define BETWEEN_LINE_TWO_X "between-line-two-x"
#define BETWEEN_LINE_TWO_Y "between-line-two-y"
#define BETWEEN_LINE_TWO_WIDTH "between-line-two-width"
#define BETWEEN_LINE_TWO_HEIGHT "between-line-two-height"

#define DEFAULT_PNG "/icon_music_shuffle_AC.png"

static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData);
static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData);

static void v_bottom_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData);
static void v_bottom_bar_add_style_to_hash (MildenhallBottomBar *bottomBar, const gchar *pKey, gpointer pValue);

void create_bottom_bar (MildenhallBottomBar *bottomBar);
void bottom_bar_one_extract_model_info(gpointer data);

static void v_bottom_bar_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar);
static void v_bottom_bar_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar);
static void v_bottom_bar_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar);

/* static APIs for bottom bar property set methods */
static void set_bottom_bar_width (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_height (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_column_one (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_column_two (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_column_three(MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_column_four (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_button_one_state (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_button_two_state (MildenhallBottomBar *self, const GValue *value);
static void set_bottom_bar_button_three_state (MildenhallBottomBar *self, const GValue *value);
static void bottom_bar_set_model (MildenhallBottomBar *self, const GValue *value);

static void v_bottom_bar_col_one_check (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_col_two_check (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_col_three_check (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_col_four_check (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_set_col_two_texture (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_set_col_one_texture (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_set_col_three_texture (MildenhallBottomBar *bottomBar);
static void v_bottom_bar_set_col_four_texture (MildenhallBottomBar *bottomBar);

static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData);

enum _enModelInfo
{
	COLUMN_ONE_ACTIVE,
	COLUMN_ONE_INACTIVE,
	COLUMN_ONE_TEXTURE_NAME,
	COLUMN_TWO_ACTIVE,
	COLUMN_TWO_INACTIVE,
	COLUMN_TWO_TEXTURE_NAME,
	COLUMN_THREE_ACTIVE,
	COLUMN_THREE_INACTIVE,
	COLUMN_THREE_TEXTURE_NAME,
	COLUMN_FOUR,
	COLUMN_LAST
};

enum MildenhallBottomBarProperty
{
	PROP_MH_BOTTOM_BAR_FIRST,
	PROP_MH_BOTTOM_BAR_WIDTH,
	PROP_MH_BOTTOM_BAR_HEIGHT,					/* Refers to the total width of the bottom bar */
	PROP_MH_BOTTOM_BAR_COLUMN_ONE,			/* Refers to the bottom bar reactivity. */
	PROP_MH_BOTTOM_BAR_COLUMN_TWO,
	PROP_MH_BOTTOM_BAR_COLUMN_THREE,
	PROP_MH_BOTTOM_BAR_COLUMN_FOUR,
	PROP_MH_BOTTOM_BAR_BUTTON_ONE_STATE,
	PROP_MH_BOTTOM_BAR_BUTTON_TWO_STATE,
	PROP_MH_BOTTOM_BAR_BUTTON_THREE_STATE,
	PROP_MH_BOTTOM_BAR_MODEL,
	PROP_MH_BOTTOM_BAR_LAST
};

enum MildenhallBottomBarSignals
{
	SIG_MH_BOTTOM_BAR_ACTION_PRESS,			/* Emitted when user seeks on the progress bar */
	SIG_MH_BOTTOM_BAR_ACTION_RELEASE,		/* Emitted when play is requMH by the user on progress bar */
	SIG_MH_BOTTOM_BAR_LAST_SIGNAL
};

static guint32 mh_bottom_bar_signals[SIG_MH_BOTTOM_BAR_LAST_SIGNAL] = {0,};

static void
bottom_bar_get_property (GObject    *object,
                         guint       property_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  MildenhallBottomBar *bottom_bar = MILDENHALL_BOTTOM_BAR (object);
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottom_bar);

  switch (property_id)
    {
    case PROP_MH_BOTTOM_BAR_WIDTH:
      g_value_set_float (value, priv->bottomBarWidth);
      break;
    case PROP_MH_BOTTOM_BAR_HEIGHT:
      g_value_set_float (value, priv->bottomBarHeight);
      break;
    case PROP_MH_BOTTOM_BAR_COLUMN_ONE:
      g_value_set_boolean (value, priv->bottomBarColumnOne);
      break;
    case PROP_MH_BOTTOM_BAR_COLUMN_TWO:
      g_value_set_boolean (value, priv->bottomBarColumnTwo);
      break;
    case PROP_MH_BOTTOM_BAR_COLUMN_THREE:
      g_value_set_boolean (value, priv->bottomBarColumnThree);
      break;
    case PROP_MH_BOTTOM_BAR_COLUMN_FOUR:
      g_value_set_boolean (value, priv->bottomBarColumnFour);
      break;
    case PROP_MH_BOTTOM_BAR_BUTTON_ONE_STATE:
      g_value_set_boolean (value, priv->columnOneState);
      break;
    case PROP_MH_BOTTOM_BAR_BUTTON_TWO_STATE:
      g_value_set_boolean (value, priv->columnTwoState);
      break;
    case PROP_MH_BOTTOM_BAR_BUTTON_THREE_STATE:
      g_value_set_boolean (value, priv->columnThreeState);
      break;
    case PROP_MH_BOTTOM_BAR_MODEL:
      g_value_set_object (value, priv->pColumnModel);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
bottom_bar_set_property (GObject      *object,
                         guint         property_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  MildenhallBottomBar *self = MILDENHALL_BOTTOM_BAR (object);

  switch (property_id)
    {
    case PROP_MH_BOTTOM_BAR_WIDTH:
      set_bottom_bar_width (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_HEIGHT:
      set_bottom_bar_height (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_COLUMN_ONE:
      set_bottom_bar_column_one (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_COLUMN_TWO:
      set_bottom_bar_column_two (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_COLUMN_THREE:
      set_bottom_bar_column_three (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_COLUMN_FOUR:
      set_bottom_bar_column_four (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_BUTTON_ONE_STATE:
      set_bottom_bar_button_one_state (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_BUTTON_TWO_STATE:
      set_bottom_bar_button_two_state (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_BUTTON_THREE_STATE:
      set_bottom_bar_button_three_state (self, value);
      break;
  
    case PROP_MH_BOTTOM_BAR_MODEL:
      bottom_bar_set_model (self, value);
      break;
  
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

/*********************************************************************************************
 * Function:    set_bottom_bar_height
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_height (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gfloat bottomBarHeight = g_value_get_float (value);

  if (priv->bottomBarHeight == bottomBarHeight)
    return;

  priv->bottomBarHeight = bottomBarHeight;
  clutter_actor_set_size (CLUTTER_ACTOR (self), priv->bottomBarWidth, priv->bottomBarHeight);

}


/*********************************************************************************************
 * Function:    set_bottom_bar_width
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_width (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gfloat bottomBarWidth = g_value_get_float (value);

  if (priv->bottomBarWidth == bottomBarWidth)
    return;

  priv->bottomBarWidth = bottomBarWidth;
  clutter_actor_set_size (CLUTTER_ACTOR (self), priv->bottomBarWidth, priv->bottomBarHeight);
}

/*********************************************************************************************
 * Function:    set_bottom_bar_column_one
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_column_one (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gboolean bottomBarColumnOne = g_value_get_boolean (value);

  priv->bottomBarColumnOne = bottomBarColumnOne;
  if (!(priv->bottomBarColumnOne))
    {
      clutter_actor_hide (priv->betweenLine);
      clutter_actor_hide (priv->columnOneGroup);
      clutter_actor_set_reactive (priv->columnOneGroup, FALSE);
    }
}

/*********************************************************************************************
 * Function:    set_bottom_bar_column_two
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_column_two (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gboolean bottomBarColumnTwo = g_value_get_boolean (value);

  if (priv->bottomBarColumnTwo == bottomBarColumnTwo)
    return;

  priv->bottomBarColumnTwo = bottomBarColumnTwo;
  if (!(priv->bottomBarColumnTwo))
    {
      clutter_actor_hide (priv->betweenLineTwo);
      clutter_actor_hide (priv->columnTwoGroup);
      clutter_actor_set_reactive (priv->columnTwoGroup, FALSE);
    }
}

/*********************************************************************************************
 * Function:    set_bottom_bar_column_three
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_column_three (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gboolean bottomBarColumnThree = g_value_get_boolean (value);

  if (priv->bottomBarColumnThree == bottomBarColumnThree)
    return;

  priv->bottomBarColumnThree = bottomBarColumnThree;
  if (!(priv->bottomBarColumnThree))
  {
    clutter_actor_hide (priv->betweenLineTwo);
    clutter_actor_hide (priv->columnThreeGroup);
    clutter_actor_set_reactive (priv->columnThreeGroup, FALSE);
  }
}


/*********************************************************************************************
 * Function:    set_bottom_bar_column_four
 * Description: Set the bottom bar width property
 * Parameters:  The object reference, and property value for the
 *              the property
 * Return:      void
 ********************************************************************************************/
static void
set_bottom_bar_column_four (MildenhallBottomBar *self, const GValue *value)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);
  gboolean bottomBarColumnFour = g_value_get_boolean (value);

  if (priv->bottomBarColumnFour == bottomBarColumnFour)
    return;

  priv->bottomBarColumnFour = bottomBarColumnFour;
  if (!(priv->bottomBarColumnFour))
    {
      clutter_actor_hide (priv->columnFourGroup);
    }
}

static void
set_bottom_bar_button_one_state (MildenhallBottomBar *self, const GValue *value)
{
  GValue ColumnOneActive = { 0, };
  GValue ColumnOneInactive = { 0, };
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);

  priv->columnOneState = g_value_get_boolean (value);

  if (G_IS_OBJECT(priv->pColumnModel) && thornbury_model_get_n_rows(priv->pColumnModel) > 0)
    {
      guint iCurrentrow = 0;
      for (; iCurrentrow < thornbury_model_get_n_rows (priv->pColumnModel);
          iCurrentrow++)
        {
          ThornburyModelIter *pIter = 
            thornbury_model_get_iter_at_row (priv->pColumnModel, iCurrentrow);

          if (!pIter)
            continue;

          thornbury_model_iter_get_value (pIter, COLUMN_ONE_ACTIVE, &ColumnOneActive);

          thornbury_model_iter_get_value (pIter, COLUMN_ONE_INACTIVE, &ColumnOneInactive);
        }

      if(priv->columnOneState)
        {
          thornbury_ui_texture_set_from_file (priv->columnOneTexture,
            g_value_get_string (&ColumnOneActive), 0, 0, FALSE, FALSE);
          priv->columnOneState = FALSE;
        }
      else
        {
          thornbury_ui_texture_set_from_file (priv->columnOneTexture,
            g_value_get_string (&ColumnOneInactive), 0, 0, FALSE, FALSE);
          priv->columnOneState = TRUE;
        }
    }
}


static void
set_bottom_bar_button_two_state (MildenhallBottomBar *self, const GValue *value)
{
  GValue ColumnTwoActive = { 0, };
  GValue ColumnTwoInactive = { 0, };
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);

  priv->columnTwoState = g_value_get_boolean (value);

  if (G_IS_OBJECT (priv->pColumnModel) && thornbury_model_get_n_rows (priv->pColumnModel) > 0)
    {
      guint iCurrentrow = 0;
      for (; iCurrentrow < thornbury_model_get_n_rows (priv->pColumnModel);
          iCurrentrow++)
        {
          ThornburyModelIter *pIter =
            thornbury_model_get_iter_at_row (priv->pColumnModel, iCurrentrow);

          if (!pIter)
            continue;

          thornbury_model_iter_get_value (pIter, COLUMN_TWO_ACTIVE, &ColumnTwoActive);
          thornbury_model_iter_get_value (pIter, COLUMN_TWO_INACTIVE, &ColumnTwoInactive);
        }

      if(priv->columnTwoState)
        {
          thornbury_ui_texture_set_from_file (priv->columnTwoTexture,
            g_value_get_string (&ColumnTwoActive), 0, 0, FALSE, FALSE);
          priv->columnTwoState = FALSE;
        }
      else
        {
          thornbury_ui_texture_set_from_file (priv->columnTwoTexture,
            g_value_get_string (&ColumnTwoInactive), 0, 0, FALSE, FALSE);
          priv->columnTwoState = TRUE;
        }
    }
}

static void
set_bottom_bar_button_three_state (MildenhallBottomBar *self, const GValue *value)
{
  GValue ColumnThreeActive = { 0, };
  GValue ColumnThreeInactive = { 0, };
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);

  priv->columnThreeState = g_value_get_boolean (value);

  if (G_IS_OBJECT (priv->pColumnModel) && thornbury_model_get_n_rows (priv->pColumnModel) > 0)
    {
      guint iCurrentrow = 0;
      for (; iCurrentrow < thornbury_model_get_n_rows (priv->pColumnModel);
          iCurrentrow++)
        {
          ThornburyModelIter *pIter =
            thornbury_model_get_iter_at_row (priv->pColumnModel, iCurrentrow);

          if (!pIter)
            continue;

          thornbury_model_iter_get_value (pIter, COLUMN_THREE_ACTIVE, &ColumnThreeActive);
          thornbury_model_iter_get_value (pIter, COLUMN_THREE_INACTIVE, &ColumnThreeInactive);
        }
      if(priv->columnThreeState)
        {
          thornbury_ui_texture_set_from_file (priv->columnThreeTexture,
            g_value_get_string (&ColumnThreeActive), 0, 0, FALSE, FALSE);
        }
      else
        {
          thornbury_ui_texture_set_from_file (priv->columnThreeTexture, 
            g_value_get_string (&ColumnThreeInactive), 0, 0, FALSE, FALSE);
        }
    }
}

/**
 * bottom_bar_set_model:
 * @pObject : bottom bar object reference
 * @pModel : model for the bottom bar
 *
 * Model contains the data to be displayed on the butoon.
 * Bottom Bar model contains:
 *			1. Active Image Path for first column.
 *			2. Inactive Image Path for first column.
 *			3. Button name for the first column to differentiate the button press and release callbacks of different buttons.
 *			4. Active Image Path for second column.
 *			5. Inactive Image Path for second column.
 *			6. Button name for the second column
 *			7. Active Image Path for third column.
 *			8. Inactive Image Path for third column.
 *			9. Button name for the third column.
 *			10.Text for the fourth column.
 */
static void
bottom_bar_set_model (MildenhallBottomBar *self, const GValue *value)
{
  ThornburyModel *pModel = NULL;
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);

  /* if model is set again, remove previous model */
  if(NULL != priv->pColumnModel)
    {
      g_signal_handlers_disconnect_by_func (priv->pColumnModel,
        G_CALLBACK (v_bottom_bar_row_added_cb),
        self);
      g_signal_handlers_disconnect_by_func (priv->pColumnModel,
        G_CALLBACK (v_bottom_bar_row_changed_cb),
        self);
      g_signal_handlers_disconnect_by_func (priv->pColumnModel,
        G_CALLBACK (v_bottom_bar_row_removed_cb),
        self);
      g_object_unref (priv->pColumnModel);

      priv->pColumnModel = NULL;
    }

  pModel = g_value_get_object (value);

  /* update the new model with signals */
  if (pModel != NULL)
    {
      g_return_if_fail (G_IS_OBJECT (pModel));

      priv->pColumnModel = g_object_ref (pModel);

      g_signal_connect (priv->pColumnModel,
        "row-added",
        G_CALLBACK (v_bottom_bar_row_added_cb),
        self);

      g_signal_connect (priv->pColumnModel,
        "row-changed",
        G_CALLBACK (v_bottom_bar_row_changed_cb),
        self);

      g_signal_connect (priv->pColumnModel,
        "row-removed",
        G_CALLBACK (v_bottom_bar_row_removed_cb),
        self);
    }
  bottom_bar_one_extract_model_info (self);
}

static void v_bottom_bar_col_one_check(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);

	gfloat columnOneX, columnOneY, columnOneWidth, columnOneHeight;
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_X), "%f", &columnOneX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_Y), "%f", &columnOneY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_WIDTH), "%f", &columnOneWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_HEIGHT), "%f", &columnOneHeight);

	if(priv->bottomBarColumnOne)
	{
		priv->columnOneTexture=thornbury_ui_texture_create_new(PKGTHEMEDIR DEFAULT_PNG, 0,0,FALSE,FALSE);
		clutter_actor_set_reactive(priv->columnOneGroup, TRUE);
		clutter_actor_add_child(priv->columnOneGroup,priv->columnOneTexture);
		clutter_actor_set_size(priv->columnOneTexture,columnOneWidth,columnOneHeight);
		clutter_actor_set_position(priv->columnOneGroup,columnOneX,columnOneY);
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->columnOneGroup);
	}
	else
	{
		clutter_actor_hide(priv->columnOneGroup);
		clutter_actor_set_reactive(priv->columnOneGroup, FALSE);
	}
}

static void v_bottom_bar_col_two_check(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);

	ClutterColor lineColor = {0x98,0xA9,0xB2,0xFF};
	gfloat columnTwoX, columnTwoY, columnTwoWidth, columnTwoHeight;
	gfloat betweenLineOneX, betweenLineOneY, betweenLineOneWidth, betweenLineOneHeight;

	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_X), "%f", &columnTwoX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_Y), "%f", &columnTwoY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_WIDTH), "%f", &columnTwoWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_HEIGHT), "%f", &columnTwoHeight);

	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_X), "%f", &betweenLineOneX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_Y), "%f", &betweenLineOneY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_WIDTH), "%f", &betweenLineOneWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_HEIGHT), "%f", &betweenLineOneHeight);


	if(priv->bottomBarColumnTwo)
	{
		priv->columnTwoTexture=thornbury_ui_texture_create_new(PKGTHEMEDIR DEFAULT_PNG,0,0,FALSE,FALSE);
		clutter_actor_set_reactive(priv->columnTwoGroup, TRUE);
		clutter_actor_add_child(priv->columnTwoGroup,priv->columnTwoTexture);
		clutter_actor_set_size(priv->columnTwoTexture,columnTwoWidth,columnTwoHeight);
		clutter_actor_set_position(priv->columnTwoGroup,columnTwoX,columnTwoY);
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->columnTwoGroup);


	}
	else
	{
		clutter_actor_hide(priv->columnTwoGroup);
		clutter_actor_set_reactive(priv->columnTwoGroup, FALSE);
	}
	//Check is column one and column two are exists then add the divider line.
	if(priv->bottomBarColumnOne && priv->bottomBarColumnTwo)
	{
		priv->betweenLine = clutter_actor_new();
		clutter_actor_set_size(priv->betweenLine,betweenLineOneWidth,betweenLineOneHeight);
		clutter_actor_set_position(priv->betweenLine,betweenLineOneX,betweenLineOneY);
		clutter_actor_set_background_color(priv->betweenLine,&lineColor);
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->betweenLine);
	}
}

static void v_bottom_bar_col_three_check(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	ClutterColor lineColor = {0x98,0xA9,0xB2,0xFF};
	gfloat columnThreeX, columnThreeY, columnThreeWidth, columnThreeHeight;
	gfloat betweenLineTwoX, betweenLineTwoY, betweenLineTwoWidth, betweenLineTwoHeight;
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_X), "%f", &columnThreeX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_Y), "%f", &columnThreeY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_WIDTH), "%f", &columnThreeWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_HEIGHT), "%f", &columnThreeHeight);

	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_X), "%f", &betweenLineTwoX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_Y), "%f", &betweenLineTwoY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_WIDTH), "%f", &betweenLineTwoWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_HEIGHT), "%f", &betweenLineTwoHeight);

	if(priv->bottomBarColumnThree)
	{
		priv->columnThreeTexture=thornbury_ui_texture_create_new(PKGTHEMEDIR DEFAULT_PNG,0,0,FALSE,FALSE);
		clutter_actor_set_reactive(priv->columnThreeGroup, TRUE);
		clutter_actor_add_child(priv->columnThreeGroup,priv->columnThreeTexture);
		clutter_actor_set_size(priv->columnThreeTexture,columnThreeWidth,columnThreeHeight);
		clutter_actor_set_position(priv->columnThreeGroup,columnThreeX,columnThreeY);
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->columnThreeGroup);
	}
	else
	{
		clutter_actor_hide(priv->columnThreeGroup);
		clutter_actor_set_reactive(priv->columnThreeGroup, FALSE);
	}
	//Check is column two and column three are exists then add the divider line.
	if(priv->bottomBarColumnTwo && priv->bottomBarColumnThree)
	{
		priv->betweenLineTwo = clutter_actor_new();
		clutter_actor_set_size(priv->betweenLineTwo,betweenLineTwoWidth,betweenLineTwoHeight);
		clutter_actor_set_position(priv->betweenLineTwo,betweenLineTwoX,betweenLineTwoY);
		clutter_actor_set_background_color(priv->betweenLineTwo,&lineColor);
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->betweenLineTwo);
	}
}

/*********************************************************************************************
 * Function:    v_bottom_bar_set_col_one_texture
 * Description: extract the model information for bottom bar first icon.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
static void v_bottom_bar_col_four_check(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	ClutterColor lineColor = {0x98,0xA9,0xB2,0xFF};
	gfloat columnFourX, columnFourY, columnFourWidth, columnFourHeight;
	gchar *columnFourFont = (gchar*) g_hash_table_lookup(priv->pLocalHash,COLUMN_FOUR_FONT);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_X), "%f", &columnFourX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_Y), "%f", &columnFourY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_WIDTH), "%f", &columnFourWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_HEIGHT), "%f", &columnFourHeight);

	if(priv->bottomBarColumnFour)
	{
		priv->columnFourText = clutter_text_new();
		clutter_text_set_text(CLUTTER_TEXT(priv->columnFourText),NULL);
		clutter_text_set_ellipsize(CLUTTER_TEXT(priv->columnFourText),PANGO_ELLIPSIZE_END);
		clutter_text_set_single_line_mode(CLUTTER_TEXT(priv->columnFourText),TRUE);
		clutter_text_set_justify(CLUTTER_TEXT(priv->columnFourText), TRUE);
		clutter_text_set_line_alignment(CLUTTER_TEXT(priv->columnFourText),PANGO_ALIGN_RIGHT);
		clutter_text_set_font_name(CLUTTER_TEXT(priv->columnFourText),columnFourFont);
		clutter_actor_set_position(priv->columnFourGroup,columnFourX,columnFourY);
		clutter_actor_set_size(CLUTTER_ACTOR(priv->columnFourText),columnFourWidth,columnFourHeight);
		//clutter_actor_set_width(priv->columnFourText,100);
		clutter_text_set_color(CLUTTER_TEXT(priv->columnFourText),&lineColor);
		clutter_actor_add_child(priv->columnFourGroup,CLUTTER_ACTOR(priv->columnFourText));
		clutter_actor_add_child(CLUTTER_ACTOR(bottomBar),priv->columnFourGroup);
	}
	else
	{
		clutter_actor_hide(priv->columnFourGroup);

	}

}

/*********************************************************************************************
 * Function:    create_bottom_bar
 * Description: create the bottom bar with default property values
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/

void create_bottom_bar(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);

	priv->columnOneGroup = clutter_actor_new();
	priv->columnTwoGroup = clutter_actor_new();
	priv->columnThreeGroup = clutter_actor_new();
	priv->columnFourGroup = clutter_actor_new();

	clutter_actor_set_size(CLUTTER_ACTOR(bottomBar),priv->bottomBarWidth,priv->bottomBarHeight);

	//Check if the column one is needed or not.
	v_bottom_bar_col_one_check(bottomBar);

	//Check if the column two is needed or not.
	v_bottom_bar_col_two_check(bottomBar);

	//Check if the column three is needed or not.
	v_bottom_bar_col_three_check(bottomBar);

	//Check if the column four is needed or not.
	v_bottom_bar_col_four_check(bottomBar);

}


/*********************************************************************************************
 * Function:    v_bottom_bar_set_col_one_texture
 * Description: extract the model information for bottom bar first icon.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
static void v_bottom_bar_set_col_one_texture(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	gfloat columnOneX, columnOneY, columnOneWidth, columnOneHeight;
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_X), "%f", &columnOneX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_Y), "%f", &columnOneY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_WIDTH), "%f", &columnOneWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_ONE_HEIGHT), "%f", &columnOneHeight);

	if(priv->bottomBarColumnOne)
	{
		//Set the texture according to the state of the column(For active and inactive)
		if(priv->columnOneState)
		{
			thornbury_ui_texture_set_from_file(priv->columnOneTexture,priv->columnOneActiveImagePath, 0,0,FALSE,FALSE);
			priv->columnOneState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnOneTexture,priv->columnOneInactiveImagePath, 0,0,FALSE,FALSE);
			priv->columnOneState=TRUE;
		}

		clutter_actor_set_size(priv->columnOneTexture,columnOneWidth,columnOneHeight);
		clutter_actor_set_position(priv->columnOneGroup,columnOneX,columnOneY);
		g_signal_connect (priv->columnOneGroup, "button-press-event", G_CALLBACK(on_button_press), bottomBar);
		g_signal_connect (priv->columnOneGroup, "button-release-event", G_CALLBACK(on_button_release), bottomBar);
		g_signal_connect (priv->columnOneGroup, "touch-event", G_CALLBACK(touch_event), bottomBar);
	}
	else
	{
		clutter_actor_hide(priv->columnOneGroup);
		clutter_actor_set_reactive(priv->columnOneGroup, FALSE);
	}
}

/*********************************************************************************************
 * Function:    v_bottom_bar_set_col_two_texture
 * Description: extract the model information for bottom bar second icon.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
static void v_bottom_bar_set_col_two_texture(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	gfloat columnTwoX, columnTwoY, columnTwoWidth, columnTwoHeight;
	gfloat betweenLineOneX, betweenLineOneY, betweenLineOneWidth, betweenLineOneHeight;
	ClutterColor lineColor = {0xff,0xff,0xff,0xff};
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_X), "%f", &columnTwoX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_Y), "%f", &columnTwoY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_WIDTH), "%f", &columnTwoWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_TWO_HEIGHT), "%f", &columnTwoHeight);

	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_X), "%f", &betweenLineOneX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_Y), "%f", &betweenLineOneY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_WIDTH), "%f", &betweenLineOneWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_ONE_HEIGHT), "%f", &betweenLineOneHeight);

	if(priv->bottomBarColumnTwo)
	{
		//Set the texture according to the state of the column(For active and inactive)
		if(priv->columnTwoState)
		{
			thornbury_ui_texture_set_from_file(priv->columnTwoTexture,priv->columnTwoActiveImagePath,0,0,FALSE,FALSE);
			priv->columnTwoState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnTwoTexture,priv->columnTwoInactiveImagePath,0,0,FALSE,FALSE);
			priv->columnTwoState=TRUE;
		}
		clutter_actor_set_size(priv->columnTwoTexture,columnTwoWidth,columnTwoHeight);
		clutter_actor_set_position(priv->columnTwoGroup,columnTwoX,columnTwoY);
		g_signal_connect (priv->columnTwoGroup, "button-press-event", G_CALLBACK(on_button_press), bottomBar);
		g_signal_connect (priv->columnTwoGroup, "button-release-event", G_CALLBACK(on_button_release), bottomBar);
		g_signal_connect (priv->columnTwoGroup, "touch-event", G_CALLBACK(touch_event), bottomBar);
	}
	else
	{
		clutter_actor_hide(priv->columnTwoGroup);
		clutter_actor_set_reactive(priv->columnTwoGroup, FALSE);
	}
	if(priv->bottomBarColumnOne && priv->bottomBarColumnTwo)
	{
		clutter_actor_set_size(priv->betweenLine,betweenLineOneWidth,betweenLineOneHeight);
		clutter_actor_set_position(priv->betweenLine,betweenLineOneX,betweenLineOneY);
		clutter_actor_set_background_color(priv->betweenLine,&lineColor);
	}
	else
	{
		clutter_actor_hide(priv->betweenLine);
	}
}

/*********************************************************************************************
 * Function:    v_bottom_bar_set_col_three_texture
 * Description: extract the model information for bottom bar third icon.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
static void v_bottom_bar_set_col_three_texture(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	gfloat columnThreeX, columnThreeY, columnThreeWidth, columnThreeHeight;
	gfloat betweenLineTwoX, betweenLineTwoY, betweenLineTwoWidth, betweenLineTwoHeight;
	ClutterColor lineColor = {0xff,0xff,0xff,0xff};
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_X), "%f", &columnThreeX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_Y), "%f", &columnThreeY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_WIDTH), "%f", &columnThreeWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_THREE_HEIGHT), "%f", &columnThreeHeight);

	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_X), "%f", &betweenLineTwoX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_Y), "%f", &betweenLineTwoY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_WIDTH), "%f", &betweenLineTwoWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, BETWEEN_LINE_TWO_HEIGHT), "%f", &betweenLineTwoHeight);

	if(priv->bottomBarColumnThree)
	{
		//Set the texture according to the state of the column(For active and inactive)
		if(priv->columnThreeState)
		{
			thornbury_ui_texture_set_from_file(priv->columnThreeTexture,priv->columnThreeActiveImagePath,0,0,FALSE,FALSE);
			priv->columnThreeState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnThreeTexture,priv->columnThreeInactiveImagePath,0,0,FALSE,FALSE);
			priv->columnThreeState=TRUE;
		}
		clutter_actor_set_size(priv->columnThreeTexture,columnThreeWidth,columnThreeHeight);
		clutter_actor_set_position(priv->columnThreeGroup,columnThreeX,columnThreeY);
		g_signal_connect (priv->columnThreeGroup, "button-press-event", G_CALLBACK(on_button_press), bottomBar);
		g_signal_connect (priv->columnThreeGroup, "button-release-event", G_CALLBACK(on_button_release), bottomBar);
		g_signal_connect (priv->columnThreeGroup, "touch-event", G_CALLBACK(touch_event), bottomBar);
	}
	else
	{
		clutter_actor_hide(priv->columnThreeGroup);
		clutter_actor_set_reactive(priv->columnThreeGroup, FALSE);
	}

	if(priv->bottomBarColumnTwo && priv->bottomBarColumnThree)
	{
		clutter_actor_set_size(priv->betweenLineTwo,betweenLineTwoWidth,betweenLineTwoHeight);
		clutter_actor_set_position(priv->betweenLineTwo,betweenLineTwoX,betweenLineTwoY);
		clutter_actor_set_background_color(priv->betweenLineTwo,&lineColor);
	}
	else
	{
		clutter_actor_hide(priv->betweenLineTwo);
	}
}

/*********************************************************************************************
 * Function:    v_bottom_bar_set_col_four_texture
 * Description: extract the model information for bottom bar text.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
static void v_bottom_bar_set_col_four_texture(MildenhallBottomBar *bottomBar)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	ClutterColor lineColor = {0xff,0xff,0xff,0xff};

	gfloat columnFourX, columnFourY, columnFourWidth, columnFourHeight;
	gchar *columnFourFont = (gchar*) g_hash_table_lookup(priv->pLocalHash,COLUMN_FOUR_FONT);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_X), "%f", &columnFourX);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_Y), "%f", &columnFourY);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_WIDTH), "%f", &columnFourWidth);
	sscanf ((gchar*) g_hash_table_lookup (priv->pLocalHash, COLUMN_FOUR_HEIGHT), "%f", &columnFourHeight);

	if(priv->bottomBarColumnFour)
	{
		clutter_text_set_text(CLUTTER_TEXT(priv->columnFourText),priv->columnFourData);
		clutter_actor_set_position(priv->columnFourGroup,columnFourX,columnFourY);
		clutter_actor_set_size(CLUTTER_ACTOR(priv->columnFourText),columnFourWidth,columnFourHeight);
		clutter_actor_set_width(CLUTTER_ACTOR(priv->columnFourText),100);
		clutter_text_set_font_name(CLUTTER_TEXT(priv->columnFourText),columnFourFont);
		clutter_text_set_color(CLUTTER_TEXT(priv->columnFourText),&lineColor);

	}
	else
	{
		clutter_actor_hide(priv->columnFourGroup);

	}
}

/*********************************************************************************************
 * Function:    bottom_bar_one_extract_model_info
 * Description: extract the model information.
 * Parameters:  The object reference
 * Return:      void
 ********************************************************************************************/
void bottom_bar_one_extract_model_info(gpointer data)
{
  MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (data);
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);

	GValue ColumnOneActive = { 0, };
	GValue ColumnOneInactive = { 0, };
	GValue ColumnOneTexture = { 0, };
	GValue ColumnTwoActive = { 0, };
	GValue ColumnTwoInactive = { 0, };
	GValue ColumnTwoTexture = { 0, };
	GValue ColumnThreeActive = { 0, };
	GValue ColumnThreeInactive = { 0, };
	GValue ColumnThreeTexture = { 0, };
	GValue ColumnFour = { 0, };

	if (G_IS_OBJECT(priv->pColumnModel) && thornbury_model_get_n_rows(priv->pColumnModel) > 0)
	{
		guint iCurrentrow = 0;
		for (; iCurrentrow < thornbury_model_get_n_rows(priv->pColumnModel);
				iCurrentrow++)
		{
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(
                                        priv->pColumnModel, iCurrentrow);
			BOTTOM_BAR_PRINT ("\n bottom_bar_one_extract_model_info loop \n");

			if (NULL != pIter)
			{
				BOTTOM_BAR_PRINT("\n iCurrentrow %d\n", iCurrentrow);

				thornbury_model_iter_get_value(pIter, COLUMN_ONE_ACTIVE, &ColumnOneActive);
				thornbury_model_iter_get_value(pIter, COLUMN_ONE_INACTIVE, &ColumnOneInactive);
				thornbury_model_iter_get_value(pIter, COLUMN_ONE_TEXTURE_NAME, &ColumnOneTexture);

				thornbury_model_iter_get_value(pIter, COLUMN_TWO_ACTIVE, &ColumnTwoActive);
				thornbury_model_iter_get_value(pIter, COLUMN_TWO_INACTIVE, &ColumnTwoInactive);
				thornbury_model_iter_get_value(pIter, COLUMN_TWO_TEXTURE_NAME, &ColumnTwoTexture);

				thornbury_model_iter_get_value(pIter, COLUMN_THREE_ACTIVE, &ColumnThreeActive);
				thornbury_model_iter_get_value(pIter, COLUMN_THREE_INACTIVE, &ColumnThreeInactive);
				thornbury_model_iter_get_value(pIter, COLUMN_THREE_TEXTURE_NAME, &ColumnThreeTexture);
				thornbury_model_iter_get_value(pIter, COLUMN_FOUR, &ColumnFour);
			}
		}

		priv->columnOneTextureName = (gchar *)g_value_get_string(&ColumnOneTexture);
		priv->columnTwoTextureName = (gchar *)g_value_get_string(&ColumnTwoTexture);
		priv->columnThreeTextureName = (gchar *)g_value_get_string(&ColumnThreeTexture);

		priv->columnOneActiveImagePath = (gchar *)g_value_get_string(&ColumnOneActive);
		priv->columnOneInactiveImagePath = (gchar *)g_value_get_string(&ColumnOneInactive);
		priv->columnTwoActiveImagePath = (gchar *)g_value_get_string(&ColumnTwoActive);
		priv->columnTwoInactiveImagePath = (gchar *)g_value_get_string(&ColumnTwoInactive);
		priv->columnThreeActiveImagePath = (gchar *)g_value_get_string(&ColumnThreeActive);
		priv->columnThreeInactiveImagePath = (gchar *)g_value_get_string(&ColumnThreeInactive);

		priv->columnFourData = (gchar *)g_value_get_string(&ColumnFour);
		clutter_actor_set_name(priv->columnOneGroup,priv->columnOneTextureName);
		clutter_actor_set_name(priv->columnTwoGroup,priv->columnTwoTextureName);
		clutter_actor_set_name(priv->columnThreeGroup,priv->columnThreeTextureName);

		v_bottom_bar_set_col_one_texture(bottomBar);
		v_bottom_bar_set_col_two_texture(bottomBar);
		v_bottom_bar_set_col_three_texture(bottomBar);
		v_bottom_bar_set_col_four_texture(bottomBar);
	}

}

/*********************************************************************************************
 * Function:    touch_event
 * Description: Callback for touch events.
 * Parameters:  1.Clutter actor.
 * 				2.Clutter Event.
 * 				3.User Data
 * Return:      gboolean
 ********************************************************************************************/
static gboolean touch_event(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
  if (event->type == CLUTTER_BUTTON_PRESS ||
      event->type == CLUTTER_TOUCH_BEGIN)
    {
      on_button_press (actor, event, userData);
      return TRUE;
    }
  else if (event->type == CLUTTER_BUTTON_RELEASE ||
           event->type == CLUTTER_TOUCH_END)
    {
      on_button_release (actor, event, userData);
      return TRUE;
    }

  return FALSE;
}




/*********************************************************************************************
 * Function:    on_button_press
 * Description: Callback for button press signal.
 * Parameters:  1.Clutter actor.
 * 				2.Clutter Event.
 * 				3.User Data
 * Return:      gboolean
 ********************************************************************************************/
static gboolean on_button_press(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
	MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (userData);
	gchar *name;
	name = (gchar *)clutter_actor_get_name(actor);
	g_signal_emit_by_name(bottomBar,"action-press",name);

	return TRUE;
}
/*********************************************************************************************
 * Function:    on_button_release
 * Description: Callback for button release signal.
 * Parameters:  1.Clutter actor.
 * 				2.Clutter Event.
 * 				3.User Data
 * Return:      gboolean
 ********************************************************************************************/
static gboolean on_button_release(ClutterActor *actor, ClutterEvent *event, gpointer userData)
{
  MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (userData);
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	gchar *name;
	name = (gchar *)clutter_actor_get_name(actor);
	g_signal_emit_by_name(bottomBar,"action-release",name);
	//Check if the column one  texture has been released.
	if(!(g_strcmp0(priv->columnOneTextureName,name)))
	{
		//According to the state of the column update the texture.
		if(priv->columnOneState)
		{
			thornbury_ui_texture_set_from_file(priv->columnOneTexture,priv->columnOneActiveImagePath, 0,0,FALSE,FALSE);
			priv->columnOneState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnOneTexture,priv->columnOneInactiveImagePath, 0,0,FALSE,FALSE);
			priv->columnOneState=TRUE;
		}
	}
	//Check if the column two  texture has been released.
	if(!(g_strcmp0(priv->columnTwoTextureName,name)))
	{
		//According to the state of the column update the texture.
		if(priv->columnTwoState)
		{
			thornbury_ui_texture_set_from_file(priv->columnTwoTexture,priv->columnTwoActiveImagePath, 0,0,FALSE,FALSE);
			priv->columnTwoState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnTwoTexture,priv->columnTwoInactiveImagePath, 0,0,FALSE,FALSE);
			priv->columnTwoState=TRUE;
		}
	}
	//Check if the column three  texture has been released.
	if(!(g_strcmp0(priv->columnThreeTextureName,name)))
	{
		//According to the state of the column update the texture.
		if(priv->columnThreeState)
		{
			thornbury_ui_texture_set_from_file(priv->columnThreeTexture,priv->columnThreeActiveImagePath, 0,0,FALSE,FALSE);
			priv->columnThreeState=FALSE;
		}
		else
		{
			thornbury_ui_texture_set_from_file(priv->columnThreeTexture,priv->columnThreeInactiveImagePath, 0,0,FALSE,FALSE);
			priv->columnThreeState=TRUE;
		}
	}
	return TRUE;
}

static void
mildenhall_bottom_bar_class_init (MildenhallBottomBarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec = NULL;
	const char *pEnvString;
	pEnvString = g_getenv ("BOTTOM_BAR_DEBUG");
	if (pEnvString != NULL)
	{
		bottom_bar_debug_flags = g_parse_debug_string (pEnvString, bottom_bar_debug_keys, G_N_ELEMENTS (bottom_bar_debug_keys));

	}
	object_class->get_property = bottom_bar_get_property;
	object_class->set_property = bottom_bar_set_property;

	/* Install the bottom bar height property */
	pspec = g_param_spec_float("bottom-bar-height", "BOTTOM-BAR-HEIGHT", "The bottom bar height", 0.0,1000.0,
			0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_HEIGHT, pspec);

	/* Install the bottom bar width property */
	pspec = g_param_spec_float("bottom-bar-width", "BOTTOM-BAR-WIDTH", "The bottom bar width", 0.0,1000.0,
			0.0, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_WIDTH, pspec);


	/* Install the column-one property */
	pspec = g_param_spec_boolean("column-one", "COLUMN-ONE", "State to indicate that column one texture is active or not",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_COLUMN_ONE, pspec);

	/* Install the column-two property */
	pspec = g_param_spec_boolean("column-two", "COLUMN-TWO", "State to indicate that column two texture is active or not",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_COLUMN_TWO, pspec);

	/* Install the column-three property */
	pspec = g_param_spec_boolean("column-three", "COLUMN-THREE", "State to indicate that column three texture is active or not",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_COLUMN_THREE, pspec);
	/* Install the column-four property */
	pspec = g_param_spec_boolean("column-four", "COLUMN-FOUR", "State to indicate that column four texture is active or not",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_COLUMN_FOUR, pspec);

	/* Install the column-one property */
	pspec = g_param_spec_boolean("button-one-state", "BUTTON-ONE-STATE", "State of the button one if user wants to set it explicitly",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_BUTTON_ONE_STATE, pspec);

	/* Install the column-one property */
	pspec = g_param_spec_boolean("button-two-state", "BUTTON-TWO-STATE", "State of the button two if user wants to set it explicitly",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_BUTTON_TWO_STATE, pspec);

	/* Install the column-one property */
	pspec = g_param_spec_boolean("button-three-state", "BUTTON-THREE-STATE", "State of the button three if user wants to set it explicitly",
			FALSE, G_PARAM_READWRITE);
	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_BUTTON_THREE_STATE, pspec);

	/* Install the bottom-bar-model property */
	pspec = g_param_spec_object("model", "MODEL","Model information of bottom-bar widget", G_TYPE_OBJECT,G_PARAM_READWRITE);

	g_object_class_install_property(object_class, PROP_MH_BOTTOM_BAR_MODEL, pspec);

	/* Create the action-press signal and add it to the class */
	mh_bottom_bar_signals[SIG_MH_BOTTOM_BAR_ACTION_PRESS] = 
	  g_signal_new("action-press",
                       G_TYPE_FROM_CLASS (object_class),
                       G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                       G_STRUCT_OFFSET (MildenhallBottomBarClass, action_press),
                       NULL, NULL, NULL,
                       G_TYPE_NONE, 1,
                       G_TYPE_STRING);

	/* Create the action-release signal and add it to the class */
	mh_bottom_bar_signals[SIG_MH_BOTTOM_BAR_ACTION_RELEASE] = 
          g_signal_new("action-release",
                       G_TYPE_FROM_CLASS (object_class),
                       G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
                       G_STRUCT_OFFSET (MildenhallBottomBarClass, action_release),
                       NULL, NULL, NULL,
                       G_TYPE_NONE, 1,
                       G_TYPE_STRING);
}
static void v_bottom_bar_row_added_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar)
{
}

static void v_bottom_bar_row_changed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar)
{
	MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
	GValue ColumnFour = { 0, };

	if (G_IS_OBJECT(priv->pColumnModel) && thornbury_model_get_n_rows(priv->pColumnModel) > 0)
		thornbury_model_iter_get_value(pIter, COLUMN_FOUR, &ColumnFour);



	clutter_text_set_text(CLUTTER_TEXT(priv->columnFourText),g_value_get_string(&ColumnFour));

}
static void v_bottom_bar_row_removed_cb (ThornburyModel *pModel, ThornburyModelIter *pIter, MildenhallBottomBar *bottomBar)
{
	BOTTOM_BAR_PRINT("Row removed");
}


static void
mildenhall_bottom_bar_init (MildenhallBottomBar *self)
{
  g_autofree gchar *json_file = NULL;
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (self);

  priv->bottomBarWidth = 200.0;
  priv->bottomBarHeight = 100.0;
  priv->bottomBarColumnOne = TRUE;
  priv->bottomBarColumnTwo = TRUE;
  priv->bottomBarColumnThree = TRUE;
  priv->bottomBarColumnFour = TRUE;
  priv->columnOneState = FALSE;
  priv->columnTwoState = FALSE;
  priv->columnThreeState = FALSE;

  json_file = _mildenhall_get_resource_path ("mh_bottom_bar_type_one_style.json");
  priv->pStyleHash = thornbury_style_set (json_file);

        if(NULL != priv->pStyleHash)
	{
		GHashTableIter iter;
		gpointer key, value;
		g_hash_table_iter_init (&iter, priv->pStyleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				priv->pLocalHash = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
				g_hash_table_foreach(pHash, v_bottom_bar_parse_style, self);
			}
		}
	}
	thornbury_style_free (priv->pStyleHash);
	create_bottom_bar(self);

}


/********************************************************
 * Function : v_bottom_bar_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void v_bottom_bar_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (pUserData);

	// store the style images in a hash for setting syle based on set properties

	if(g_strcmp0(pStyleKey, "column-one-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_ONE_X, pValue);
	else if(g_strcmp0(pStyleKey, "column-one-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_ONE_Y, pValue);
	else if(g_strcmp0(pStyleKey, "column-one-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_ONE_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "column-one-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_ONE_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "column-two-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_TWO_X, pValue);
	else if(g_strcmp0(pStyleKey, "column-two-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_TWO_Y, pValue);
	else if(g_strcmp0(pStyleKey, "column-two-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_TWO_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "column-two-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_TWO_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "column-three-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_THREE_X, pValue);
	else if(g_strcmp0(pStyleKey, "column-three-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_THREE_Y, pValue);
	else if(g_strcmp0(pStyleKey, "column-three-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_THREE_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "column-three-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_THREE_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "column-four-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_FOUR_X, pValue);
	else if(g_strcmp0(pStyleKey, "column-four-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_FOUR_Y, pValue);
	else if(g_strcmp0(pStyleKey, "column-four-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_FOUR_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "column-four-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_FOUR_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "column-four-font") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, COLUMN_FOUR_FONT, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-one-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_ONE_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-one-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_ONE_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-one-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_ONE_X, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-one-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_ONE_Y, pValue);

	else if(g_strcmp0(pStyleKey, "between-line-two-width") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_TWO_WIDTH, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-two-height") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_TWO_HEIGHT, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-two-x") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_TWO_X, pValue);
	else if(g_strcmp0(pStyleKey, "between-line-two-y") == 0)
		v_bottom_bar_add_style_to_hash(bottomBar, BETWEEN_LINE_TWO_Y, pValue);

	if(pStyleKey)
		g_free(pStyleKey);
}

/********************************************************
 * Function : v_bottom_bar_add_style_to_hash
 * Description: maintain style hash
 * Parameter :  *bottomBar, *pKey, pValue
 * Return value: void
 ********************************************************/
static void
v_bottom_bar_add_style_to_hash (MildenhallBottomBar *bottomBar,
                                const char *pKey,
                                gpointer pValue)
{
  MildenhallBottomBarPrivate *priv = mildenhall_bottom_bar_get_instance_private (bottomBar);
  gchar *value = NULL;

  g_return_if_fail (pKey);
  g_return_if_fail (pValue);

  if (G_VALUE_HOLDS_INT64(pValue))
    {
      value = g_strdup_printf("%" G_GINT64_FORMAT, g_value_get_int64 (pValue));
    }
  else
    {
      value = g_value_dup_string (pValue);
    }

  g_hash_table_insert (priv->pLocalHash,
                       g_strdup (pKey), value);
}
