/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-cabinet-roller.c */
/***********************************************************************************
*       @Filename       :       mildenhall-cabinet-roller.c
*       @Module         :       MildenhallCabinetRoller
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       23/04/2013
*----------------------------------------------------------------------------------
*       @Description    :      Cabinet roller is a widget
*				to browse folders
*
************************************************************************************
Description of FIXES:
-----------------------------------------------------------------------------------
        Description                             Date                    Name
        ----------                              ----                    ----
************************************************************************************/

#include "mildenhall_cabinet_item.h"

#include <math.h>
#include <mx/mx.h>
#include <thornbury/thornbury.h>

#include "liblightwood-fixedroller.h"
#include "liblightwood-roller.h"
#include "liblightwood-varroller.h"

#include "sample-item.h"

/***********************************************************************************
        @Prototypes of local functions
************************************************************************************/

static ThornburyModel *_create_footer_model(gchar *pRollNum,gchar* pLabel,gfloat fWidth);
static ClutterActor *_create_roller(MildenhallCabinetRoller *self,gfloat fRollWidth,ThornburyModel *pFooterModel);
static ClutterActor *_get_roller(ClutterActor *pRollerGroup);

static ClutterActor *_DrawVerticalSeperator(ClutterColor *lineColor,ClutterColor *depthLineColor,gfloat width,gfloat height);
static void _cabinet_roller_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer data);
static void _cabinet_footer_item_activated_cb (MildenhallRollerContainer *pRollerCont,  gpointer pUserData);
static void _cabinet_roller_locking_finished_cb (MildenhallRollerContainer *roller, gpointer data);
/************************************************************************************
        @Macro Definitions
************************************************************************************/
#define MILDENHALL_CABINETROLL_DEBUG(...)   //g_print( __VA_ARGS__)//g_printerr(__VA_ARGS__)/*g_print( __VA_ARGS__)*/
#define MILDENHALL_CABINET_FULL_ROLLER_WIDTH 651.0
#define MILDENHALL_CABINET_ROLLER_WIDTH 411.0
#define MILDENHALL_CABINET_FULL_ROLLER_HEIGHT 480.0
#define MILDENHALL_CABINET_LINE_WIDTH 1.0
#define ROOT_ROLLER_NAME "01"
#define MILDENHALL_CABINET_BORDER_LINE_NAME "borderline"
#define MILDENHALL_CABINET_TRANS_RECT_NAME "trans Actor"


/***********************************************************************************
        @Global variable
************************************************************************************/


/***********************************************************************************
        @Local type Definitions
************************************************************************************/
enum
{
	FOOTER_COLUMN_WIDTH,
	FOOTER_COLUMN_WIDTH_VALUE,
	FOOTER_COLUMN_NAME,
	FOOTER_COLUMN_LABEL,
	FOOTER_COLUMN_CONTENT,
	FOOTER_COLUMN_MID_LABEL,
	FOOTER_COLUMN_FOOTER,
	FOOTER_COLUMN_LINE,
	FOOTER_COLUMN_ARROW_SHOW,
	FOOTER_COLUMN_ARROW,
	FOOTER_COLUMN_NONE,
};


G_DEFINE_TYPE (MildenhallCabinetRoller, mildenhall_cabinet_roller, CLUTTER_TYPE_ACTOR)

#define CABINET_ROLLER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_CABINET_ROLLER, MildenhallCabinetRollerPrivate))

struct _MildenhallCabinetRollerPrivate
{
	gboolean bShowRootRoller;
	gboolean bIsRecursiveAnimate;
	gchar *pRollName;
	gchar *pShade;
	gchar *pLeftArrow;
	gchar *pRightArrow;
	gchar *pUpArrow;
	gchar *pDownArrow;
	gchar *pLeftBlockArrow;
	gchar *pRightBlockArrow;
	gchar *pUpBlockArrow;
	gchar *pDownBlockArrow;
	gchar *pArrowFont;
	GType itemType;
	ThornburyModel *pFooterModel;
	ThornburyModel *pModel;
	ClutterActor *pActiveRoller;
	ClutterActor *pRootRoller;
	ClutterActor *pVerticalLine;
	ClutterActor *pRollerGroup;
	ClutterActor *pHbox;
	ClutterActor *pBorderLineLast;
	GList *pRollerList;
	GList *pLastActivatedRowList;
};
typedef struct _animationData
{
	ClutterActor *roller;
	MildenhallCabinetRoller *cabinet;
	gboolean bRemoving;
}_animationData;
enum
{
	PROP_0,
	PROP_ITEM_TYPE,
	PROP_MODEL,
	PROP_SHOW_ROOT_ROLL,
	PROP_ROLL_NAME,
	PROP_ACTIVE_ROLL,
	PROP_CREATE_ROLLER,
	PROP_SET_ROLLER,
	PROP_SHOW_PREV_ROLL,

	PROP_LAST
};

enum{
	LOCKING_FINISHED,
	PREV_ITEM_ACTIVATED,
	SAME_PREV_ITEM_ACTIVATED,
	ACTIVE_ITEM_ACTIVATED,
	WIPE_STARTED,
	WIPE_COMPLETED,
	LAST_SIGNAL
};
static guint signals[LAST_SIGNAL] = { 0, };

/*********************************************************************************************
 * Function: mildenhall_cabinet_roller_get_property
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
mildenhall_cabinet_roller_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
	switch (property_id)
	{
		case PROP_ITEM_TYPE:
			break;
#if 0
		case PROP_ROLL_NAME:
			break;
#endif
		case PROP_ACTIVE_ROLL:
			{
				MildenhallCabinetRoller * cabinet = (MildenhallCabinetRoller *)object;
				g_value_set_object (value, cabinet->priv->pActiveRoller);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}
/**
 * v_CABINETROLL_set_item_type : Public method
 * @container           	: Object reference
 * @itemType            	: Value which need to be set for roller item type
 *
 *	TBD
 */
void
v_CABINETROLL_set_item_type(MildenhallCabinetRoller *cabinet,
				GType itemType)
{


}
/*********************************************************************************************
 * Function: _get_roller_num
 * Description: calculates roller number
 * Parameters:
 * Return:
 ********************************************************************************************/
static gchar *
_get_roller_num(MildenhallCabinetRoller *cabinet)
{

	MildenhallCabinetRollerPrivate *priv = cabinet->priv;
	gint nRoller = g_list_length(priv->pRollerList);
	gchar *pRollNum = g_strdup_printf("%d",nRoller+1);
	if( 1 == strlen(pRollNum) )
	{
		pRollNum = g_strconcat("0",pRollNum,NULL);
		MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s:rollerNUM = %s\n",__FILE__, __LINE__,__FUNCTION__,pRollNum);
	}
	return pRollNum;
}
/*********************************************************************************************
 * Function: _animation_started_cb
 * Description: callback called when timeline attached to roller aniomation completed
 * Parameters:
 * Return:
 ********************************************************************************************/
static void _animation_started_cb (ClutterAnimation *animation, gpointer userData)
{
	_animationData *animData = (_animationData *) userData;
	MildenhallCabinetRoller *cabinet = (MildenhallCabinetRoller *)animData->cabinet;
	MildenhallCabinetRollerPrivate *priv = cabinet->priv;
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);

        g_signal_emit (G_OBJECT (cabinet), signals[WIPE_STARTED], 0,animData->roller);
	if(priv->pRollerList)
	{
		GList *iter;
		for (iter = priv->pRollerList;iter; iter = iter->next)
		{
			GList *children = clutter_actor_get_children(iter->data);
			GList *child_iter;
			for (child_iter = children; child_iter; child_iter = child_iter->next)
			{
				ClutterActor *roller = (ClutterActor *) child_iter->data;

				if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
				{

					g_signal_handlers_disconnect_by_func (roller,
							_cabinet_roller_item_activated_cb,
							cabinet);
					g_signal_handlers_disconnect_by_func (roller,
							_cabinet_footer_item_activated_cb,
							cabinet);
					g_signal_handlers_disconnect_by_func (roller,
							_cabinet_roller_locking_finished_cb,
							cabinet);
				}
			}
		}
	}
        MILDENHALL_CABINETROLL_DEBUG("\n animation_started_cb \n ");
}

/*********************************************************************************************
 * Function: _animation_completed_cb
 * Description: callback called when timeline attached to roller completed.
 * Parameters:
 * Return:
 ********************************************************************************************/
static void _animation_completed_cb (ClutterAnimation *animation,gpointer userData)
{
	_animationData *animData = (_animationData *) userData;
	MildenhallCabinetRoller *cabinet = (MildenhallCabinetRoller *)animData->cabinet;
	MildenhallCabinetRollerPrivate *priv = cabinet->priv;
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
	if(animData->bRemoving)
	{
		ClutterActor *parent = clutter_actor_get_parent(animData->roller);
		GList *iter;
		gint i;
		for (iter = priv->pRollerList,i = 0;iter; i++,iter = iter->next)
		{
			if(parent == iter->data)
				break;
		}
			while (g_list_length (priv->pRollerList) > (unsigned int) (i + 1))
			{
				ClutterActor *activeRoller = g_list_nth_data(priv->pRollerList,g_list_length(priv->pRollerList) - 1);
				ClutterActor *prevRoller = g_list_nth_data(priv->pRollerList,g_list_length(priv->pRollerList) - 2);
                                ClutterActor *roller = NULL;
				clutter_actor_remove_child(CLUTTER_ACTOR(priv->pHbox),activeRoller);
				priv->pRollerList = g_list_delete_link(priv->pRollerList,g_list_last(priv->pRollerList));
				priv->pLastActivatedRowList = g_list_delete_link(priv->pLastActivatedRowList,g_list_last(priv->pLastActivatedRowList));
                                roller = _get_roller (prevRoller);
				if(roller)
					priv->pActiveRoller = roller;
				else
					g_warning("%s:%s:%d:Roller object is NULL\n",__FILE__,__FUNCTION__,__LINE__);
			}

	}
	if(priv->pRollerList)
        {
                GList *iter;
                for (iter = priv->pRollerList;iter; iter = iter->next)
                {
			ClutterActor *rollerCont = iter->data;
                        GList *children = clutter_actor_get_children(iter->data);
                        GList *child_iter;
                        for (child_iter = children; child_iter; child_iter = child_iter->next)
                        {
                                ClutterActor *roller = (ClutterActor *) child_iter->data;

                                if (MILDENHALL_IS_ROLLER_CONTAINER (roller) && (!(animData->bRemoving) || (rollerCont == priv->pRootRoller)))
				{
					g_signal_connect (G_OBJECT (roller), "roller-item-activated", G_CALLBACK (_cabinet_roller_item_activated_cb), cabinet);
					g_signal_connect (G_OBJECT (roller), "roller-footer-clicked", G_CALLBACK (_cabinet_footer_item_activated_cb), cabinet);
					g_signal_connect (G_OBJECT (roller), "roller-locking-finished", G_CALLBACK (_cabinet_roller_locking_finished_cb), cabinet);
				}
                        }
                }
        }
        mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (animData->roller), FALSE);
        g_signal_emit (G_OBJECT (cabinet), signals[WIPE_COMPLETED], 0,animData->roller);

}
static void
_animate_border_line(gchar *pName,gfloat fWidth,ClutterActor *actor)
{
	if(fWidth > 0.0)
	{
		MILDENHALL_CABINETROLL_DEBUG("******%s\n",pName);
		clutter_actor_save_easing_state (actor);
		clutter_actor_set_easing_duration (actor,1000);//clutter_timeline_get_duration(timeline));
		clutter_actor_set_easing_mode (actor, CLUTTER_EASE_IN_QUART);
		clutter_actor_set_position (actor,fWidth - 5,0);
		clutter_actor_restore_easing_state (actor);
	}
}

static void
_animate_roller_container(MildenhallCabinetRoller *cabinet,ClutterActor *roller,gfloat fWidth,gboolean remove)
{

	if(fWidth > 0.0)
	{
		gint focused;
		_animationData *userData;
		ClutterTimeline *timeline = NULL;
		MILDENHALL_CABINETROLL_DEBUG ("CABINET:roller animation %f \n", fWidth);
		userData = g_new0(_animationData,1);
		if(userData == NULL)
		{
			g_warning("%s:%s:%d Memeory could not be allocated !\n",__FILE__,__FUNCTION__,__LINE__);
			return;
		}
		userData->roller = roller;
		userData->cabinet = cabinet;
		userData->bRemoving = remove;
		timeline = clutter_timeline_new (1000);
		g_object_get(roller,"focused-row",&focused,NULL);
		g_signal_connect (G_OBJECT (timeline), "started", G_CALLBACK (_animation_started_cb), userData);
		g_signal_connect (G_OBJECT (timeline), "completed", G_CALLBACK (_animation_completed_cb), userData);

		mildenhall_roller_container_set_show_animation (MILDENHALL_ROLLER_CONTAINER (roller), TRUE);
		mildenhall_roller_container_resize (MILDENHALL_ROLLER_CONTAINER(roller),
				fWidth - 5, /* roller width */
				fWidth - 5, /* item width */
				-1, /* item height */
				focused,
				timeline, TRUE);
		clutter_actor_save_easing_state (roller);
		clutter_actor_set_easing_duration (roller,1000);//clutter_timeline_get_duration(timeline));
		clutter_actor_set_easing_mode (roller, CLUTTER_EASE_IN_QUART);
		g_object_set(roller,"width",fWidth - 5,NULL);
		clutter_actor_restore_easing_state (roller);
	}
}
static void
_animate_prev_roller(ClutterActor *prevRoller,gfloat fWidth)
{
	clutter_actor_save_easing_state (prevRoller);
	clutter_actor_set_easing_duration (prevRoller,1000);//clutter_timeline_get_duration(timeline));
	clutter_actor_set_easing_mode (prevRoller, CLUTTER_EASE_IN_QUART);
	g_object_set(prevRoller,"width",fWidth,NULL);
	clutter_actor_restore_easing_state (prevRoller);
}
/*********************************************************************************************
 * Function: _animate_roller
 * Description: Performs Roller animation
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
_animate_roller(MildenhallCabinetRoller *cabinet,ClutterActor *prevRoller,gfloat fWidth,gboolean remove)
{
	GList *children = clutter_actor_get_children(prevRoller);
	GList *iter;
	gint i;
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);

	for (iter = children, i = 0; iter; iter = iter->next, i++)
	{
		ClutterActor *roller = (ClutterActor *) iter->data;
		gchar *pName = g_strdup(clutter_actor_get_name(roller));
		if(pName)
		{
			if (g_strcmp0(pName,MILDENHALL_CABINET_BORDER_LINE_NAME) == 0)
			{
				_animate_border_line(pName,fWidth,roller);
			}
		}

		if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
		{
			_animate_roller_container(cabinet,roller,fWidth,remove);
		}
	}
	_animate_prev_roller(prevRoller,fWidth);
}
/**
 * v_CABINETROLL_set_roller : Public method
 * @cabinet           	: Object reference
 * @pModel            	:
 *
 * Sets the model to active roller only.
 */
static void
v_CABINETROLL_set_roller(MildenhallCabinetRoller *cabinet,
                                ThornburyModel *pModel)
{
  MildenhallCabinetRollerPrivate *priv = cabinet->priv;
  MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n",__FILE__, __LINE__,__FUNCTION__);
  if (pModel == NULL)
    return;
	MILDENHALL_CABINETROLL_DEBUG("%s\n",__FUNCTION__);
  if(priv->pActiveRoller)
    g_object_set(priv->pActiveRoller,"model",pModel,NULL);
	MILDENHALL_CABINETROLL_DEBUG("%s\n",__FUNCTION__);
}
/**
 * v_CABINETROLL_create_roller : Public method
 * @cabinet           	: Object reference
 * @pModel            	:
 *
 * Item type as GType
 */
void
v_CABINETROLL_create_roller(MildenhallCabinetRoller *cabinet,
				ThornburyModel *pModel)
{
        MildenhallCabinetRollerPrivate *priv = cabinet->priv;
        ThornburyModel *model = NULL;
        gint nRoller = 0;
        ClutterActor *prevRoller = NULL;
        ClutterActor *pprevRoller = NULL;
        /* whenever new model has to set create new roller */
        gchar *pRollerNum = NULL;
        ThornburyModel *pFooterModel = NULL;
        ClutterActor *roller = NULL;

	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);
	if (pModel == NULL)
		return;

	nRoller = g_list_length (priv->pRollerList);
	prevRoller = g_list_nth_data (priv->pRollerList, nRoller-1);
	pprevRoller = g_list_nth_data (priv->pRollerList, nRoller-2);

	/* if root roller has no model then set it first */
	if(NULL != priv->pRootRoller)
	{
		g_object_get(G_OBJECT(priv->pActiveRoller),"model",&model,NULL);

		if(NULL == model)
		{
			g_object_set(G_OBJECT(priv->pActiveRoller),"model",pModel,NULL);
			mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "left-overlay", 0);
			mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "left-icon", 1);
			mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "label", 2);
			mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "mime", 3);
			return;
		}
	}
	/* whenever new model has to set create new roller */
	pRollerNum = _get_roller_num (cabinet);

	//ClutterColor line_color = {0x00, 0x00, 0x00, 0x66};
	pFooterModel = _create_footer_model (pRollerNum, NULL, MILDENHALL_CABINET_ROLLER_WIDTH);
	roller = _create_roller (cabinet, MILDENHALL_CABINET_ROLLER_WIDTH, pFooterModel);
	clutter_actor_set_x_expand(roller,TRUE);
	clutter_actor_set_y_align(roller,TRUE);
	clutter_actor_add_child(CLUTTER_ACTOR(priv->pHbox),roller);

	if(prevRoller)
	{
		_animate_roller(cabinet,prevRoller,245.0,FALSE);

		if(pprevRoller)
		{
			_animate_roller(cabinet,pprevRoller,0.0,FALSE);
			//clutter_actor_remove_child(CLUTTER_ACTOR(priv->pHbox),transRect);
		}
	}

	/* If active roller is NULL return */
	if(NULL == priv->pActiveRoller)
	{
		MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s:active roller is NULL! \n",__FILE__, __LINE__,__FUNCTION__);
		return;
	}

	/* set the model to active roller */
	g_object_set(G_OBJECT(priv->pActiveRoller),"model",pModel,NULL);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "left-overlay", 0);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "left-icon", 1);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "label", 2);
	mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (priv->pActiveRoller), "mime", 3);
}
/*********************************************************************************************
 * Function: _get_roller
 * Description: gets roller object from group.
 * Parameters:
 * Return:
 ********************************************************************************************/
static ClutterActor *
_get_roller(ClutterActor *pRollerGroup)
{
	GList *children = clutter_actor_get_children(pRollerGroup);
	GList *iter;
	gint i;

        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n", __FILE__, __LINE__, __FUNCTION__);

	for (iter = children, i = 0; iter; iter = iter->next, i++)
	{
		ClutterActor *roller = (ClutterActor *) iter->data;

		if (MILDENHALL_IS_ROLLER_CONTAINER (roller))
		{
			return roller;
		}
	}
	return NULL;
}

/**
 * v_CABINETROLL_show_root_roller : Public method
 * @container           	: Object reference
 * @bShow            		: boolean value
 *
 * Item type as GType
 */
void
v_CABINETROLL_show_root_roller (MildenhallCabinetRoller *cabinet,gboolean bShow)
{
	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n",__FILE__, __LINE__,__FUNCTION__);
	if(bShow)
	{
		MildenhallCabinetRollerPrivate *priv = cabinet->priv;
		if(priv->pRootRoller)
		{
			if(g_list_length(priv->pRollerList) > 1)
			{
				if(priv->pRollerList)
				{
					GList *iter;
					for (iter = g_list_last(priv->pRollerList);iter; iter = iter->prev)
					{
						_animate_roller(cabinet,CLUTTER_ACTOR(iter->data),MILDENHALL_CABINET_FULL_ROLLER_WIDTH,TRUE);

					}
				}
			}
		}
	}
}
/**
 * v_CABINETROLL_show_prev_roller : Public method
 * @container           	: Object reference
 * @bShow            		: boolean value
 *
 * Item type as GType
 */

void
v_CABINETROLL_show_prev_roller (MildenhallCabinetRoller *cabinet,gboolean bShow)
{
	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n",__FILE__, __LINE__,__FUNCTION__);
	if(bShow)
	{
		MildenhallCabinetRollerPrivate *priv = cabinet->priv;
		gint nRoller = g_list_length(priv->pRollerList);
		ClutterActor *prevRoller = (ClutterActor *)g_list_nth_data(priv->pRollerList,nRoller - 2);
		ClutterActor *pprevRoller = (ClutterActor *)g_list_nth_data(priv->pRollerList,nRoller - 3);
		if(pprevRoller)
		{
			_animate_roller(cabinet,pprevRoller,245.0,FALSE);
		}
		if(prevRoller)
		{
			if(prevRoller == priv->pRootRoller)
				_animate_roller(cabinet,prevRoller,MILDENHALL_CABINET_FULL_ROLLER_WIDTH,TRUE);
			else
				_animate_roller(cabinet,prevRoller,MILDENHALL_CABINET_ROLLER_WIDTH,TRUE);

		}

	}
}
/*********************************************************************************************
 * Function: mildenhall_cabinet_roller_set_property
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
mildenhall_cabinet_roller_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  switch (property_id)
  {
    case PROP_ITEM_TYPE:
      v_CABINETROLL_set_item_type(MILDENHALL_CABINET_ROLLER(object), //g_value_get_string(value));
	g_value_get_gtype(value));
      break;
    case PROP_CREATE_ROLLER:
      v_CABINETROLL_create_roller(MILDENHALL_CABINET_ROLLER(object),
	  (ThornburyModel *)g_value_get_object(value));
      break;
    case PROP_SHOW_ROOT_ROLL:
      v_CABINETROLL_show_root_roller(MILDENHALL_CABINET_ROLLER(object),
	  g_value_get_boolean(value));
      break;
    case PROP_SET_ROLLER:
      v_CABINETROLL_set_roller(MILDENHALL_CABINET_ROLLER(object),
	  (ThornburyModel *)g_value_get_object(value));
      break;
    case PROP_SHOW_PREV_ROLL:
      v_CABINETROLL_show_prev_roller(MILDENHALL_CABINET_ROLLER(object),
	  g_value_get_boolean(value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

/*********************************************************************************************
 * Function: mildenhall_cabinet_roller_dispose
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
mildenhall_cabinet_roller_dispose (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_cabinet_roller_parent_class)->dispose (object);
}

/*********************************************************************************************
 * Function:	mildenhall_cabinet_roller_finalize
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
mildenhall_cabinet_roller_finalize (GObject *object)
{
  G_OBJECT_CLASS (mildenhall_cabinet_roller_parent_class)->finalize (object);
}
/*********************************************************************************************
 * Function: mildenhall_cabinet_roller_class_init
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/

static void
mildenhall_cabinet_roller_class_init (MildenhallCabinetRollerClass *klass)
{
	GParamSpec *pspec;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (MildenhallCabinetRollerPrivate));

  object_class->get_property = mildenhall_cabinet_roller_get_property;
  object_class->set_property = mildenhall_cabinet_roller_set_property;
  object_class->dispose = mildenhall_cabinet_roller_dispose;
  object_class->finalize = mildenhall_cabinet_roller_finalize;

#if 0
	pspec = g_param_spec_gtype ("item-type",
                        "ItemType",
                        "Item Type for the roller item factory",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_ITEM_TYPE, pspec);
#endif

	 pspec = g_param_spec_object ("create-roller",
                        "create-roller",
                        "Model to use to construct the items",
                        G_TYPE_OBJECT,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (object_class, PROP_CREATE_ROLLER, pspec);

	 pspec = g_param_spec_object ("set-roller",
                        "set-roller",
                        "Model to use to construct the items",
                        G_TYPE_OBJECT,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (object_class, PROP_SET_ROLLER, pspec);

	pspec = g_param_spec_boolean ("show-root-roller",
                        "show-root-roller",
                        "show root roller directly",
                        FALSE,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (object_class, PROP_SHOW_ROOT_ROLL, pspec);

	pspec = g_param_spec_boolean ("show-prev-roller",
                        "show-prev-eroller",
                        "show prev roller directly",
                        FALSE,
                        G_PARAM_WRITABLE);
        g_object_class_install_property (object_class, PROP_SHOW_PREV_ROLL, pspec);

#if 0
	pspec = g_param_spec_string ("roller-name",
                        "roller-name",
                        "Nome of the Roller",
                        NULL,
                        G_PARAM_READABLE);

        g_object_class_install_property (object_class, PROP_ROLL_NAME, pspec);
#endif

	pspec = g_param_spec_object ("active-roller",
                        "active-roller",
                        "Active Roller Object",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READABLE);
        g_object_class_install_property (object_class, PROP_ACTIVE_ROLL, pspec);
#if 0
	pspec = g_param_spec_object ("root-roller",
                        "root-roller",
                        "Root Roller Object",
                        CLUTTER_TYPE_ACTOR,
                        G_PARAM_READWRITE);
        g_object_class_install_property (object_class, PROP_ROOT_ROLL, pspec);
#endif


	/**
         * MildenhallCabinetRoller::cabinet-locking-finished
         *
         * Emitted when the locking animation finishes
         */
        signals[LOCKING_FINISHED] =
        g_signal_new ("cabinet-locking-finished",
                  G_TYPE_FROM_CLASS (klass),
                  G_SIGNAL_RUN_LAST,
                  G_STRUCT_OFFSET (MildenhallCabinetRollerClass, locking_finished),
                  NULL, NULL,
                  g_cclosure_marshal_VOID__VOID,
                  G_TYPE_NONE, 0);

        /**
         * MildenhallCabinetRoller::cabinet-prev-item-activated
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
        signals[PREV_ITEM_ACTIVATED] =
                g_signal_new ("cabinet-prev-item-activated",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallCabinetRollerClass, prev_item_activated),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_OBJECT);
        /**
         * MildenhallCabinetRoller::cabinet-prev-item-activated
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
        signals[SAME_PREV_ITEM_ACTIVATED] =
                g_signal_new ("cabinet-same-prev-item-activated",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallCabinetRollerClass, same_prev_item_activated),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_OBJECT);
	
	
        /**
         * MildenhallCabinetRoller::cabinet-active-item-activated
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
	signals[ACTIVE_ITEM_ACTIVATED] =
		g_signal_new ("cabinet-active-item-activated",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallCabinetRollerClass, active_item_activated),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_OBJECT);
        /**
         * MildenhallCabinetRoller::cabinet-wipe-started
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
	signals[WIPE_STARTED] =
		g_signal_new ("cabinet-wipe-started",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallCabinetRollerClass, wipe_started),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_OBJECT);
        /**
         * MildenhallCabinetRoller::cabinet-wipe-completed
         *
         * Emitted when an item gets activated, only if the item doesn't block the
         * release event.
         */
	signals[WIPE_COMPLETED] =
		g_signal_new ("cabinet-wipe-completed",
                                G_TYPE_FROM_CLASS (klass),
                                G_SIGNAL_RUN_LAST,
                                G_STRUCT_OFFSET (MildenhallCabinetRollerClass, wipe_completed),
                                NULL, NULL,
                                g_cclosure_marshal_generic,
                                G_TYPE_NONE, 2, G_TYPE_UINT,G_TYPE_OBJECT);
}

/********************************************************
 * Function : _mildenhall_cabinet_roller_parse_style
 * Description: parse the style hash and maintain styles
 * Parameter :  pKey, pValue, pUserData
 * Return value: void
 ********************************************************/
static void
_mildenhall_cabinet_roller_parse_style(gpointer pKey, gpointer pValue, gpointer pUserData)
{
        MildenhallCabinetRoller *pCabinetRoller = pUserData;
        MildenhallCabinetRollerPrivate *priv = pCabinetRoller->priv;
        gchar *pStyleKey = NULL;

	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s\n",__FILE__, __LINE__,__FUNCTION__);

	g_return_if_fail ( MILDENHALL_IS_CABINET_ROLLER (pUserData ) );

	pStyleKey = g_strdup(pKey);

	if(g_strcmp0(pStyleKey, "shade") == 0)
	{
		gchar *path = (gchar*)g_value_get_string(pValue);
		if(NULL != path)
		{
			priv->pShade = g_strdup_printf( PKGTHEMEDIR"/%s",path);
			MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pShade);
		}
	}
#if 0
	if(g_strcmp0(pStyleKey, "upEmptyArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pUpArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:upEmptyArrow= %s\n", __FUNCTION__,priv->pUpArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "downEmptyArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pDownArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pDownArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "leftEmptyArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pLeftArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pLeftArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "rightEmptyArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pRightArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pRightArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "upBlockedArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pUpBlockArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pUpBlockArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "downBlockedArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pDownBlockArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pDownBlockArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "leftBlockedArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pLeftBlockArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pLeftBlockArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "rightBlockedArrow") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pRightBlockArrow = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pRightBlockArrow);
                }
        }
	if(g_strcmp0(pStyleKey, "arrow-font") == 0)
        {
                gchar *path = (gchar*)g_value_get_string(pValue);
                if(NULL != path)
                {
                        priv->pArrowFont = g_strdup_printf( PKGTHEMEDIR"/%s",path);
                        MILDENHALL_CABINETROLL_DEBUG("%s:shade= %s\n", __FUNCTION__,priv->pArrowFont);
                }
        }
#endif


	if(pStyleKey != NULL)
	{
		g_free(pStyleKey);
		pStyleKey = NULL;
	}
}
/*********************************************************************************************
 * Function: _DrawVerticalSeperator
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static ClutterActor *
_DrawVerticalSeperator(ClutterColor *lineColor,ClutterColor *depthLineColor,gfloat width,gfloat height)
{
	/*Create a separator line group */
	ClutterActor *separatorLineGroup = clutter_actor_new ();
        /*The main separator line */
        ClutterActor *separatorLine = clutter_actor_new (); //clutter_rectangle_new_with_color (lineColor);
	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n",__FILE__,__LINE__,__FUNCTION__);

	clutter_actor_set_background_color(separatorLine,lineColor);
	MILDENHALL_CABINETROLL_DEBUG("%s\n",__FUNCTION__);
	clutter_actor_set_position (separatorLine, 0, 0);
	clutter_actor_set_size (separatorLine, width, height);
	clutter_actor_add_child(CLUTTER_ACTOR(separatorLineGroup),separatorLine);
	clutter_actor_show(separatorLine);

	/*Second line for the depth effect */
	if(NULL != depthLineColor)
	{
		ClutterActor *separatorLine2 = clutter_actor_new();
		clutter_actor_set_background_color(separatorLine2,depthLineColor);//clutter_rectangle_new_with_color (depthLineColor);
		clutter_actor_set_position (separatorLine2, 1, 0);
		clutter_actor_set_size (separatorLine2, width, height);
		clutter_actor_add_child(CLUTTER_ACTOR(separatorLineGroup),separatorLine2);
	}



	/*Return the separator line group that gets addded to the roller background */
	return separatorLineGroup;
}
static ThornburyModel *
_create_footer_model(gchar *pRollNum,gchar* pLabel,gfloat fWidth)
{
	ThornburyModel *pModel = (ThornburyModel*)thornbury_list_model_new(FOOTER_COLUMN_NONE,
						G_TYPE_STRING,NULL,
						G_TYPE_FLOAT,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_BOOLEAN,NULL,
						G_TYPE_STRING,NULL,
						G_TYPE_INT,NULL,
						-1);
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);

	thornbury_model_append (pModel,
			FOOTER_COLUMN_WIDTH,g_strdup("footer-width"),
			FOOTER_COLUMN_WIDTH_VALUE,fWidth,
			FOOTER_COLUMN_NAME,g_strdup("number-label"),
			FOOTER_COLUMN_LABEL, pRollNum?pRollNum:NULL,
			FOOTER_COLUMN_CONTENT,g_strdup("label"),
			FOOTER_COLUMN_MID_LABEL, pLabel?pLabel:NULL,
			FOOTER_COLUMN_FOOTER, g_strdup("footer"),
			FOOTER_COLUMN_LINE,TRUE,
			FOOTER_COLUMN_ARROW_SHOW,g_strdup("sort-arrow"),
			FOOTER_COLUMN_ARROW,ARROW_NONE,
			-1);
	return pModel;
}

/*********************************************************************************************
 * Function: _cabinet_roller_locking_finished_cb
 * Description: locking_finished_cb callback is called when the roller item gets the signal::locking_finished
 * Parameters: RollerContainer *, gpointer
 * Return:
 ********************************************************************************************/
static void
_cabinet_roller_locking_finished_cb (MildenhallRollerContainer *roller,
                     gpointer data)
{
        MildenhallCabinetRoller *cabinet = (MildenhallCabinetRoller *) data;
	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);

        MILDENHALL_CABINETROLL_DEBUG ("locking_finished\n");

        g_signal_emit (G_OBJECT (cabinet), signals[LOCKING_FINISHED], 0,roller);
        MILDENHALL_CABINETROLL_DEBUG ("locking_finished\n");
}
static void
_store_activated_row(MildenhallRollerContainer *roller,MildenhallCabinetRoller *cabinet,guint row)
{
	MildenhallCabinetRollerPrivate *priv = MILDENHALL_CABINET_ROLLER(cabinet)->priv;
	gint *pRow = NULL;
        if(priv->pActiveRoller == CLUTTER_ACTOR(roller))
        {
                pRow = g_list_nth_data(priv->pLastActivatedRowList,g_list_length(priv->pLastActivatedRowList) - 1);
                *pRow = row;
        }
        else
        {
                ClutterActor *parent = clutter_actor_get_parent(CLUTTER_ACTOR(roller));
                if(priv->pRollerList)
                {
                        GList *iter;
                        gint i = 0;
                        for (iter = priv->pRollerList;iter; i++,iter = iter->next)
                        {
                                if(parent == iter->data)
                                {
                                        gint *n_pRow = g_list_nth_data (priv->pLastActivatedRowList, i);
                                        *n_pRow = row;
                                        break;
                                }
                        }

                }
        }
}
/*********************************************************************************************
 * Function: _cabinet_roller_item_activated_cb
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
_cabinet_roller_item_activated_cb (MildenhallRollerContainer *roller, guint row, gpointer data)
{
	MildenhallCabinetRollerPrivate *priv = MILDENHALL_CABINET_ROLLER(data)->priv;
	ThornburyModel *model = NULL;
	g_object_get(roller,"model",&model,NULL);
	MILDENHALL_CABINETROLL_DEBUG("CABINET:%s\n",__FUNCTION__);

	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n",__FILE__,__LINE__,__FUNCTION__);
	if (priv->pActiveRoller == CLUTTER_ACTOR(roller))
		g_signal_emit (G_OBJECT(data), signals[ACTIVE_ITEM_ACTIVATED], 0, row,model);
	else
	{
		if(priv->pRollerList)
		{
			ClutterActor *rollerGroup = g_list_nth_data(priv->pRollerList,g_list_length(priv->pRollerList) - 2);
			ClutterActor *prevRoller = _get_roller(rollerGroup);
			if(prevRoller)
			{
				if(CLUTTER_ACTOR(roller) == prevRoller)
				{
					gint *lastActiveRow = g_list_nth_data(priv->pLastActivatedRowList,g_list_length(priv->pLastActivatedRowList) - 2);
					if ((unsigned int) *lastActiveRow == row)
					{
						MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s showing prev roller \n",__FILE__,__LINE__,__FUNCTION__);
						MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s showing prev roller \n",__FILE__,__LINE__,__FUNCTION__);
						g_signal_emit (G_OBJECT(data), signals[SAME_PREV_ITEM_ACTIVATED], 0, row,model);
						v_CABINETROLL_show_prev_roller(data,TRUE);
					}
					else
						g_signal_emit (G_OBJECT(data), signals[PREV_ITEM_ACTIVATED], 0, row,model);
				}
			}
		}
	}
	/* Store activated row of the corresponding roller for further reference */
	_store_activated_row(roller,data,row);
}

/*********************************************************************************************
 * Function: _mildenhall_cabinet_sort_fields_by_name
 * Description: Interface to sort the Thornbury Model Columns based on the string comparison
 * Parameters:
 * Return:
 ********************************************************************************************/

static gint
_mildenhall_cabinet_sort_fields_by_name( ThornburyModel *model,
                const GValue *pRow1,
                const GValue *pRow2,
                gpointer      user_data)
{
        MILDENHALL_CABINETROLL_DEBUG (" %s  %d \n", __FUNCTION__ , __LINE__);

        if(NULL != pRow1 && NULL != pRow2)
        {
                /*Take in the sort field related to that item and just sort in
                 * the order */
                const gchar* text1 = g_value_get_string(pRow1);
                const gchar* text2 = g_value_get_string(pRow2);
                gint bSortOrderIsAssending = GPOINTER_TO_INT (user_data);

                if((NULL == text1) || (NULL == text2))
                        return -1;
                return (bSortOrderIsAssending == 1 ? strcasecmp (text1, text2) : (-strcasecmp (text1, text2))) ;

        }
        return -1;

}
/*********************************************************************************************
 * Function: _cabinet_footer_item_activated_cb
 * Description: callback called on search view footer is clicked . Function will sort the Category Roller
 * Parameters: RollerContainer *, gpointer
 * Return:
 ********************************************************************************************/

static void
_cabinet_footer_item_activated_cb(MildenhallRollerContainer *pRollerCont,  gpointer pUserData)
{
	ThornburyModel *pFooterModel = NULL;
	ThornburyModel *pRollModel = NULL;
        ThornburyModelIter *iter = NULL;
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);
	g_object_get(G_OBJECT(pRollerCont),"model",&pRollModel,NULL);
	g_object_get(G_OBJECT(pRollerCont),"footer-model",&pFooterModel,NULL);
	iter = thornbury_model_get_iter_at_row (pFooterModel, 0);
	if(iter)
	{
		GValue value1 = {0,};
                gint iSorder = 0;

		thornbury_model_iter_get_value(iter,FOOTER_COLUMN_ARROW,&value1);
		iSorder = g_value_get_int (&value1);
		g_value_set_int(&value1,!iSorder);

		/* toggle the arrow */
		if(pFooterModel)
		{
			thornbury_model_insert_value(pFooterModel,0,FOOTER_COLUMN_ARROW,&value1);
		}
		if(pRollModel)
		{
			thornbury_model_set_sort (pRollModel, 2/*ROLLER_TEXT*/, (ThornburyModelSortFunc) _mildenhall_cabinet_sort_fields_by_name, GINT_TO_POINTER (!iSorder), NULL);
		}
		g_value_unset (&value1);
		g_object_unref(iter);
	}

}
/*********************************************************************************************
 * Function: _create_roller
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/

static ClutterActor *
_create_roller(MildenhallCabinetRoller *self,gfloat fRollwidth,ThornburyModel *pFooterModel)
{
        ClutterActor *pRoller = NULL;
        ClutterActor *verticalLine = NULL;
        ClutterActor *borderLine = NULL;
        gint *row = g_new0 (gint, 1);
	ClutterColor line_color = {0x00, 0x00, 0x00, 0x66};
	ClutterColor depthLineColor = {0xFF, 0xFF, 0xFF, 0x33};
	gfloat width;
	/* create roller group */
	ClutterActor *rollerGroup = clutter_actor_new ();
        MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n", __FILE__, __LINE__, __FUNCTION__);

	/* create transperant rectangle */
	if( rollerGroup != self->priv->pRootRoller)
	{
		ClutterActor *transRect = _DrawVerticalSeperator(&line_color,NULL,5.0,MILDENHALL_CABINET_FULL_ROLLER_HEIGHT);
		clutter_actor_add_child(CLUTTER_ACTOR(rollerGroup),transRect);
		clutter_actor_set_name(transRect,MILDENHALL_CABINET_TRANS_RECT_NAME);
		g_object_set(G_OBJECT(transRect),"opacity",0,NULL);
		width = fRollwidth - 5;
	}
	else
		width = fRollwidth;

	/* create roller */
	self->priv = CABINET_ROLLER_PRIVATE (self);
	pRoller = g_object_new (MILDENHALL_TYPE_ROLLER_CONTAINER,
			"width", width, //MILDENHALL_CABINET_FULL_ROLLER_WIDTH,
			"height", MILDENHALL_CABINET_FULL_ROLLER_HEIGHT,
			"roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER,
			"roll-over", TRUE,
			"item-type", MILDENHALL_TYPE_CABINET_ITEM,
			"footer-model", pFooterModel,
			NULL);
	g_object_set(G_OBJECT(pRoller),"animate-footer",CLUTTER_LINEAR,NULL);
	/* set recently created roller object as active roller object */
	self->priv->pActiveRoller = pRoller;

	/* name roller */
	clutter_actor_set_name(pRoller,g_strdup_printf("%d",g_list_length(self->priv->pRollerList) + 1));

	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s roller name = %s\n",__FILE__,__LINE__,__FUNCTION__,clutter_actor_get_name(pRoller));

	/* roller to container */
	clutter_actor_add_child(CLUTTER_ACTOR(rollerGroup),pRoller);
	if( rollerGroup != self->priv->pRootRoller)
		clutter_actor_set_x(pRoller,5.0);
	clutter_actor_set_position(pRoller,7.0,0.0);
	g_signal_connect (G_OBJECT (pRoller), "roller-item-activated", G_CALLBACK (_cabinet_roller_item_activated_cb), self);
	g_signal_connect (G_OBJECT (pRoller), "roller-footer-clicked", G_CALLBACK (_cabinet_footer_item_activated_cb), self);
	g_signal_connect (G_OBJECT (pRoller), "roller-locking-finished", G_CALLBACK (_cabinet_roller_locking_finished_cb), self);
	/* create vertical line */
	verticalLine = _DrawVerticalSeperator (&line_color, &depthLineColor, MILDENHALL_CABINET_LINE_WIDTH, MILDENHALL_CABINET_FULL_ROLLER_HEIGHT - 86);
	clutter_actor_set_position(verticalLine,89.0,0.0);
	clutter_actor_add_child(CLUTTER_ACTOR(rollerGroup),verticalLine);

	/* border line for the rollers*/
	borderLine = _DrawVerticalSeperator (&depthLineColor, NULL, MILDENHALL_CABINET_LINE_WIDTH, MILDENHALL_CABINET_FULL_ROLLER_HEIGHT); //- 86);
	clutter_actor_set_position (borderLine,7.0,0.0);
	clutter_actor_add_child (CLUTTER_ACTOR(rollerGroup),borderLine);

	self->priv->pBorderLineLast = _DrawVerticalSeperator (&depthLineColor,NULL,MILDENHALL_CABINET_LINE_WIDTH,MILDENHALL_CABINET_FULL_ROLLER_HEIGHT - 86);
	clutter_actor_set_position (self->priv->pBorderLineLast,fRollwidth - 5,0.0);
	clutter_actor_set_name (self->priv->pBorderLineLast,MILDENHALL_CABINET_BORDER_LINE_NAME);
	clutter_actor_add_child (CLUTTER_ACTOR(rollerGroup),self->priv->pBorderLineLast);
	/* add to GList of rollers */
	self->priv->pRollerList = g_list_append (self->priv->pRollerList,rollerGroup);
	*row = -1;
	self->priv->pLastActivatedRowList = g_list_append (self->priv->pLastActivatedRowList,row);

	return rollerGroup;
}

/*********************************************************************************************
 * Function: mildenhall_cabinet_roller_init
 * Description:
 * Parameters:
 * Return:
 ********************************************************************************************/
static void
mildenhall_cabinet_roller_init (MildenhallCabinetRoller *self)
{
        /* parse style *.json file */
        /* get the hash table for style properties */
        GHashTable *styleHash = thornbury_style_set (PKGDATADIR"/mildenhall_cabinet_roller_style.json");
        ThornburyModel *pFooterModel = NULL;
        /* creating Layout manager */
        ClutterLayoutManager* pLayout = clutter_box_layout_new ();
        ClutterActor *background = NULL;

	self->priv = CABINET_ROLLER_PRIVATE (self);
	self->priv->pActiveRoller = NULL;
			self->priv->bIsRecursiveAnimate = FALSE;
	MILDENHALL_CABINETROLL_DEBUG ("%s:%d:%s \n",__FILE__,__LINE__,__FUNCTION__);
	/* pares the hash for styles */
	if(NULL != styleHash)
	{
		GHashTableIter iter;
		gpointer key, value;

		g_hash_table_iter_init(&iter, styleHash);
		/* iter per layer */
		while (g_hash_table_iter_next (&iter, &key, &value))
		{
			GHashTable *pHash = value;
			if(NULL != pHash)
			{
				g_hash_table_foreach(pHash, _mildenhall_cabinet_roller_parse_style, self);
			}
		}
		/* free the style hash */
		thornbury_style_free(styleHash);
	}
	/* create footer model to create footer */
	pFooterModel = _create_footer_model (g_strdup (ROOT_ROLLER_NAME), NULL, MILDENHALL_CABINET_FULL_ROLLER_WIDTH);
	/* create Root roller */
	self->priv->pRootRoller = _create_roller(self,MILDENHALL_CABINET_FULL_ROLLER_WIDTH,pFooterModel);

	clutter_box_layout_set_orientation(CLUTTER_BOX_LAYOUT(pLayout),CLUTTER_ORIENTATION_HORIZONTAL);

	self->priv->pHbox = clutter_actor_new();
	clutter_actor_set_layout_manager(self->priv->pHbox,pLayout);	//clutter_box_new (pLayout);
	clutter_actor_add_child(CLUTTER_ACTOR(self),self->priv->pHbox);

	clutter_actor_add_child(CLUTTER_ACTOR(self->priv->pHbox),self->priv->pRootRoller);
	clutter_actor_set_x_expand(self->priv->pRootRoller,TRUE);
	clutter_actor_set_y_align(self->priv->pRootRoller,TRUE);
	/* Shadow background */
	background  = thornbury_ui_texture_create_new (self->priv->pShade, 0,0, FALSE, TRUE);
	clutter_actor_set_position(background, 7.0, 50.0);
	clutter_actor_add_child(CLUTTER_ACTOR(self), background);
	//clutter_actor_raise_top(background);
	clutter_actor_set_child_above_sibling(CLUTTER_ACTOR(self),background,(void *)0);
}
/**
 * mildenhall_cabinet_roller_new : Public method
 *
 *
 */

MildenhallCabinetRoller *
mildenhall_cabinet_roller_new (void)
{
	return g_object_new (MILDENHALL_TYPE_CABINET_ROLLER, NULL);
}
