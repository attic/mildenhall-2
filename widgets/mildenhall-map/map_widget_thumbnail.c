/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* map_widget_thumbnail.c
 *
 * Created on: Jan 10, 2013
 *
 *
 * map_widget_thumbnail.c */

#include "map_widget_internal.h"

#include <thornbury/thornbury.h>

MapWidget* MildenhallThumbnailMap = NULL;
GThread *ThumbnailWorkerThread = NULL;
gint ThumbnailWorkerFlag = 1;
GAsyncQueue* MildenhallThumbnailQueue = NULL;
ClutterActor *ThumbnailMapActor;
G_LOCK_DEFINE_STATIC(lock);
typedef struct _MildenhallThumbnailMQData MildenhallThumbnailMQData;
struct  _MildenhallThumbnailMQData
{

    gint width;
    gint height;
    gfloat latitude;
    gfloat longitude;
    gpointer call_bk;
};

/******************************************************************************
* FUNCTION		: map_widget_thumbnail_capature
* PARAMETER		: gpointer
* RETURN VALUE	: gboolean
* DESCRIPTION	: Responsible for capturing/generating the Thumbnail
*
*
*******************************************************************************/
gboolean map_widget_thumbnail_capature ( gpointer UserData)
{
#if 1 

	  ClutterActor *viewMap =  p_map_widget_get_map(MAP_WIDGET(MildenhallThumbnailMap));
	    MildenhallThumbnailMQData *em = UserData;
	    gfloat width=0,height=0;
	    GList *children = NULL;


	    clutter_actor_get_size(viewMap,&width,&height);

	    if( (width!=em->width) || (height != em->height) )
	    {
	        clutter_actor_set_size(viewMap,em->width,em->height);
	    }


	    ThumbnailMapActor = clutter_actor_new();
	    clutter_actor_set_size(CLUTTER_ACTOR(ThumbnailMapActor),em->width,em->height);
	    ClutterActor *MapLayer =  champlain_view_get_map_layer(CHAMPLAIN_VIEW(viewMap));

	    if(MapLayer == NULL)
	    {
	        g_warning("No child found \n");
	        clutter_actor_destroy(ThumbnailMapActor);
	        return FALSE;
	    }
	    else
	    {
	        children = clutter_actor_get_children (CLUTTER_ACTOR (MapLayer));


	    }

    gint loop =0;
    GList *child=NULL;

    if(children != NULL)
        child = g_list_first(children);
    ClutterActor* TempActor=NULL;
    for(loop = 0; loop<g_list_length(children); loop++)
    {
        MAP_WIDGET_DEBUG("CHILD ACTOR:%d X:%d Y:%d\n",loop,champlain_tile_get_x(CHAMPLAIN_TILE(child->data)),champlain_tile_get_y(CHAMPLAIN_TILE(child->data)));

        ClutterTexture* temp = CLUTTER_TEXTURE(champlain_tile_get_content(CHAMPLAIN_TILE(child->data)));


        if(!CLUTTER_IS_TEXTURE(temp))
        {
        	MAP_WIDGET_DEBUG("\n map_widget_thumbnail_capature   22222222222222222 \n");
            return TRUE;
        }

        CoglHandle texture_handle = clutter_texture_get_cogl_texture(CLUTTER_TEXTURE(temp));

        int data_len = cogl_texture_get_data(texture_handle, COGL_PIXEL_FORMAT_RGBA_8888,0, NULL);
        guchar* data = (guchar*) g_malloc(sizeof(guchar) * (data_len+5));
        cogl_texture_get_data(texture_handle, COGL_PIXEL_FORMAT_RGBA_8888,0, data);
        GdkPixbuf* thumbnail = gdk_pixbuf_new_from_data(data, GDK_COLORSPACE_RGB,TRUE, 8,
                              em->width,
                              em->height,
                               cogl_texture_get_width(texture_handle) * 4,
                               (GdkPixbufDestroyNotify) g_free, NULL);


        TempActor = clutter_texture_new ();
        GError *gerror = NULL;
        GdkPixbuf*pixbuf= gdk_pixbuf_copy(thumbnail);

        if (!clutter_texture_set_from_rgb_data (CLUTTER_TEXTURE (TempActor),
                                                gdk_pixbuf_get_pixels (pixbuf),
                                                gdk_pixbuf_get_has_alpha (pixbuf),
                                                gdk_pixbuf_get_width (pixbuf),
                                                gdk_pixbuf_get_height (pixbuf),
                                                gdk_pixbuf_get_rowstride (pixbuf),
                                                gdk_pixbuf_get_bits_per_sample (pixbuf) *																																    gdk_pixbuf_get_n_channels (pixbuf) / 8,
                                                0, &gerror))
        {

        }

        ClutterContent *pImage = clutter_image_new ();
        clutter_image_set_data(CLUTTER_IMAGE (pImage),
                                        gdk_pixbuf_get_pixels (pixbuf),
                                        gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
                                        gdk_pixbuf_get_width (pixbuf),
                                        gdk_pixbuf_get_height (pixbuf),
                                        gdk_pixbuf_get_rowstride (pixbuf),
                                        &gerror);
        			clutter_actor_set_content (TempActor, pImage);

        if(loop == 0)
        {
            if(CLUTTER_IS_ACTOR(TempActor))
                clutter_actor_add_child(CLUTTER_ACTOR(ThumbnailMapActor),CLUTTER_ACTOR(TempActor));

            break;

        }
        child = child->next;
    }
    g_signal_emit_by_name(MildenhallThumbnailMap,"thumbnail-complete",TempActor);
    G_UNLOCK(lock);
    g_free(em);
    return FALSE;
#else
    return FALSE;
#endif 
}





/******************************************************************************
* FUNCTION		: map_widget_thumbnail_update_map_postion
* PARAMETER		: gpointer
* RETURN VALUE	: gboolean
* DESCRIPTION	: Responsible for updating thumnail map to given location
*
*
*******************************************************************************/
gboolean map_widget_thumbnail_update_map_postion(gpointer data)
{
#if 1
	MildenhallThumbnailMQData *em = data;
	ClutterActor *viewMap =  p_map_widget_get_map(MAP_WIDGET(MildenhallThumbnailMap));
	champlain_view_center_on(CHAMPLAIN_VIEW(viewMap),em->latitude,em->longitude);
    g_timeout_add(1000,map_widget_thumbnail_capature,data);
   // g_free(em);
    return FALSE;
#else

    return FALSE;

#endif
}








/******************************************************************************
* FUNCTION		: map_widget_thumbnail_worker_thread
* PARAMETER		: gpointer
* RETURN VALUE	: gpointer
* DESCRIPTION	: Process the thumnail map requets one after another
*
*
*******************************************************************************/
gpointer map_widget_thumbnail_worker_thread(gpointer data)
{
#if 1
	MAP_WIDGET_DEBUG(" \n map_widget_thumbnail_worker_thread \n ");
    MildenhallThumbnailMQData *em;


    while(!ThumbnailWorkerFlag)
    {
        em = (MildenhallThumbnailMQData*)g_async_queue_pop(MildenhallThumbnailQueue);
        G_LOCK(lock);
        g_timeout_add(2, map_widget_thumbnail_update_map_postion, em);
    }
    ThumbnailWorkerThread = NULL;
    g_thread_exit(NULL);
    return NULL;
#else
return NULL;
#endif

}






/**
 *
 * map_widget_thumbnail_map_init
 *
 * @return : Pointer to hiden map
 * Creates a hidden map for thumbnails
 *
 **/
MapWidget* map_widget_thumbnail_map_init()
{
#if 1
    if(MildenhallThumbnailMap == NULL)
    {

        MildenhallThumbnailMap = g_object_new(MAP_TYPE_WIDGET,NULL);
        clutter_actor_add_child(CLUTTER_ACTOR(clutter_stage_get_default()),CLUTTER_ACTOR(MildenhallThumbnailMap));


        clutter_actor_set_position(CLUTTER_ACTOR(MildenhallThumbnailMap),790,470);
        g_object_set (MildenhallThumbnailMap, "zoom-level", 15, NULL);
        if(MildenhallThumbnailMap == NULL)
        {
            g_warning("Creation of Screen shot MAP failed \n");
            return NULL;
        }
    }

    if(MildenhallThumbnailQueue== NULL)
    {
        //create event message message queue
        MildenhallThumbnailQueue = g_async_queue_new();

        if(MildenhallThumbnailQueue == NULL)
        {
            g_warning("Screen Shot MQ- creation failed (%s)-(%d)",__FUNCTION__,__LINE__);
            return NULL;
        }
    }

    if(ThumbnailWorkerThread == NULL)
    {

        ThumbnailWorkerThread = g_thread_new((const gchar*)"thumbnail",map_widget_thumbnail_worker_thread,NULL);
        if(ThumbnailWorkerThread == NULL)
        {
            g_warning(" Creation of Screen shot Worker Thread failed \n");
            return NULL;
        }
        ThumbnailWorkerFlag = 0;
    }
    return (MildenhallThumbnailMap);
#else
return NULL;
#endif
}

/**
 *
 * map_widget_get_thumbnail_map
 *
 * @width:    	width of the thumbnail
 * @height:   	height of thumb nail
 * @latitude: 	location of thumbnail
 * @longitude: 	location of thumbnail
 * @return : 	Pointer to hiden map
 *
 *Generates a thumnail for given location
 **/


void map_widget_get_thumbnail_map(gint width,gint height,gfloat latitude, gfloat longitude)
{
#if 1

    if(MildenhallThumbnailMap == NULL)
        return ;
    MAP_WIDGET_DEBUG(" \n map_widget_get_thumbnail_map \n ");
    MildenhallThumbnailMQData *em = g_new0(MildenhallThumbnailMQData,1);
    em->width = width;
    em->height = height;
    em->longitude = longitude;
    em->latitude = latitude;
    g_async_queue_push(MildenhallThumbnailQueue,em);
#endif
}




