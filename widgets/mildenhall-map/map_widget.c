/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* map_widget.c
 *
 * Created on: Dec 11, 2012
 *
 * map_widget.c */

#include "map_widget_internal.h"

#include <thornbury/thornbury.h>

G_DEFINE_TYPE (MapWidget, map_widget, LIGHTWOOD_TYPE_MAP_BASE)

#define WIDGET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), MAP_TYPE_WIDGET, MapWidgetPrivate))

typedef enum _MapProperties MapProperties;


enum _MapProperties
{
    MAP_PROP_0,
    MAP_PROP_MODEL,
    MAP_PROP_DRAW_PATH,
    MAP_PROP_MARKER_VISIBLE

};



guint map_widget_debug_flags;





/******************************************************************************
 * FUNCTION		: v_map_widget_model_row_added_cb
 * PARAMETER		: ThornburyModel *,ThornburyModelIter *,gpointer.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called whenever new row is added to model
 *
 *******************************************************************************/
void v_map_widget_model_row_added_cb(ThornburyModel *pModel, ThornburyModelIter *pIter,
                                     gpointer pUserData)
{
    MapWidget *pSelf = MAP_WIDGET(pUserData);
    ClutterColor *color = p_map_widget_return_normal_colour(pSelf);
    ClutterColor *SelectedColor = p_map_widget_return_selected_colour(pSelf);
    ClutterActor *pLayerActor = p_map_widget_get_layer_actor(pSelf);
    ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);
    ClutterActor *pMarker = NULL;
    GPtrArray *glMarkerslist = p_map_widget_get_markers_list(pSelf);
    GPtrArray *glImagelist = p_map_widget_get_image_list(pSelf);
    GPtrArray *pPathMarkerslist = p_map_widget_get_path_markers_list(pSelf);
    GPtrArray *pPathImagelist = p_map_widget_get_path_image_list(pSelf);
    gchar *pNormalImage=NULL;
    gchar *pSelectedImage=NULL;
    GValue Latitude = { 0, };
    GValue Longitude = { 0, };
    GValue MarkerName = { 0, };

    GValue NormalImage = { 0, };
    GValue SelectedImage = { 0, };
    GValue HighlightMarker = { 0, };
    GValue ViewPointType = { 0, };

    thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
    thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
    thornbury_model_iter_get_value(pIter, COLUMN_MARKER_NAME, &MarkerName);

    thornbury_model_iter_get_value(pIter, COLUMN_NORMAL_IMAGE, &NormalImage);
    thornbury_model_iter_get_value(pIter, COLUMN_SELECTED_IMAGE, &SelectedImage);
    thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
                               &HighlightMarker);
    thornbury_model_iter_get_value(pIter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);

    guint inRow = thornbury_model_iter_get_row(pIter);
    MAP_WIDGET_DEBUG("\n inRow is %d\n", inRow);

    Imagedata *pExImageInfo = g_new0(Imagedata, 1);

    if (g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CURRENT)
    {
        pSelf->priv->current_type_counter++;
        if(pSelf->priv->current_type_counter > 1)
        {
            g_warning("current position cannot be more than one");
            return;
        }

        else
        {

            pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"current-normal-image"), NULL);
            pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"current-selected-image"), NULL);
            pMarker=champlain_label_new_from_file (pNormalImage,NULL);
            pExImageInfo->normalImage = pNormalImage;
            pExImageInfo->selectedImage = pSelectedImage;

        }

    }
    else if(g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_DESTINATION)
    {
        MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_DESTINATION \n");
        pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"destination-normal-image"), NULL);
        pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"destination-selected-image"), NULL);
        pMarker=champlain_label_new_from_file (pNormalImage, NULL);
        pExImageInfo->normalImage = pNormalImage;
        pExImageInfo->selectedImage = pSelectedImage;

    }

    else if(g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_POI)
    {
        MAP_WIDGET_DEBUG("\n MAP_WIDGET_VPTYPE_POI \n");
        pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"poi-normal-image"), NULL);
        pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"poi-selected-image"), NULL);
        pMarker=champlain_label_new_from_file (pNormalImage, NULL);
        pExImageInfo->normalImage = pNormalImage;
        pExImageInfo->selectedImage = pSelectedImage;

    }

    else if(g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CUSTOM)
    {

        if((G_VALUE_HOLDS_STRING(&NormalImage) && (NULL != g_value_get_string(&NormalImage)))&&(G_VALUE_HOLDS_STRING(&SelectedImage) && (NULL != g_value_get_string(&SelectedImage))))
           {

            pMarker=champlain_label_new_from_file (g_value_get_string(&NormalImage), NULL);
             pExImageInfo->normalImage = g_strdup(g_value_get_string(&NormalImage));
             pExImageInfo->selectedImage = g_strdup(g_value_get_string(&SelectedImage));
           }
        else
        {
            pNormalImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"poi-normal-image"), NULL);
            pSelectedImage=g_strconcat(PKGTHEMEDIR, "/",(gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,"poi-selected-image"), NULL);
            pExImageInfo->normalImage = pNormalImage;
            pExImageInfo->selectedImage = pSelectedImage;
            pMarker=champlain_label_new_from_file (pNormalImage, NULL);

        }

    }

    if (pMarker != NULL)
    {

        champlain_label_set_color(CHAMPLAIN_LABEL(pMarker), color);
        MAP_WIDGET_DEBUG(" \n LONG AND LAT is %f %f \n", g_value_get_float(&Latitude),
                         g_value_get_float(&Longitude));
        if(G_VALUE_HOLDS_FLOAT(&Latitude) && G_VALUE_HOLDS_FLOAT(&Longitude))
            champlain_location_set_location(CHAMPLAIN_LOCATION(pMarker),
                                            g_value_get_float(&Latitude), g_value_get_float(&Longitude));

        if (G_VALUE_HOLDS_STRING(&MarkerName)
                && (NULL != g_value_get_string(&MarkerName)))
        {
            MAP_WIDGET_DEBUG("\n marker name is %s \n", g_value_get_string(&MarkerName));
            clutter_actor_set_name(pMarker, g_value_get_string(&MarkerName));
        }

        MAP_WIDGET_DEBUG("\n pMarker NOT NULL \n");
        clutter_actor_set_reactive(pMarker, TRUE);
    }
    champlain_marker_set_selection_color(SelectedColor);

    if ((g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CURRENT) || (g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_DESTINATION))
    {
        MAP_WIDGET_DEBUG("\n champlain_path_layer_add_node \n");
        if (pMarker != NULL)
        {
            g_signal_connect(pMarker, "button-release-event",
                             G_CALLBACK(b_map_widget_path_marker_selected_cb), pSelf);
            g_signal_connect(pMarker, "touch-event",
                                        G_CALLBACK(b_map_widget_path_marker_selected_touch_cb), pSelf);
            g_ptr_array_add(pPathMarkerslist, (gpointer) pMarker);
            g_ptr_array_add(pPathImagelist, (gpointer) pExImageInfo);
            champlain_marker_layer_add_marker(
                CHAMPLAIN_MARKER_LAYER(pSelf->priv->pPosDestinationLayer),
                CHAMPLAIN_MARKER(pMarker));

            v_map_widget_draw_path(pSelf,pPathLayerActor,0.0);

            if (G_VALUE_HOLDS_BOOLEAN(&HighlightMarker))
            {
                if (TRUE == g_value_get_boolean(&HighlightMarker))
                {
                    pSelf->priv->path_highlight_marker_counter++;

                    if(pSelf->priv->path_highlight_marker_counter > 1)
                    {
                        g_warning("you cannot highlight more than one marker");
                        pSelf->priv->path_highlight_marker_counter--;
                        return;
                    }


                    else
                    {

                        pSelf->priv->selectedPathMarkerIndex=pPathMarkerslist->len-1;
                        champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), TRUE);
                        champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
                        champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
                                                  CLUTTER_ACTOR(
                                                      thornbury_ui_texture_create_new(pExImageInfo->selectedImage,
                                                              0, 0, FALSE, FALSE)));
                        if(G_VALUE_HOLDS_FLOAT(&Latitude) && G_VALUE_HOLDS_FLOAT(&Longitude))
                            v_map_widget_move_to_location(pSelf,g_value_get_float(&Latitude),g_value_get_float(&Longitude),20);
                    }
                }




            }
        }
    }
    else if((g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_ROUTE))
    {

        champlain_path_layer_add_node(
            CHAMPLAIN_PATH_LAYER(pPathLayerActor),
            CHAMPLAIN_LOCATION(champlain_coordinate_new_full (g_value_get_float(&Latitude), g_value_get_float(&Longitude))));

    }

    else
    {

        g_signal_connect(pMarker, "button-release-event",
                         G_CALLBACK(b_map_widget_marker_selected_cb), pSelf);
        g_signal_connect(pMarker, "touch-event",
                                 G_CALLBACK(b_map_widget_marker_selected_touch_cb), pSelf);
        champlain_marker_layer_add_marker(CHAMPLAIN_MARKER_LAYER(pLayerActor),
                                          CHAMPLAIN_MARKER(pMarker));
        g_ptr_array_add(glMarkerslist, (gpointer) pMarker);
        g_ptr_array_add(glImagelist, (gpointer) pExImageInfo);

        if (G_VALUE_HOLDS_BOOLEAN(&HighlightMarker))
        {
            if (TRUE == g_value_get_boolean(&HighlightMarker))
            {
                pSelf->priv->highlight_marker_counter++;

                if(pSelf->priv->highlight_marker_counter > 1)
                {
                    g_warning("you cannot highlight more than one marker");
                    pSelf->priv->highlight_marker_counter--;
                    return;
                }


                else
                {

                    pSelf->priv->selectedMarkerIndex=glMarkerslist->len-1;
                    champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), TRUE);
                    champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
                    champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
                                              CLUTTER_ACTOR(
                                                  thornbury_ui_texture_create_new(pExImageInfo->selectedImage,
                                                          0, 0, FALSE, FALSE)));
                }
            }



        }

    }

v_map_widget_draw_path(pSelf,pPathLayerActor,0.0);

}


static void v_map_widget_free_index_array(GList *array)
{
	if(array !=NULL)
	{
	    g_list_free(array);
	    array=NULL;
	}
}


/******************************************************************************
 * FUNCTION		: v_map_widget_model_row_changed_cb
 * PARAMETER		: ThornburyModel *,ThornburyModelIter *,gpointer.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called whenever row is changed in  model
 *
 *******************************************************************************/

static void v_map_widget_model_row_changed_cb(ThornburyModel *pModel,
        ThornburyModelIter *pIter, gpointer pUserData)
{

    MapWidget *pSelf = MAP_WIDGET(pUserData);
    ClutterColor *color = p_map_widget_return_normal_colour(pSelf);
    GPtrArray *glMarkerslist = p_map_widget_get_markers_list(pSelf);
    GPtrArray *glImagelist = p_map_widget_get_image_list(pSelf);
    GPtrArray *pPathMarkerslist = p_map_widget_get_path_markers_list(pSelf);
    GPtrArray *pPathImagelist = p_map_widget_get_path_image_list(pSelf);
    guint inRow = thornbury_model_iter_get_row(pIter);
   // gint column_changed;
   // g_object_get(pModel,"column-changed",&column_changed,NULL);
    ClutterActor *pMarker = NULL;
    gboolean isPathMarker=FALSE;

    GValue Latitude = { 0, };
    GValue Longitude = { 0, };
    GValue MarkerName = { 0, };

    GValue NormalImage = { 0, };
    GValue SelectedImage = { 0, };
    GValue HighlightMarker = { 0, };
    GValue ViewPointType = { 0, };

    thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
    thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
    thornbury_model_iter_get_value(pIter, COLUMN_MARKER_NAME, &MarkerName);

    thornbury_model_iter_get_value(pIter, COLUMN_NORMAL_IMAGE, &NormalImage);
    thornbury_model_iter_get_value(pIter, COLUMN_SELECTED_IMAGE, &SelectedImage);
    thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
                               &HighlightMarker);
    thornbury_model_iter_get_value(pIter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);
    GList *pathindexarray=NULL;
    GList *poiindexarray=NULL;

    gint inLoopcounter=0;

    while(inLoopcounter < thornbury_model_get_n_rows(pModel))
    {
        ThornburyModelIter *Iter = thornbury_model_get_iter_at_row(pModel,
                               inLoopcounter);
        thornbury_model_iter_get_value(Iter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);

         switch(g_value_get_int(&ViewPointType))
         {
         case MAP_WIDGET_VPTYPE_CURRENT:
         case MAP_WIDGET_VPTYPE_DESTINATION:
         {
        	 pathindexarray=g_list_append(pathindexarray,GINT_TO_POINTER(inLoopcounter));
        	 break;
         }
         case MAP_WIDGET_VPTYPE_POI:
         case MAP_WIDGET_VPTYPE_CUSTOM:
         {
        	 poiindexarray= g_list_append(poiindexarray,GINT_TO_POINTER(inLoopcounter));
        	 break;
         }
         }
        inLoopcounter++;

    }
    gint path=0;

    while(path < g_list_length(pathindexarray))
    {
      if((inRow == GPOINTER_TO_INT(g_list_nth_data(pathindexarray,path))) &&(pPathMarkerslist->len))
      {
            pMarker=CLUTTER_ACTOR(g_ptr_array_index(pPathMarkerslist, path));
           /* pExImageInfo=(Imagedata *) g_ptr_array_index(pPathImagelist,
                         path);*/

            isPathMarker=TRUE;
            break;
      }
      path++;
    }

    gint poi=0;
    while(poi < g_list_length(poiindexarray))
    {
      if((inRow == GPOINTER_TO_INT(g_list_nth_data(poiindexarray,poi)))&&(glMarkerslist->len))
      {
           pMarker=CLUTTER_ACTOR(g_ptr_array_index(glMarkerslist, poi));
           /*pExImageInfo=(Imagedata *) g_ptr_array_index(glImagelist,
                       poi);*/
           isPathMarker=FALSE;
           break;

      }
      poi++;
    }

 v_map_widget_free_index_array(poiindexarray);
 v_map_widget_free_index_array(pathindexarray);
 champlain_label_set_color(CHAMPLAIN_LABEL(pMarker), color);
 switch(pSelf->priv->coumn_changed)
 {
 case COLUMN_LATITUDE:
 case COLUMN_LONGITUDE:
	 {
	                champlain_location_set_location(CHAMPLAIN_LOCATION(pMarker),
	                                         g_value_get_float(&Latitude), g_value_get_float(&Longitude));
                              break;
	 }
 case COLUMN_MARKER_NAME:
 {
	    if ((NULL != g_value_get_string(&MarkerName)) && pMarker)
	    {
	        clutter_actor_set_name(pMarker, g_value_get_string(&MarkerName));
	    }
 break;
 }
 case COLUMN_HIGHLIGHT_MARKER:
 {
	 if(isPathMarker)
	     {

	             if (TRUE == g_value_get_boolean(&HighlightMarker))
	             {
	                  pSelf->priv->selectedPathMarkerIndex=path;
	                  pSelf->priv->path_highlight_marker_counter++;

	                         if(pSelf->priv->path_highlight_marker_counter > 1)
	                         {
	                             g_warning("you cannot highlight more than one marker");
	                             pSelf->priv->path_highlight_marker_counter--;
	                             return;
	                         }

	                 Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(pPathImagelist,
	                                        path);

	                 champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
	                                           CLUTTER_ACTOR(
	                                               thornbury_ui_texture_create_new(ImageInfo->selectedImage, 0, 0, FALSE,
	                                                       FALSE)));
	                 if(pSelf->priv->path_touch_event == FALSE)
	                 {

	                     champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), TRUE);
	                     champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
	                     if(pSelf->priv->selectedPathMarkerIndex !=-1 )
	                     {

	                         Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(pPathImagelist,
	                                                pSelf->priv->selectedPathMarkerIndex);
	                         champlain_label_set_image(
	                             CHAMPLAIN_LABEL(
	                                 g_ptr_array_index(pPathMarkerslist, pSelf->priv->selectedPathMarkerIndex)),
	                             CLUTTER_ACTOR(
	                                 thornbury_ui_texture_create_new(ImageInfo->selectedImage, 0, 0,
	                                                       FALSE, FALSE)));
	                     }

	                         v_map_widget_move_to_location(pSelf,g_value_get_float(&Latitude),g_value_get_float(&Longitude),20);
	                 }



	                 pSelf->priv->path_touch_event=FALSE;
	             }
	             else
	             {
	                  pSelf->priv->path_highlight_marker_counter--;
	                  if(pSelf->priv->path_highlight_marker_counter < 0)
	                  {
	                      g_warning("No marker is highlighted");
	                      pSelf->priv->path_highlight_marker_counter=0;
	                      return;
	                  }
	                  if(pSelf->priv->path_touch_event == FALSE)
	                 {
	                  champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), FALSE);
	                 }
	                 if(-1 != path )
	                 {
	                     Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(pPathImagelist,
	                                            path);
	                     champlain_label_set_image(
	                         CHAMPLAIN_LABEL(
	                             g_ptr_array_index(pPathMarkerslist, path)),
	                         CLUTTER_ACTOR(
	                             thornbury_ui_texture_create_new(ImageInfo->normalImage, 0, 0,
	                                                   FALSE, FALSE)));
	                 }
	             }
	     }
	     else
	     {
	             if (TRUE == g_value_get_boolean(&HighlightMarker))
	             {
	                    pSelf->priv->selectedMarkerIndex=poi;
	                   pSelf->priv->highlight_marker_counter++;
	                         if(pSelf->priv->highlight_marker_counter > 1)
	                         {
	                             g_warning("you cannot highlight more than one marker");
	                             pSelf->priv->highlight_marker_counter--;
	                             return;
	                         }

	                 Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(glImagelist,
	                                        poi);

	                 champlain_label_set_image(CHAMPLAIN_LABEL(pMarker),
	                                           CLUTTER_ACTOR(thornbury_ui_texture_create_new(ImageInfo->selectedImage, 0, 0, FALSE,
	                                                         FALSE)));
	                 if(pSelf->priv->poi_touch_event == FALSE)
	                 {

	                     champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), TRUE);
	                     champlain_marker_animate_in(CHAMPLAIN_MARKER(pMarker));
	                     if(pSelf->priv->selectedMarkerIndex !=-1 )
	                     {


	                         Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(glImagelist,
	                                                pSelf->priv->selectedMarkerIndex);
	                         champlain_label_set_image(
	                             CHAMPLAIN_LABEL(
	                                 g_ptr_array_index(glMarkerslist, pSelf->priv->selectedMarkerIndex)),
	                             CLUTTER_ACTOR(
	                                 thornbury_ui_texture_create_new(ImageInfo->selectedImage, 0, 0,
	                                                       FALSE, FALSE)));
	                     }
	                         v_map_widget_move_to_location(pSelf,g_value_get_float(&Latitude),g_value_get_float(&Longitude),20);

	                 }

	                 pSelf->priv->poi_touch_event=FALSE;
	             }
	             else
	             {
	                 pSelf->priv->highlight_marker_counter--;
	                  if(pSelf->priv->highlight_marker_counter < 0)
	                  {
	                      g_warning("Already  marker is not highlighted");
	                      pSelf->priv->highlight_marker_counter=0;
	                      return;
	                  }
	                  if(pSelf->priv->poi_touch_event == FALSE)
	                 {
	                 champlain_marker_set_selected(CHAMPLAIN_MARKER(pMarker), FALSE);
	                 }
	                 if( -1 != poi )
	                 {
	                     Imagedata *ImageInfo = (Imagedata *) g_ptr_array_index(glImagelist,
	                                            poi);
	                     champlain_label_set_image(
	                         CHAMPLAIN_LABEL(
	                             g_ptr_array_index(glMarkerslist, poi)),
	                         CLUTTER_ACTOR(
	                             thornbury_ui_texture_create_new(ImageInfo->normalImage, 0, 0,
	                                                   FALSE, FALSE)));
	                 }

	             }
	     }
	 break;
 }
 }
}


static void v_map_widget_free_imagedata(Imagedata *imagedata)
{
    if(NULL !=imagedata)
 {
    g_free(imagedata->normalImage);
 	imagedata->normalImage=NULL;
 	g_free(imagedata->selectedImage);
 	imagedata->selectedImage=NULL;
 }
}

static void v_map_widget_destroy_actor(ClutterActor *Marker)
{
	if(CLUTTER_IS_ACTOR(Marker) && (NULL != Marker))
	 {
	   clutter_actor_destroy(Marker);
	   Marker=NULL;
	 }
}

/******************************************************************************
 * FUNCTION		: v_map_widget_model_row_removed_cb
 * PARAMETER		: ThornburyModel *,ThornburyModelIter *,gpointer.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called whenever row is removed from   model
 *
 *******************************************************************************/

static void v_map_widget_model_row_removed_cb(ThornburyModel *pModel,
        ThornburyModelIter *pIter, gpointer pUserData)
{
    MapWidget *pSelf = MAP_WIDGET(pUserData);
    ClutterActor *pLayerActor = p_map_widget_get_layer_actor(pSelf);
    ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);
    GPtrArray *glMarkerslist = p_map_widget_get_markers_list(pSelf);
    GPtrArray *glImagelist = p_map_widget_get_image_list(pSelf);
    GPtrArray *pPathMarkerslist = p_map_widget_get_path_markers_list(pSelf);
    GPtrArray *pPathImagelist = p_map_widget_get_path_image_list(pSelf);
    guint inRow = thornbury_model_iter_get_row(pIter);
    GValue ViewPointType = { 0, };
    GValue Latitude = { 0, };
    GValue Longitude = { 0, };
     GValue HighlightMarker={ 0, };
    thornbury_model_iter_get_value(pIter, COLUMN_LATITUDE, &Latitude);
    thornbury_model_iter_get_value(pIter, COLUMN_LONGITUDE, &Longitude);
    thornbury_model_iter_get_value(pIter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);
    thornbury_model_iter_get_value(pIter, COLUMN_HIGHLIGHT_MARKER,
                               &HighlightMarker);
    ClutterActor *pMarker = NULL;

    GList *pathindexarray=NULL;
        GList *poiindexarray=NULL;

        gint inLoopcounter=0;

        while(inLoopcounter < thornbury_model_get_n_rows(pModel))
        {
            ThornburyModelIter *Iter = thornbury_model_get_iter_at_row(pModel,
                                   inLoopcounter);
            thornbury_model_iter_get_value(Iter, COLUMN_VIEW_POINT_TYPE, &ViewPointType);
            switch(g_value_get_int(&ViewPointType))
                    {
                    case MAP_WIDGET_VPTYPE_CURRENT:
                    case MAP_WIDGET_VPTYPE_DESTINATION:
                    {
                   	 pathindexarray=g_list_append(pathindexarray,GINT_TO_POINTER(inLoopcounter));
                   	 break;
                    }
                    case MAP_WIDGET_VPTYPE_POI:
                    case MAP_WIDGET_VPTYPE_CUSTOM:
                    {
                   	 poiindexarray= g_list_append(poiindexarray,GINT_TO_POINTER(inLoopcounter));
                   	break;
                    }
                    }
            inLoopcounter++;

        }

     switch(g_value_get_int(&ViewPointType))
     {
     case MAP_WIDGET_VPTYPE_CURRENT:
     case MAP_WIDGET_VPTYPE_DESTINATION:
     {
    	 gint path=0;
    	 while(path < g_list_length(pathindexarray))
    	 {
    	    if(inRow == GPOINTER_TO_INT(g_list_nth_data(pathindexarray,path)))
    	  {
    	  pMarker = CLUTTER_ACTOR(g_ptr_array_index(pPathMarkerslist, path));
    	   champlain_marker_layer_remove_marker(CHAMPLAIN_MARKER_LAYER(pSelf->priv->pPosDestinationLayer),
    	     	            	                                             CHAMPLAIN_MARKER(pMarker));
    	     	            	        ClutterActor *Marker=g_ptr_array_remove_index(pPathMarkerslist, path);
    	     	            	        Imagedata *imagedata=g_ptr_array_remove_index(pPathImagelist, path);
    	     	            	        v_map_widget_destroy_actor(Marker);
    	     	            	        v_map_widget_free_imagedata(imagedata);
    	   if((g_value_get_int(&ViewPointType) == MAP_WIDGET_VPTYPE_CURRENT))
    	    pSelf->priv->current_type_counter--;
    	    if(G_VALUE_HOLDS_BOOLEAN(&HighlightMarker) && g_value_get_boolean(&HighlightMarker) == TRUE)
    	    {
    	       pSelf->priv->path_highlight_marker_counter--;
    	       if(pSelf->priv->path_highlight_marker_counter < 0)
    	        {
    	          pSelf->priv->path_highlight_marker_counter=0;
    	        }

    	      }
    	     break;
    	     }
    	     path++;
    	   }
    	break;
     }
     case MAP_WIDGET_VPTYPE_ROUTE:
     {
   GList *list=champlain_path_layer_get_nodes(CHAMPLAIN_PATH_LAYER(pPathLayerActor));
   if(CHAMPLAIN_IS_LOCATION(g_list_nth_data(list,inRow)) && (NULL !=g_list_nth_data(list,inRow)))
   {
   champlain_path_layer_remove_node(CHAMPLAIN_PATH_LAYER(pPathLayerActor),CHAMPLAIN_LOCATION(g_list_nth_data(list,inRow)));
   }
   v_map_widget_free_index_array(list);
     break;
     }
   default:
   {
	   gint poi=0;
	       	    while(poi < g_list_length(poiindexarray))
	       	    {


	       	        if(inRow == GPOINTER_TO_INT(g_list_nth_data(poiindexarray,poi)))
	       	        {
	       	        	  pMarker = CLUTTER_ACTOR(g_ptr_array_index(glMarkerslist, poi));


	       	        	        champlain_marker_layer_remove_marker(CHAMPLAIN_MARKER_LAYER(pLayerActor),
	       	        	                                             CHAMPLAIN_MARKER(pMarker));
	       	        	        Imagedata *imagedata=g_ptr_array_remove_index(glImagelist, poi);
	       	        	       ClutterActor *Marker=g_ptr_array_remove_index(glMarkerslist, poi);
	       	        	       v_map_widget_destroy_actor(Marker);
	       	        	       v_map_widget_free_imagedata(imagedata);
	       	        	       if(G_VALUE_HOLDS_BOOLEAN(&HighlightMarker) && g_value_get_boolean(&HighlightMarker) == TRUE)
	       	        	                                {
	       	        	                                    pSelf->priv->highlight_marker_counter--;
	       	        	                                       if(pSelf->priv->highlight_marker_counter < 0)
	       	        	                                       {
	       	        	                                         pSelf->priv->highlight_marker_counter=0;
	       	        	                                       }

	       	        	                                }
	       	            break;
	       	        }
	       	        poi++;
	       	    }
   }
     }

     v_map_widget_free_index_array(poiindexarray);
     v_map_widget_free_index_array(pathindexarray);

}

static void map_widget_column_changed_fnc(gint column,gpointer userdata)
{
	MapWidget *pSelf = MAP_WIDGET(userdata);
	pSelf->priv->coumn_changed=column;
}

/******************************************************************************
 * FUNCTION		: v_map_widget_set_model
 * PARAMETER		: GObject *,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to set model for map widget
 *
 *******************************************************************************/
void v_map_widget_set_model(GObject *pObject, const GValue *pValue,
                            GParamSpec *pPspec)
{

    if (!MAP_IS_WIDGET(pObject))
    {
        g_warning("invalid map object\n");
        return;
    }
    MapWidget *pSelf = MAP_WIDGET(pObject);

    if (NULL != pSelf->priv->pModel)
    {
        g_signal_handlers_disconnect_by_func(pSelf->priv->pModel,
                                             G_CALLBACK(v_map_widget_model_row_added_cb), pObject);
        g_signal_handlers_disconnect_by_func(pSelf->priv->pModel,
                                             G_CALLBACK(v_map_widget_model_row_changed_cb), pObject);
        g_signal_handlers_disconnect_by_func(pSelf->priv->pModel,
                                             G_CALLBACK(v_map_widget_model_row_removed_cb), pObject);
        //g_object_unref(pSelf->priv->pModel);

	thornbury_list_model_destroy(pSelf->priv->pModel);
        pSelf->priv->pModel = NULL;
    }
    ThornburyModel *pModel = g_value_get_object(pValue);

    if(NULL == pModel)
        return;


    if (pModel != NULL)
    {
        g_return_if_fail(G_IS_OBJECT(pModel));

        pSelf->priv->pModel = g_object_ref(pModel);

        g_signal_connect(pSelf->priv->pModel, "row-added",
                         G_CALLBACK(v_map_widget_model_row_added_cb), pObject);

        g_signal_connect(pSelf->priv->pModel, "row-changed",
                         G_CALLBACK(v_map_widget_model_row_changed_cb), pObject);

        g_signal_connect(pSelf->priv->pModel, "row-removed",
                         G_CALLBACK(v_map_widget_model_row_removed_cb), pObject);
        thornbury_model_register_column_changed_cb(pSelf->priv->pModel,map_widget_column_changed_fnc,pSelf);

    }


    v_extract_model_to_add_view_points(pSelf);
    g_object_notify_by_pspec(pObject, pPspec);

}

/******************************************************************************
 * FUNCTION		: v_map_widget_set_markers_visible
 * PARAMETER		: GObject *,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to make all markers and layers visible
 *
 *******************************************************************************/
static void v_map_widget_set_markers_visible(GObject *pObject, const GValue *pValue,
                            GParamSpec *pPspec)
        {
          if (!MAP_IS_WIDGET(pObject))
    {
        g_warning("invalid map object\n");
        return;
    }
    MapWidget *pSelf = MAP_WIDGET(pObject);

 gboolean markersvisible= g_value_get_boolean(pValue);
 if(markersvisible)
 {
champlain_view_ensure_layers_visible(CHAMPLAIN_VIEW(pSelf->priv->content_map),TRUE);

 }
 else

 {
champlain_view_ensure_layers_visible(CHAMPLAIN_VIEW(pSelf->priv->content_map),FALSE);
 }
        }


/******************************************************************************
 * FUNCTION		: v_map_widget_set_latitude
 * PARAMETER		: GObject *,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to set latitude on map widget
 *
 *******************************************************************************/

static void v_map_widget_set_latitude(MapWidget *pSelf)
{


    g_return_if_fail(MAP_WIDGET(pSelf));

    gfloat flLatitude;
    g_object_get(pSelf,"latitude",&flLatitude,NULL);
    pSelf->priv->latitude = flLatitude;

    // champlain_view_center_on(CHAMPLAIN_VIEW(pSelf->priv->content_map),pSelf->priv->latitude,pSelf->priv->longitude);
    v_map_widget_move_to_location(pSelf, pSelf->priv->latitude,
                                  pSelf->priv->longitude, 10);

}

/******************************************************************************
 * FUNCTION		: v_map_widget_set_longitude
 * PARAMETER	: GObject *,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is used to set longitude on map widget
 *
 *******************************************************************************/

static void v_map_widget_set_longitude(MapWidget *pSelf)
{


    g_return_if_fail(MAP_WIDGET(pSelf));

    gfloat flLongitude;
    g_object_get(pSelf,"longitude",&flLongitude,NULL);
    pSelf->priv->longitude = flLongitude;

    // champlain_view_center_on(CHAMPLAIN_VIEW(pSelf->priv->content_map),pSelf->priv->latitude,pSelf->priv->longitude);
    v_map_widget_move_to_location(pSelf, pSelf->priv->latitude,
                                  pSelf->priv->longitude, 10);

}






/******************************************************************************
 * FUNCTION		: v_map_widget_get_property
 * PARAMETER		: GObject *,guint,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called when ever g_object_get is called on the object
 *
 *******************************************************************************/

static void v_map_widget_get_property(GObject *pObject, guint uinPropertyId,
                                      GValue *pValue, GParamSpec *pPspec)
{
    if (!MAP_IS_WIDGET(pObject))
    {
        g_warning("invalid map  object\n");
        return;
    }

    MapWidget *pSelf = MAP_WIDGET(pObject);

    switch (uinPropertyId)
    {

    case MAP_PROP_MODEL:
        g_value_set_object(pValue, pSelf->priv->pModel);
        break;

    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(pObject, uinPropertyId, pPspec);
    }
}

/******************************************************************************
 * FUNCTION		: v_map_widget_set_property
 * PARAMETER		: GObject *,guint,const GValue*,GParamSpec *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called when ever g_object_set is called on the object
 *
 *******************************************************************************/

static void v_map_widget_set_property(GObject *pObject, guint uinPropertyId,
                                      const GValue *pValue, GParamSpec *pPspec)
{
    MapWidget *pSelf = MAP_WIDGET(pObject);
    ClutterActor *pPathLayerActor = p_map_widget_get_path_layer_actor(pSelf);

    switch (uinPropertyId)
    {


    case MAP_PROP_MODEL:
        v_map_widget_set_model(pObject, pValue, pPspec);
        break;
        case MAP_PROP_DRAW_PATH:
            {

                    gboolean value = g_value_get_boolean(pValue);

                   if(value)
                v_map_widget_draw_path(pSelf, pPathLayerActor,2.0);
                else
				{
                  v_map_widget_draw_path(pSelf, pPathLayerActor,0.0);
				  champlain_path_layer_remove_all(CHAMPLAIN_PATH_LAYER(pPathLayerActor));
				}
                break;
            }
        case MAP_PROP_MARKER_VISIBLE:
        v_map_widget_set_markers_visible(pObject, pValue, pPspec);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(pObject, uinPropertyId, pPspec);
    }
}

/******************************************************************************
 * FUNCTION		: v_map_widget_dispose
 * PARAMETER		: GObject *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called just before  object is freed
 *
 *******************************************************************************/

static void v_map_widget_dispose(GObject *pObject)
{
    G_OBJECT_CLASS(map_widget_parent_class)->dispose(pObject);
}

/******************************************************************************
 * FUNCTION		: v_map_widget_finalize
 * PARAMETER		: GObject *.
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is called just before  object is freed
 *
 *******************************************************************************/

static void v_map_widget_finalize(GObject *pObject)
{
    G_OBJECT_CLASS(map_widget_parent_class)->finalize(pObject);
}

/******************************************************************************
 * FUNCTION		: map_widget_class_init
 * PARAMETER		: The object's class reference
 * RETURN VALUE	: void
 * DESCRIPTION	: Class initialisation function for the object type.
 *               Called automatically on the first call to g_object_new
 *
 *******************************************************************************/

static void map_widget_class_init(MapWidgetClass *pKlass)
{
    GObjectClass *pObjectClass = G_OBJECT_CLASS(pKlass);

    g_type_class_add_private(pKlass, sizeof(MapWidgetPrivate));
    GParamSpec *pPspec = NULL;
    pObjectClass->get_property = v_map_widget_get_property;
    pObjectClass->set_property = v_map_widget_set_property;
    pObjectClass->dispose = v_map_widget_dispose;
    pObjectClass->finalize = v_map_widget_finalize;




    pPspec = g_param_spec_object("model", "model",
                                 "model information of the map widget", G_TYPE_OBJECT,
                                 G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_MODEL, pPspec);
    pPspec = g_param_spec_boolean("draw-path", "Todraw path",
                                 "path handlingof the map widget",FALSE,
                                 G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_DRAW_PATH, pPspec);

     pPspec = g_param_spec_boolean("markers-visible", "markers visible",
                                 "To ensure all markers and layers are visible",FALSE,
                                 G_PARAM_READWRITE);
    g_object_class_install_property(pObjectClass, MAP_PROP_MARKER_VISIBLE, pPspec);




}

/******************************************************************************
 * FUNCTION		: v_map_widget_add_style_to_hash
 * PARAMETER		: MapWidget *,gchar *,gpointer
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is for storing the style part of widget in a private hash
 *
 *******************************************************************************/

static void v_map_widget_add_style_to_hash(MapWidget *pSelf, gchar *pKey,
        gpointer pValue)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid  map object\n");
        return;
    }

    if (NULL != pKey || NULL != pValue)
    {

        /* maintain key and value pair for style name and its value */
        if (G_VALUE_HOLDS_STRING(pValue))
            g_hash_table_insert (pSelf->priv->pPrivHash, g_strdup (pKey),
                                 g_value_dup_string (pValue));

    }

}

/******************************************************************************
 * FUNCTION		: v_map_widget_parse_style
 * PARAMETER		: gpointer,gpointer,gpointer
 * RETURN VALUE	: void
 * DESCRIPTION	: this function is for storing the style part of widget in a private hash
 *
 *******************************************************************************/
static void v_map_widget_parse_style(gpointer pKey, gpointer pValue,
                                     gpointer pUserData)
{
    gchar *pStyleKey = g_strdup(pKey);
    MapWidget *pSelf = MAP_WIDGET(pUserData);
    if (g_strcmp0(pStyleKey, "map-background-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "map-normal-image-colour") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "map-selected-image-colour") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "map-path-colour") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "current-normal-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "current-selected-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "destination-normal-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "destination-selected-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "poi-normal-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
    else if (g_strcmp0(pStyleKey, "poi-selected-image") == 0)
        v_map_widget_add_style_to_hash(pSelf, pStyleKey, pValue);
}

/******************************************************************************
 * FUNCTION		: map_widget_init
 * PARAMETER		: The object's reference
 * RETURN VALUE	: void
 * DESCRIPTION	: initialisation function for the object type.
 *                Called automatically on every call to g_object_new
 *
 *******************************************************************************/

static void map_widget_init(MapWidget *pSelf)
{
    LightwoodMapBase *map = LIGHTWOOD_MAP_BASE (pSelf);

    pSelf->priv = WIDGET_PRIVATE (pSelf);
    gfloat flLongitude,flLatitude;
    g_object_get(pSelf,"longitude",&flLongitude,NULL);
    g_object_get(pSelf,"latitude",&flLatitude,NULL);
    pSelf->priv->longitude = flLongitude;
    pSelf->priv->latitude = flLatitude;
    g_object_get (pSelf, "zoom-level", &pSelf->priv->zoom_level, NULL);
    pSelf->priv->current_type_counter=0;
    pSelf->priv->highlight_marker_counter=0;
    pSelf->priv->path_highlight_marker_counter=0;
    pSelf->priv->selectedPathMarkerIndex=-1;
    pSelf->priv->selectedMarkerIndex=-1;
    pSelf->priv->poi_touch_event=FALSE;
    pSelf->priv->path_touch_event=FALSE;
    pSelf->priv->touchendpoints=0;

    pSelf->priv->content_map = lightwood_map_base_get_map (map);
    pSelf->priv->content_bkground = NULL;

    pSelf->priv->pModel = NULL;
    pSelf->priv->bEnableMove = FALSE;
    pSelf->priv->glMarkerslist = g_ptr_array_new();
    pSelf->priv->glImagelist = g_ptr_array_new();
    pSelf->priv->pPathMarkerslist = g_ptr_array_new();
    pSelf->priv->pPathImagelist = g_ptr_array_new();
    g_signal_connect (pSelf, "notify::latitude", G_CALLBACK (v_map_widget_set_latitude), pSelf);
    g_signal_connect (pSelf, "notify::longitude", G_CALLBACK (v_map_widget_set_longitude), pSelf);
    GHashTable *pHash = NULL;
    const char *pEnvString;
    pEnvString = g_getenv("MAP_WIDGET_DEBUG");
    if (pEnvString != NULL)
    {
        map_widget_debug_flags = g_parse_debug_string(pEnvString,
                                 map_widget_debug_keys, G_N_ELEMENTS(map_widget_debug_keys));
        //g_print("MAP_WIDGET_PRINT: env_string %s %d %d \n", pEnvString,
        //        G_LIKELY(MAP_WIDGET_HAS_DEBUG), map_widget_debug_flags);
    }


    pSelf->priv->pStyleHash = thornbury_style_set(PKGDATADIR"/mildenhall_map_style.json");

    if (NULL != pSelf->priv->pStyleHash)
    {
        GHashTableIter iter;
        gpointer pKey, pValue;

        g_hash_table_iter_init(&iter, pSelf->priv->pStyleHash);

        while (g_hash_table_iter_next(&iter, &pKey, &pValue))
        {
            pHash = pValue;
            MAP_WIDGET_DEBUG("style layer = %s\n", (gchar*) pKey);
            if (NULL != pHash)
            {
                pSelf->priv->pPrivHash = g_hash_table_new_full(g_str_hash,
                                         g_str_equal, g_free, g_free);
                g_hash_table_foreach(pHash, v_map_widget_parse_style, pSelf);
            }

        }
    }

    /* free the style hash */
    thornbury_style_free(pSelf->priv->pStyleHash);

    gchar* pMap_bkg_image = g_strconcat(PKGTHEMEDIR, "/",
                                        (gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,
                                                "map-background-image"), NULL);
    g_object_set (pSelf, "background", pMap_bkg_image, NULL);
    pSelf->priv->content_bkground = lightwood_map_base_get_map_background (map);

    pSelf->priv->pPathLayerActor = lightwood_map_base_get_path_layer_actor (map);
    pSelf->priv->pLayerActor = lightwood_map_base_get_layer_actor (map);
    pSelf->priv->pPosDestinationLayer=lightwood_map_base_get_pos_destination_layer (map);

    g_signal_connect(pSelf->priv->content_map, "button-release-event",
                     G_CALLBACK(b_map_widget_map_selected_clb), pSelf);
    g_signal_connect(pSelf->priv->content_map, "touch-event",
                        G_CALLBACK(b_map_widget_map_selected_touch_clb), pSelf);

    clutter_actor_show(CLUTTER_ACTOR(pSelf));

}

MapWidget *
p_map_widget_new(void)
{
    MapWidget *pSelf = g_object_new(MAP_TYPE_WIDGET, NULL);

    return pSelf;
}



/**
 * FUNCTION		: map_widget_go_to_location
 * PARAMETER		: ClutterActor *,gfloat,gfloat.
 * RETURN VALUE	: ClutterActor*
 * DESCRIPTION	: this function is used to set the location on map
 *
 */
void map_widget_go_to_location(ClutterActor *actor, gfloat flLatitude,
                               gfloat flLongitude)
{

    MapWidget *pSelf = MAP_WIDGET(actor);
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return;
    }
    pSelf->priv->latitude = flLatitude;
    pSelf->priv->longitude = flLongitude;

    champlain_view_go_to(CHAMPLAIN_VIEW(pSelf->priv->content_map),
                         pSelf->priv->latitude, pSelf->priv->longitude);

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_map
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: ClutterActor*
 * DESCRIPTION	: this function is used to return the map actor
 *
 *******************************************************************************/

ClutterActor* p_map_widget_get_map(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->content_map;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_bkground
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: ClutterActor*
 * DESCRIPTION	: this function is used to return the map background
 *
 *******************************************************************************/

ClutterActor* p_map_widget_get_bkground(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->content_bkground;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_model
 * PARAMETER	: MapWidget *.
 * RETURN VALUE	: ThornburyModel *
 * DESCRIPTION	: this function is used to return the model of map
 *
 *******************************************************************************/

ThornburyModel* p_map_widget_get_model(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pModel;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_layer_actor
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: ClutterActor *
 * DESCRIPTION	: this function is used to return the layeractor  of map
 *
 *******************************************************************************/

ClutterActor* p_map_widget_get_layer_actor(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pLayerActor;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_path_layer_actor
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: ClutterActor *
 * DESCRIPTION	: this function is used to return the path layeractor  of map
 *
 *******************************************************************************/

ClutterActor* p_map_widget_get_path_layer_actor(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pPathLayerActor;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_markers_list
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: GPtrArray *
 * DESCRIPTION	: this function is used to return the markerslist on map
 *
 *******************************************************************************/

GPtrArray *p_map_widget_get_markers_list(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->glMarkerslist;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_image_list
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: GPtrArray *
 * DESCRIPTION	: this function is used to return the imagelist on map
 *
 *******************************************************************************/
GPtrArray *p_map_widget_get_image_list(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->glImagelist;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_return_normal_colour
 * PARAMETER    : MapWidget *.
 * RETURN VALUE	: ClutterColor *
 * DESCRIPTION	: this function is used to return the normal colour of marker
 *
 *******************************************************************************/

ClutterColor *p_map_widget_return_normal_colour(MapWidget *pSelf)
{
    ClutterColor *color = clutter_color_new(0, 0, 0, 0);
    clutter_color_from_string(color,
                              (gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,
                                      "map-normal-image-colour"));
    return color;
}

/******************************************************************************
 * FUNCTION		: p_map_widget_return_selected_colour
 * PARAMETER    : MapWidget *.
 * RETURN VALUE	: ClutterColor *
 * DESCRIPTION	: this function is used to return the selected colour of marker
 *
 *******************************************************************************/

ClutterColor *p_map_widget_return_selected_colour(MapWidget *pSelf)
{
    ClutterColor *color = clutter_color_new(0, 0, 0, 0);
    clutter_color_from_string(color,
                              (gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,
                                      "map-selected-image-colour"));
    return color;
}

/******************************************************************************
 * FUNCTION		: p_map_widget_return_selected_colour
 * PARAMETER    : MapWidget *.
 * RETURN VALUE	: ClutterColor *
 * DESCRIPTION	: this function is used to return the path colour of marker
 *
 *******************************************************************************/

ClutterColor *p_map_widget_return_path_colour(MapWidget *pSelf)
{
    ClutterColor *color = clutter_color_new(0, 0, 0, 0);
    clutter_color_from_string(color,
                              (gchar*) g_hash_table_lookup(pSelf->priv->pPrivHash,
                                      "map-path-colour"));
    return color;
}


/******************************************************************************
 * FUNCTION		: p_map_widget_get_private_hash
 * PARAMETER    : MapWidget *.
 * RETURN VALUE	: GHashTable *
 * DESCRIPTION	: this function is used to return the private hash table of style info
 *
 *******************************************************************************/

GHashTable *p_map_widget_get_private_hash(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pPrivHash;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_path_markers_list
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: GPtrArray *
 * DESCRIPTION	: this function is used to return the markerslist on map
 *
 *******************************************************************************/

GPtrArray *p_map_widget_get_path_markers_list(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pPathMarkerslist;

}

/******************************************************************************
 * FUNCTION		: p_map_widget_get_path_image_list
 * PARAMETER		: MapWidget *.
 * RETURN VALUE	: GPtrArray *
 * DESCRIPTION	: this function is used to return the imagelist on map
 *
 *******************************************************************************/
GPtrArray *p_map_widget_get_path_image_list(MapWidget *pSelf)
{
    if (!MAP_IS_WIDGET(pSelf))
    {
        g_warning("invalid map object\n");
        return NULL;
    }

    return pSelf->priv->pPathImagelist;

}


