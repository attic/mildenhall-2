/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_bottom_bar_type_one.h"
#include "mildenhall-internal.h"

#include <stdio.h>
#include <thornbury/thornbury.h>

gboolean button_pressed_cb (MildenhallBottomBar *bottomBar, gchar *button, gpointer data);
gboolean button_released_cb (MildenhallBottomBar *bottomBar, gchar *button, gpointer data);
static gboolean set_column_one(gpointer data);
ThornburyModel *model=NULL;
enum _enModelInfo
{
	 	COLUMN_ONE_ACTIVE,
	    COLUMN_ONE_INACTIVE,
	    COLUMN_ONE_TEXTURE_NAME,
	    COLUMN_TWO_ACTIVE,
	    COLUMN_TWO_INACTIVE,
	    COLUMN_TWO_TEXTURE_NAME,
	    COLUMN_THREE_ACTIVE,
	    COLUMN_THREE_INACTIVE,
	    COLUMN_THREE_TEXTURE_NAME,
	    COLUMN_FOUR,
	    COLUMN_LAST
};
int main(int argc, char *argv[])
{
	g_autofree gchar *active_love_icon_file = NULL;
	g_autofree gchar *inactive_love_icon_file = NULL;

	g_autofree gchar *active_hate_icon_file = NULL;
	g_autofree gchar *inactive_hate_icon_file = NULL;

	g_autofree gchar *active_skip_icon_file = NULL;
	g_autofree gchar *inactive_skip_icon_file = NULL;

	g_autofree gchar *prop_file = NULL;

	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;
	//clutter_threads_init ();

	model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
									  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  G_TYPE_STRING, NULL,
	                                  -1);
	ClutterActor *stage;
	ClutterColor stageColor = {0x00, 0x00, 0x00, 0xff};
	//ClutterColor bottomColor = {0x00, 0x0f, 0xee, 0xdd};
	GObject *pObject = NULL;
	stage = clutter_stage_new();
	clutter_actor_set_background_color(CLUTTER_ACTOR(stage), &stageColor);
	clutter_actor_set_size(stage, 800, 800);

	prop_file = _mildenhall_get_resource_path ("mildenhall_bottom_bar_type_one_prop.json");
	ThornburyItemFactory *itemFactory =
		thornbury_item_factory_generate_widget_with_props (
			MILDENHALL_TYPE_BOTTOM_BAR,
			prop_file);
	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (pObject);

	g_signal_connect(bottomBar,"action-press", G_CALLBACK(button_pressed_cb),NULL);
	g_signal_connect(bottomBar,"action-release",G_CALLBACK(button_released_cb),NULL);

	active_love_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Love_AC.png",
	    NULL);
	inactive_love_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Love_IN.png",
	    NULL);

	active_hate_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Hate_AC.png",
	    NULL);
	inactive_hate_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Hate_IN.png",
	    NULL);

	active_skip_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Skip_AC.png",
	    NULL);
	inactive_skip_icon_file = g_build_filename (_mildenhall_get_theme_path(),
	    "LastFM_Skip_IN.png",
	    NULL);

	thornbury_model_append (model,  COLUMN_ONE_ACTIVE, active_love_icon_file,
				COLUMN_ONE_INACTIVE, inactive_love_icon_file,
				COLUMN_ONE_TEXTURE_NAME,"button-one",
				COLUMN_TWO_ACTIVE, active_hate_icon_file,
				COLUMN_TWO_INACTIVE, inactive_hate_icon_file,
				COLUMN_TWO_TEXTURE_NAME,"button-two",
				COLUMN_THREE_ACTIVE, active_skip_icon_file,
				COLUMN_THREE_INACTIVE, inactive_skip_icon_file,
				COLUMN_THREE_TEXTURE_NAME,"botton-three",
				COLUMN_FOUR,"03:05",-1);
	g_object_set(bottomBar,"model",model,NULL);


	clutter_actor_set_position(CLUTTER_ACTOR(bottomBar), 70, 400);
	clutter_actor_add_child(CLUTTER_ACTOR(stage), CLUTTER_ACTOR(bottomBar));
	g_timeout_add_seconds(3,set_column_one,(gpointer)bottomBar);
	clutter_actor_show(stage);
	clutter_main();
	return 0;
}
gchar *i="hai";
GValue temp = { 0, };
static gboolean set_column_one(gpointer data)
{
	MildenhallBottomBar *bottomBar = MILDENHALL_BOTTOM_BAR (data);

	g_value_init(&temp,G_TYPE_STRING);


	g_value_set_string(&temp,i);
	//sleep(10);
//	g_object_set(bottomBar,"column-two",columnOne,NULL);
	thornbury_model_insert_value(model,0,COLUMN_FOUR,&temp);
	g_object_set(bottomBar,"button-one-state",TRUE,NULL);
	g_print("\nColumnONE TESTING IN TEST CLIENT\n");
	return FALSE;
}

gboolean
button_pressed_cb (MildenhallBottomBar *bottomBar, gchar *button, gpointer data)
{
	g_print("\n button %s\n",button);

	return FALSE;
}

gboolean
button_released_cb (MildenhallBottomBar *bottomBar, gchar *button, gpointer data)
{
	g_print("\n button %s\n",button);

	return FALSE;
}
