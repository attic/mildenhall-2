/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "mildenhall_pullup_roller.h"

#include <thornbury/thornbury.h>

#include "mildenhall_meta_info_header.h"
#include "mildenhall_info_roller.h"
#include "sample-item.h"
#include "sample-variable-item.h"
#include "mildenhall_context_drawer.h"
#include "test-utils.h"

/* header model columns */
enum
{
        COLUMN_LEFT_ICON_TEXT,
        COLUMN_MID_TEXT,
        COLUMN_RIGHT_ICON_TEXT,
        COLUMN_NONE
};

/* model columns */
enum
{
        COLUMN_NAME1,
        COLUMN_ICON1,
        COLUMN_TOOLTIP_TEXT,
        COLUMN_REACTIVE,
        COLUMN_LAST1
};


/* creation of model */
ThornburyModel *create_header_model()
{
        g_autofree gchar *music_icon_file = NULL;
        ThornburyModel *model = NULL;
        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_NONE,
                                        G_TYPE_POINTER, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        -1);

        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_AC.png",
            NULL);
        thornbury_model_append (model,
                                        //COLUMN_LEFT_ICON_TEXT, "Time",
                                        COLUMN_LEFT_ICON_TEXT, music_icon_file,
                                        COLUMN_MID_TEXT, "ALBUMS",
                                        COLUMN_RIGHT_ICON_TEXT,NULL,
                                        -1);

        return model;

}

/* creation of model */
ThornburyModel *create_footer_model()
{
        g_autofree gchar *music_icon_file = NULL;
        ThornburyModel *model = NULL;
        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_NONE,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_POINTER, NULL,
                                        -1);

        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_AC.png",
            NULL);
        thornbury_model_append (model,
                                        COLUMN_LEFT_ICON_TEXT, music_icon_file,
                                        COLUMN_MID_TEXT, "A L B U M S",
                                        COLUMN_RIGHT_ICON_TEXT,NULL,
                                        -1);

        return model;

}

/* creation of model */
ThornburyModel *create_drawer_model()
{
        ThornburyModel *model = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST1,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME1, "MUSIC",
                                        COLUMN_ICON1, "test-drawer-base/icon_music_AC.png",
                                        COLUMN_TOOLTIP_TEXT, "MUSIC",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME1, "ARTISTS",
                                        COLUMN_ICON1, "test-drawer-base/icon_music_artists_AC.png",
                                        COLUMN_TOOLTIP_TEXT, "ARTIST",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model,  COLUMN_NAME1, "INTERNET",
                                        COLUMN_ICON1, "test-drawer-base/icon_internet_AC.png",
                                        COLUMN_TOOLTIP_TEXT, "INTERNET",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME1, "ALBUMS",
                                        COLUMN_ICON1, "test-drawer-base/icon_music_AC.png",
                                        COLUMN_TOOLTIP_TEXT, "ALBUMS",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);


        return model;

}


static void pullup_item_selected_cb (MildenhallPullupRoller *roller, guint row, gpointer data)
{
        g_print("***************row = %d\n", row);

}

static void pullup_up_animation_started_cb(MildenhallPullupRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
	g_object_set(MILDENHALL_INFO_ROLLER(data), "show", FALSE, NULL);
}

static void pullup_down_animation_started_cb(MildenhallPullupRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}

static void pullup_animated_cb(MildenhallPullupRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}

static void info_pullup_item_selected_cb (MildenhallInfoRoller *roller, guint row, gpointer data)
{
        g_print("***************row = %d\n", row);

}

static void info_up_animation_started_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
	//g_object_set(MILDENHALL_PULLUP_ROLLER(data), "show", FALSE, NULL);
}

static void info_down_animation_started_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
	g_object_set(MILDENHALL_PULLUP_ROLLER(data), "show", FALSE, NULL);
}

static void info_pullup_animated_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}


int main (int argc, char **argv)
{
        g_autofree gchar *pImage = NULL;
        g_autofree gchar *overlay_img_file = NULL;
        g_autofree gchar *info_roller_prop_file = NULL;
        g_autofree gchar *pullup_roller_prop_file = NULL;
	ThornburyItemFactory *itemFactory1;
	ThornburyItemFactory *itemFactory;

        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;

        stage = clutter_stage_new ();
        pImage = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base", 
            "background.png",
            NULL);
        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

# if 0
	/* first roller */
	GObject *pRoller1 = NULL;
        pRoller1 = g_object_new(ROLLER_TYPE_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 277.0, "height", 480.0, "roll-over", TRUE, "reactive", TRUE, "arrow", TRUE, "arrowPosition", 190.0, NULL);
	clutter_actor_add_child(stage, CLUTTER_ACTOR(pRoller1));
	/* second roller */
	GObject *pRoller2 = NULL;
        pRoller2 = g_object_new(ROLLER_TYPE_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 375.0, "height", 480.0, "x", 281.0, "roll-over", TRUE, "reactive", TRUE, "arrow", TRUE, "arrowPosition", 190.0, NULL);
	clutter_actor_add_child(stage, CLUTTER_ACTOR(pRoller2));
	/* context menu */
	ClutterActor *pContextDrawer = mildenhall_context_drawer_new();
	ThornburyModel *model = create_drawer_model();
        g_object_set(pContextDrawer, "model", model, NULL);
        clutter_actor_add_child(stage, pContextDrawer);

# endif

	/* creation of info roller */
        info_roller_prop_file = _mildenhall_get_resource_path ("mildenhall_info_roller_prop.json");
	itemFactory1 = thornbury_item_factory_generate_widget_with_props (
            MILDENHALL_TYPE_INFO_ROLLER, info_roller_prop_file);
	GObject *pObject1 = NULL;
	g_object_get(itemFactory1, "object", &pObject1, NULL);
        ClutterActor *pInfoRoller = CLUTTER_ACTOR(pObject1);
	clutter_actor_add_child(stage, pInfoRoller);
	/* set header model */
	ThornburyModel *pHeaderModel1 = create_header_model();
	g_object_set(pInfoRoller, "header-model", pHeaderModel1, NULL);
	/* info roller model */
	ThornburyModel *model1 = lightwood_create_model (10);
	g_object_set(pInfoRoller, "model", model1,  NULL);
	/* info roller attributes */
	mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER(pInfoRoller), "name", 0);
        //mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER(pInfoRoller), "icon", 1);
        mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER (pInfoRoller), "label", 2);

	/* creation fo pullup roller */
        pullup_roller_prop_file = _mildenhall_get_resource_path ("mildenhall_pullup_roller_prop.json");
	itemFactory = thornbury_item_factory_generate_widget_with_props (
            MILDENHALL_TYPE_PULLUP_ROLLER, pullup_roller_prop_file);
	GObject *pObject2 = NULL;
	g_object_get(itemFactory, "object", &pObject2, NULL);
        ClutterActor *pPullupRoller = CLUTTER_ACTOR(pObject2);
	clutter_actor_add_child(stage, pPullupRoller);

	/* connect signals */
	g_signal_connect (MILDENHALL_INFO_ROLLER(pInfoRoller), "info-roller-animated", G_CALLBACK (info_pullup_animated_cb), pPullupRoller);
	g_signal_connect (MILDENHALL_INFO_ROLLER(pInfoRoller), "info-roller-up-animation-started", G_CALLBACK (info_up_animation_started_cb), pPullupRoller);
	g_signal_connect (MILDENHALL_INFO_ROLLER(pInfoRoller), "info-roller-down-animation-started", G_CALLBACK (info_down_animation_started_cb), pPullupRoller);
	g_signal_connect (MILDENHALL_INFO_ROLLER(pInfoRoller), "info-roller-item-selected", G_CALLBACK (info_pullup_item_selected_cb), pPullupRoller);

	/* connect signals */
	g_signal_connect (pPullupRoller, "pullup-roller-item-selected", G_CALLBACK (pullup_item_selected_cb), pInfoRoller);
	g_signal_connect (pPullupRoller, "pullup-roller-animated", G_CALLBACK (pullup_animated_cb), pInfoRoller);
	g_signal_connect (pPullupRoller, "pullup-roller-up-animation-started", G_CALLBACK (pullup_up_animation_started_cb), pInfoRoller);
	g_signal_connect (pPullupRoller, "pullup-roller-down-animation-started", G_CALLBACK (pullup_down_animation_started_cb), pInfoRoller);
        /* set header model */
	ThornburyModel *pFooterModel = create_footer_model();
	g_object_set(pPullupRoller, "footer-model", pFooterModel, NULL);
	/* pullup roller model */
	ThornburyModel *model2 = lightwood_create_model (20);
	/* add attributes */
	mildenhall_pullup_roller_add_attribute (MILDENHALL_PULLUP_ROLLER(pPullupRoller), "name", 0);
        //mildenhall_pullup_roller_add_attribute (MILDENHALL_PULLUP_ROLLER(pPullupRoller), "icon", 1);
        mildenhall_pullup_roller_add_attribute (MILDENHALL_PULLUP_ROLLER (pPullupRoller), "label", 2);
        mildenhall_pullup_roller_add_attribute (MILDENHALL_PULLUP_ROLLER (pPullupRoller), "extra-height", 5);
	/* if variable roller, set model after setting the attributes */
	g_object_set(pPullupRoller, "item-type", TYPE_SAMPLE_VARIABLE_ITEM, "model", model2,  NULL);

	/* add overlay */
        overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
            "content_overlay.png",
            NULL);
        actor = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
        clutter_actor_add_child(stage, actor);

	/* show stage */
	clutter_actor_show (stage);
	clutter_main();
        return 0;
}
