/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_info_roller.h"
#include "mildenhall-internal.h"

#include <thornbury/thornbury.h>

#include "mildenhall_meta_info_header.h"
#include "sample-item.h"
#include "sample-variable-item.h"
#include "test-utils.h"

/* header model columns */
enum
{
        COLUMN_LEFT_ICON_TEXT,
        COLUMN_MID_TEXT,
        COLUMN_RIGHT_ICON_TEXT,
        COLUMN_NONE
};

/* creation of model */
ThornburyModel *create_header_model()
{
        g_autofree gchar *music_icon_file = NULL;
        ThornburyModel *model = NULL;
        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_NONE,
                                        G_TYPE_POINTER, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        -1);
        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_AC.png",
            NULL);
        thornbury_model_append (model,
                                        //COLUMN_LEFT_ICON_TEXT, "Time",
                                        COLUMN_LEFT_ICON_TEXT, music_icon_file,
                                        COLUMN_MID_TEXT, "ALBUMS",
                                        COLUMN_RIGHT_ICON_TEXT,NULL,
                                        -1);

        return model;

}

static void item_selected_cb (MildenhallInfoRoller *roller, guint row, gpointer data)
{
        g_print("***************row = %d\n", row);
}

static void down_animation_started_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}

static void up_animation_started_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}

static void animated_cb(MildenhallInfoRoller *roller, gpointer data)
{
	g_print("%s\n", __FUNCTION__);
}


int main (int argc, char **argv)
{
        g_autofree gchar *pImage = NULL;
	g_autofree gchar *overlay_img_file = NULL;

        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;

        stage = clutter_stage_new ();
        pImage = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "background.png",
            NULL);
        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);


	//GObject *pObject = NULL;
        //pObject = g_object_new(ROLLER_TYPE_CONTAINER, "roller-type", MILDENHALL_FIXED_ROLLER_CONTAINER, "width", 277.0, "height", 480.0, "roll-over", TRUE, "reactive", TRUE, "arrow", TRUE, "arrowPosition", 190.0, NULL);
	//clutter_actor_add_child(stage, CLUTTER_ACTOR(pObject));



	/* NOTE: Update the json file for "roller-type" and "item-type" */

	ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_INFO_ROLLER, PKGDATADIR"/mildenhall_info_roller_prop.json");
	GObject *pObject2 = NULL;
	g_object_get(itemFactory, "object", &pObject2, NULL);
        ClutterActor *pActor = CLUTTER_ACTOR(pObject2);
	clutter_actor_add_child(stage, pActor);

	g_signal_connect (G_OBJECT (pActor), "info-roller-item-selected", G_CALLBACK (item_selected_cb), NULL);
	g_signal_connect (G_OBJECT (pActor), "info-roller-animated", G_CALLBACK (animated_cb), NULL);
	g_signal_connect (G_OBJECT (pActor), "info-roller-up-animation-started", G_CALLBACK (up_animation_started_cb), NULL);
	g_signal_connect (G_OBJECT (pActor), "info-roller-down-animation-started", G_CALLBACK (down_animation_started_cb), NULL);


	/* set header model */
	ThornburyModel *pHeaderModel = create_header_model();
	g_object_set(pActor, "header-model", pHeaderModel, NULL);

	ThornburyModel *model = lightwood_create_model (60);

	/* below attributed common for fixed/variable roller */
	mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER(pActor), "name", 0);
        //mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER(pActor), "icon", 1);
        mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER (pActor), "label", 2);


	/* if variable roller */
        //mildenhall_info_roller_add_attribute (MILDENHALL_INFO_ROLLER (pActor), "extra-height", 5);

	g_object_set(pActor, "item-type", TYPE_SAMPLE_VARIABLE_ITEM, "model", model,  NULL);

	overlay_img_file = g_build_filename (_mildenhall_get_theme_path (),
	    "content_overlay.png",
	    NULL);
	/* add overlay */
        actor = thornbury_ui_texture_create_new (overlay_img_file, 0, 0, FALSE, TRUE);
        clutter_actor_add_child(stage, actor);

	/* show stage */
	clutter_actor_show (stage);
	clutter_main();
        return 0;
}
