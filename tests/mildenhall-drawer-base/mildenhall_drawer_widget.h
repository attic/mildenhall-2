/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-drawer-widget.h */

#ifndef _MILDENHALL_DRAWER_WIDGET_H
#define _MILDENHALL_DRAWER_WIDGET_H

#include <glib-object.h>
#include "mildenhall_drawer_base.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_DRAWER_WIDGET mildenhall_drawer_widget_get_type()

#define MILDENHALL_DRAWER_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_DRAWER_WIDGET, MildenhallDrawerWidget))

#define MILDENHALL_DRAWER_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_DRAWER_WIDGET, MildenhallDrawerWidgetClass))

#define MILDENHALL_IS_DRAWER_WIDGET(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_DRAWER_WIDGET))

#define MILDENHALL_IS_DRAWER_WIDGET_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_DRAWER_WIDGET))

#define MILDENHALL_DRAWER_WIDGET_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_DRAWER_WIDGET, MildenhallDrawerWidgetClass))

typedef struct _MildenhallDrawerWidget MildenhallDrawerWidget;
typedef struct _MildenhallDrawerWidgetClass MildenhallDrawerWidgetClass;
typedef struct _MildenhallDrawerWidgetPrivate MildenhallDrawerWidgetPrivate;

struct _MildenhallDrawerWidget
{
  MildenhallDrawerBase parent;

  MildenhallDrawerWidgetPrivate *priv;
};

struct _MildenhallDrawerWidgetClass
{
  MildenhallDrawerBaseClass parent_class;
};

/* model columns */
enum
{
        COLUMN_NAME,
        COLUMN_ICON,
        COLUMN_TOOLTIP_TEXT,
        COLUMN_REACTIVE,
        COLUMN_LAST
};

GType mildenhall_drawer_widget_get_type (void) G_GNUC_CONST;

ClutterActor *mildenhall_drawer_widget_new (void);

G_END_DECLS

#endif /* _MILDENHALL_DRAWER_WIDGET_H */
