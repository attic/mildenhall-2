/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-drawer-widget.c */

#include "mildenhall_drawer_widget.h"
#include "mildenhall-internal.h"

G_DEFINE_TYPE (MildenhallDrawerWidget, mildenhall_drawer_widget, MILDENHALL_DRAWER_TYPE_BASE)

#define DRAWER_WIDGET_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_DRAWER_WIDGET, MildenhallDrawerWidgetPrivate))

/* needs to be part of style.json */
#define CONTEXT_DRAWER_POSITION         660.0,394.0
#define VIEWS_DRAWER_POSITION           660.0,51.0
#define NAVI_DRAWER_POSITION            4.0,394.0


struct _MildenhallDrawerWidgetPrivate
{
	gboolean bDummy;
};

void drawer_opened(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void drawer_closed(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void tooltip_hidden(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}

void tooltip_shown(GObject *pActor, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);

}


void drawer_button_released(GObject *pActor, gchar *pName, gpointer pUserData)
{
        g_print("DRAWER_WIDGET: %s\n", __FUNCTION__);
        g_print("DRAWER_WIDGET: button released = %s\n", pName);

}

static void mildenhall_drawer_widget_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
	switch (property_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void mildenhall_drawer_widget_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	switch (property_id)
	{
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void mildenhall_drawer_widget_dispose (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_drawer_widget_parent_class)->dispose (object);
}

static void mildenhall_drawer_widget_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_drawer_widget_parent_class)->finalize (object);
}

static void mildenhall_drawer_widget_class_init (MildenhallDrawerWidgetClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (MildenhallDrawerWidgetPrivate));

	object_class->get_property = mildenhall_drawer_widget_get_property;
	object_class->set_property = mildenhall_drawer_widget_set_property;
	object_class->dispose = mildenhall_drawer_widget_dispose;
	object_class->finalize = mildenhall_drawer_widget_finalize;
}

static void mildenhall_drawer_widget_init (MildenhallDrawerWidget *self)
{
	self->priv = DRAWER_WIDGET_PRIVATE (self);
# if 0
	g_object_set(self, "type", MILDENHALL_CONTEXT_DRAWER, NULL);
	clutter_actor_set_position(CLUTTER_ACTOR(self), CONTEXT_DRAWER_POSITION);
# endif

# if 1
	g_object_set(self, "type", MILDENHALL_NAVI_DRAWER, NULL);
	clutter_actor_set_position(CLUTTER_ACTOR(self), NAVI_DRAWER_POSITION);
# endif

# if 0
	g_object_set(self, "type", MILDENHALL_VIEWS_DRAWER, NULL);
	clutter_actor_set_position(CLUTTER_ACTOR(self), VIEWS_DRAWER_POSITION);
# endif

}

ClutterActor *mildenhall_drawer_widget_new (void)
{
	return g_object_new (MILDENHALL_TYPE_DRAWER_WIDGET, NULL);
}


/* creation of model */
ThornburyModel *create_model()
{
        g_autofree gchar *music_icon_file = NULL;
        g_autofree gchar *artist_icon_file = NULL;
        g_autofree gchar *internet_icon_file = NULL;

        ThornburyModel *model = NULL;

        model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_STRING, NULL,
                                        G_TYPE_BOOLEAN, NULL,
                                        -1);
        music_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_AC.png",
            NULL);

        artist_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_music_artists_AC.png",
            NULL);

        internet_icon_file = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "icon_internet_AC.png",
            NULL);

        thornbury_model_append (model, COLUMN_NAME, "MUSIC",
                                        COLUMN_ICON, music_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "MUSIC",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME, "ARTISTS",
                                        COLUMN_ICON, artist_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "ARTIST",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model,  COLUMN_NAME, "INTERNET",
                                        COLUMN_ICON, internet_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "INTERNET",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);
        thornbury_model_append (model, COLUMN_NAME, "ALBUMS",
                                        COLUMN_ICON, music_icon_file,
                                        COLUMN_TOOLTIP_TEXT, "ALBUMS",
                                        COLUMN_REACTIVE, TRUE,
                                        -1);


        return model;

}
int main (int argc, char **argv)
{
        g_autofree gchar *pImage = NULL;
        int clInErr = clutter_init(&argc, &argv);
        if (clInErr != CLUTTER_INIT_SUCCESS)
                return -1;

        ClutterActor *stage;
        //ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

        stage = clutter_stage_new ();
        //clutter_actor_set_background_color (stage, &black);
        pImage = g_build_filename (_mildenhall_get_theme_path (),
            "test-drawer-base",
            "background.png",
            NULL);

        //ClutterActor *actor = clutter_texture_new_from_file("background.png", NULL);
        ClutterActor *actor = thornbury_ui_texture_create_new(pImage, 0, 0, FALSE, FALSE);
        clutter_actor_add_child(stage, actor);
        clutter_actor_set_size (stage, 728, 480);
        g_signal_connect (stage, "destroy", G_CALLBACK (clutter_main_quit), NULL);

# if 0
        /* context drawer */
        ClutterActor *pContextDrawer = mildenhall_drawer_widget_new();
        ThornburyModel *model1 = create_model();
        g_object_set(pContextDrawer, "model", model1, NULL);
        clutter_actor_add_child(stage, pContextDrawer);

	g_signal_connect(pContextDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        g_signal_connect(pContextDrawer, "drawer-close", G_CALLBACK(drawer_closed), NULL);
        g_signal_connect(pContextDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pContextDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);
        g_signal_connect(pContextDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);
# endif
# if 1
        /* navi drawer */
        ClutterActor *pNaviDrawer = mildenhall_drawer_widget_new();
        ThornburyModel *model2 = create_model();
        g_object_set(pNaviDrawer, "model", model2, NULL);
        clutter_actor_add_child(stage, pNaviDrawer);

        g_signal_connect(pNaviDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        g_signal_connect(pNaviDrawer, "drawer-close", G_CALLBACK(drawer_closed), NULL);
        g_signal_connect(pNaviDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);
	g_signal_connect(pNaviDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pNaviDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);

#endif
# if 0
        ClutterActor *pViewsDrawer = mildenhall_drawer_widget_new();
        ThornburyModel *model3 = create_model();
        g_object_set(pViewsDrawer, "model", model3, NULL);
        clutter_actor_add_child(stage, pViewsDrawer);

        g_signal_connect(pViewsDrawer, "drawer-open", G_CALLBACK(drawer_opened), NULL);
        g_signal_connect(pViewsDrawer, "drawer-close", G_CALLBACK(drawer_closed), NULL);
        g_signal_connect(pViewsDrawer, "drawer-button-released", G_CALLBACK(drawer_button_released), NULL);
	g_signal_connect(pViewsDrawer, "drawer-tooltip-hidden", G_CALLBACK(tooltip_hidden), NULL);
        g_signal_connect(pViewsDrawer, "drawer-tooltip-shown", G_CALLBACK(tooltip_shown), NULL);

# endif
        clutter_actor_show (stage);
	clutter_main();
        return 0;
}
