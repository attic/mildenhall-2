/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-internal.h"
#include "popup_info.h"

#include <thornbury/thornbury.h>

typedef enum _enModelInfo enModelInfo;
typedef enum _enPopupType enPopupType;
gboolean shown_clb(PopupInfo *actor,gpointer data);
gboolean hidden_clb(PopupInfo *actor,gpointer data);
gboolean action_clb(PopupInfo *actor,gchar *button,gpointer value,gpointer data);
enum _enPopupType
{
    POPUP_TYPE_ENUM_FIRST,
    POPUP_TYPE_ENUM_INFO_OR_ERROR,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_TEXT,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_TEXT,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_TEXT,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_ONE_WITH_IMAGE,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_TWO_WITH_IMAGE,
    POPUP_TYPE_ENUM_CONFIRM_OPTION_THREE_WITH_IMAGE,
    POPUP_TYPE_ENUM_CONFIRM_WITH_RATING,
    POPUP_TYPE_ENUM_WITH_RECORDING_STATUS,
    POPUP_TYPE_ENUM_WITH_VOICECONTROL,
    POPUP_TYPE_ENUM_WITH_READ_AND_CONFIRM,
    POPUP_TYPE_ENUM_FOR_IM_WITH_WEBCAM,
    POPUP_TYPE_ENUM_FOR_PHONE_WITH_TIME,
    POPUP_TYPE_ENUM_FOR_OTHER_TYPE

};
/* model columns */
enum _enModelInfo
{
    MSGSTYLE,
    MSGTEXT,
    BUTTONNUM,
    BUTTONTEXT,
    BUTTONIMAGE,
    IMAGEICON,
    IMAGEPATH,
    TIMER,
    COLUMN_LAST

};


static gboolean
show_popup (PopupInfo *actor)
{
    popup_info_show (actor);
    return FALSE;
}

int main (int argc, char **argv)
{
    g_autofree gchar *request_inactive_icon_file = NULL;
    g_autofree gchar *prop_file = NULL;
    ThornburyItemFactory *itemFactory;
    int clInErr = clutter_init(&argc, &argv);
    if (clInErr != CLUTTER_INIT_SUCCESS)
        return -1;
    gdouble timeout;

    ThornburyModel *model=NULL;
    model = (ThornburyModel*)thornbury_list_model_new (COLUMN_LAST, G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_STRING, NULL,
                                  G_TYPE_INT, NULL,
                                  -1);

    ClutterActor *pStage;
//    ClutterColor black = { 0x00, 0x00, 0x00, 0x00 };

    pStage = clutter_stage_new();

    clutter_actor_set_size (pStage, 728, 480);
    prop_file = _mildenhall_get_resource_path ("mildenhall_popup_prop.json");
    itemFactory = thornbury_item_factory_generate_widget_with_props (
        POPUP_TYPE_INFO, prop_file);
 GObject *pObject = NULL;
g_object_get(itemFactory, "object", &pObject, NULL);
    PopupInfo *actor= POPUP_INFO(pObject);
    g_signal_connect(actor,"popup-shown",(GCallback)shown_clb,NULL);
    g_signal_connect(actor,"popup-hidden",(GCallback)hidden_clb,NULL);
    g_signal_connect(actor,"popup-action",(GCallback)action_clb,NULL);

    request_inactive_icon_file = g_build_filename (_mildenhall_get_theme_path (),
        "icon_request_inactive.png",
        NULL); 
    thornbury_model_append (model, MSGSTYLE, "Text-Active",
                        MSGTEXT, "RATE THE APP",
                        IMAGEICON,"AppIcon",
                        BUTTONTEXT,"R A T E",
                        BUTTONNUM,"BUTTON1",
                        IMAGEPATH, request_inactive_icon_file,
                        TIMER, 10,
                        -1);
    thornbury_model_append (model,
        BUTTONTEXT, "N O",
        BUTTONNUM, "BUTTON2",
        IMAGEICON, "MsgIcon",
        IMAGEPATH, request_inactive_icon_file,
        -1);

    thornbury_model_append (model, BUTTONTEXT,"T H I R D",
                        -1);
    //  g_object_set(actor,"timeout",8.0,NULL);
      g_object_set(actor,"model",model,NULL);
    // g_object_set(actor,"popupinfo-type",POPUP_TYPE_ENUM_WITH_VOICECONTROL,NULL);

    g_object_get(actor,"timeout",&timeout,NULL);
    g_print("timeout is %lf",timeout);

    clutter_actor_add_child(pStage,CLUTTER_ACTOR(actor));
    g_timeout_add_seconds (timeout, show_popup, actor);




    clutter_actor_show (pStage);

    clutter_main();

    return 0;

}


gboolean shown_clb(PopupInfo *actor,gpointer data)
{

g_print("\nshown\n");
return FALSE;
}

gboolean hidden_clb(PopupInfo *actor,gpointer data)
{

g_print("\n hidden \n");
return FALSE;
}

gboolean action_clb(PopupInfo *actor,gchar *button,gpointer value,gpointer data)
{

g_print("\n button %s\n",button);
g_print("\n value %d \n",GPOINTER_TO_INT(value));
return FALSE;
}


