/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH <sdk.support@de.bosch.com>
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <thornbury/thornbury.h>

#ifndef __TEST_UTILS_H__
#define __TEST_UTILS_H__

G_BEGIN_DECLS

enum
{
  COLUMN_NAME,
  COLUMN_ICON,
  COLUMN_LABEL,
  COLUMN_TOGGLE,
  COLUMN_VIDEO,
  COLUMN_EXTRA_HEIGHT,
  COLUMN_COVER,
  COLUMN_THUMB,
  COLUMN_LONG_TEXT,

  COLUMN_LAST
};

ThornburyModel * lightwood_create_model (guint num_of_items);

G_END_DECLS

#endif /* __TEST_UTILS_H__ */
