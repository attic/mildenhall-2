/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "testvm.h"
#include "canterbury.h"

CanterburyAppManager *app_mgr_proxy = NULL;
static void
app_mgr_name_appeared (GDBusConnection *connection,
                       const gchar     *name,
                       const gchar     *name_owner,
                       gpointer         user_data);


static void
app_mgr_name_vanished( GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data);

static void
testvm_register_clb( GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data);

static void
new_app_status( CanterburyAppManager  *object,
                const gchar             *app_name,
                gint                     new_app_state,
                GVariant                *arguments,
                gpointer                 user_data);


void initialize_app_manager_client_handler(ClutterActor* actor)
{
  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Canterbury",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    app_mgr_name_appeared,
                    app_mgr_name_vanished,
                    actor,
                    NULL);

}

static void
app_mgr_proxy_clb( GObject *source_object,
                   GAsyncResult *res,
                   gpointer user_data)
{
 GError *error;

  /* finishes the proxy creation and gets the proxy ptr */
 app_mgr_proxy = canterbury_app_manager_proxy_new_finish(res , &error);

 if(app_mgr_proxy == NULL)
 {
   g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
   return;
 }

  /* register for app manager signals */
  g_signal_connect (app_mgr_proxy,
                    "new-app-state",
                    G_CALLBACK (new_app_status),
                    user_data);

  canterbury_app_manager_call_register_my_app(app_mgr_proxy ,
    testvm_APP_NAME , 0, NULL ,
    testvm_register_clb , user_data);
}



static void
app_mgr_name_appeared (GDBusConnection *connection,
                       const gchar     *name,
                       const gchar     *name_owner,
                       gpointer         user_data)
{

  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_app_manager_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "org.apertis.Canterbury",
                                          "/org/apertis/Canterbury/AppManager",
                                          NULL,
                                          app_mgr_proxy_clb,
                                          user_data);
}


static void
app_mgr_name_vanished( GDBusConnection *connection,
                       const gchar     *name,
                       gpointer         user_data)
{

 if(NULL != app_mgr_proxy)
   g_object_unref(app_mgr_proxy);
}

static void
new_app_status( CanterburyAppManager  *object,
                const gchar             *app_name,
                gint                     new_app_state,
                GVariant                *arguments,
                gpointer                 user_data)
{
  if( g_strcmp0(testvm_APP_NAME ,app_name ) == FALSE )
  {
    switch (new_app_state)
    {
      case CANTERBURY_APP_STATE_START:
       g_print("start hello world ");
       break;

     case CANTERBURY_APP_STATE_BACKGROUND:
      /* see if it can do a clutter actor hide */
      break;

     case CANTERBURY_APP_STATE_SHOW:
      /* check if the application needs to resynchronize with its server */
      break;

     case CANTERBURY_APP_STATE_RESTART:
      /* check if the application needs to resynchronize with its server */
      break;

     case CANTERBURY_APP_STATE_OFF:
      /* store all persistence infornmation immediately.. */
      break;

     case CANTERBURY_APP_STATE_PAUSE:
      /* store all persistence infornmation immediately..*/
      break;

     default:
      g_printerr("hello world app in unknown state ???  \n" );
      break;
    }
  }
}

static void
testvm_register_clb( GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
  gboolean return_val;
  GError *error = NULL;

  return_val = canterbury_app_manager_call_register_my_app_finish(app_mgr_proxy , res , &error);

  if(return_val == FALSE)
  {
     gchar* error_msg = g_dbus_error_get_remote_error(error);
     g_printerr("error %s \n",error_msg);
     g_free(error_msg);

     g_dbus_error_strip_remote_error(error);
     g_printerr("error %s \n",error->message);
     return;
  }
}
