/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall_loading_bar.h"

#include <stdio.h>
#include <thornbury/thornbury.h>

static gboolean b_set_loading_bar_current_duration(gpointer data);

/*********************************************************************************************
 * Function:    main
 * Description: Main function
 * Parameters:  argc and argv[]
 * Return:      int
 ********************************************************************************************/
int main(int argc, char *argv[])
{
	ClutterActor *pStage;
	ClutterColor pStageColor = {0x00, 0x00, 0x00, 0xff};

	int clInErr = clutter_init(&argc, &argv);
	if (clInErr != CLUTTER_INIT_SUCCESS)
		return -1;
	ThornburyItemFactory *itemFactory = thornbury_item_factory_generate_widget_with_props(MILDENHALL_TYPE_LOADING_BAR,PKGDATADIR"/mildenhall_loading_bar_prop.json");
	GObject *pObject = NULL;
	g_object_get(itemFactory, "object", &pObject, NULL);
	MildenhallLoadingBar *loadingBar=MILDENHALL_LOADING_BAR(pObject);
	pStage = clutter_stage_new();
	clutter_actor_set_background_color(CLUTTER_ACTOR(pStage), &pStageColor);
	clutter_actor_set_size(pStage, 800, 600);
	clutter_actor_add_child(CLUTTER_ACTOR(pStage), CLUTTER_ACTOR(loadingBar));
	clutter_actor_set_position(CLUTTER_ACTOR(loadingBar), 30, 200);
	g_timeout_add_seconds(1,b_set_loading_bar_current_duration,(gpointer)pObject);
	g_print("After calling");
	clutter_actor_show(pStage);
	clutter_main();
	return 0;
}
gfloat fDuration=0.0;

/*********************************************************************************************
 * Function:    b_set_loading_bar_current_duration
 * Description: Used to set the current duration property for loading bar.
 * Parameters:  The object's reference
 * Return:      void
 ********************************************************************************************/
static gboolean b_set_loading_bar_current_duration(gpointer pData)
{
fDuration = fDuration + 0.01;
MildenhallLoadingBar *pLoadingBar=MILDENHALL_LOADING_BAR(pData);
if(fDuration<1.0)
{
g_object_set(pLoadingBar,"loading-bar-pos",fDuration,NULL);
}
else
{
g_object_set(pLoadingBar,"loading-bar-show",FALSE,NULL);
return FALSE;
}
return TRUE;
}
/* *********************************************************************
                        End of file
 *********************************************************************** */

