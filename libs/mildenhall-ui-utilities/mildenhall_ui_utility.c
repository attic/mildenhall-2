/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <clutter/gdk/clutter-gdk.h>

#include "mildenhall-internal.h"
#include "mildenhall_ui_utility.h"

#ifndef MILDENHALL_DISABLE_DEPRECATED

/**
 * SECTION:mildenhall-ui-utilities
 * @short_description: Abstraction layer for a set of clutter APIs
 * @title: MILDENHALL UI UTILITIES
 * @stability: stable
 * @include: mildenhall_ui_utilities.h
 *
 * Abstraction layer for a set of clutter APIs
 *
 * Deprecated: 0.1612.2
 */
gint initialize_ui_toolkit(gint argc, gchar **argv )
{
  clutter_set_windowing_backend("gdk");

	if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
		return -1;

  return CLUTTER_INIT_SUCCESS;
}

ClutterActor* create_mildenhall_window(gchar *app_name)
{
  return mildenhall_stage_new (app_name);
}

#endif /* MILDENHALL_DISABLE_DEPRECATED */

ClutterActor* mildenhall_ui_utils_create_application_background()
{
  GError *err = NULL;
  ClutterActor *bg = clutter_texture_new_from_file (PKGTHEMEDIR "/background.png", &err);
	if (err)
	{
		    g_warning ("unable to load texture: %s", err->message);
		    g_clear_error (&err);
	}
	clutter_actor_set_position(bg,0.0,0.0);
  clutter_actor_set_name(bg,"background");
  return bg;
}

ClutterActor* mildenhall_ui_utils_create_application_overlay()
{
  GError *err = NULL;
  ClutterActor *overlay = clutter_texture_new_from_file (PKGTHEMEDIR "/content_overlay.png", &err);
	if (err)
	{
		    g_warning ("unable to load texture: %s", err->message);
		    g_clear_error (&err);
	}
	clutter_actor_set_position(overlay,0.0,0.0);
  clutter_actor_set_name(overlay,"overlay");
  return overlay;
}

ClutterActor* mildenhall_ui_utils_create_application_background_with_overlay()
{
  GError *err = NULL;
  ClutterActor *bg = clutter_texture_new_from_file (PKGTHEMEDIR "/app-bkg-with-overlay.png", &err);
	if (err)
	{
		    g_warning ("unable to load texture: %s", err->message);
		    g_clear_error (&err);
	}
	clutter_actor_set_position(bg,0.0,0.0);
  clutter_actor_set_name(bg,"background");
  return bg;
}

ClutterActor* mildenhall_ui_utils_create_application_roller_background()
{
  GError *err = NULL;
  ClutterActor *bg = clutter_texture_new_from_file (PKGTHEMEDIR "/normal-app-bkg.png", &err);
	if (err)
	{
		    g_warning ("unable to load texture: %s", err->message);
		    g_clear_error (&err);
	}
	clutter_actor_set_position(bg,0.0,0.0);
  clutter_actor_set_name(bg,"background");
  return bg;
}

/**
 * mildenhall_stage_new:
 *
 * Create a new #ClutterStage returned by clutter_stage_new()
 *
 * A newly created @stage has mildenhall-specific parameters.
 *  - size (w: 728, h: 480)
 *  - color { 0x00, 0x00, 0x00, 0xff }
 * 
 * Return value: a new stage, or %NULL if the default backend does
 *   not support multiple stages. Refer to clutter_stage_new().
 *
 * Since: 0.1612.2
 */
ClutterActor *
mildenhall_stage_new (const gchar *name)
{
  ClutterColor stage_color = { 0x00, 0x00, 0x00, 0xff };
  ClutterActor* stage;
  GdkWindow *gdk_window;

  g_set_prgname (name);

  stage = clutter_stage_new ();

  if (stage == NULL)
    return NULL;

  /* Disable throttling of motion events because it causes touchscreen scrolling
   * to be jerky on embedded devices with low frame rates.
   *
   * See: https://bugs.apertis.org/show_bug.cgi?id=401 */
  clutter_stage_set_throttle_motion_events (CLUTTER_STAGE (stage), FALSE);
  clutter_actor_set_size (stage, 728, 480);
  clutter_actor_set_background_color (CLUTTER_ACTOR (stage), &stage_color);
  clutter_actor_show (stage);

  gdk_window = clutter_gdk_get_stage_window (stage);
  gdk_window_set_decorations (gdk_window, 0);

  return stage;
}

const gchar *
_mildenhall_get_theme_path (void)
{
  static gchar *mildenhall_theme_path = NULL;

  if (g_once_init_enter (&mildenhall_theme_path))
    {
      const gchar *path = g_getenv ("MILDENHALL_THEME_PATH");

      if (path == NULL || path[0] == '\0')
        path = PKGTHEMEDIR;

      g_once_init_leave (&mildenhall_theme_path, g_strdup (path));
    }

  return mildenhall_theme_path;
}

gchar *
_mildenhall_get_resource_path (const gchar *path)
{
  g_autofree gchar *resource_path = NULL;
  const gchar * const *dirs = g_get_system_data_dirs ();
  int i;

  for (i = 0; dirs[i] != NULL; i++)
    {
      resource_path = g_build_filename (
        dirs[i], "mildenhall", path,
        NULL);
      if (g_file_test (resource_path, G_FILE_TEST_EXISTS))
        break;
    }

  return g_steal_pointer (&resource_path);
}
