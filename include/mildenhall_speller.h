/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller.h
 *
 *
 * mildenhall_speller.h */

#ifndef __MILDENHALL_SPELLER_H__
#define __MILDENHALL_SPELLER_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"
#include <unistd.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <string.h>
#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "mildenhall_speller_multiple_entry.h"
#include "mildenhall_speller_multi_line_entry.h"
#include "mildenhall_speller_four_toggle_entry.h"
#include "mildenhall_speller_five_toggle_entry.h"
#include "mildenhall_speller_three_toggle_entry.h"
#include "mildenhall_speller_default_entry.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER mildenhall_speller_get_type()

#define MILDENHALL_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER, MildenhallSpeller))

#define MILDENHALL_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER, MildenhallSpellerClass))

#define MILDENHALL_IS_SPELLER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER))

#define MILDENHALL_IS_SPELLER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER))

#define MILDENHALL_SPELLER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER, MildenhallSpellerClass))

/*
#define MILDENHALL_EBAY_LAYOUT    "/mildenhall_button_ebay_style.json"
#define MILDENHALL_PHONE_LAYOUT   "/mildenhall_button_phone_style.json"
#define MILDENHALL_SPELLER_LAYOUT "/mildenhall_button_autocomplete_speller_style.json"
*/

#define MILDENHALL_SPELLER_PRINT(...) //g_print(__VA_ARGS__)

typedef struct _MildenhallSpeller MildenhallSpeller;
typedef struct _MildenhallSpellerClass MildenhallSpellerClass;
typedef struct _MildenhallSpellerPrivate MildenhallSpellerPrivate;

/**
 * MildenhallSpeller:
 *
 * The #MildenhallSpeller struct contains only private data.
 *
 */
struct _MildenhallSpeller
{
  ClutterActor parent;
  MildenhallSpellerPrivate *priv;
};

struct _MildenhallSpellerClass
{
  ClutterActorClass parent_class;

  void (*shown)                         (MildenhallSpeller *self);
  void (*hidden)                        (MildenhallSpeller *self);
  void (*go_pressed)                    (MildenhallSpeller *self,
                                         const GVariant *variant);
  void (*key_pad_pressed)               (MildenhallSpeller *self,
                                         guint keypad,
                                         const gchar *keyname);
  void (*entry_status)                  (MildenhallSpeller *self,
                                         const gchar *button_name,
                                         const gchar *button_id);
};

/* Speller state machine status */
typedef enum
{
  MILDENHALL_SPELLER_SHOW_REQUEST,
  MILDENHALL_SPELLER_SHOWN,
  MILDENHALL_SPELLER_HIDE_REQUEST,
  MILDENHALL_SPELLER_HIDDEN,
  MILDENHALL_SPELLER_LAST
} MildenhallSpellerState;

/* Speller entry types */
typedef enum
{
  MILDENHALL_SPELLER_TYPE_NONE = -1,
  MILDENHALL_SPELLER_NO_ENTRY,
  MILDENHALL_SPELLER_DEFAULT_ENTRY,
  MILDENHALL_SPELLER_THREE_TOGGLE_ENTRY,
  MILDENHALL_SPELLER_FOUR_TOGGLE_ENTRY,
  MILDENHALL_SPELLER_FIVE_TOGGLE_ENTRY,
  MILDENHALL_SPELLER_MULTILINE_ENTRY,
  MILDENHALL_SPELLER_MULTIPLE_ENTRY
} MildenhallSpellerEntryType;

/**
 * MildenhallSpellerModelColums:
 * @MILDENHALL_SPELLER_COLUMN_LEFT_IS_TEXT: G_TYPE_BOOLEAN,If TRUE then it is text else Icon.
 * @MILDENHALL_SPELLER_COLUMN_LEFT_WIDTH: G_TYPE_FLOAT,button width
 * @MILDENHALL_SPELLER_COLUMN_LEFT_ID: G_TYPE_STRING,Name of the button
 * @MILDENHALL_SPELLER_COLUMN_LEFT_INFO: G_TYPE_POINTER,It is an Gvariant type,give key as name and value as data on the button to display(text /icon path)
 * @MILDENHALL_SPELLER_COLUMN_RIGHT_IS_TEXT: G_TYPE_BOOLEAN,If TRUE then it is text else Icon. 
 * @MILDENHALL_SPELLER_COLUMN_RIGHT_WIDTH: G_TYPE_FLOAT,button width. 
 * @MILDENHALL_SPELLER_COLUMN_RIGHT_ID: G_TYPE_STRING,Name of the button
 * @MILDENHALL_SPELLER_COLUMN_RIGHT_INFO: G_TYPE_POINTER,It is an Gvariant type,give key as name and value as data on the button to display(text /icon path)
 * @MILDENHALL_SPELLER_COLUMN_TEXT_BOX_DEFAULT_TEXT: G_TYPE_STRING,Default text to be shown in entry box.
 * @MILDENHALL_SPELLER_COLUMN_ENTRY_ID: G_TYPE_STRING,unique name to entry box.
 *
 *   		---------------------------------------------------------------------------------------------------------
 *   		| SPELLER_COLUMN_LEFT_IS_TEXT	|SPELLER_COLUMN_LEFT_WIDTH	|...........	|SPELLER_COLUMN_ENTRY_ID|
 *   		| (G_TYPE_BOOLEAN)(col 0)	|(G_TYPE_FLOAT)(col 1) 	  	|...........	|(G_TYPE_STRING)(col n) |
 *  		|-------------------------------|-------------------------------|---------------|-----------------------|
 *ROWS->   	|-------------------------------|-------------------------------|---------------|-----------------------|
 *   		|-------------------------------|-------------------------------|---------------|-----------------------|
 *   		|-------------------------------|-------------------------------|---------------|-----------------------|
 *   		|-------------------------------|-------------------------------|---------------|-----------------------|
 *
 * Model has to be created using these enumerations.
 * Example:
 * ThornburyModel * model = thornbury_list_model_new(SPELLER_COLUMN_LAST,G_TYPE_BOOLEAN,0,
 *					G_TYPE_FLOAT , 0,
 *                                   	G_TYPE_STRING, NULL,
 *                                   	G_TYPE_POINTER, NULL,
 *                                   	G_TYPE_BOOLEAN ,0,
 *                                   	G_TYPE_FLOAT , 0,
 *                                   	G_TYPE_STRING, NULL,
 *                                   	G_TYPE_POINTER, NULL,
 *                                   	G_TYPE_STRING, NULL,
 *                                   	G_TYPE_STRING, NULL,
 *                                    -	1);
 *----------------------------------------------------------------------------------------------
 *  To fill SPELLER_COLUMN_LEFT_INFO column of model data should be created in the mentioned way:
 *----------------------------------------------------------------------------------------------
 *   GVariantBuilder *pRowValues = NULL;
 *   pRowValues = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
 *   g_variant_builder_add(pRowValues, "{ss}","SEARCH","SEARCH");
 *   g_variant_builder_add(pRowValues, "{ss}","URL","URL" );
 *   GVariant *p = g_variant_builder_end(pRowValues);
 *
 *----------------------------------------------------------------------------------------------
 *  To fill SPELLER_COLUMN_RIGHT_INFO column of model data should be created in the mentioned way:
 *----------------------------------------------------------------------------------------------
 *   GVariantBuilder *pRowValues1 = NULL;
 *   pRowValues1 = g_variant_builder_new( G_VARIANT_TYPE_ARRAY );
 *   g_variant_builder_add(pRowValues1, "{ss}", "album", "/test-drawer-base/icon_music_artists_AC.png");
 *   g_variant_builder_add(pRowValues1, "{ss}", "artist", "/test-drawer-base/icon_internet_AC.png" );
 *   GVariant *q = g_variant_builder_end(pRowValues1);
 *-----------------------------
 * Add the data as shown below 
 *-----------------------------
 * thornbury_model_append (model,
 *			MILDENHALL_COLUMN_LEFT_IS_TEXT ,TRUE,
 *                         MILDENHALL_COLUMN_LEFT_WIDTH ,158.0,
 *                         MILDENHALL_COLUMN_LEFT_ID ,g_strdup("BUTTON1"),
 *                         MILDENHALL_COLUMN_LEFT_INFO, (gpointer)p,
 *                         MILDENHALL_COLUMN_RIGHT_IS_TEXT ,FALSE,
 *                         MILDENHALL_COLUMN_RIGHT_WIDTH ,58.0,
 *                         MILDENHALL_COLUMN_RIGHT_ID ,g_strdup("BUTTON2"),
 *                         MILDENHALL_COLUMN_RIGHT_INFO, (gpointer)q,
 *                         MILDENHALL_COLUMN_TEXT_BOX_DEFAULT_TEXT, "",
 *                         MILDENHALL_COLUMN_ENTRY_ID ,g_strdup("entry"),
 *                         -1);
 **/
typedef enum
{
  MILDENHALL_SPELLER_COLUMN_LEFT_IS_TEXT,
  MILDENHALL_SPELLER_COLUMN_LEFT_WIDTH,
  MILDENHALL_SPELLER_COLUMN_LEFT_ID,
  MILDENHALL_SPELLER_COLUMN_LEFT_INFO,
  MILDENHALL_SPELLER_COLUMN_RIGHT_IS_TEXT,
  MILDENHALL_SPELLER_COLUMN_RIGHT_WIDTH,
  MILDENHALL_SPELLER_COLUMN_RIGHT_ID,
  MILDENHALL_SPELLER_COLUMN_RIGHT_INFO,
  MILDENHALL_SPELLER_COLUMN_TEXT_BOX_DEFAULT_TEXT,
  MILDENHALL_SPELLER_COLUMN_ENTRY_ID,
  MILDENHALL_SPELLER_COLUMN_LAST
} MildenhallSpellerModelColumns;

GType mildenhall_speller_get_type (void) G_GNUC_CONST;

/*function to create new speller */
MildenhallSpeller *mildenhall_speller_new (void);

/*function to show speller */
void mildenhall_speller_show (ClutterActor *speller , gboolean animate);

/*function to hide speller*/
void mildenhall_speller_hide (ClutterActor *speller , gboolean animate);

void mildenhall_speller_set_state               (MildenhallSpeller *self,
                                                 gint state);

void mildenhall_speller_set_yoffset             (MildenhallSpeller *self,
                                                 gfloat yoffset);

void mildenhall_speller_set_hidden              (MildenhallSpeller *self,
                                                 gboolean hide);

void mildenhall_speller_set_shown               (MildenhallSpeller *self,
                                                 gboolean show);

void mildenhall_speller_set_history_support     (MildenhallSpeller *self,
                                                 gboolean support);

void mildenhall_speller_set_entry               (MildenhallSpeller *self,
                                                 MildenhallSpellerEntryType entry_type);

void mildenhall_speller_set_layout_state        (MildenhallSpeller *self,
                                                 const gchar *layout_state);

void mildenhall_speller_set_layout              (MildenhallSpeller *self,
                                                 const gchar *layout);

void mildenhall_speller_clear_text              (MildenhallSpeller *self);

void v_speller_set_cursor_to_focus              (MildenhallSpeller *pSelf,
                                                 gint inSpellerFocusEntry);

G_END_DECLS
#endif /* __MILDENHALL_SPELLER_H__ */
