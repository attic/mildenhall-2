/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_speller_default_entry.h
 *
 *
 * mildenhall_speller_default_entry.h */

#ifndef __MILDENHALL_SPELLER_DEFAULT_ENTRY_H__
#define __MILDENHALL_SPELLER_DEFAULT_ENTRY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include "mildenhall_speller.h"
#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall_toggle_button.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY mildenhall_speller_default_entry_get_type()

#define MILDENHALL_SPELLER_DEFAULT_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, MildenhallSpellerDefaultEntry))

#define MILDENHALL_SPELLER_DEFAULT_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, MildenhallSpellerDefaultEntryClass))

#define MILDENHALL_IS_SPELLER_DEFAULT_ENTRY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY))

#define MILDENHALL_IS_SPELLER_DEFAULT_ENTRY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY))

#define MILDENHALL_SPELLER_DEFAULT_ENTRY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_SPELLER_DEFAULT_ENTRY, MildenhallSpellerDefaultEntryClass))

typedef struct _MildenhallSpellerDefaultEntry MildenhallSpellerDefaultEntry;
typedef struct _MildenhallSpellerDefaultEntryClass MildenhallSpellerDefaultEntryClass;
typedef struct _MildenhallSpellerDefaultEntryPrivate MildenhallSpellerDefaultEntryPrivate;


/**
 * MildenhallSpellerDefaultEntry:
 *
 * The #MildenhallSpellerDefaultEntry struct contains only private data.
 *
 */
struct _MildenhallSpellerDefaultEntry
{
    ClutterActor parent;
    MildenhallSpellerDefaultEntryPrivate *priv;
};


/**
 * MildenhallSpellerDefaultEntryClass:
 *
 *@state_change : class handler for the #MildenhallSpellerDefaultEntryClass::state_change signal
 * The #MildenhallSpellerDefaultEntryClass struct contains only private data.
 *
 */
struct _MildenhallSpellerDefaultEntryClass
{
    ClutterActorClass parent_class;
    void (* state_change) (GObject *pButton, gchar *pButtonName ,gchar *pButtonId);
};

GType mildenhall_speller_default_entry_get_type (void) G_GNUC_CONST;

/*function to create new default entry */
ClutterActor *mildenhall_speller_default_entry_new (void);
void v_default_speller_set_text(MildenhallSpellerDefaultEntry *pSelf, GVariant *pArgList);
void v_default_speller_set_clear_text(MildenhallSpellerDefaultEntry *pSelf, gboolean clearText);
void v_default_speller_set_entry_editable(MildenhallSpellerDefaultEntry *pSelf, gboolean editable);
void v_default_speller_set_enable_history(MildenhallSpellerDefaultEntry *pSelf, gboolean historySupport);
void v_default_speller_set_model(MildenhallSpellerDefaultEntry *pSelf, ThornburyModel *pModel);
void v_speller_remove_model_data (ThornburyModel *pModel);
void v_speller_set_text (MildenhallSpellerDefaultEntry *pSelf, gchar *pText);
void v_default_speller_set_mark_text (MildenhallSpellerDefaultEntry *pSelf, gboolean enable_textsel);
void v_default_speller_column_changed_cb (gint col, gpointer userdata);
gboolean cap_event_cb (ClutterActor *actor, ClutterEvent *event, gpointer data);
ThornburyModel *create_text_box_model (void);
ThornburyModel *create_toggle_model (void);


G_END_DECLS

#endif /* __MILDENHALL_SPELLER_DEFAULT_ENTRY_H__ */
