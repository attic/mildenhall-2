/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef __MILDENHALL_POPUP_STRINGS_H__
#define __MILDENHALL_POPUP_STRINGS_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

G_BEGIN_DECLS

#define MILDENHALL_POPUP_GLOBAL_TEXT_NO_CONNECTIVITY_ERROR "No_Connectivity"
#define MILDENHALL_POPUP_GLOBAL_TEXT_NO_INFORMATION_ERROR  "No_Information_Available"
#define MILDENHALL_POPUP_GLOBAL_SEARCH_QUERY_STRING  "What are you Searching for?"
#define MILDENHALL_POPUP_VOICENOTE_RECORDING_STRING "Recording"
#define MILDENHALL_POPUP_VOICENOTE_RECORDING_ERROR "cannot_record"
#define MILDENHALL_POPUP_VOICENOTE_RECORDING_RENAME_STRING "Do you want to rename your new voicenote ?"
#define MILDENHALL_POPUP_VOICENOTE_RECORDING_DELETE_STRING "Do you want to delete Voice Note?"
#define MILDENHALL_POPUP_VOICENOTE_RECORDING_DELETE_ERROR "Can not delete playing Voice Note !"
#define MILDENHALL_POPUP_VOICENOTE_SERACH_ERROR "VoiceNote_not_found"
#define MILDENHALL_POPUP_VOICENOTE_SERACH_RENAME_ERROR_1 "Voicenote_name_is_invalid"
#define MILDENHALL_POPUP_VOICENOTE_SERACH_RENAME_ERROR_2 "Voicenote_already_exists"
#define MILDENHALL_POPUP_MONEYCONVERT_CONVERSION_ERROR    "Money_convert_conversion_error"
#define MILDENHALL_POPUP_MONEYCONVERT_INFO_STRING         "Money_convert_info_string"
#define MILDENHALL_POPUP_APPSTORE_SETTINGS_USER_ACCOUNT_STRING "Create a new user account ?"
#define MILDENHALL_POPUP_APPSTORE_SETTINGS_ENTRY_ERROR "Do Not Leave Blank Entry"
#define MILDENHALL_POPUP_APPSTORE_SETTINGS_REGISTRATION_ERROR "User with this mail-id is already registered"
#define MILDENHALL_POPUP_APPSTORE_SETTINGS_INVALID_PASSWORD "Password must have minimum six characters !"
#define MILDENHALL_POPUP_BUTTON_STRING_CONTINUE "continue"
#define MILDENHALL_POPUP_BUTTON_STRING_STOP "stop"
#define MILDENHALL_POPUP_BUTTON_STRING_YES "yes"
#define MILDENHALL_POPUP_BUTTON_STRING_NO "no"
#define MILDENHALL_POPUP_BUTTON_STRING_CLOSE "close"
#define MILDENHALL_POPUP_BUTTON_STRING_CANCEL "cancel"

#define MILDENHALL_POPUP_WIFI_SETTINGS_NWSEARCH "Network_Search"
#define MILDENHALL_POPUP_WIFI_SETTINGS_WIFI_OFF "Wifi_Off"
#define MILDENHALL_POPUP_WIFI_SETTINGS_SELECT_NETWORK "Select_Network"
#define MILDENHALL_POPUP_NEWS_NO_FEED_TO_DISPLAY "No feeds to display"
#define MILDENHALL_POPUP_NEWS_WANT_TO_DELETE "Do you want to delete this feed ?"

G_END_DECLS

#endif /* __MILDENHALL_POPUP_STRINGS_H__ */
