/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall-audio-player-item.h */

#ifndef _MILDENHALL_AUDIO_PLAYER_ITEM_H
#define _MILDENHALL_AUDIO_PLAYER_ITEM_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

/***********************************************************************************
*       @Filename       :       mildenhall_audio_player_item.h
*       @Module         :       AudioPlayerItem
*       @Project        :       --
*----------------------------------------------------------------------------------
*       @Date           :       01/02/2013
*----------------------------------------------------------------------------------
*       @Description    :       Header file for mildenhall_audio_player_item type
*
************************************************************************************/

/***********************************************************************************
        @Includes
************************************************************************************/
#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_AUDIO_PLAYER_ITEM mildenhall_audio_player_item_get_type()
G_DECLARE_FINAL_TYPE (MildenhallAudioPlayerItem, mildenhall_audio_player_item, MILDENHALL, AUDIO_PLAYER_ITEM, ClutterActor)

MildenhallAudioPlayerItem *mildenhall_audio_player_item_new (void);

G_END_DECLS

#endif /* _MILDENHALL_AUDIO_PLAYER_ITEM_H */
