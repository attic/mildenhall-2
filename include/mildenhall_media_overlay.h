/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_MEDIA_OVERLAY_H__
#define __MILDENHALL_MEDIA_OVERLAY_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>

#define MILDENHALL_TYPE_MEDIA_OVERLAY	(mildenhall_media_overlay_get_type())
#define MILDENHALL_MEDIA_OVERLAY(obj)	(G_TYPE_CHECK_INSTANCE_CAST((obj), MILDENHALL_TYPE_MEDIA_OVERLAY, MildenhallMediaOverlay))
#define MILDENHALL_IS_MEDIA_OVERLAY(obj)	(G_TYPE_CHECK_INSTANCE_TYPE((obj), MILDENHALL_TYPE_MEDIA_OVERLAY))

typedef struct _MildenhallMediaOverlay MildenhallMediaOverlay;
typedef struct _MildenhallMediaOverlayClass MildenhallMediaOverlayClass;
typedef struct _MildenhallMediaOverlayPrivate MildenhallMediaOverlayPrivate;

/**
 * MildenhallMediaOverlay:
 *
 * The contents of this structure are private and should only be accessed
 * through the public API.
 */

struct _MildenhallMediaOverlay
{
	ClutterActor parent;
};

/**
 * MildenhallMediaOverlayClass:
 *
 * The #MildenhallMediaOverlayClass struct contains only private data.
 *
 */
struct _MildenhallMediaOverlayClass
{
	ClutterActorClass parent;
	ClutterActor *mediaOverlay;
};

GType mildenhall_media_overlay_get_type (void);

MildenhallMediaOverlay* mildenhall_media_overlay_new (gfloat width, gfloat height, gboolean needTitleRect);


#endif /* __MILDENHALL_MEDIA_OVERLAY_H__ */


