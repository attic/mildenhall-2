/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* mildenhall_widget_container.h */

#ifndef _MILDENHALL_WIDGET_CONTAINER_H
#define _MILDENHALL_WIDGET_CONTAINER_H

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
#include <thornbury/thornbury.h>

G_BEGIN_DECLS

#define MILDENHALL_TYPE_WIDGET_CONTAINER mildenhall_widget_container_get_type()

#define MILDENHALL_WIDGET_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_WIDGET_CONTAINER, MildenhallWidgetContainer))

#define MILDENHALL_WIDGET_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_WIDGET_CONTAINER, MildenhallWidgetContainerClass))

#define MILDENHALL_IS_WIDGET_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_WIDGET_CONTAINER))

#define MILDENHALL_IS_WIDGET_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_WIDGET_CONTAINER))

#define MILDENHALL_WIDGET_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_WIDGET_CONTAINER, MildenhallWidgetContainerClass))

typedef struct _MildenhallWidgetContainer MildenhallWidgetContainer;
typedef struct _MildenhallWidgetContainerClass MildenhallWidgetContainerClass;
typedef struct _MildenhallWidgetContainerPrivate MildenhallWidgetContainerPrivate;

/**
 * MildenhallWidgetContainer:
 * 
 * The #MildenhallWidgetContainer struct contains only private data.
 *
 */
struct _MildenhallWidgetContainer
{
	ClutterActor parent;

	MildenhallWidgetContainerPrivate *priv;
};

/**
 * MildenhallWidgetContainerClass:
 * @move_left_completed: class handler for the #MildenhallWidgetContainer::move_left_completed signal
 * @move_right_completed: class handler for the #MildenhallWidgetContainer::move_right_completed signal
 * @maximize_completed: class handler for the #MildenhallWidgetContainer::maximize_completed signal 
 * @minimize_completed: class handler for the #MildenhallWidgetContainer::minimize_completed signal
 *
 * The #MildenhallWidgetContainerClass struct contains only private data.
 *
 */
struct _MildenhallWidgetContainerClass
{
	ClutterActorClass parent_class;
	void (*move_left_completed) ( GObject *pWidgetContainer);
	void (*move_right_completed)( GObject *pWidgetContainer);
	void (*maximize_completed)  ( GObject *pWidgetContainer);
	void (*minimize_completed)  ( GObject *pWidgetContainer);
};

GType mildenhall_widget_container_get_type (void) G_GNUC_CONST;

MildenhallWidgetContainer *mildenhall_widget_container_new (void);

ThornburyModel *mildenhall_widget_container_get_model(MildenhallWidgetContainer *pWidgetContainer);
gfloat mildenhall_widget_container_get_height(MildenhallWidgetContainer *pWidgetContainer);
gfloat mildenhall_widget_container_get_width(MildenhallWidgetContainer *pWidgetContainer);

void mildenhall_widget_container_set_model(MildenhallWidgetContainer *pWidgetContainer, ThornburyModel *pModel);
void mildenhall_widget_container_set_move_right(MildenhallWidgetContainer *pWidgetContainer, gfloat inMoveRight);
void mildenhall_widget_container_set_move_left(MildenhallWidgetContainer *pWidgetContainer, gfloat inMoveLeft);
void mildenhall_widget_container_set_maximize(MildenhallWidgetContainer *pWidgetContainer, gfloat inMaximize);
void mildenhall_widget_container_set_minimize(MildenhallWidgetContainer *pWidgetContainer, gfloat inMinimize);
void mildenhall_widget_container_set_animation_timeline(MildenhallWidgetContainer *pWidgetContainer, gint uinTimeline);
void mildenhall_widget_container_set_animation_mode(MildenhallWidgetContainer *pWidgetContainer, gint uinAnimationMode);


G_END_DECLS

#endif /* _MILDENHALL_WIDGET_CONTAINER_H */
