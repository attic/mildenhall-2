/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* view-manager-lib.h */

#ifndef __VIEW_MANAGER_LIB_H__
#define __VIEW_MANAGER_LIB_H__

#include <glib-object.h>
#include <thornbury/thornbury.h>
#include "mildenhall_roller_container.h"

G_BEGIN_DECLS

#define VIEW_TYPE_MANAGER_LIB view_manager_lib_get_type()

#define VIEW_MANAGER_LIB(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  VIEW_TYPE_MANAGER_LIB, ViewManagerLib))

#define VIEW_MANAGER_LIB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  VIEW_TYPE_MANAGER_LIB, ViewManagerLibClass))

#define VIEW_IS_MANAGER_LIB(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  VIEW_TYPE_MANAGER_LIB))

#define VIEW_IS_MANAGER_LIB_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  VIEW_TYPE_MANAGER_LIB))

#define VIEW_MANAGER_LIB_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  VIEW_TYPE_MANAGER_LIB, ViewManagerLibClass))

typedef struct _ViewManagerLib ViewManagerLib;
typedef struct _ViewManagerLibClass ViewManagerLibClass;
typedef struct _ViewManagerLibPrivate ViewManagerLibPrivate;
typedef struct _ViewManagerLibAnimInfo ViewManagerLibAnimInfo;



struct _ViewManagerLib
{
	ThornburyViewManager parent;

  ViewManagerLibPrivate *priv;
};



struct _ViewManagerLibClass
{
	ThornburyViewManagerClass parent_class;
};

struct _ViewManagerLibAnim_Struct
{
	ClutterActor *actor;
	gchar *effect;
};



GType view_manager_lib_get_type (void) G_GNUC_CONST;

ViewManagerLib *view_manager_lib_new (void);
void v_switch_view(ThornburyViewManager * view_manager,ThornburyViewManagerViewData *currentview, ThornburyViewManagerViewData *newview, gboolean animation_needed,guint iSelected_row);
ThornburyViewManagerError v_set_property(ThornburyViewManager *view_manager, ClutterActor* actor, va_list var_args);
void v_set_animation(ThornburyViewManager * view_manager,ClutterActor *widget,ThornburyViewManagerAnimData *anim_data);




G_END_DECLS

#endif /* __VIEW_MANAGER_LIB_H__ */
