/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * Sample item for MildenhallRoller
 *
 *
 */

#ifndef __SAMPLE_THUMBNAIL_ITEM_H__
#define __SAMPLE_THUMBNAIL_ITEM_H__

#if !defined (__MILDENHALL_H_INSIDE__) && !defined (MILDENHALL_COMPILATION)
#warning "Only <mildenhall/mildenhall.h> can be included directly."
#endif

#include <clutter/clutter.h>
#include <cogl/cogl.h>
#include <mx/mx.h>

G_BEGIN_DECLS

#define TYPE_SAMPLE_THUMBNAIL_ITEM             (sample_thumbnail_item_get_type ())
#define SAMPLE_THUMBNAIL_ITEM(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SAMPLE_THUMBNAIL_ITEM, SampleThumbnailItem))
#define SAMPLE_THUMBNAIL_ITEM_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SAMPLE_THUMBNAIL_ITEM, SampleThumbnailItemClass))
#define IS_SAMPLE_THUMBNAIL_ITEM(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SAMPLE_THUMBNAIL_ITEM))
#define IS_SAMPLE_THUMBNAIL_ITEM_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SAMPLE_THUMBNAIL_ITEM))
#define SAMPLE_THUMBNAIL_ITEM_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SAMPLE_THUMBNAIL_ITEM, SampleThumbnailItemClass))

typedef struct _SampleThumbnailItem        SampleThumbnailItem;
typedef struct _SampleThumbnailItemClass   SampleThumbnailItemClass;

struct _SampleThumbnailItem
{
  ClutterTexture texture;
};

struct _SampleThumbnailItemClass
{
  ClutterTextureClass parent_class;
};


GType          sample_thumbnail_item_get_type         (void) G_GNUC_CONST;
ClutterActor*  sample_thumbnail_item_new              ();

G_END_DECLS

#endif /* __SAMPLE_THUMBNAIL_ITEM_H__ */

